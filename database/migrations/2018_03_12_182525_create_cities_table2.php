<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCitiesTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cities', function(Blueprint $table)
		{
		    $table->increments('city_id');
			$table->integer('country_id')->index('country_id');
			$table->boolean('important')->index('important');
			$table->string('title_ru', 150)->nullable();
			$table->string('title_ua', 150)->nullable();
			$table->string('title_be', 150)->nullable();
			$table->string('title_en', 150)->nullable();
			$table->string('title_es', 150)->nullable();
			$table->string('title_pt', 150)->nullable();
			$table->string('title_de', 150)->nullable();
			$table->string('title_fr', 150)->nullable();
			$table->string('title_it', 150)->nullable();
			$table->string('title_pl', 150)->nullable();
			$table->string('title_ja', 150)->nullable();
			$table->string('title_lt', 150)->nullable();
			$table->string('title_lv', 150)->nullable();
			$table->string('title_cz', 150)->nullable();
			$table->string('title_hr', 60);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cities');
	}

}
