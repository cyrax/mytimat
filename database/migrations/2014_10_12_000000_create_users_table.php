<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('second_name')->nullable();
            $table->enum('group', ['user', 'administrator', 'manager', 'translator', 'company_admin', 'company_manager', 'company_specialist'])->default('user');
            $table->unsignedInteger('company_id')->default(0);
            $table->boolean('specialist')->default(0);
            $table->string('phone')->unique();
            $table->string('email')->nullable();
            $table->string('city')->nullable();
            $table->string('avatar')->nullable();
            $table->integer('tims')->default(0);
            $table->smallInteger('language_id')->default(0);
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
