<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('administrator')->after('specialist')->default(0);
            $table->boolean('user')->after('specialist')->default(0);
            $table->boolean('manager')->after('specialist')->default(0);
            $table->boolean('techsupport')->after('specialist')->default(0);
            $table->boolean('translator')->after('specialist')->default(0);
            $table->boolean('moderator')->after('specialist')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
