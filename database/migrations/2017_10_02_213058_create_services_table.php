<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('company_id');
            $table->string('name');
            $table->string('supposed_category')->nullable();
            $table->unsignedInteger('category_id')->nullable();
            $table->text('description')->nullable();
            $table->unsignedBigInteger('price');
            $table->mediumInteger('currency')->default(0);
            $table->tinyInteger('days')->default(0);
            $table->tinyInteger('hours')->default(0);
            $table->tinyInteger('minutes')->default(0);
            $table->boolean('animals')->default(0);
            $table->boolean('departure')->default(0);
            $table->boolean('adult')->default(0);
            /*TODO Модерация*/
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
