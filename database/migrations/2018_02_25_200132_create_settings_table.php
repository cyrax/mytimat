<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->morphs('settingable');
            $table->index('settingable_type');
            $table->index('settingable_id');
            $table->boolean('display_phone');
            $table->boolean('display_email');
            $table->boolean('display_social');
            $table->boolean('is_active');
            $table->boolean('chat_with_admin');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
