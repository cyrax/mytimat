<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesTable2 extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('countries', function(Blueprint $table)
		{
			$table->increments('country_id');
			$table->string('title_ru', 60)->nullable();
			$table->string('title_ua', 60)->nullable();
			$table->string('title_be', 60)->nullable();
			$table->string('title_en', 60)->nullable();
			$table->string('title_es', 60)->nullable();
			$table->string('title_pt', 60)->nullable();
			$table->string('title_de', 60)->nullable();
			$table->string('title_fr', 60)->nullable();
			$table->string('title_it', 60)->nullable();
			$table->string('title_pl', 60)->nullable();
			$table->string('title_ja', 60)->nullable();
			$table->string('title_lt', 60)->nullable();
			$table->string('title_lv', 60)->nullable();
			$table->string('title_cz', 60)->nullable();
			$table->string('title_hr', 60)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('countries');
	}

}
