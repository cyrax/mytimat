<?php

use Illuminate\Database\Seeder;

class LanguageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $language = new \Waavi\Translation\Models\Language();
        $language->locale = 'ru';
        $language->name = 'Русский';
        $language->save();

        $language = new \Waavi\Translation\Models\Language();
        $language->locale = 'en';
        $language->name = 'English';
        $language->save();
    }
}
