<?php

use App\Language;
use Illuminate\Database\Seeder;

class LanguagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Language::create(['name'=>'Azərbaycan dili', 'english_name' => 'Azerbaijani']);
        Language::create(['name'=>'Bahasa Indonesia', 'english_name' => 'Indonesian']);
        Language::create(['name'=>'Bosanski', 'english_name' => 'Bosnian']);
        Language::create(['name'=>'Dansk', 'english_name' => 'Danish']);
        Language::create(['name'=>'Deutsch', 'english_name' => 'German']);
        Language::create(['name'=>'Eesti', 'english_name' => 'Estonian']);
        Language::create(['name'=>'English', 'english_name' => 'English']);
        Language::create(['name'=>'Español', 'english_name' => 'Spanish']);
        Language::create(['name'=>'Français', 'english_name' => 'French']);
        Language::create(['name'=>'Hrvatski', 'english_name' => 'Croatian']);
        Language::create(['name'=>'Italiano', 'english_name' => 'Italian']);
        Language::create(['name'=>'Kiswahili', 'english_name' => 'Swahili']);
        Language::create(['name'=>'Latviešu', 'english_name' => 'Latvian']);
        Language::create(['name'=>'Lietuvių', 'english_name' => 'Lithuanian']);
        Language::create(['name'=>'Magyar', 'english_name' => 'Hungarian']);
        Language::create(['name'=>'Moldovenească', 'english_name' => 'Moldavian']);
        Language::create(['name'=>'Nederlands', 'english_name' => 'Dutch']);
        Language::create(['name'=>'Norsk', 'english_name' => 'Norwegian']);
        Language::create(['name'=>'O‘zbek', 'english_name' => 'Uzbek']);
        Language::create(['name'=>'Polski', 'english_name' => 'Polish']);
        Language::create(['name'=>'Português', 'english_name' => 'Portuguese']);
        Language::create(['name'=>'Português brasileiro', 'english_name' => 'Brazilian Portuguese']);
        Language::create(['name'=>'Română', 'english_name' => 'Romanian']);
        Language::create(['name'=>'Shqip', 'english_name' => 'Albanian']);
        Language::create(['name'=>'Slovenščina', 'english_name' => 'Slovenian']);
        Language::create(['name'=>'Suomi', 'english_name' => 'Finnish']);
        Language::create(['name'=>'Svenska', 'english_name' => 'Swedish']);
        Language::create(['name'=>'Tagalog', 'english_name' => 'Tagalog']);
        Language::create(['name'=>'Tiếng Việt', 'english_name' => 'Vietnamese']);
        Language::create(['name'=>'Türkmen', 'english_name' => 'Turkmen']);
        Language::create(['name'=>'Türkçe', 'english_name' => 'Turkish']);
        Language::create(['name'=>'Čeština', 'english_name' => 'Czech']);
        Language::create(['name'=>'Ελληνικά', 'english_name' => 'Greek']);
        Language::create(['name'=>'Slovenčina', 'english_name' => 'Slovak']);
        Language::create(['name'=>'Беларуская', 'english_name' => 'Belarusian']);
        Language::create(['name'=>'Български', 'english_name' => 'Bulgarian']);
        Language::create(['name'=>'Кыргыз тили', 'english_name' => 'Kirghiz']);
        Language::create(['name'=>'Монгол', 'english_name' => 'Mongolian']);
        Language::create(['name'=>'Русский', 'english_name' => 'Russian']);
        Language::create(['name'=>'Татарча', 'english_name' => 'Tatar']);
        Language::create(['name'=>'Тоҷикӣ', 'english_name' => 'Tajik']);
        Language::create(['name'=>'Српски', 'english_name' => 'Serbian']);
        Language::create(['name'=>'Українська', 'english_name' => 'Ukrainian']);
        Language::create(['name'=>'Қазақша', 'english_name' => 'Kazakh']);
        Language::create(['name'=>'ქართული', 'english_name' => 'Georgian']);
        Language::create(['name'=>'日本語', 'english_name' => 'Japanese']);
        Language::create(['name'=>'汉语', 'english_name' => 'Chinese']);
        Language::create(['name'=>'臺灣話', 'english_name' => 'Taiwanese']);
        Language::create(['name'=>'한국어', 'english_name' => 'Korean']);
        Language::create(['name'=>'Հայերեն', 'english_name' => 'Armenian']);
        Language::create(['name'=>'اردو', 'english_name' => 'Urdu']);
        Language::create(['name'=>'العربية', 'english_name' => 'Arabic']);
        Language::create(['name'=>'فارسی', 'english_name' => 'Persian']);
    }
}
