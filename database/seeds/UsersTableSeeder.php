<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Serik',
            'group' => 'user',
            'company_id' => 0,
            'specialist' => 0,
            'phone' => '77770667305',
            'email' => 'kenzhegalin.serik@gmail.com',
            'tims' => 0,
            'language_id' => 0,
            'password' => bcrypt('sdu321Sdu'),
            'country_id' => 1,
            'city_id' => 2
        ]);

        DB::table('users')->insert([
            'name' => 'Серик',
            'group' => 'user',
            'company_id' => 0,
            'specialist' => 0,
            'phone' => '77089109493',
            'tims' => 0,
            'language_id' => 0,
            'password' => bcrypt('sdu321Sdu'),
            'country_id' => 1,
            'city_id' => 1
        ]);
    }
}
