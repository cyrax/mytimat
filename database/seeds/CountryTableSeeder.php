<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(0, 10) as $index) {
            $city = $faker->country;
            Db::table('countries')->insert([
                'title_ru' => $city,
                'title_en' => $city,
                'title_hr' => $city,
                'title_it' => $city,
            ]);
        }
    }
}
