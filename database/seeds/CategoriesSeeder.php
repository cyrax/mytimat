<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $data['name'] = 'IT, телеком';
        $data['url'] = 'it_telecom';
        $data['name_translation'] = 'app.it_telecom';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Консалтинг, аутсорсинг';
        $data['url'] = 'consulting_outsourcing';
        $data['name_translation'] = 'app.consulting_outsourcing';
        Category::create($data);
        $data['name'] = 'Программирование, разработка';
        $data['url'] = 'coding_development';
        $data['name_translation'] = 'app.coding_development';
        Category::create($data);
        $data['name'] = 'Сетевые технологии';
        $data['url'] = 'network';
        $data['name_translation'] = 'app.network';
        Category::create($data);
        $data['name'] = 'Телекоммуникации';
        $data['url'] = 'telecommunication';
        $data['name_translation'] = 'app.telecommunication';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Авто-, мото-, спецтехника';
        $data['url'] = 'auto_moto_special_equipment';
        $data['name_translation'] = 'app.auto_moto_special_equipment';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Аренда автомобилей';
        $data['url'] = 'car_rent';
        $data['name_translation'] = 'app.car_rent';
        Category::create($data);
        $data['name'] = 'Автомойки';
        $data['url'] = 'car_wash';
        $data['name_translation'] = 'app.car_wash';
        Category::create($data);
        $data['name'] = 'Автосервис, СТО';
        $data['url'] = 'car_service';
        $data['name_translation'] = 'app.car_service';
        Category::create($data);
        $data['name'] = 'Автостоянки, гаражи, паркинги';
        $data['url'] = 'car_parks';
        $data['name_translation'] = 'app.car_parks';
        Category::create($data);
        $data['name'] = 'Услуги по техническому осмотру';
        $data['url'] = 'vehicle_inspection';
        $data['name_translation'] = 'app.vehicle_inspection';
        Category::create($data);
        $data['name'] = 'Эвакуация транспорта';
        $data['url'] = 'vehicle_evacuation';
        $data['name_translation'] = 'app.vehicle_evacuation';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Воспитание';
        $data['url'] = 'upbringing';
        $data['name_translation'] = 'app.upbringing';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Детские образовательные центры, клубы';
        $data['url'] = 'children_s_club';
        $data['name_translation'] = 'app.children_s_club';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Животный мир';
        $data['url'] = 'animals';
        $data['name_translation'] = 'app.animals';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Ветеринарные услуги';
        $data['url'] = 'vet';
        $data['name_translation'] = 'app.vet';
        Category::create($data);
        $data['name'] = 'Груминг услуги';
        $data['url'] = 'grooming';
        $data['name_translation'] = 'app.grooming';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Здоровье';
        $data['url'] = 'health';
        $data['name_translation'] = 'app.health';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Бани, сауны';
        $data['url'] = 'baths_saunas';
        $data['name_translation'] = 'app.baths_saunas';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Красота';
        $data['url'] = 'beauty';
        $data['name_translation'] = 'app.beauty';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Парикмахерские';
        $data['url'] = 'hair_salon';
        $data['name_translation'] = 'app.hair_salon';
        Category::create($data);
        $data['name'] = 'Салоны красоты, СПА-салоны';
        $data['url'] = 'spa_beauty_salon';
        $data['name_translation'] = 'app.spa_beauty_salon';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Медицина';
        $data['url'] = 'medicine';
        $data['name_translation'] = 'app.medicine';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Медицинские и диагностические центры, клиники';
        $data['url'] = 'medical_and_diagnostic_centers_clinics';
        $data['name_translation'] = 'app.medical_and_diagnostic_centers_clinics';
        Category::create($data);
        $data['name'] = 'Частнопрактикующие врачи';
        $data['url'] = 'private_doctors';
        $data['name_translation'] = 'app.private_doctors';
        Category::create($data);
        $data['name'] = 'Пластическая и эстетическая хирургия';
        $data['url'] = 'plastic_surgery';
        $data['name_translation'] = 'app.plastic_surgery';
        Category::create($data);
        $data['name'] = 'Женские консультации';
        $data['url'] = 'women_health';
        $data['name_translation'] = 'app.women_health';
        Category::create($data);
        $data['name'] = 'Лаборатории';
        $data['url'] = 'laboratory';
        $data['name_translation'] = 'app.laboratory';
        Category::create($data);
        $data['name'] = 'Поликлиники';
        $data['url'] = 'policlinic';
        $data['name_translation'] = 'app.policlinic';
        Category::create($data);
        $data['name'] = 'Психология, Тренинги';
        $data['url'] = 'psychology_trainings';
        $data['name_translation'] = 'app.psychology_trainings';
        Category::create($data);
        $data['name'] = 'Стоматология';
        $data['url'] = 'dentist';
        $data['name_translation'] = 'app.dentist';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Обучение, развитие';
        $data['url'] = 'tuition';
        $data['name_translation'] = 'app.tuition';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Автошколы, курсы вождения';
        $data['url'] = 'driving_courses';
        $data['name_translation'] = 'app.driving_courses';
        Category::create($data);
        $data['name'] = 'Йога, духовные практики, эзотерика';
        $data['url'] = 'yoga_spiritual_practices';
        $data['name_translation'] = 'app.yoga_spiritual_practices';
        Category::create($data);
        $data['name'] = 'Студии танца';
        $data['url'] = 'dancing';
        $data['name_translation'] = 'app.dancing';
        Category::create($data);
        $data['name'] = 'Тренинги и курсы';
        $data['url'] = 'trainings_courses';
        $data['name_translation'] = 'app.trainings_courses';
        Category::create($data);
        $data['name'] = 'Художественные студии';
        $data['url'] = 'art_studios';
        $data['name_translation'] = 'app.art_studios';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Отдых';
        $data['url'] = 'recreation';
        $data['name_translation'] = 'app.recreation';
        $category = Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Растительный мир';
        $data['url'] = 'vegetable_world';
        $data['name_translation'] = 'app.vegetable_world';
        $category = Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Рестораны, кафе';
        $data['url'] = 'restaurants_cafe';
        $data['name_translation'] = 'app.restaurants_cafe';
        $category = Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Юридические, государственные услуги';
        $data['url'] = 'legal_state_services';
        $data['name_translation'] = 'app.legal_state_services';
        $category = Category::create($data);
        $data['parent_id'] = $category->id;
        $data['name'] = 'Посольства, консульства';
        $data['url'] = 'embassies_consulates';
        $data['name_translation'] = 'app.embassies_consulates';
        Category::create($data);
        $data['name'] = 'Юридические и адвокатские услуги';
        $data['url'] = 'legal_attorney_services';
        $data['name_translation'] = 'app.legal_attorney_services';
        Category::create($data);


        unset($data);
        unset($category);
        $data['name'] = 'Разное';
        $data['url'] = 'other';
        $data['name_translation'] = 'app.other';
        Category::create($data);
    }
}
