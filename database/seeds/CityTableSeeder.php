<?php

use Illuminate\Database\Seeder;

class CityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        foreach (\App\Country::all() as $country) {
            foreach (range(0, 10) as $index) {
                $city = $faker->city;
                DB::table('cities')->insert([
                    'country_id' => $country->country_id,
                    'important' => 1,
                    'title_ru' => $city,
                    'title_hr' => $city,
                    'title_en' => $city,
                    'title_it' => $city
                ]);
            }
        }
    }
}
