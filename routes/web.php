<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => \UriLocalizer::localeFromRequest(), 'middleware' => ['localize']], function() {

    Route::get('/', 'PagesController@index')->name('index');

    Route::get('profile/edit', 'ProfileController@edit')->name('profile-edit-get');
    Route::post('profile/edit', 'ProfileController@store_profile')->name('update-profile');
    Route::get('profile', 'ProfileController@index')->name('profile_index');
    Route::post('profile/password_change', 'ProfileController@store_password');
    Route::post('profile/change_number/send-sms', 'ProfileController@send_sms')->middleware('normalize-phone');
    Route::post('profile/change_number/confirm-sms', 'ProfileController@confirm_sms')->middleware('normalize-phone');
    Route::get('cities/{id}', 'ProfileController@get_cities')->where('id', '[0-9]+');
    Route::get('user/balance', 'PagesController@balance')->name('balance')->middleware('exitifnotauth');

//Сообщения
    Route::get('user/messages', 'MessagesController@index')->name('messages')->middleware('exitifnotauth');
    Route::get('user/messages/subject/{message_id}/{subject_id}', 'MessagesController@show')->where('id', '[0-9]+')->middleware('exitifnotauth');
    Route::post('user/messages/subject/{id}/send', 'MessagesController@store')->where('id', '[0-9]+')->middleware('exitifnotauth');
    Route::delete('user/messages', 'MessagesController@delete')->middleware('exitifnotauth');
//Сообщения

//Управление компанией
    Route::get('company/add', 'CompaniesController@add')->name('company_add'); //Добавление компании
    Route::get('company/{id}/edit', 'CompaniesController@edit'); //Добавление компании
    Route::post('company/add', 'CompaniesController@store'); //Сохранение компании в базе
    Route::post('company/{id}/edit', 'CompaniesController@edit')->name('company-edit'); //Сохранение компании в базе
    Route::post('company/{id}/order', 'CompaniesController@order')->where('id', '[0-9]+'); //Подача заявки на услуги
    Route::get('specialist/profile/{id?}', 'SpecialistController@profile')->where('id', '[0-9]+')->name('specialist-profile');
    Route::get('specialist/{id}/edit', 'SpecialistController@edit'); //Сохранение компании в базе
    Route::post('specialist/{id}/edit', 'SpecialistController@edit'); //Сохранение компании в базе
    Route::get('company/members', 'MembersController@members')->name('company-members');
    Route::get('company/orders', 'CompaniesController@orders')->name('company-all-orders');
    Route::get('specialist/orders', 'SpecialistController@orders')->name('specialist-orders');
    Route::delete('company/members/delete', 'MembersController@delete');
    Route::delete('company/roles/delete', 'MembersController@delete_role');
    Route::put('company/members', 'MembersController@update');
    Route::put('company/roles', 'MemberRolesController@update');
    Route::get('company/join', 'MembersController@join')->name('company_join');
    Route::post('company/members/store', 'MembersController@add_member')->middleware('normalize-phone');
    Route::post('company/role/store', 'MembersController@store_role');

    Route::get('company/manage', 'ManageCompaniesController@index')->middleware('administrator')->name('company-manage'); //Список компании для менеджеров

    Route::post('company/{id}/bookmark', 'CompaniesController@add_bookmark')->where('id', '[0-9]+');
    Route::post('company/{id}/bookmark-remove', 'CompaniesController@remove_bookmark')->where('id', '[0-9]+');
    Route::get('bookmarks', 'BookmarkController@index')->name('bookmarks');


    Route::get('company/by-rate', 'CompaniesController@companies_by_rating'); //Добавление компании


    Route::post('company/rights-picture', 'CompaniesController@file_upload'); //Сохранение компании в базе
    Route::post('company/pictures-upload', 'CompaniesController@picture_upload'); //Сохранение компании в базе
    Route::post('specialist/pictures-upload', 'SpecialistController@picture_upload'); //Сохранение компании в базе
    //Управление компанией

    //Управление заявками
    Route::get('company/{id}/specialist/orders', 'OrdersController@index')->where('id', '[0-9]+')->name('company-specialist-orders');
    Route::put('order/{order_id}/accept', 'OrdersController@accept')->where('id', '[0-9]+');
    // Route::get('order/{order_id}/reject', 'OrdersController@reject')->where('id', '[0-9]+');
    Route::put('user-order/{order_id}/accept', 'OrdersController@accept')->where('id', '[0-9]+');
    Route::any('user-order/reject', 'OrdersController@reject')->where('id', '[0-9]+');
    Route::post('order/reject', 'OrdersController@reject');
    Route::post('order-from-table', 'CompaniesController@orderFromTable');
    Route::post('company-order', 'CompaniesController@companyOrder')->name('company-order');
    Route::post('company/{order_id}/edit', 'OrdersController@editTime')->where('id', '[0-9]+');
    Route::post('user-order/{order_id}/edit', 'UserOrdersController@editTime')->where('id', '[0-9]+');
    Route::get('user/orders', 'UserOrdersController@index')->name('user_orders');

    //Управление заявками


    //Категории
    Route::get('category', 'CompaniesController@index')->name('categories'); //Каталог категории
    Route::get('category/{id}', 'CompaniesController@category_show')->name('category'); //Каталог категории
    //Категории


//Поиск
    Route::get('company/search', 'CompaniesController@search');
    Route::get('company/search-ajax', 'CompaniesController@searchAjax');
    Route::post('company/rate', 'CompaniesController@rate');
    Route::post('specialist/rate', 'SpecialistController@rate');
//Поиск


//Подарки
    Route::get('gift/{id?}', 'GiftsController@index')->where('id', '[0-9]+'); //Показать подарки


    Route::get('gift/manage', 'GiftsController@administrate'); //Управление подарками
    Route::get('orders/{id?}', 'OrdersController@index')->name('orders');
    Route::get('order/{id?}', 'OrdersController@view');
    Route::post('gift/store', 'GiftsController@store_gift'); //Добавление подарка
    Route::put('gift/edit', 'GiftsController@edit_gift'); //Добавление подарка
    Route::delete('gift/delete', 'GiftsController@delete_gift'); //Удаление подарка
    Route::post('gift/category/add', 'GiftsController@add_category'); //Удаление подарка
    Route::delete('gift/category/delete', 'GiftsController@delete_category'); //Удаление категории подарка
    //Подарки

    //Акции
    Route::get('discounts/manage', 'DiscountsController@manage'); //Управление подарками
    //Акции




    //Авторизация
    //Auth::routes();
    //Authentication Routes...
    Route::get('login', 'Auth\RegController@successLoginKit')->name('login');
    Route::post('login', 'Auth\LoginController@login')->middleware('normalize-phone');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
    Route::post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('send-sms','Auth\RegController@send_sms')->middleware('normalize-phone');
    Route::post('send-restore-sms','Auth\RegController@send_restore_sms')->middleware('normalize-phone');
    Route::post('confirm-restore-sms','Auth\RegController@confirm_restore_sms')->middleware('normalize-phone');
    Route::post('confirm-sms','Auth\RegController@confirm_sms')->middleware('normalize-phone');
    Route::post('register-complete','Auth\RegController@complete');

    /*Route::post('register-user-complete','Auth\RegController@complete1');
    Route::get('register-user','Auth\RegController@ready_to_complete');
    Route::post('register-user','Auth\RegController@send_sms1');*/

    Route::get('/home', 'HomeController@index')->name('home');

    // Route::get('/help', 'ChatsController@index')->name('help');
    // Route::get('/help', 'PagesController@help')->name('help');
    Route::get('messages', 'ChatsController@fetchMessages');
    Route::post('messages', 'ChatsController@sendMessage');
    Route::get('get-cities/{id}', 'CityController@getByCountry');
    Route::get('support-users/{id}', 'ChatController@index')->name('message.read');
    Route::get('chat/{id}', 'ChatController@index')->name('chat');

    Route::get('support', 'ChatController@support')->name('help');

    Route::get('chat-with-company/{company_id}/{id}', 'ChatController@chatWithCompany')->name('chat-with-company');
    Route::get('chat-with-users/{company_id}', 'ChatController@chatWithUsers')->name('chat-with-company');
    Route::get('techsupport/chat/{id}', 'ChatController@techSupport')->name('chat-techsupport')->where('id', '[0-9+]');

    Route::group(['prefix'=>'ajax', 'as'=>'ajax::'], function() {
        Route::post('message/send', 'ChatController@ajaxSendMessage')->name('message.new');
    });

    Route::get('user/settings', 'UserController@settings')->name('user_settings');
    Route::post('user/settings', 'UserController@settings');
    Route::get('specialist/settings', 'SpecialistController@settings')->name('specialist-settings');
    Route::post('specialist/settings', 'SpecialistController@settings');
    Route::get('company/settings', 'CompaniesController@settings')->name('company-settings');
    Route::post('company/settings', 'CompaniesController@settings');


    Route::get('company/{id}', 'CompaniesController@show')->where('id', '[0-9]+')->name('company-view');

    // translator
    Route::get('translator/ui-elements', 'TranslatorController@ui_elements')->name('translator_ui_elements');
    Route::put('translator/update', 'TranslatorController@update')->name('translator_update');
    Route::get('translator/rights', 'TranslatorController@rights')->name('translator_rights');
    Route::post('translator/rights-update', 'TranslatorController@rightsUpdate')->name('translator_rights_update');
    Route::get('translator/users', 'TranslatorController@users')->name('translator_users');
    Route::post('translator/users-update', 'TranslatorController@usersUpdate')->name('translator_users_update');
    // end of translator


    // pages
    Route::get('about', 'PagesController@about')->name('about-page');
    Route::get('contacts', 'PagesController@contacts')->name('contacts-page');
    Route::get('page/{url}', 'PagesController@view')->name('pages-view');

    // pages
});
