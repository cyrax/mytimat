<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/** @var \Dingo\Api\Routing\Router $api */
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->post('login', 'App\Http\Controllers\Api\AuthenticateController@authenticate');

    $api->get('specialists', 'App\Http\Controllers\Api\SpecialistController@index')->middleware('api.auth');
    $api->get('specialists/available/{company_id}', 'App\Http\Controllers\Api\SpecialistController@available')->middleware('api.auth');

    $api->get('company/{id}', 'App\Http\Controllers\Api\CompaniesController@view')->where('id', '0-9+');
    $api->post('company/{id}/edit', 'App\Http\Controllers\Api\CompaniesController@view')->where('id', '0-9+');

    $api->get('bookmark', 'App\Http\Controllers\Api\BookmarkController@index');
    $api->post('bookmark', 'App\Http\Controllers\Api\BookmarkController@store');


    $api->get('users/', 'App\Http\Controllers\Api\UsersController@index');
    $api->post('register', 'App\Http\Controllers\Api\RegController@store');
    $api->post('restore-password', 'App\Http\Controllers\Api\RegController@srestorePassword');
    $api->post('confirm-password', 'App\Http\Controllers\Api\RegController@confirmPassword');
    $api->post('send-sms', 'App\Http\Controllers\Api\RegController@sendSms');

    $api->post('order', 'App\Http\Controllers\Api\OrderController@order');
    $api->get('user-orders', 'App\Http\Controllers\Api\UsersController@orders');
    $api->get('admin-orders', 'App\Http\Controllers\Api\UsersController@adminOrders');
    $api->get('specialist-orders', 'App\Http\Controllers\Api\SpecialistController@orders');

    $api->get('user-settings', 'App\Http\Controllers\Api\SettingsController@userSettings');
    $api->post('user-settings', 'App\Http\Controllers\Api\SettingsController@saveUserSettings');
    $api->get('company-settings', 'App\Http\Controllers\Api\SettingsController@companySettings');
    $api->post('company-settings', 'App\Http\Controllers\Api\SettingsController@saveCompanySettings');
    $api->get('specialist-settings', 'App\Http\Controllers\Api\SettingsController@specialistSettings');
    $api->post('specialist-settings', 'App\Http\Controllers\Api\SettingsController@saveSpecialistSettings');



});
