<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute должен быть принят.',
    'active_url'           => ':attribute не является допустимым URL-адресом.',
    'after'                => ':attribute должно быть датой после :date.',
    'after_or_equal'       => ':attribute должно быть датой :date или датой после.',
    'alpha'                => ':attribute может содержать лишь буквы.',
    'alpha_dash'           => ':attribute может содержать только буквы, цифры и тире.',
    'alpha_num'            => ':attribute может содержать лишь буквы и цифры.',
    'array'                => ':attribute должно быть массивом элементов.',
    'before'               => ':attribute должно быть датой до :date.',
    'before_or_equal'      => ':attribute должно быть датой до :date включительно.',
    'between'              => [
        'numeric' => ':attribute должно быть от :min до :max.',
        'file'    => ':attribute должно быть от :min до :max Кбайт.',
        'string'  => ':attribute должно быть от :min до :max символов.',
        'array'   => ':attribute должно иметь от :min до :max элементов.',
    ],
    'boolean'              => ':attribute поле должно быть да или нет.',
    'confirmed'            => ':attribute подтверждение не соответствует.',
    'date'                 => ':attribute не является действительной датой.',
    'date_format'          => ':attribute не совпадает с форматом :format.',
    'different'            => ':attribute и :other не должны совпадать.',
    'digits'               => ':attribute должно иметь :digits цифр.',
    'digits_between'       => ':attribute должно иметь от :min до :max цифр.',
    'dimensions'           => ':attribute имеет больший размер, чем разрешено.',
    'distinct'             => ':attribute поле имеет повторяющееся значение.',
    'email'                => ':attribute должен быть правильным email адресом.',
    'exists'               => 'Выбранный :attribute недействителен.',
    'file'                 => ':attribute должно быть файлом.',
    'filled'               => ':attribute поле должно иметь значение.',
    'image'                => ':attribute должно быть изображением.',
    'in'                   => 'Выбранный :attribute недействителен.',
    'in_array'             => ':attribute поле не присутствует в :other.',
    'integer'              => ':attribute должен быть числом integer.',
    'ip'                   => ':attribute должен быть IP адресом.',
    'ipv4'                 => ':attribute должен быть IPv4 адресом.',
    'ipv6'                 => ':attribute должен быть IPv6 адресом.',
    'json'                 => ':attribute должен быть JSON строкой.',
    'max'                  => [
        'numeric' => ':attribute должен быть не больше :max.',
        'file'    => ':attribute должен быть не больше :max Кбайт.',
        'string'  => ':attribute должен быть не больше :max символов.',
        'array'   => ':attribute должен иметь не больше :max элементов.',
    ],
    'mimes'                => ':attribute должен быть файлом формата: :values.',
    'mimetypes'            => ':attribute должен быть файлом формата: :values.',
    'min'                  => [
        'numeric' => ':attribute должен быть не меньше :min.',
        'file'    => ':attribute должен быть не меньше :min Кбайт.',
        'string'  => ':attribute должен быть не меньше :min символов.',
        'array'   => ':attribute должен иметь не меньше :min элементов.',
    ],
    'not_in'               => 'Выбранный :attribute недействителен.',
    'numeric'              => ':attribute должен быть числом.',
    'present'              => ':attribute должен присутствовать.',
    'regex'                => ':attribute неверный формат.',
    'required'             => ':attribute поле обязательно.',
    'required_if'          => ':attribute поле необходимо когда :other равен :value.',
    'required_unless'      => ':attribute поле необходимо если :other не равен :values.',
    'required_with'        => ':attribute поле необходимо когда присутствует один из :values.',
    'required_with_all'    => ':attribute поле необходимо когда все :values присутствуют.',
    'required_without'     => ':attribute поле необходимо когда :values не предоставлено.',
    'required_without_all' => ':attribute поле необходимо когда ни одно из :values не предоставлено.',
    'same'                 => ':attribute и :other должны быть одинаковыми.',
    'size'                 => [
        'numeric' => ':attribute должен быть :size.',
        'file'    => ':attribute должен быть :size Кбайт.',
        'string'  => ':attribute должен содержать :size символов.',
        'array'   => ':attribute должен содержать :size элементов.',
    ],
    'string'               => ':attribute должен быть строкой.',
    'timezone'             => ':attribute должен быть временной зоной.',
    'unique'               => ':attribute уже есть в базе.',
    'uploaded'             => ':attribute ошибка загрузки.',
    'url'                  => ':attribute должен быть ссылкой.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'email' => 'Email адрес',
        'phone' => 'Номер телефона',
    ],

];
