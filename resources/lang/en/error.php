<?php
return [
    'sms_code' => 'You sms code does not correspond',
    'agree_with_conditions' => 'You did not agree with terms and conditions',
    'sending_sms' => 'Error sending sms',
    'company_not_working' => 'Company is not working at this time',
    'adding_to_bookmarks' => 'Error adding to bookmarks!',
    'technical_problems' => 'There have been technical errors!',
    'uploading_file' => 'Error downloading file',
    '500' => 'Sorry, something went wrong...',
    '404' => 'Sorry, page not found',
    '' => '',
];