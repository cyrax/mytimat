@extends('partials.app')

@section('content')
    <div class="aksii podarki message">
        <div class="container">
            {{--<span class="add-span">{{ trans('app.manage_profile') }}</span>--}}
        </div>
    </div>
    <div class="add-company ">
        <div class="container">
            <div class="grey-admin">
              <span class="grey-span">{{ trans('app.private_info') }}</span>
                <div class="row">
                    <div class="col-md col-sm nopad">
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.name') }}</label>
                            </div>
                            <div class="col-md col">
                                <label class="add-label">{{ Auth::user()->name }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.lastname') }}</label>
                            </div>
                            <div class="col-md col">
                                <label class="add-label">{{ Auth::user()->second_name }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">Email</label>
                            </div>
                            <div class="col-md col">
                                <label class="add-label">{{ Auth::user()->email }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.city') }}</label>
                            </div>
                            <div class="col-md col tabs">
                                <div class="col nopad">
                                    <label class="add-label">{{ optional($country)->name }}</label>
                                </div>
                                <div class="col nopad">
                                    <label class="add-label">{{ optional($city)->name }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.language') }}</label>
                            </div>
                            <div class="col-md col tabs">
                                <label class="add-label">{{ optional($language)->name }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.photo') }}</label>
                            </div>
                            <div class="col-md col">
                                <img src="{{ Auth::user()->avatar ? '/images/user/thumbnail/'.Auth::user()->avatar.'.jpg' : '/image/myphoto.png' }}" class="pro-my-ph" style="vertical-align: baseline; max-width: 100px;">
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                    <div class="col-md-offset-1 col-md col-sm nopad">
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">VK</label>
                            </div>
                            <div class="col-md col">
                                <div class="inputwithimg">
                                    <img src="/image/soc1.png">
                                    <label class="add-label">{{ Auth::user()->vk }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">Facebook</label>
                            </div>
                            <div class="col-md col">
                                <div class="inputwithimg">
                                    <img src="/image/soc2.png">
                                    <label class="add-label">{{ Auth::user()->facebook }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">Twitter</label>
                            </div>
                            <div class="col-md col">
                                <div class="inputwithimg">
                                    <img src="/image/soc3.png">
                                    <label class="add-label">{{ Auth::user()->twitter }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">Telegram</label>
                            </div>
                            <div class="col-md col">
                                <div class="inputwithimg">
                                    <img src="/image/soc4.png">
                                    <label class="add-label">{{ Auth::user()->telegram }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">WhatsApp</label>
                            </div>
                            <div class="col-md col">
                                <div class="inputwithimg">
                                    <img src="/image/soc5.png">
                                    <label class="add-label">{{ Auth::user()->whatsapp }}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                  <a href="/profile/edit" class="btn prof-but">{{ trans('app.change') }}</a>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        .inputwithimg .add-label{
            padding-left: 40px;
        }
    </style>
@endsection
