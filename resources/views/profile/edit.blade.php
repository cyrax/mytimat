@extends('partials.app')

@section('content')
    <div class="bread">
        <div class="container">
            <a href="/profile">{{ trans('app.profile') }}</a> » <span>{{ trans('app.manage_profile') }}</span>
        </div>
    </div>
    @if (session()->has('changes_saved'))
        <div class="alert alert-success">
            {{ trans('app.changes_saved') }}
        </div>
    @endif
    <div class="aksii podarki message">
        <div class="container">
            <span class="add-span">{{ trans('app.manage_profile') }}</span>
        </div>
    </div>
    <div class="add-company ">
        <div class="container">
            <div class="grey-admin">
                <form method="post" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <span class="grey-span">{{ trans('app.private_info') }}</span>
                    @if ($errors->profile->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->profile->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @if ($errors->password->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->password->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <div class="row">
                      <div class="col-md col-sm nopad">
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.name') }}</label>
                            </div>
                            <div class="col-md col">
                                <input class="add-input" type="text" name="name" value="{{ old('name') ? old('name') : Auth::user()->name }}" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.lastname') }}</label>
                            </div>
                            <div class="col-md col">
                                <input class="add-input" type="text" name="second_name" value="{{ old('second_name') ? old('second_name') : Auth::user()->second_name }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">Email</label>
                            </div>
                            <div class="col-md col">
                                <input class="add-input" type="text" name="email" placeholder="example@mail.com" value="{{ old('email') ? old('email') : Auth::user()->email }}">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.city') }}</label>
                            </div>
                            <div class="col-md col tabs">
                              <div class="row">
                                <div class="col">
                                    @include('partials.geo.country', ['countryId' => Auth::user()->country_id])
                                </div>
                                <div class="col">
                                    @include('partials.geo.city', ['selectedCityId' => Auth::user()->city_id, 'countryId' => Auth::user()->country_id])
                                </div>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.language') }}</label>
                            </div>
                            <div class="col-md col tabs">
                                <select name="language_id" id="language_id" style="width: 100%;" required>
                                    <option value="">{{ trans('app.choose_language') }}</option>
                                    @foreach($languages as $language)
                                        <option value="{{ $language->id }}" {{ $language->id == Auth::user()->language_id ? 'selected' : '' }}>{{ $language->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md col">
                                <label class="add-label">{{ trans('app.photo') }}</label>
                            </div>
                            <div class="col-md col">
                                <img src="{{ Auth::user()->avatar ? '/images/user/thumbnail/'.Auth::user()->avatar.'.jpg' : '/image/myphoto.png' }}" class="pro-my-ph" style="vertical-align: baseline; max-width: 100px;">
                                <label class="add-label" style="color: #0066cc;">{{ trans('app.change_photo') }} <br><input type="file" name="picture" class="add-input"></label>
                            </div>
                            <div class="clear"></div>
                        </div>
                    </div>
                      <div class="col-md-offset-1 col-md col-sm nopad">
                    <div class="row">
                        <div class="col-md col">
                            <label class="add-label">VK</label>
                        </div>
                        <div class="col-md col">
                            <div class="inputwithimg">
                                <img src="/image/soc1.png">
                                <input type="text" name="vk" placeholder="/id" class="add-input" value="{{ old('vk') ? old('vk') : Auth::user()->vk }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md col">
                            <label class="add-label">Facebook</label>
                        </div>
                        <div class="col-md col">
                            <div class="inputwithimg">
                                <img src="/image/soc2.png">
                                <input type="text" name="facebook" placeholder="id" class="add-input" value="{{ old('facebook') ? old('facebook') : Auth::user()->facebook }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md col">
                            <label class="add-label">Twitter</label>
                        </div>
                        <div class="col-md col">
                            <div class="inputwithimg">
                                <img src="/image/soc3.png">
                                <input type="text" name="twitter" placeholder="@id" class="add-input" value="{{ old('twitter') ? old('twitter') : Auth::user()->twitter }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md col">
                            <label class="add-label">Telegram</label>
                        </div>
                        <div class="col-md col">
                            <div class="inputwithimg">
                                <img src="/image/soc4.png">
                                <input type="text" name="telegram" placeholder="{{ trans('app.number') }}" class="add-input" value="{{ old('telegram') ? old('telegram') : Auth::user()->telegram }}">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md col">
                            <label class="add-label">WhatsApp</label>
                        </div>
                        <div class="col-md col">
                            <div class="inputwithimg">
                                <img src="/image/soc5.png">
                                <input type="text" name="whatsapp" placeholder="{{ trans('app.number') }}" class="add-input" value="{{ old('whatsapp') ? old('whatsapp') : Auth::user()->whatsapp }}">
                            </div>
                        </div>
                    </div>
                </div>
                    </div>
                    <button class="prof-but">{{ trans('app.save_changes') }}</button>
                </form>
            </div>
            <div class="row">
            <div class="col-sm spec-grey f-spec-grey">
                <div class="grey-admin">
                    <span class="spec-grey-span">{{ trans('app.change_number') }}</span>
                    <div id="errors_display"></div>
                    <div id="change_number">
                        <div class="col-sm col-sm-offset-2">
                            <input id="new_phone_number" type="tel" name="number" class="add-input"></div>
                        <div class="clear"></div>
                        <button id="change_number_btn" class="prof-but" type="button">{{ trans('app.save_changes') }}</button>
                    </div>
                    <div id="confirm_sms" style="display: none;">
                        <p class="hydra_register_1_p">{{ trans('app.enter_code_from_sms') }}</p>
                        <p><input type="password" class="hydra_login_input" id="sms-confirmation" required></p>
                        <p style="margin-top: 20px; clear: both;">
                            <button type="button" class="aksii-search register_submit" onclick="hydra_confirm_sms()">{{ trans('app.save') }}
                                <i class="fa fa-arrow-right"></i>
                            </button>
                            <a style="cursor: pointer;" onclick="hydra_send_sms_again()">{{ trans('app.enter_code_again') }}</a>
                            <span style="margin-left: 105px; color: #b4b164;"><span id="seconds_timer">59</span> {{ trans('app.second') }}</span>
                        </p>
                        <p style="margin-top: 45px; clear: both; text-align: right; margin-bottom: 0;">
                            <a onclick="enter_number_again()" style="cursor: pointer;">{{ trans('app.try_again') }}</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm spec-grey">
                <div class="grey-admin">
                    <span class="spec-grey-span">{{ trans('app.change_password') }}</span>
                    <form action="/profile/password_change" method="post">
                        {{ csrf_field() }}
                    <div class="col-sm">
                        <input type="password" name="old_password" class="add-input" placeholder="{{ trans('app.old_password') }}">
                    </div>
                    <div class="col-sm"></div>
                    <div class="col-sm">
                        <input type="password" name="password" class="add-input" placeholder="{{ trans('app.new_password') }}">
                    </div>
                    <div class="col-sm">
                        <input type="password" name="password_confirmation" class="add-input" placeholder="{{ trans('app.again') }}">
                    </div>
                    <div class="clear"></div>
                    <button class="prof-but" type="submit">{{ trans('app.save_changes') }}</button>
                    </form>
                </div>
            </div>
          </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $("#country_id").select2();
        $("#city_id").select2();
        $("#language_id").select2();

        $("#country_id").change(function () {
            var country = $("#country_id").val();
            if(country == 0) {
                $("#city_id").html("<option value=\"\">{{ trans('app.choose_city') }}</option>");
            } else {
                $.ajax({
                    url: "/get-cities/"+ country
                }).done(function( msg ) {
                    $("#city_id").html("<option value=\"\">{{ trans('app.choose_city') }}</option>");
                    if(msg){
                        msg = $.parseJSON(msg);
                        msg.forEach(function (t) {
                            $("#city_id").append("<option value=\"" + t.id + "\">" + t.name + "</option>");
                        });
                    } else {
                        $("#city_id").html("<option value=\"0\">{{ trans('app.choose_city') }}</option>");
                        alert("{{ trans('app.no_city_in_this_country') }}");
                    }
                }).fail(function() {
                    alert("{{ trans('app.error') }}");
                });
            }
        });

        $("#change_number_btn").click(function () {
            $("#errors_display").html("");
            var new_number = $("#new_phone_number").val().trim();
            if(new_number) {
                $.ajax({
                    method: "POST",
                    url: "/profile/change_number/send-sms",
                    data: { phone: new_number, "_token": "{{ csrf_token() }}" }
                }).done(function( msg ) {
                    if(msg.error) {
                        var errors = "<div class=\"alert alert-danger\">" +
                            "<ul>";
                        if(msg.errors.phone) {
                            msg.errors.phone.forEach(function (item, i) {
                                errors = errors + "<li>" + item + "</li>";
                            });
                        }
                        if(msg.sendingsms){
                            errors = errors + "<li>" + msg.sendingsms + "</li>";
                        }
                        errors = errors + "</ul></div>";
                        $("#errors_display").html(errors);
                    } else {
                        $("#change_number").hide();
                        $("#confirm_sms").show();

                        var seconds = 59;
                        var timerId = setInterval(function() {
                            seconds--;
                            $("#seconds_timer").text(seconds);
                            if(seconds <= 0){
                                clearInterval(timerId);
                            }
                        }, 1000);

                    }
                }).fail(function( msg ) {
                    alert("{{ trans('error.sending_sms') }}");
                });
            } else {
                alert("{{ trans('app.enter_phone') }}");
            }

        });

        function hydra_confirm_sms() {
            $("#errors_display").html("");
            var code = $("#sms-confirmation").val().trim();
            if(code) {
                $.ajax({
                    method: "POST",
                    url: "/profile/change_number/confirm-sms",
                    data: { code: code, "_token": "{{ csrf_token() }}" }
                }).done(function( msg ) {
                    if(msg === "error") {
                        var errors = "<div class=\"alert alert-danger\">{{ trans('app.enter_right_code') }}</div>";
                        $("#errors_display").html(errors);
                    }
                    if(msg === "success") {
                        location.href = '/profile/edit';
                    }
                }).fail(function( msg ) {
                    $("html").html(msg.responseText);
                    var errors = "<div class=\"alert alert-danger\">{{ trans('app.enter_right_code') }}</div>";
                    $("#errors_display").html(errors);
                });
            } else {
                alert("{{ trans('enter_code') }}");
            }
        }

        function enter_number_again() {
            $("#errors_display").html("");
            $("#change_number").show();
            $("#confirm_sms").hide();
        }


    </script>
@endsection
