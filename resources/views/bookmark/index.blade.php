@extends('partials.app')

@section('content')
    <div class="aksii">
        <div class="container">
            <div class="grey-admin">
                <span class="add-span">{{ trans('app.favourites') }}</span>
                @foreach($bookmarks as $bookmark)
                  @php $image_url = optional($bookmark->company->pictures->first())->url; @endphp
                    <div class="form-group row">
                        <div class="col-md">
                            @if ($image_url)
                              <img src="{{ $image_url }}" class="img-fluid" title="img" alt="img"/>
                            @endif
                        </div>
                        <div class="col-md">
                            <h3>{{ $bookmark->company->name }}</h3>
                            <p>{{ $bookmark->company->description }}</p>
                            <p>{{ $bookmark->company->address }}</p>
                            <a class="btn btn-primary" href="/company/{{ $bookmark->company_id }}">{{ trans('app.view_company') }}</a>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
