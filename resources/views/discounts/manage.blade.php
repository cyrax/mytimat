@extends('partials.app')

@section('content')
    <div class="aksii">
        <div class="container">
            <span class="add-span">Управление акциями</span>
            <div class="aksii-filter">
                <div class="col-sm nopad">
                    <select name="category_id" id="category_id" style="width: 100%;" required>
                        <option value="0">Все компании</option>
                    </select>
                </div>
                <div class="col-sm">
                    <select name="category_id" id="category_id" style="width: 100%;" required>
                        <option value="0">Все типы</option>
                    </select>
                </div>
                <div class="col-sm">
                    <button class="aksii-search">Показать</button>
                </div>
            </div>
            <div class="aksii-table-head">
                <div class="a-t-text a-t-names">Наименование</div>
                <div class="a-t-text a-t-type">Тип акции</div>
                <div class="a-t-text a-t-variant">Вариант<br/> акции</div>
                <div class="a-t-text a-t-cena">Цена услуги</div>
                <div class="a-t-text a-t-cena">Цена для пользователя</div>
                <div class="a-t-text a-t-podarok">Подарок</div>
                <div class="a-t-text a-t-prodol">Продолжительность</div>
                <div class="a-t-text a-t-summa">Сумма</div>
            </div>
            <span class="aksii-company-name">ТОО «Тестовая компания»</span>
            <div class="aksii-company-budget"><span>100</span> tim</div>
            <button class="aksii-add-company">Добавить акцию</button>
            <div class="clear"></div>
            <div class="card-group" id="accordion">
                <div class="card">
                    <div class="card-header aksii-card-header">
                        <div class="a-t-text a-t-names">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                                    <img src="/image/pan_up.png" class="card-bagit pan_down">
                                    <img src="/image/pan_down.png" class="card-bagit pan_up">
                                    Бонус за бронирование услуги
                                </a>
                            </h4>
                        </div>
                        <div class="a-t-text a-t-type">
                            <select name="category_id" id="category_id" style="width: 100%;" required></select>
                        </div>
                        <div class="a-t-text a-t-variant">
                            <select name="category_id" id="category_id" style="width: 100%;" required></select>
                        </div>
                        <div class="a-t-text a-t-cena">
                            <input type="" name="" class="add-input">
                        </div>
                        <div class="a-t-text a-t-cena">
                            <input type="" name="" class="add-input">
                        </div>
                        <div class="a-t-text a-t-podarok">
                            <input type="" name="" class="add-input">
                        </div>
                        <div class="a-t-text a-t-prodol">
                            <select name="category_id" id="category_id" style="width: 100%;" required></select>
                        </div>
                        <div class="a-t-text a-t-summa">
                            <input type="" name="" class="add-input">
                            <img src="/image/h-close.png" class="close-aksii">
                        </div>
                    </div>
                    <div id="collapse1" class="card-collapse collapse">
                        <div class="card-body sub-card-body aksii-sub-card-body">
                            <label class="add-label">Услуги</label>
                            <div class="radio-cover2 aksii-radio-cover2">
                                <label class="checkbox-label">
                                    <input type="checkbox" name="dostavka" class="radio" value="samo" id="samo">
                                    <span class="checkbox-icon" for="samo"></span>
                                    <span class="checkbox-text">
                    Стрижка
                  </span>
                                </label>
                            </div>
                            <div class="radio-cover2 aksii-radio-cover2">
                                <label class="checkbox-label">
                                    <input type="checkbox" name="dostavka" class="radio" value="samo" id="samo">
                                    <span class="checkbox-icon" for="samo"></span>
                                    <span class="checkbox-text">
                    Разработка и дизайн
                  </span>
                                </label>
                            </div>
                            <div class="radio-cover2 aksii-radio-cover2">
                                <label class="checkbox-label">
                                    <input type="checkbox" name="dostavka" class="radio" value="samo" id="samo">
                                    <span class="checkbox-icon" for="samo"></span>
                                    <span class="checkbox-text">
                   Стрижка волос
                  </span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="aksii-serii-div">Пользователь получает: 0  Пользователь затратит: 0 </div>
            <div class="card">
                <div class="card-header aksii-card-header">
                    <div class="a-t-text a-t-names">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1e1">
                                <img src="/image/pan_up.png" class="card-bagit pan_down">
                                <img src="/image/pan_down.png" class="card-bagit pan_up">
                                Бонус за бронирование услуги
                            </a>
                        </h4>
                    </div>
                    <div class="a-t-text a-t-type">
                        <select name="category_id" id="category_id" style="width: 100%;" required></select>
                    </div>
                    <div class="a-t-text a-t-variant">
                        <select name="category_id" id="category_id" style="width: 100%;" required></select>
                    </div>
                    <div class="a-t-text a-t-cena">
                        <input type="" name="" class="add-input">
                    </div>
                    <div class="a-t-text a-t-cena">
                        <input type="" name="" class="add-input">
                    </div>
                    <div class="a-t-text a-t-podarok">
                        <input type="" name="" class="add-input">
                    </div>
                    <div class="a-t-text a-t-prodol">
                        <select name="category_id" id="category_id" style="width: 100%;" required></select>
                    </div>
                    <div class="a-t-text a-t-summa">
                        <input type="" name="" class="add-input">
                        <img src="/image/h-close.png" class="close-aksii">
                    </div>
                </div>
                <div id="collapse1e1" class="card-collapse collapse">
                    <div class="card-body sub-card-body aksii-sub-card-body">
                        <label class="add-label">Услуги</label>
                        <div class="radio-cover2 aksii-radio-cover2">
                            <label class="checkbox-label">
                                <input type="checkbox" name="dostavka" class="radio" value="samo" id="samo">
                                <span class="checkbox-icon" for="samo"></span>
                                <span class="checkbox-text">
                    Стрижка
                  </span>
                            </label>
                        </div>
                        <div class="radio-cover2 aksii-radio-cover2">
                            <label class="checkbox-label">
                                <input type="checkbox" name="dostavka" class="radio" value="samo" id="samo">
                                <span class="checkbox-icon" for="samo"></span>
                                <span class="checkbox-text">
                    Разработка и дизайн
                  </span>
                            </label>
                        </div>
                        <div class="radio-cover2 aksii-radio-cover2">
                            <label class="checkbox-label">
                                <input type="checkbox" name="dostavka" class="radio" value="samo" id="samo">
                                <span class="checkbox-icon" for="samo"></span>
                                <span class="checkbox-text">
                   Стрижка волос
                  </span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="aksii-serii-div">Пользователь получает: 0  Пользователь затратит: 0 </div>
            <button class="aksii-add-company sohranit-aksii">Сохранить изменения</button>
            <button class="olatit-aksii">Оплатить</button>
            <span class="obshaya-summa-aksia">Общая сумма акции: <span>2300</span></span>
        </div>
    </div>
@endsection

@section('css')

@endsection

@section('script')

@endsection