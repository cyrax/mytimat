@extends('partials.app')

@section('content')
    <div class="aksii podarki">
        <div class="container">
            <span class="add-span">Подарки</span>
            <div class="aksii-filter">
                <div class="col-sm nopad">
                    <select name="category_id" id="category_id" style="width: 100%;" required>
                        <option value="0" @if($id == 0) selected @endif>Все категории</option>
                        @foreach($data['categories'] as $category)
                            <option value="{{ $category['id'] }}" @if($category['id'] == $id) selected @endif>{{ $category['name'] }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm">
                    <button class="aksii-search" onclick="select_category()">Показать</button>
                </div>
            </div>
            <div class="aksii-container">
            @foreach($data['services'] as $service)
                    <div class="col-sm">
                    <div class="aksii-text-cover">
                        <span class="podarki-span">{{ $service->name }}</span>
                    </div>
                    <div class="aksii-cover">
                        <span class="aksii-cena">{{ $service->price }}<span>Tim</span></span>
                        <div class="aksii-img" style="background-image: url('/images/gifts/{{ $service->picture }}.jpg'); background-size: cover;">
                        </div>
                        <p class="moto-desc">{{ $service->text }}</p>
                        <button>Посмотреть</button>
                    </div>
                </div>
            @endforeach
                <div class="clear"></div>
                {{--<button class="aksii-more">Показать еще</button>--}}
            </div>
        </div>
    </div>
@endsection

@section('css')

@endsection

@section('script')
    <script>
        $("#category_id").select2();
        
        function select_category() {
            var id = $("#category_id").val();
            location.href = '/gift/' + id;
        }
    </script>
@endsection