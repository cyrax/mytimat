@extends('partials.app')

@section('content')
    <div class="aksii podarki">
        <div class="container">
            <span class="add-span">Управление подарками</span>
            @if(session('category_added'))
                <div class="alert alert-success"><b>Категория добавлена!</b></div>
            @endif
            @if(session('gift_added'))
                <div class="alert alert-success"><b>Подарок добавлен!</b></div>
            @endif
            @if(session('gift_edited'))
                <div class="alert alert-success"><b>Подарок обновлен!</b></div>
            @endif
            @if(session('category_deleted'))
                <div class="alert alert-warning"><b>Категория удалена!</b></div>
            @endif
            @if(session('category_exists'))
                <div class="alert alert-warning"><b>Категория с таким же названием уже существует!</b></div>
            @endif
            <div class="aksii-filter">
                <div class="col-sm nopad">
                    <select name="category_id" id="category_id" style="width: 100%;" required>
                        <option value="0">Все категории</option>
                        @foreach($data['categories'] as $category) <option value="{{ $category['id'] }}">{{ $category['name'] }}</option> @endforeach
                    </select>
                </div>
                <div class="col-sm">
                    <input type="text" name="search" class="add-input" placeholder="Поиск">
                </div>
                <div class="col-sm nopad">
                    <button class="aksii-search">Найти</button>

                    <a href="#add-category"><button class="podarki-search">Добавить категорию</button></a>
                </div>
            </div>
            <div class="aksii-table-head nopad">
                <div class="col">
                    <div class="a-t-text">Наименование</div>
                </div>
                <div class="col">
                    <div class="a-t-text">Текст</div>
                </div>
                <div class="col nopad">
                    <div class="a-t-text">Цена</div>
                </div>
                <div class="col">
                    <div class="a-t-text">Кол-во</div>
                </div>
                <div class="col">
                    <div class="a-t-text">Действия</div>
                    @if(!empty($data['categories']->toArray()))<a href="#add-gift"><button class="add-podarok" style="float: right;">Добавить подарок</button></a>@endif
                </div>
            </div>
            @forelse($data['categories'] as $category)
                <div id="category_block_{{ $category->id }}" class="podarki-cover"><div class="podarki-names">
                    <div class="col">
                        <div class="podarki-span">{{ $category['name'] }}</div>
                    </div>

                    <div class="col col-offset-6">
                        <img src="/image/h-close.png" class="close-aksii" onclick="delete_category({{ $category->id }}, '{{ addslashes($category->name) }}')">
                    </div>
                        <form id="deleting_category_{{ $category->id }}" action="/gift/category/delete" method="post">
                            {{ csrf_field() }} <input type="hidden" name="_method" value="DELETE"><input type="hidden" name="category" value="{{ $category->id }}">
                        </form>
                </div>
                @foreach($category->gifts()->latest()->get() as $gift)
                <div id="gift_block_{{ $gift->id }}" class="podarki-list">
                    <div class="col">
                        <div class="img-podarki" style="background-image: url('/images/gifts/thumbnail/{{ $gift->picture  }}.jpg'); background-size: cover;"></div>
                        {{ $gift->name  }}
                    </div>
                    <div class="col">
                        {{ $gift->text  }}
                    </div>
                    <div class="col nopad">
                        {{ $gift->price  }}
                    </div>
                    <div class="col">
                        {{ $gift->quantity  }}
                    </div>
                    <div class="col">
                        <a href="#edit-gift-{{ $gift->id }}"><button type="button" class="btn btn-primary"><i class="fa fa-pencil"></i> Редактировать</button></a>
                        <button type="button" class="btn btn-danger" onclick="delete_gift({{$gift->id}}, '{{ addslashes($gift->name) }}')"><i class="fa fa-times"></i> Удалить</button>
                    </div>
                </div>

                        <div class="dm-overlay" id="edit-gift-{{ $gift->id }}">
                            <div class="dm-table">
                                <div class="dm-cell">
                                    <div class="dm-modal" style="color: #111111;"><form action="/gift/edit" method="post" enctype="multipart/form-data">
                                            {{ csrf_field() }}
                                            <input type="hidden" name="_method" value="PUT">
                                            <input type="hidden" name="id" value="{{ $gift->id }}">
                                            <h3>Редактировать подарок</h3><br>
                                            @if (session('gift_edit_error_'.$gift->id))
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif
                                            <p style="text-align: left;">
                                                <input name="name" type="text" class="add-input" value="@if(!empty(old('name_'.$gift->id))){{ old('name_'.$gift->id) }}@else{{ $gift->name }}@endif" placeholder="Название подарка" style="width: 100%;" required>
                                                @if(!empty(old('category_id_'.$gift->id)))
                                                    <select name="category_id" id="edit_gifts_category_{{ $gift->id }}" style="width: 100%;" required>
                                                        @foreach($data['categories'] as $category) <option value="{{ $category['id'] }}" @if($category['id'] == old('category_id_'.$gift->id)) selected @endif>{{ $category['name'] }}</option> @endforeach
                                                    </select>
                                                @else
                                                    <select name="category_id" id="edit_gifts_category_{{ $gift->id }}" style="width: 100%;" required>
                                                        @foreach($data['categories'] as $category) <option value="{{ $category['id'] }}" @if($category['id'] == $gift->category_id) selected @endif>{{ $category['name'] }}</option> @endforeach
                                                    </select>
                                                @endif
                                                <br><br>Изображение <br>(оставьте пустым, если не хотите изменить)<br><br>
                                                <input name="picture" type="file">
                                                <br><textarea name="text" class="add-textarea" cols="30" rows="3" required style="width: 100%; height: auto;" placeholder="Описание">@if(!empty(old('text_'.$gift->id))){{ old('text_'.$gift->id) }}@else{{ $gift->text }}@endif</textarea>
                                                <br><input name="price" type="number" value="@if(old('price_'.$gift->id)){{ old('price_'.$gift->id) }}@else{{ $gift->price }}@endif" class="add-input" placeholder="Цена" style="width: 100%;" required>
                                                <br><input name="quantity" type="number" value="@if(old('quantity_'.$gift->id)){{ old('quantity_'.$gift->id) }}@else{{ $gift->quantity }}@endif" class="add-input" placeholder="Количество" style="width: 100%;" required>
                                            </p>
                                            <p><button type="submit" class="aksii-search gifts_add_cat"><i class="fa fa-check"></i> Добавить</button></p>
                                            <p style="margin: 15px 0 0;"><a href="#close">Отменить</a></p></form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <script>
                            $(function () {
                                $("#edit_gifts_category_{{ $gift->id }}").select2();
                            });
                        </script>
                @endforeach{{--
                <button class="add-button">
                    Сохранить изменения
                </button>--}}
            </div>
            @empty
                <div id="selected_placeholder" style="text-align: center; font-size: 1.5em; padding: 50px 0 200px;">Сначала добавьте категории подарков</div>
            @endforelse
        </div>
    </div>


    <div class="dm-overlay" id="add-category">
        <div class="dm-table">
            <div class="dm-cell">
                <div class="dm-modal"><form action="/gift/category/add" method="post">
                    <h3>Новая категория</h3><br>
                    <p>
                        {{ csrf_field() }}
                        <input name="name" type="text" class="add-input" placeholder="Название категории" style="width: 250px;" required>
                    </p>
                    <p><button type="submit" class="aksii-search gifts_add_cat"><i class="fa fa-check"></i> Добавить</button></p>
                    <p style="margin: 15px 0 0;"><a href="#close">Отменить</a></p></form>
                </div>
            </div>
        </div>
    </div>


    @if(!empty($data['categories']->toArray()))<div class="dm-overlay" id="add-gift">
        <div class="dm-table">
            <div class="dm-cell">
                <div class="dm-modal" style="color: #111111;"><form action="/gift/store" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    <h3>Новый подарок</h3><br>
                        @if (session('gift_add_error'))
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    <p style="text-align: left;">
                        <input name="name" type="text" class="add-input" value="{{ old('name') }}" placeholder="Название подарка" style="width: 100%;" required>
                        <select name="category_id" id="new_gifts_category" style="width: 100%;" required>
                            @foreach($data['categories'] as $category) <option value="{{ $category['id'] }}" @if($category['id'] == old('category_id')) selected @endif>{{ $category['name'] }}</option> @endforeach
                        </select>
                        <br><br>Изображение<br>
                        <input name="picture" type="file" style="width: 100%;" required>
                        <br><textarea name="text" class="add-textarea" cols="30" rows="3" required style="width: 100%; height: auto;" placeholder="Описание">{{ old('text') }}</textarea>
                        <br><input name="price" type="number" value="{{ old('price') }}" class="add-input" placeholder="Цена" style="width: 100%;" required>
                        <br><input name="quantity" type="number" value="{{ old('quantity') }}" class="add-input" placeholder="Количество" style="width: 100%;" required>
                    </p>
                    <p><button type="submit" class="aksii-search gifts_add_cat"><i class="fa fa-check"></i> Добавить</button></p>
                    <p style="margin: 15px 0 0;"><a href="#close">Отменить</a></p></form>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('css')

@endsection

@section('script')
    <script>
        $(function () {
            $("#category_id").select2();
        });
        $(function () {
            $("#new_gifts_category").select2();
        });

        function delete_gift(id, name) {
            if (confirm('Вы точно хотите удалить подарок ' + name +'?')) {
                $.ajax({
                    method: "POST",
                    url: "/gift/delete",
                    data: { "_method": 'DELETE', "_token": '{{csrf_token()}}', gift: id }
                }).done(function( msg ) {
                    $("#gift_block_" + id).hide('slow', function () {
                        $( this ).remove();
                    });
                }).fail(function() {
                    alert("Ошибка при удалении!");
                });
            }
        }

        function delete_category(id, name) {
            if (confirm('Вы точно хотите удалить категорию подарков: ' + name +'?')) {
                if(confirm('Это удалит все подарки в категории. Все равно продолжить?')) {
                    $("#deleting_category_"+id).submit();
                }
            }
        }
    </script>
@endsection