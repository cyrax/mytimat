@php
    $message = $exception->getMessage();
@endphp
@extends('partials.app')

@section('content')
    <div class="aksii">
        <div class="container text-center">
            <h1 style="color: black;">404</h1>
            <h2>{{ !empty($message) ? $message : trans('error.404') }}</h2>
        </div>
    </div>
@endsection