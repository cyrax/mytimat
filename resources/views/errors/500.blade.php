@extends('partials.app')

@section('content')
    <div class="aksii">
        <div class="container text-center">
            <h1 style="color: black;">500</h1>
            <h2>{{ trans('error.500') }}</h2>
        </div>
    </div>
@endsection