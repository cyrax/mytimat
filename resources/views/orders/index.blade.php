@extends('partials.app')

@section('content')
    <div class="aksii message zayavki">
        <div class="container">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <span class="add-span">{{ trans('app.manage_orders') }}</span>
                <h2 style="margin: 2em 0;">{{ trans('app.new_orders') }}</h2>
                @include('partials.orders.order', ['orders' => $data['new_orders']])
            <div class="row">
                <div class="col-md">
                    <form class="pull-left">
                        <select id="filter_roles" name="roles">
                            <option value="">{{ trans('app.roles') }}</option>
                            @foreach ($data['roles'] as $role)
                                <option value="{{ $role->id }}" {{ isset($_GET['roles']) && $role->id == $_GET['roles'] ? 'selected':'' }}>{{ $role->name }}</option>
                            @endforeach
                        </select>
                        <select id="filter_specialists" name="specialists">
                            <option value="">{{ trans('app.specialists') }}</option>
                            @foreach ($data['specialists'] as $specialist)
                                <option value="{{ $specialist->id }}" {{ isset($_GET['specialists']) && $specialist->id == $_GET['specialists'] ? 'selected' : '' }}>{{ $specialist->name }} {{ $specialist->last_name }}</option>
                            @endforeach
                        </select>
                        <button class="btn btn-primary">{{ trans('app.filter') }}</button>
                    </form>
                </div>
            </div>
            <div class="calendar-cover zayavki">
                <div class="text-center">
                    <a href='#company-order' class="btn">{{ trans('app.choose_date_and_time') }}</a>
                </div>
                <span class="kok-span">{{ trans('app.calendar') }}
                    <a href="#kok-span" onclick="return calendarToggle(this);">
                        <span class="select2-container select2-container--open">
                        <span class="select2-selection--single">
                            <span class="select2-selection__arrow" role="presentation" style="top: -4px; left: -20px;">
                                <b role="presentation"></b>
                            </span>
                        </span>
                        </span>
                    </a>
                </span>
                {!! $calendar->calendar() !!}
                <div class="clear"></div>
            </div>
                <h2 style="margin: 2em 0;">{{ trans('app.completed_orders') }}</h2>
                @php
                    $acceptedOrders = $orders->filter(function ($item) {
                        return $item->status == (\App\Order::STATUS_ACCEPTED);
                    })->values();
                @endphp
                @include('partials.orders.order', ['orders' => $acceptedOrders])
        </div>
        <div class="dm-overlay" id="reject-modal">
            <div class="dm-table">
                <div class="dm-cell">
                    <div class="dm-modal">
                        <h2>{{ trans('app.reject_reason') }}</h2>
                        <form method="POST" id="reject-form">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_id">
                            <div class="form-group">
                                <select title="rejected_status" class="form-control" name="reject_status">
                                    <option value="">{{ trans('app.select_reason') }}</option>
                                    @foreach(\app\Order::rejectedStatuses() as $key => $status)
                                        <option value="{{ $key }}" name="rejected_status">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea name="reject_text" title="Rejected_text" rows="5" style="height: auto" class="form-control" placeholder="{{ trans('app.reason_to_change_application') }}" required></textarea>
                            </div>
                            <button class="btn btn-primary" onclick="return send_reject_form()">{{ trans('app.send') }}</button>
                        </form>
                        <hr>
                        <a href="#close" onclick="$('#reject-modal').modal('toggle')" id="close_button">{{ trans('app.close') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="dm-overlay" id="accept-modal">
            <div class="dm-table">
                <div class="dm-cell">
                    <div class="dm-modal">
                        <h2>{{ trans('app.application_confirmed') }}</h2>
                        <a href="#close" onclick="$('#accept-modal').modal('toggle');$('#accept-modal').modal('toggle')" id="close_button">{{ trans('app.close') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="dm-overlay" id="company-order">
            <div class="dm-table">
                <div class="dm-cell">
                    <div class="dm-modal">
                        <a href="#close" class="close"></a>
                        <h2 class="text-center">{{ trans('app.order') }}</h2>
                        <form method="POST" id="order-form-table-form-date" action="{{ route('company-order') }}">
                            {{ csrf_field() }}
                            <input type="hidden" name="company_id" value="{{ $data['company']->id }}">
                            <div class="form-group">
                                <span class="j-tus"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                <input type="date" name="order_date" min="{{ date('Y-m-d') }}" required="">
                                <input class="order_time" type="time" name="order_time">
                            </div>
                            <div class="form-group">
                                <select title="" class="form-control" name="service_id" required>
                                    <option value="">{{ trans('app.select_service') }}</option>
                                    @foreach($data['services'] as $service)
                                        <option value="{{ $service->id }}">{{ $service->name }} ({{ \App\Http\Controllers\CompaniesController::get_duration($service->days, $service->hours, $service->minutes) }})</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select title="" class="form-control" name="specialist_id">
                                    <option value="">{{ trans('app.any') }}</option>
                                    @foreach($data['specialists'] as $specialist)
                                        <option value="{{ $specialist->id }}">{{ $specialist->name }} {{ $specialist->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-primary" onclick="">{{ trans('app.send') }}</button>
                        </form>
                        <hr>
                        <a href="#close" onclick="$('#new-order-modal').modal('hide');$('.modal-backdrop').remove()" id="close_button">{{ trans('app.close') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="dm-overlay" id="new-order-modal">
            <div class="dm-table">
                <div class="dm-cell">
                    <div class="dm-modal">
                        <h2>{{ trans('app.chosen_time') }} <br /><span id="chosenTime"></span></h2>
                        <form method="POST" id="order-form-table-form" action="/order-from-table">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_date" id="order_date">
                            <input type="hidden" name="company_id" value="{{ $data['company']->id }}">
                            <div class="form-group">
                                <select title="rejected_status" class="form-control" name="service_id">
                                    <option value="">{{ trans('app.select_service') }}</option>
                                    @foreach($data['services'] as $service)
                                        <option value="{{ $service->id }}" name="rejected_status">{{ $service->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <select title="rejected_status" class="form-control" name="specialist_id">
                                    <option value="">{{ trans('app.choose_specialist') }}</option>
                                    @foreach($data['specialists'] as $specialist)
                                        <option value="{{ $specialist->id }}" name="rejected_status">{{ $specialist->name }} {{ $specialist->last_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="btn btn-primary" onclick="">{{ trans('app.send') }}</button>
                        </form>
                        <hr>
                        <a href="#close" onclick="$('#new-order-modal').modal('hide');$('.modal-backdrop').remove()" id="close_button">{{ trans('app.close') }}</a>
                    </div>
                </div>
            </div>
        </div>
        @endsection

        @section('css')
            <link rel="stylesheet" href="/datepicker/css/bootstrap-datepicker3.min.css">
            <link rel="stylesheet" href="/css/fullcalendar.min.css"/>
            <link rel="stylesheet" href="/js/scheduler.min.css"/>
        @endsection

        @section('script')
            <script src="/js/moment-with-locales.min.js"></script>
            <script src="/js/fullcalendar.min.js"></script>
            <script src="/js/locale-all.js"></script>
            <script src="/js/scheduler.min.js"></script>
            <script src="/datepicker/js/bootstrap-datepicker.min.js"></script>
            {!! $calendar->script() !!}
            <script>
                $(function () {
                    $('#datepicker_months').datepicker({
                        format: "M yyyy",
                        startDate: "today",
                        minViewMode: 1,
                        maxViewMode: 2
                    });

                    $('#datepicker1').datepicker({
                        format: "d-M-yyyy",
                        maxViewMode: 1,
                        todayBtn: "linked",
                        todayHighlight: true
                    });

                    $('#company_id').select2();
                    $('#status').select2();
                    $('#filter_roles').select2();
                    $('#filter_specialists').select2();
                    $('#order_specialists_1').select2();

                });

                function reject_order(id) {
                    $('#reject-modal').modal().show();
                    $('.modal-backdrop').remove();
                    $('[name=order_id]').val(id);
                    $('#order-block-'+id).hide();

                    // $('#name_' + id).append(" <span id='rejected_" + id + "' style=\"color: red;\">Отменен</span>");
                    // $('#rejected_input_' + id).val(1);
                    // $('#order_specialists_' + id).prop("disabled", true);
                    // $('#order_off_' + id).hide();
                    // $('#order_on_' + id).show();

                    return false;
                }

                function send_reject_form() {
                    $.ajax({
                        url: '/order/reject',
                        method: 'POST',
                        data: $('#reject-form').serialize(),
                        success: function (data) {
                            $('#reject-form').html(data);
                        }
                    });
                    return false;
                }
                function send_new_order_form() {
                    $.ajax({
                        url: '/order-from-table',
                        method: 'POST',
                        data: $('#order-form-table-form').serialize(),
                        success: function (data) {
                            $('#order-form-table-form').html(data);
                        }
                    });
                    return false;
                }

                function accept_order(id) {
                    $.ajax({
                        url: '/order/'+id+'/accept',
                        method: 'PUT',
                        data: $('#order-block-'+id+' form').serialize(),
                        success: function (data) {
                            $('#accept-modal').toggle();
                            $('.modal-backdrop').remove();
                            $('#order-block-'+id).hide();
                        }
                    });

                    return false;
                    // $('#rejected_' + id).remove();
                    // $('#rejected_input_' + id).val(0);
                    // $('#order_specialists_' + id).prop("disabled", false);
                    // $('#order_off_' + id).show();
                    // $('#order_on_' + id).hide();
                }

                function edit_time(id) {
                    $('#order_date_raw_' + id).hide();
                    $('#order_date_input_' + id).show();

                    $('#order_date_input_' + id + ' #date').datepicker({
                        format: "dd.mm.yy",
                        weekStart: 1,
                        startDate: "today",
                        maxViewMode: 2,
                        todayBtn: "linked",
                        defaultViewDate: {year: 1977, month: 04, day: 25}
                    });
                    $('#order_time_' + id).next().hide();
                }

                function submit_time(id) {
                    $.ajax({
                        url: '/company/' + id + '/edit',
                        method: 'POST',
                        data: {
                            '_token': $('#order_date_input_' + id + ' [name=_token]').val(),
                            date: $('#order_date_input_' + id + ' #date').val(),
                            time: $('#order_date_input_' + id + ' #time').val(),
                            specialist: $('#order_specialists_' + id).val()
                        },
                        success: function () {
                            $('#order_time_' + id).text($('#order_date_input_' + id + ' #time').val());
                            $('#order_date_' + id).text($('#order_date_input_' + id + ' #date').val());
                            $('#order_name_' + id).text($('#order_specialists_' + id + ' :selected').text());
                            $('#order_date_input_' + id).hide();
                            $('#order_date_raw_' + id).show();
                            $('#order_name_' + id).show();
                            $('#accept-' + id).hide();
                            $('#reject-' + id).hide();
                            $('#order_date_input_' + id).parents('.zayavki-li').append('{{ trans('app.waits_confirm') }}');
                        }
                    })
                    return false;
                }
            </script>
@endsection
