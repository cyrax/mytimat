@extends('partials.app')

@section('content')
    <div class="bread">
        <div class="container">
            <a href="{{ route('company-specialist-orders', ['id' => $order->company_id]) }}">{{ trans('app.orders') }}</a> » <span>{{ $order->id }}</span>
        </div>
    </div>
    <div class="aksii">
        <div class="container">
            <h2>{{ trans('app.order_number') }} #{{ $order->id }}</h2>
            <p><b>{{ trans('app.service') }}</b>: {{ $order->service->name }}</p>
            <p><b>{{ trans('app.service_duration') }}</b>: {{ \App\Http\Controllers\CompaniesController::get_duration($order->service->days, $order->service->hours, $order->service->minutes) }}</p>
            <p><b>{{ trans('app.specialist') }}</b>: {{ optional($order->specialist)->name }}</p>
            <p><b>{{ trans('app.company') }}</b>: {{ $order->company->name }}</p>
            <p><b>{{ trans('app.date') }}</b>: {{ $order->date }} {{ $order->time }}</p>
            <p><b>{{ trans('app.created_at') }}</b>: {{ $order->created_at }}</p>
            <p><b>{{ trans('app.status') }}</b>: {{ $order->getStatus() }}</p>
        </div>
    </div>
@endsection