@extends('partials.app')

@section('content')
    <div class="aksii message zayavki">
        <div class="container">
            <span class="add-span">{{ trans('app.manage_orders') }}</span>
            <div class="">
                <span class="kok-span">{{ trans('app.calendar') }}
                    <a href="#kok-span" onclick="return calendarToggle(this);">
                        <span class="select2-container select2-container--open">
                        <span class="select2-selection--single">
                            <span class="select2-selection__arrow" role="presentation" style="top: -4px; left: -20px;">
                                <b role="presentation"></b>
                            </span>
                        </span>
                        </span>
                    </a>
                </span>
                {!! $calendar->calendar() !!}

                <h2 style="margin: 2em 0;">{{ trans('app.my_applications') }}</h2>
                @foreach($data['orders'] as $new_order)

                    <div class="zayavki-cover">
                        @php
                            $user = $new_order->user;
                            $orders = \App\Order::where('group', $new_order->group)->get();
                        @endphp
                        <form action="/order/{{ $new_order->id }}/accept"
                              method="POST" accept-charset="UTF-8">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="zayavki-info">
                                <span class="kara-zayavka">{{ trans('app.order_number') }}</span>
                                <img src="/image/store-new-badges.png">
                                <span class="names-zayavka">(
                                    <a target="_blank" href="/specialist/profile/{{ $user->id }}">{{ $user->getFullName() }}</a>
                                <a href="tel:{{ $user->phone }}">{{ $user->phone }}</a>)</span>
                                <img src="/image/speech-bubble.png">
                                    <span class="names-zayavka">
                                        <a href="/chat-with-company/{{ $new_order['company_id'] }}/{{ $new_order->company->user_id }}">{{ trans('app.chat') }}</a>
                                    </span>
                                </div>
                            @foreach($orders as $order)
                                @php
                                    $service = $order->service;
                                @endphp
                                <div class="row">
                                    <input type="hidden" id="rejected_input_{{ $order->id }}"
                                           name="order[{{ $order->id }}][rejected]" value="0">
                                    <div class="col-sm" id="name_{{ $order->id }}"><span
                                                class="zayavki-ati">{{ $service->name }}</span></div>
                                    <div class="col-sm">
                                        <div id="order_date_input_{{ $order->id }}" style="display: none">
                                            {{ csrf_field() }}
                                            <input type="text" id="date" class="col" name="date" value="{{ $order->date }}">
                                            <input type="time" id="time" class="col" name="time" value="{{ $order->time }}">
                                            <div class="col">
                                                <select name="services[{{ $order->id }}][specialist]" id="order_specialists_{{ $order->id }}" style="width: 100%;" required>
                                                    <option value="0">{{ trans('app.any') }}</option>
                                                    @foreach($data['specialists'] as $specialist)
                                                        @if ($specialist['id'] == $order->specialist_id)
                                                            <option value="{{ $specialist['id'] }}" selected>{{ $specialist->getFullName() }}</option>
                                                        @else
                                                            <option value="{{ $specialist['id'] }}">{{ $specialist->getFullName() }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>
                                                <script>
                                                document.addEventListener("DOMContentLoaded", function(event) {
                                                  $(function () {
                                                      $("#order_specialists_{{ $order->id }}").select2();
                                                  });
                                                });
                                                </script>
                                            </div>
                                            <a class="btn btn-warning" onclick="return submit_time({{ $order->id }})">{{ trans('app.accept_changes') }}</a>
                                        </div>
                                        <div id="order_date_raw_{{ $order->id }}">
                                        <span id="order_date_{{ $order->id }}" class="zayavki-ati">
                                            <img class="calendar2"
                                                 src="/image/calendar2.png">{{ \Carbon\Carbon::parse($order->date)->format('d.m.y') }}
                                        </span>
                                            <span id="order_time_{{ $order->id }}" class="zayavki-ati">{{ \Carbon\Carbon::parse($order->time)->format('H:i') }}</span>
                                            {{--@if($order->specialist_id == 0) @if(Auth::user()->group != 'company_specialist')--}}
                                            {{----}}
                                            {{--@else--}}
                                            {{--(Будет оформлен на вас)--}}
                                            {{--@endif--}}
                                            {{--@else --}}
                                            <span id="order_name_{{ $order->id }}" class="zayavki-ati">{{ optional($order->specialist)->getFullName() }}</span>
                                            {{--@endif--}}
                                            <i class="fa fa-pencil" style="cursor: pointer; color: #1850bd; font-size: 18px;" onclick="edit_time({{ $order->id }})"></i>
                                        </div>
                                    </div>
                                    <div class="col-sm">{{ \App\Http\Controllers\CompaniesController::get_duration($service->days, $service->hours, $service->minutes) }}</div>
                                    <div class="col-sm"><span class="zayavki-ati">{{ $service->price }}</span>
                                        <a id="order_off_{{ $order->id }}" class="zayavki-del"
                                           onclick="reject_order({{$order->id}})" style="cursor: pointer;"><i
                                                    class="fa fa-times" aria-hidden="true"></i></a>
                                        <a id="order_on_{{ $order->id }}" class="zayavki-del zayavki-plus"
                                           onclick="accept_order({{$order->id}})" style="cursor: pointer; display: none;"><i
                                                    class="fa fa-plus" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                                @if ($order->status === 'new')
                                    <button class="add-button" id="accept-{{ $order->id }}" onclick="accept_order({{ $order->id }})">
                                        {{ trans('app.confirm') }}
                                    </button>
                                    <a href="#" href="/order/{{ $order->id }}/reject" id="reject-{{ $order->id }}" class="kizil-a" onclick="return reject_order({{ $order->id }})">{{ trans('app.cancel_application') }}</a>
                                @elseif ($order->status === 'accepted')
                                    {{ trans('app.application_confirmed') }}
                                @elseif ($order->status === 'rejected')
                                    {{ trans('app.application_rejected') }}
                                @elseif ($order->status === 'need_accept_changes')
                                    <button class="add-button" id="accept-{{ $order->id }}" style="width: 218px;" onclick="accept_order({{ $order->id }})">
                                        {{ trans('app.accept_changes') }}
                                    </button>
                                @endif
                            @endforeach
                        </form>
                    </div>
                @endforeach

            </div>
        </div>
        <div class="dm-overlay" id="reject-modal">
            <div class="dm-table">
                <div class="dm-cell">
                    <div class="dm-modal">
                        <h2>{{ trans('app.reject_reason') }}</h2>
                        <form method="POST" id="reject-form">
                            {{ csrf_field() }}
                            <input type="hidden" name="order_id">
                            <div class="form-group">
                                <select title="rejected_status" class="form-control" name="reject_status">
                                    <option value="">{{ trans('app.select_reason') }}</option>
                                    @foreach(\app\Order::rejectedStatuses() as $key => $status)
                                        <option value="{{ $key }}" name="rejected_status">{{ $status }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea name="reject_text" title="Rejected_text" rows="5" style="height: auto" class="form-control" placeholder="{{ trans('app.reason_to_change_application') }}" required></textarea>
                            </div>
                            <button class="btn btn-primary" onclick="return send_reject_form()">{{ trans('app.send') }}</button>
                        </form>
                        <hr>
                        <a href="#close" onclick="$('#reject-modal').modal('toggle')" id="close_button">{{ trans('app.close') }}</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="dm-overlay" id="accept-modal">
            <div class="dm-table">
                <div class="dm-cell">
                    <div class="dm-modal">
                        <h2>{{ trans('app.application_confirmed') }}</h2>
                        <a href="#close" onclick="$('#accept-modal').modal('toggle');$('#accept-modal').modal('toggle')" id="close_button">{{ trans('app.close') }}</a>
                    </div>
                </div>
            </div>
        </div>
        @endsection
    </div>
        @section('css')
            <link rel="stylesheet" href="/datepicker/css/bootstrap-datepicker3.min.css">
            <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
        @endsection

        @section('script')
        {!! $calendar->script() !!}
            <script src="/js/moment.min.js"></script>
            <script src="/js/fullcalendar.min.js"></script>
            <script src="/datepicker/js/bootstrap-datepicker.min.js"></script>
            <script>
                $(function () {
                    $('#datepicker_months').datepicker({
                        format: "M yyyy",
                        startDate: "today",
                        minViewMode: 1,
                        maxViewMode: 2
                    });

                    $('#datepicker1').datepicker({
                        format: "d-M-yyyy",
                        maxViewMode: 1,
                        todayBtn: "linked",
                        todayHighlight: true
                    });

                    $('#company_id').select2();
                    $('#status').select2();
                    $('#order_specialists_1').select2();

                });

                function reject_order(id) {
                    // $('#name_' + id).append(" <span id='rejected_" + id + "' style=\"color: red;\">{{ trans('app.canceled') }}</span>");
                    // $('#rejected_input_' + id).val(1);
                    // $('#order_specialists_' + id).prop("disabled", true);
                    // $('#order_off_' + id).hide();
                    // $('#order_on_' + id).show();

                    $('#reject-modal').modal().show();
                    $('.modal-backdrop').remove();
                    $('[name=order_id]').val(id);
                    $('#order-block-'+id).hide();
                }

                function send_reject_form() {
                    $.ajax({
                        url: '/user-order/reject',
                        method: 'POST',
                        data: $('#reject-form').serialize(),
                        success: function (data) {
                            $('#reject-form').html(data);
                        }
                    });
                    return false;
                }

                function accept_order(id) {
                    $('#rejected_' + id).remove();
                    $('#rejected_input_' + id).val(0);
                    $('#order_specialists_' + id).prop("disabled", false);
                    $('#order_off_' + id).show();
                    $('#order_on_' + id).hide();
                }

                function edit_time(id) {
                    $('#order_date_raw_' + id).hide();
                    $('#order_date_input_' + id).show();

                    $('#order_date_input_' + id + ' #date').datepicker({
                        format: "dd.mm.yy",
                        weekStart: 1,
                        startDate: "today",
                        maxViewMode: 2,
                        todayBtn: "linked",
                        defaultViewDate: {year: 1977, month: 4, day: 25}
                    });
                    $('#order_time_'+id).next().hide();
                }

                function submit_time(id) {
                    $.ajax({
                        url: '/user-order/'+id+'/edit',
                        method: 'POST',
                        data: {
                            '_token': $('#order_date_input_'+id+' [name=_token]').val(),
                            date: $('#order_date_input_' + id+' #date').val(),
                            time: $('#order_date_input_' + id+' #time').val(),
                            specialist: $('#order_specialists_' + id).val()
                        },
                        success: function () {
                            $('#order_time_' + id).text($('#order_date_input_' + id+' #time').val());
                            $('#order_date_' + id).text($('#order_date_input_' + id+' #date').val());
                            $('#order_name_' + id).text($('#order_specialists_' + id+' :selected').text());
                            $('#order_date_input_' + id).hide();
                            $('#order_date_raw_' + id).show();
                            $('#order_name_'+id).show();
                            $('#accept-'+id).hide();
                            $('#reject-'+id).hide();
                            $('#order_date_input_' + id).parents('.zayavki-li').append('{{ trans('app.waits_confirm') }}');
                        }
                    });
                    return false;
                }
            </script>


@endsection
