@php
    $locale = 'ru';
    if (isset($currentLanguage)) {
        $locale = $currentLanguage->locale;
    }
    $attribute = 'title_'.$locale;
    $countryRequest = $_COOKIE['country'] ?? null;
    $cityRequest  = $_COOKIE['city'] ?? null;
@endphp
<form method="get" action="/company/search" enctype="application/x-www-form-urlencoded">
    <div class="row" id="main-form">
        <div class="col-md col select-cover" style="">
            <select name="country" id="the_country" style="width: 100%;" onchange="setCountry()">
                <option value="0">{{ trans('app.choose_country') }}</option>
                @if (isset($countries))
                    @foreach($countries as $country)
                        <option value="{{ $country->id }}" {{ $countryRequest == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                    @endforeach
                @else
                    @foreach(\App\Country::select("country_id as id", $attribute.' as name')->get() as $country)
                        <option value="{{ $country->id }}" {{ $countryRequest == $country->id ? 'selected' : '' }}>{{ $country->name }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-md col select-cover" style="">
            <select name="city" id="the_city" style="width: 100%;" onchange="setCityCookie();">
                <option value="0">{{ trans('app.choose_city') }}</option>
                @if (isset($countryRequest))
                    @foreach(\App\City::select('city_id as id', $attribute.' as name')->where('country_id', $countryRequest)->get() as $city)
                        <option value="{{ $city->id }}" {{ $cityRequest == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                    @endforeach
                @endif
            </select>
        </div>
        <div class="col-md col input-cover" style="">
            <input id="search" type="text" value="{{ request()->get('search_text') }}" name="search_text" placeholder="{{ trans('app.main_search_placeholder') }}" required>
            <button></button>
            <a href="#advanced" class="advanced-search-arrow" data-toggle="collapse"><i class="fa fa-arrow-down"></i></a>
        </div>
    </div>
    <div class="row collapse" id="advanced">
        <div class="col-md">
            <div class="form-group" style="margin-top: 10px;">
                <input value="{{ request()->get('price_from') }}" placeholder="{{ trans('app.price') }} {{ trans('app.from') }}" name="price_from" type="text" class="form-control" />
            </div>
        </div>
        <div class="col-md">
            <div class="form-group" style="margin-top: 10px;">
                <input value="{{ request()->get('price_to') }}" placeholder="{{ trans('app.price') }} {{ trans('app.to') }}" name="price_to" type="text" class="form-control" />
            </div>
        </div>
        <div class="col-md">
            <div class="form-group zayavki" style="margin-top: 10px;">
                <select name="category_id" id="category">
                    <option value="">{{ trans('app.choose_category') }}</option>
                    @foreach (\App\Category::all() as $category)
                        <option value="{{ $category->id }}" {{ request()->get('category_id') == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</form>
<script>
    function setCountry()
    {
        $('#the_city').empty();
        setCookie('country', $('#the_country').val(), 365);
        $.ajax({
            url: '/get-cities/'+$('#the_country').val(),
            success: function (data) {
                var parsedCities = $.parseJSON(data);
                parsedCities.forEach(function (item) {
                    $('<option/>', { value : item.id }).text(item.name+"").appendTo('#the_city');
                });
            }
        });
    }
    function setCityCookie()
    {
        setCookie('city', $('#the_city').val(), 365);
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }
</script>
