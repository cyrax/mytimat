<div class="popular">
    <div class="container">
        <span class="popular-span">{{ trans('app.popular_requests') }}</span>
        <div class="col-md col-sm">
            <div class="product-div">
                <div class="product-img first-img">
                </div>
                <span>{{ trans('app.electrician') }}</span>
            </div>
        </div>
    </div>
</div>