<div class="rating-first">
    <div class="container">
        <span class="rating-span">{{ trans('app.rating_establishments') }}</span>
        <div class="col-sm">
            <div class="rating-div">
                <div class="rating-img"><img src="/image/ge.png"></div>
                <div class="rating-bal">
          <span class="rating-title">
            <b>Здоровый стиль</b><br/>
            Салон красоты
          </span>
                    <span class="raing-star">
            <img src="/image/star_e.png">
            <img src="/image/star_f.png">
            <img src="/image/star_f.png">
            <img src="/image/star_f.png">
            <img src="/image/star_f.png">
          </span>
                </div>
                <p>Порой мы так устаем с детьми, домашними хлопотами
                    и заботой о близких, что часто забываем о себе.
                    Предлагаем вам приятный и полезный отдых! </p>
            </div>
        </div>

        <div class="clear"></div>
        <a class="vse-rating nopadbot" href="#">{{ trans('app.all_rating') }}</a>
    </div>
</div>