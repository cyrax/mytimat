@php
    $locale = isset($language) ? $language->locale : $currentLanguage->locale;
@endphp
<select class="country form-control" name="country_id" id="country_id" style="width: 100%;" required>
    <option value="">{{ trans('app.choose_country', [], $locale) }}</option>
    @foreach($countries as $country)
        <option value="{{ $country->id }}" {{ (int)$country->id === (int)$countryId ? 'selected' : '' }}>{{ $country->name }}</option>
    @endforeach
</select>