@php
    $attribute = 'title_'.$currentLanguage->locale;
    $locale = isset($language) ? $language->locale : $currentLanguage->locale;
@endphp
<select name="city_id" id="city_id" style="width: 100%;" class="form-control city" onchange="$('[name=address]').val('');changeMap();" required>
    <option value="">{{ trans('app.choose_city', [], $locale) }}</option>
    @if (isset($countryId))
        @foreach(\App\City::where('country_id', $countryId)->where('important', 1)->get() as $city)
            <option value="{{ $city->city_id }}" {{ $city->city_id == $selectedCityId ? 'selected' : '' }}>{{ $city->$attribute }}</option>
        @endforeach
    @endif
</select>