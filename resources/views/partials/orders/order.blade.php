@foreach($orders as $new_order)
    <div class="zayavki-cover" id="order-block-{{ $new_order->id }}">
        @php
            $user = $new_order->user;
            $orders = \App\Order::where('group', $new_order->group)->get();
        @endphp
        <form action="/order/{{ $new_order->id }}/accept"
              method="POST" accept-charset="UTF-8">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="zayavki-info">
                <span class="kara-zayavka">{{ trans('app.order_number') }} #{{ $new_order['id'] }}</span>
                <img src="/image/store-new-badges.png">
                <span class="names-zayavka">(<a target="_blank"
                                                href="/specialist/profile/{{ $user->id }}">{{ $user->getFullName() }}</a>
                                <a href="tel:{{ $user->phone }}">{{ $user->phone }}</a>)</span>
                <img src="/image/speech-bubble.png">
                <span class="names-zayavka">
                                    <a href="/chat-with-company/{{ $new_order['company_id'] }}/{{ $new_order->company->user_id }}">{{ trans('app.chat') }}</a>
                                </span>
            </div>
            @foreach($orders as $order)
                @php
                    $service = $order->service;
                @endphp
                <div class="row">
                    <input type="hidden" id="rejected_input_{{ $order->id }}"
                           name="order[{{ $order->id }}][rejected]" value="0">
                    <div class="col-sm nopad" id="name_{{ $order->id }}"><span
                                class="zayavki-ati">{{ $service->name }}</span></div>
                    <div class="col-sm">
                        <div id="order_date_input_{{ $order->id }}" style="display: none" class="row">
                            {{ csrf_field() }}
                            <input type="text" id="date" class="col" name="date"
                                   value="{{ $order->date }}">
                            <input type="time" id="time" class="col" name="time"
                                   value="{{ $order->time }}">
                            <div class="col nopad">
                                <select name="services[{{ $order->id }}][specialist]"
                                        id="order_specialists_{{ $order->id }}" style="width: 100%;"
                                        required>
                                    <option value="0">{{ trans('app.any') }}</option>
                                    @foreach($data['specialists'] as $specialist)
                                        @if ($specialist['id'] == $order->specialist_id)
                                            <option value="{{ $specialist['id'] }}"
                                                    selected>{{ $specialist->getFullName() }}</option>
                                        @else
                                            <option value="{{ $specialist['id'] }}">{{ $specialist->getFullName() }}</option>
                                        @endif
                                    @endforeach
                                </select>
                                <script>
                                document.addEventListener("DOMContentLoaded", function(event) {
                                    $(function () {
                                        $("#order_specialists_{{ $order->id }}").select2();
                                    });
                                });
                                </script>
                            </div>
                            <a class="btn btn-warning" onclick="return submit_time({{ $order->id }})">{{ trans('app.accept_changes') }}</a>
                        </div>
                        <div id="order_date_raw_{{ $order->id }}">
                                        <span id="order_date_{{ $order->id }}" class="zayavki-ati">
                                            <img class="calendar2"
                                                 src="/image/calendar2.png">{{ \Carbon\Carbon::parse($order->date)->format('d.m.y') }}
                                        </span>
                            <span id="order_time_{{ $order->id }}"
                                  class="zayavki-ati">{{ \Carbon\Carbon::parse($order->time)->format('H:i') }}</span>
                            <span id="order_name_{{ $order->id }}"
                                  class="zayavki-ati">{{ optional($order->specialist)->getFullName() }}</span>
                            <i class="fa fa-pencil"
                               style="cursor: pointer; color: #1850bd; font-size: 18px;"
                               onclick="edit_time({{ $order->id }})"></i>
                        </div>
                    </div>
                    <div class="col-sm nopad">{{ \App\Http\Controllers\CompaniesController::get_duration($service->days, $service->hours, $service->minutes) }}</div>
                    <div class="col-sm nopad"><span class="zayavki-ati">{{ $service->price }}</span>
                        <a id="order_off_{{ $order->id }}" href="/order/{{ $order->id }}/reject"
                           class="zayavki-del"
                           onclick="return reject_order({{$order->id}})" style="cursor: pointer;"><i
                                    class="fa fa-times" aria-hidden="true"></i></a>
                        <a id="order_on_{{ $order->id }}" class="zayavki-del zayavki-plus"
                           onclick="return accept_order({{$order->id}})"
                           style="cursor: pointer; display: none;"><i
                                    class="fa fa-plus" aria-hidden="true"></i></a>
                    </div>
                    <div class="clear"></div>
                </div>
                @if ($order->status === 'new')
                    <button class="add-button" id="accept-{{ $order->id }}" onclick="return accept_order({{ $order->id }})">
                        {{ trans('app.confirm') }}
                    </button>
                    <a id="reject-{{ $order->id }}" href="/order/{{ $order->id }}/reject"
                       onclick="return reject_order({{ $order->id }})" class="kizil-a">{{ trans('app.cancel_application') }}</a>
                @elseif ($order->status === 'accepted')
                    {{ trans('app.application_confirmed') }}
                @elseif ($order->status === 'rejected')
                    {{ trans('app.application_rejected') }}
                @elseif ($order->status === 'need_accept_changes')
                    {{ trans('app.awaiting_user_approval') }}
                @elseif ($order->status === 'reaccept_changes')
                    <button class="add-button" style="width: 218px;"
                            onclick="return accept_order({{ $order->id }})" id="accept-{{ $order->id }}">
                        {{ trans('app.accept_changes') }}
                    </button>
                    <a href="/order/{{ $order->id }}/reject" id="reject-{{ $order->id }}"
                       class="kizil-a" onclick="return reject_order({{ $order->id }})">{{ trans('app.cancel_application') }}</a>
                @endif
            @endforeach
        </form>
    </div>
@endforeach
