<footer>
    <div class="container">
        <div class="col-sm">
            <span class="goto">bukutime © {{ date("Y") }}</span>
            <nav>
                <li><a href="{{ route('about-page') }}">{{ trans('app.about_site') }}</a></li>
                <li><a href="{{ route('pages-view', ['url' => 'user-agreement']) }}">{{ trans('app.user_agreement') }}</a></li>
                <li><a href="{{ route('contacts-page') }}">{{ trans('app.contacts') }}</a></li>
            </nav>
        </div>
        <div class="col-sm">
            <span class="social">{{ trans('app.We are in social') }}</span>
            <div class="clear"></div>
            <a href="https://t.me/bukutime"><img class="soc-img" src="/image/telegram.png"></a>
            <a href="https://www.instagram.com/bukutime/"><img class="soc-img" src="/image/instagram.png"></a>
            <a href="https://www.facebook.com/bookyourtime/"><img class="soc-img" src="/image/facebook.png"></a>
            <a href="https://twitter.com/bukutime"><img class="soc-img" src="/image/twitter.png"></a>
            <a href="https://vk.com/public166588314"><img class="soc-img" src="/image/vk.png"></a>
            <br><br>
            <a href="mailto:support@bukutime.com">support@bukutime.com</a>
        </div>
    </div>
</footer>