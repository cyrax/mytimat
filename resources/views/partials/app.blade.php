<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="Book your time">
    <title>Book Your Time</title>
    <!--[if IE]>
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="{{ asset('/css/app.css?v=2') }}" />
    <!-- <style type="text/css">{{-- file_get_contents(public_path().'/css/app.css') --}}</style> -->
    @yield('css')
    @yield('head_script')
    <!-- <script src="{{ asset('js/jquery-3.2.1.min.js') }}" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script> -->

    <!-- HTTPS required. HTTP will give a 403 forbidden response -->
    <!-- <script src="https://sdk.accountkit.com/en_US/sdk.js"></script> -->


</head>
<body>
@if(!Auth::check())
    @include('auth.modal')
@endif
@if(Illuminate\Support\Facades\Route::currentRouteName() !== 'index')
    @include('partials.topmenu_main_page')
@endif
@yield('content')
@include('partials.footer')
<script>
    var Laravel = {
        pusherKey: '{{ env('PUSHER_APP_KEY') }}',
        pusherCluster: '{{ env("PUSHER_CLUSTER") }}',
        pusherSecret: '{{ env("PUSHER_APP_SECRET") }}',
        pusherId: '{{ env("PUSHER_APP_ID") }}'
    }
</script>
<script src="{{ asset('/js/app.js?v=6') }}"></script>
@if (Auth::check())
    <script>
        var notification_show = function(data) {
            toastr.success(data.sender.name, data.message);

            toastr.options = {
                "closeButton": false,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "showDuration": "2000",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };

            toastr.options.onclick = function() {
                window.location = '/chat/'+data.sender.id;
            };
        }
        function calendarToggle(context) {
              $(context).find('span').first().toggleClass('select2-container--open');
              $(context).parent().next().toggle();
        }
    </script>
{!! talk_live(['user'=>["id"=>auth()->user()->id, 'callback'=>['notification_show', 'msgshow']]]) !!}
@endif
@yield('script')
</body>
</html>
