@if(isset($currentLanguage))
    <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
        <img src="/image/{{ optional($currentLanguage)->locale }}.png"> {{ optional($currentLanguage)->name }} <span class="caret"></span>
    </a>
    <div class="dropdown-menu">
        @foreach($altLocalizedUrls as $alt)
              <a href="/{{ $alt['locale'].substr(app('request')->path(), 2) }}" class="dropdown-item">
                  <img src="/image/{{ $alt['locale'] }}.png"> {{ $alt['name'] }}
              </a>
        @endforeach
    </div>
@endif
