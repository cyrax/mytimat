<div class="how_to_work">
    <div class="container">
        <span class="how_span">{{ trans('app.how_to_work_with_bukutime') }}</span>
        <div class="row">
          <div class="col-md col-sm">
              <div class="how_div">
                  <img src="/image/locate.png">
                  <p>
                      {{ trans('app.find_closest_organizations') }}
                  </p>
                  <span>{{ trans('app.find_closest_organizations_text') }}</span>
              </div>
          </div>
          <div class="col-md col-sm">
              <div class="how_div">
                  <img src="/image/calendar.png">
                  <p>
                      {{ trans('app.working_schedule') }}
                  </p>
                  <span>{{ trans('app.working_schedule_text') }}</span>
              </div>
          </div>
          <div class="col-md col-sm">
              <div class="how_div">
                  <img src="/image/check.png">
                  <p>
                      {{ trans('app.reserve_in_your_comfortable_time') }}
                  </p>
                  <span>{{ trans('app.reserve_in_your_comfortable_time_text') }}</span>
              </div>
          </div>
          <div class="col-md col-sm">
              <div class="how_div">
                  <img src="/image/clock.png">
                  <p>
                      {{ trans('app.manage_your_time') }}
                  </p>
                  <span>{{ trans('app.manage_your_time_text') }}</span>
              </div>
          </div>
      </div>
        @if (!Auth::check())
            <a href="#login_register"><span class="start">{{ trans('app.start_using_services') }}</span></a>
        @endif
    </div>
</div>
