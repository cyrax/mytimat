<header class="white">
    <div class="container">
      <nav class="navbar navbar-light navbar-expand-lg">
        <a class="navbar-brand" href="/">
            <img src="/image/logo.png" class="logo">
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        @include('partials.topmenu')
      </nav>
    </div>
</header>
<div class="dm-overlay" id="country-list">
    <div class="dm-table">
        <div class="dm-cell">
            <div class="dm-modal">
                <h2>{{ trans('app.choose_country') }}</h2>
                @foreach(\App\Country::all() as $country)
                    <p>
                        <a href="#" onclick="return showCities(this)" data-id="{{ $country->id }}" class="city-select">{{ $country->name }}</a>
                    </p>
                @endforeach
                <hr>
                <a href="#close" id="close_button">{{ trans('app.close') }}</a>
            </div>
        </div>
    </div>
</div>
<div class="dm-overlay" id="city-list">
    <div class="dm-table">
        <div class="dm-cell">
            <div class="dm-modal">
                <h2 id="choose_city_title">{{ trans('app.choose_city') }}</h2>
                <hr>
                <a href="#close" id="close_button">{{ trans('app.close') }}</a>
            </div>
        </div>
    </div>
</div>
<div class="kok-poisk">
    <div class="container">
        @include('partials.poisk')
    </div>
</div>
<div class="container">
  <nav class="navbar navbar-expand-lg navbar-light">
    <ul class="navbar-nav mr-auto">
      @if(Auth::check())
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ trans('app.user') }}
                  <span class="caret"></span>
              </a>
              <div class="dropdown-menu">
                <a class="dropdown-item" href="{{ route('profile_index') }}"><img src="/image/user2.png">{{ trans('app.profile') }}</a>
                <a class="dropdown-item" href="{{ route('user_orders') }}">{{ trans('app.orders') }}</a>
                <a class="dropdown-item" href="{{ route('bookmarks') }}">{{ trans('app.favourites') }}</a>
                <a class="dropdown-item" href="{{ route('user_settings') }}">{{ trans('app.settings') }}</a>
                @if(Auth::user()->group == 'user')
                    <a class="dropdown-item" href="{{ route('company_add') }}">{{ trans('app.register_company') }}</a>
                    <a class="dropdown-item" href="{{ route('company_join') }}">{{ trans('app.register_as_specialist') }}</a>
                @endif
              </div>
          </li>
          @if(Auth::user()->administrator)
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ trans('app.administrator') }}
                      <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{ route('company-manage') }}">{{ trans('app.manage_companies') }}</a>
                      <a class="dropdown-item" href="{{ route('translator_rights') }}">{{ trans('app.rights_and_permissions') }}</a>
                      <a class="dropdown-item" href="{{ route('translator_users') }}">{{ trans('app.users') }}</a>
                  </div>
              </li>
          @endif
          @if(Auth::user()->techsupport)
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ trans('app.tech_support') }}
                      <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('chat-techsupport', ['id' => 0]) }}">{{ trans('app.chat') }}</a>
                  </div>
              </li>
          @endif
          @if(Auth::user()->translator)
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ trans('app.translator') }}
                      <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{ route('translator_ui_elements') }}">{{ trans('app.ui_elements') }}</a>
                  </div>
              </li>
          @endif
          @if(Auth::user()->group == 'manager')
          @endif
          @if(Auth::user()->group == 'company_admin')
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ trans('app.service_provider') }}
                      <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="{{ route('company-view', ['id' => Auth::user()->company_id]) }}">{{ trans('app.our_company') }}</a>
                    <a class="dropdown-item" href="{{ route('company-members') }}">{{ trans('app.participants') }}</a>
                    <a class="dropdown-item" href="{{ route('company-all-orders') }}">{{ trans('app.orders') }}</a>
                    <a class="dropdown-item" href="{{ route('company-settings') }}">{{ trans('app.settings') }}</a>
                  </div>
              </li>
          @endif
          @if(Auth::user()->specialist)
              <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">{{ trans('app.specialist') }}
                      <span class="caret"></span>
                  </a>
                  <div class="dropdown-menu">
                      <a class="dropdown-item" href="{{ route('specialist-profile') }}">{{ trans('app.profile') }}</a>
                      <a class="dropdown-item" href="{{ route('specialist-orders') }}">{{ trans('app.orders') }}</a>
                      <a class="dropdown-item" href="{{ route('specialist-settings') }}">{{ trans('app.settings') }}</a>
                  </div>
              </li>
          @endif
        @endif
      </ul>
  </nav>
</div>

<script>
    function showCities(context) {
        createCookie('country', $(context).data('id'), 31);
        $('#country-list').modal().hide();
        $('.modal-backdrop').remove();
        $('#city-name').text($(context).text());
        $.ajax({
            url: '/get-cities',
            data: {country_id: $(context).data('id')},
            success: function (data) {
                $('#city-list').modal().show();
                $('.modal-backdrop').remove();
                data = JSON.parse(data);
                data.forEach(function (item, index) {
                    $('#choose_city_title').after('<p><a href="#" onclick="return setCity(this)" data-id="'+item.id+'" class="city-select">'+item.name+'</a></p>');
                })
            }
        });
        return false;
    }

    function setCity(context) {
        createCookie('city', $(context).data('id'), 31);
        $('#city-list').modal().hide();
        $('.modal-backdrop').remove();
        $('#city-name').append(', '+$(context).text());
        $('body').removeClass('modal-open');
        var currURL= window.location.pathname;
        var afterDomain= currURL.substring(currURL.indexOf('/'), currURL.lastIndexOf('#') + 1);
        window.history.replaceState({}, document.title, currURL );
    }

    function createCookie(name, value, days) {
        var expires;

        if (days) {
            var date = new Date();
            date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
            expires = "; expires=" + date.toGMTString();
        } else {
            expires = "";
        }
        document.cookie = encodeURIComponent(name) + "=" + encodeURIComponent(value) + expires + "; path=/";
    }
</script>
