<div class="blue-div">
    <div class="container">
        <span class="blue-span">{{ trans('app.use_our_app') }}</span>
        <img src="/image/VectorSmartObject.png" class="vectorsmartobject">
        <div class="free-app">
            <img class="smartphone" src="/image/smartphone.png">
            <div class="check">
                <img class="check-symbol" src="/image/check-symbol.png">
                <p>
                    <b>{{ trans('app.fast_search') }}</b><br/>
                    {{ trans('app.mobile_apps_fast_search_desk') }}
                </p>
                <img class="check-symbol" src="/image/check-symbol.png">
                <p>
                    <b>{{ trans('app.free') }}</b><br/>
                    {{ trans('app.mobile_apps_free_desk') }}
                </p>
                <img class="check-symbol" src="/image/check-symbol.png">
                <p>
                    <b>{{ trans('app.always_online') }}</b><br/>
                    {{ trans('app.mobile_apps_always_online_desk') }}
                </p>
                <button class="check-but">{{ trans('app.download') }}</button>
            </div>
        </div>
    </div>
</div>