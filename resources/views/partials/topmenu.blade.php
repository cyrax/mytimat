<div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto">
        @if(Auth::check())
            <li class="nav-item">
                <a href="{{ route('profile_index') }}" class="nav-link">
                    <img src="/image/user2.png"> {{ trans('app.profile') }}
                </a>
            </li>
            @if(auth()->user()->group === 'user')
                <li class="nav-item">
                    <a href="{{ route('user_orders') }}" class="nav-link">
                        <img src="/image/zayavki.png"> {{ trans('app.orders') }}
                    </a>
                </li>
            @endif
            <li class="nav-item">
                @php
                    $top_menu_messages = $messages = \App\UserMessage::where(['owner_id' => Auth::id(), 'read_at' => null])->count();
                @endphp
                <a href="{{ route('messages') }}" class="nav-link">
                    <img src="/image/mail.png">
                    {{ trans('app.messages') }}
                    @if($top_menu_messages)
                        <span class="lang-b">{{ $top_menu_messages }}</span>
                    @endif
                </a>
            </li>
            <li class="nav-item dropdown">
                @include('partials.language-picker')
            </li>
            <li class="nav-item">
                <a href="{{ route('help') }}" class="nav-link">
                    <img src="/image/question.png"> {{ trans('app.help') }}
                </a>
            </li>
            <li class="nav-item">
                <a href="#" onclick="event.preventDefault();document.getElementById('logout-form').submit();" class="nav-link">
                    <span style="color: #79c5d6;"><i class="fa fa-sign-out"></i></span> {{ trans('app.exit') }}
                </a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </li>
        @else
            <li class="nav-item dropdown">
                @include('partials.language-picker')
            </li>
            <li class="nav-item" style="margin-right: 20px;">
                <a href="#login_register" style="font-family: 'GothaProReg';" class="nav-link">
                    <img src="/image/user.png">
                </a>
            </li>
        @endif
    </ul>
</div>
@section('css')
    <style>
        .navbar-nav > li > a {
            color: #FFF!important;
        }
        .navbar-nav .open .dropdown-menu > li > a {
            background: transparent;
        }
        .dropdown-menu {
            background: transparent;
            border: none;
        }
        .navbar-nav .dropdown-menu  a {
            color: #FFF!important;
            background: transparent!important;
            padding-left: 9px;
        }
        .navbar-toggle:focus, .navbar-toggle:hover {
            background: transparent!important;
        }
        /* @media(max-width:33.9em) {
        .navbar .collapse {max-height:250px;width:100%;overflow-y:auto;overflow-x:hidden;}
        .navbar .navbar-brand {float:none;display: inline-block;}
        .navbar .navbar-nav>.nav-item { float: none; margin-left: .1rem;}
        .navbar .navbar-nav {float:none !important;}
        .nav-item{width:100%;text-align:left;}
        .navbar .collapse .dropdown-menu {
          position: relative;
          float: none;
          background:none;
          border:none;
        }
      } */
    </style>
@endsection
