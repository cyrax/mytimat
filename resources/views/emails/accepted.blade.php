<h1>{{ trans('app.order_accepted') }}</h1>
<table>
    <tbody>
        <tr>
            <td>ID: {{ $order->id }}</td>
        </tr>
        <tr>
            <td>{{ trans('app.service') }}: {{ optional($order->service)->name }}</td>
        </tr>
        <tr>
            <td>{{ trans('app.company') }}: {{ optional($order->company)->name }}</td>
        </tr>
        <tr>
            <td>{{ trans('app.specialist') }}: {{ optional($order->specialist)->name }}</td>
        </tr>
        <tr>
            <td>{{ trans('app.user') }}: {{ optional($order->user)->name }}</td>
        </tr>
        <tr>
            <td>{{ trans('app.time') }}: {{ $order->date }} {{ $order->time }}</td>
        </tr>
    </tbody>
</table>
