@extends('partials.app')

@section('content')

    @php
        $languages = [];
        $selectableLanguages[] = $currentLanguage;
        $languages = $selectableLanguages->reverse();
        $currentLanguages = [$currentLanguage];
    @endphp

    <div class="add-company">
        <div class="container">
            <span class="add-span">{{ trans('app.profile') }}</span>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="grey-admin">
                <span class="grey-span">{{ trans('app.description') }}</span>
                <div id="tabs" class="tabs">
                    <nav>
                        <ul>
                            @foreach($languages as $language)
                                <li id="tab_{{ $language->locale }}" class="{{ $language->locale === $currentLanguage->locale ? 'tab-current' : '' }}">
                                    <a href="/{{ $language->locale }}/specialist/{{ $member->id }}/edit">
                                    <span>
                                        <img src="/image/{{ $language->locale }}.png">{{ $language->name }}
                                    </span>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </nav>
                    <div class="content">
                        @foreach($currentLanguages as $language)
                            <section id="section-{{ $language->locale }}" class="content-current">
                                <form method="POST" enctype="multipart/form-data" class="form" id="form-{{ $language->locale }}">
                                    {{csrf_field()}}
                                    <input type="hidden" name="lang" value="{{ $language->locale }}">
                                    {!! view('specialist.edit.info', compact('countries', 'languages', 'language', 'member')) !!}
                                    {!! view('specialist.edit.contacts', compact('language', 'member'))  !!}
                                </form>
                                <div class="row">
                                    <div class="col-md">
                                        <button style="margin-bottom:0;" type="submit" onclick="return submitSpecialist()" class="oplatit-balance">{{ trans('app.submit', [], $language->locale) }}</button>
                                    </div>
                                </div>
                            </section>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="dm-overlay" id="add_non_working_date">
        <div class="dm-table">
            <div class="dm-cell">
                <div class="dm-modal">
                    <p style="font-size: 18px; color: #111;">{{ trans('app.choose_weekends') }}</p>
                    <p>
                        <input id="non_working_day" type="date" class="add-input" min="{{ \Carbon\Carbon::now()->toDateString() }}" autofocus>
                    </p>
                    <p><button type="button" class="aksii-search" style="float: none;" onclick="add_non_working_day()">{{ trans('app.add') }}</button></p>
                    <div id="temp_non_days"></div>
                    <p><a href="#close"><button type="button" class="aksii-search aksii-search-select"><i class="fa fa-check"></i> {{ trans('app.save') }}</button></a></p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/cbpFWTabs.js"></script>
    <script>
        $('#the_city').select2();
        $('.country').select2({placeholder: "{{ trans('app.choose_country') }}"});
        $('.city').select2({placeholder: "{{ trans('app.choose_city') }}"});

        $("#country_id").change(function () {
            var country = $("#country_id").val();
            if(country === 0) {
                $("#city_id").html("<option value=\"\">{{ trans('app.choose_city') }}</option>");
            } else {
                $.ajax({
                    url: "/cities/"+ country
                }).done(function( msg ) {
                    $("#city_id").html("<option value=\"\">{{ trans('app.choose_city') }}</option>");
                    if(msg){
                        msg.forEach(function (t) {
                            $("#city_id").append("<option value=\"" + t.id + "\">" + t.name + "</option>");
                        });
                    } else {
                        $("#city_id").html("<option value=\"0\">{{ trans('app.choose_city') }}</option>");
                        alert("{{ trans('app.no_city_in_this_country') }}");
                    }
                }).fail(function() {
                    alert('{{ trans('app.error') }}');
                });
            }
        });
        function changeMap() {
            return true;
        }
        function hydra_days_change(day) {
            if($("input[name="+day+"]").is(':checked')){
                $("#hydra-start-end-"+day).show();
            } else {
                $("#hydra-start-end-"+day).hide();
            }
        }

        function hydra_lunch_change(day) {
            if($("input[name=lunch]").is(':checked')){
                $("#hydra-start-end-lunch").hide();
            } else {
                $("#hydra-start-end-lunch").show();
            }
        }

        var non_working_days = [];

        function delete_a_holiday(ind) {
            non_working_days.splice(ind, 1);
            refresh_non_working_days();
        }


        function add_non_working_day() {
            var a_day = $("#non_working_day").val();
            if( a_day === false ) {
                alert('{{ trans('app.choose_date') }}');
                return false;
            }
            if( a_day < '{{ \Carbon\Carbon::now()->toDateString() }}' ) {
                alert('{{ trans('app.chosen_date_already_passed') }}');
                return false;
            }
            if(non_working_days.indexOf( a_day ) === -1 ) {
                non_working_days.push(a_day);
                non_working_days = non_working_days.sort();
                refresh_non_working_days(a_day);
            } else {
                alert('{{ trans('app.already_chase_date') }}');
            }
        }

        function refresh_non_working_days() {
            var the_list_non = '';
            var the_temp_list_non = '';
            var the_hidden_input_non = '';
            non_working_days.forEach(function (data, index) {
                the_list_non = the_list_non + '<span id="holiday-'+index+'" class="holiday-day">'+data+'<img src="/image/h-close.png" onclick="delete_a_holiday(\''+index+'\')"></span>';
                the_temp_list_non = the_temp_list_non + '<span id="temp-holiday-'+index+'" class="holiday-day">'+data+'<img src="/image/h-close.png" onclick="delete_a_holiday(\''+index+'\')"></span><br>';
                the_hidden_input_non = the_hidden_input_non + '<input type="hidden" name="non_working_days[]" value="'+data+'">';
            });
            $("#temp_non_days").html(the_temp_list_non);
            $("#inputs_non_days").html(the_hidden_input_non);
            $("#list_of_non_days").html(the_list_non);
        }
    </script>


    <script src="/file_upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="/js/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="/file_upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="/file_upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload processing plugin -->
    <script src="/file_upload/js/jquery.fileupload-process.js"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="/file_upload/js/jquery.fileupload-image.js"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="/file_upload/js/jquery.fileupload-audio.js"></script>
    <!-- The File Upload video preview plugin -->
    <script src="/file_upload/js/jquery.fileupload-video.js"></script>
    <!-- The File Upload validation plugin -->
    <script src="/file_upload/js/jquery.fileupload-validate.js"></script>
    <script>
        $(function () {
            'use strict';
            // Change this to the location of your server-side upload handler:
            var url_picture = '/specialist/pictures-upload';
            var the_pictures_count = $('.hydra_uploaded_thumbnails').length;
            $('#pictures_fileupload').fileupload({
                url: url_picture,
                dataType: 'json',
                autoUpload: true,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 99900000,
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                previewMaxWidth: the_pictures_count === 0 ? 500 : 96,
                previewMaxHeight: the_pictures_count === 0 ? 340 : 110,
                previewCrop: true,
                maxNumberOfFiles: 10
            }).on('fileuploadadd', function (e, data) {
                data.context = $('<div class="hydra_uploaded_thumbnails"/>').appendTo('#hydra_avatar_images');

                $.each(data.files, function (index, file) {
                    var node = $('<span/>').append('<a href="#" class="delete_file" onclick="return deleteFileUploaded(this);" data-index="'+index+'">{{ trans('app.remove') }}</a>');
                    node.appendTo(data.context);
                });
            }).on('fileuploadprocessalways', function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.error) {
                    alert("{{ trans('error.uploading_file') }}: \n"+file.error+"\n\n {{ trans('app.filename') }}: \n"+file.name);
                } else if (file.preview) {
                    node
                        .prepend(file.preview);
                }
                the_pictures_count = the_pictures_count + 1;
                if (the_pictures_count > 4) {
                    $('#hydra_plus').remove();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress_picture .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }).on('fileuploaddone', function (e, data) {
                $.each(data.result.files, function (index, file) {
                    if (file.url) {
                        $("#hydra_pictures_urls").append($('<input type="hidden" name="pictures_urls[]" value="'+file.url+'"/>'));
                    } else if (file.error) {
                        alert(file.error);
                        $(data.context).html('');
                    }
                });
            }).on('fileuploadfail', function (e, data) {
                $.each(data.files, function () {
                    $(data.context).html('');
                    alert("{{ trans('error.uploading_file') }}: \n" + data.files[0].name);
                });
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
        function deleteFileUploaded(context) {
            let index = $(context).data('index') + 1;
            $('#hydra_pictures_urls').find('input:nth-child('+index+')').remove();
            $(context).parents('.hydra_uploaded_thumbnails').remove();
            return false;
        }
    </script>
@stop

@section('css')
    <link href="/css/tab.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('file_upload/css/jquery.fileupload.css') }}">
    <style>
        .map {
            width: 100%;
            height: 400px;
            background-color: grey;
        }
        .has-error .select2-selection {
            border-color: rgb(185, 74, 72) !important;
        }
        .has-error.plus-div {
            border-color: rgb(185, 74, 72) !important;
        }
    </style>
@endsection