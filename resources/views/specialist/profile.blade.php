@extends('partials.app')
@section('content')
    <div class="aksii studio">
        <div class="container">
            <span class="add-span">
                <span class="span-category">{{ optional($member->roles)->name }}</span><br/>
                {{ $member->name }}
                @foreach (auth()->user()->members as $userMember)
                    @if ($userMember->id == $member->id)
                        <a href="/specialist/{{ $member->id }}/edit" style="font-size: 20px;"><i class="fa fa-edit"></i></a>
                    @endif
                @endforeach
                <div style="position: relative;font-size: 14px;">
                  <input type="number" id="star-{{ $member->id }}" name="rating" class="rating rating-loading" value="{{ intval($member->averageRating) }}" {{ $member->userSumRating > 0 ? 'data-readonly' : '' }}>
                  {{ round($member->averageRating, 2) }}
                </div>
            </span>
            <div class="row">
              <div class="col-sm">
                  <ul class="slickslide">
                      @foreach($data['pictures'] as $picture)
                          <a class="fancy" data-fancybox="images" href="{{ $picture['url'] }}" style="">
                              <img src="{{ $picture['url'] }}" title="img" alt="img" width="95%"/>
                          </a>
                      @endforeach
                  </ul>
              </div>
              <div class="col-sm studio-small">
                  <div class="row">
                      <p class="moto-desc margintop30"><b>{{ trans('app.about_me') }}: </b>{{ optional($member->info)->about }}</p>
                  </div>
                  <div class="card-group" style="margin-top: 30px;">
                      <div class="card card">
                          <div class="card-body sub-card-body">
                              <div class="row">
                                  <div class="col-sm col nopad">
                                      <p class="moto-desc">
                                          <b>{{ trans('app.phone') }}:</b><br/>
                                          {{ $member->phone }}<br/>
                                      </p>
                                  </div>
                                  <div class="col-sm col nopad">
                                      <p class="moto-desc">
                                          @if($member->email)<b>E-mail:</b> <a href="mailto:{{ $member->email }}"
                                                                                target="_blank"><span
                                                      class="kok-color">{{ $member->email }}</span></a><br/>@endif
                                          @if($member->site)<b>{{ trans('app.website') }}:</b> <a href="{{ $member->site }}/" target="_blank"><span
                                                      class="kok-color">{{ $member->site }}</span></a>@endif
                                      </p>
                                  </div>
                                  @if($member->vk || $member->facebook || $member->twitter)
                                      <div class="col nopad">
                                          <p class="moto-desc"><b>{{ trans('app.social_network_pages') }}</b></p>
                                          @if($member->vk)<a href="https://{{ $member->vk }}">
                                              <img class="soc-img" src="/image/soc1.png"></a>@endif
                                          @if($member->facebook)<a href="https://{{ $member->facebook }}">
                                              <img class="soc-img" src="/image/soc2.png"></a>@endif
                                          @if($member->twitter)<a href="https://{{ $member->twitter }}">
                                              <img class="soc-img" src="/image/soc3.png"></a>@endif
                                      </div>@endif
                              </div>
                              <div class="row">
                                  <div class="col nopad">
                                      <p class="moto-desc"><b>{{ trans('app.work_time') }}</b></p>
                                  </div>
                              </div>
                              <div class="row">
                                  @if (isset($member->schedule))
                                  <div class="col-sm col nopad">
                                      <p class="moto-desc">
                                          @if($member->schedule->monday)<span
                                                  class="days-name">{{ trans('app.monday_short') }}.</span> {{ \Carbon\Carbon::parse($member->schedule->monday_start)->format('H:i') }}
                                          - {{ \Carbon\Carbon::parse($member->schedule->monday_end)->format('H:i') }}<br/>@endif
                                          @if($member->schedule->tuesday)<span
                                                  class="days-name">{{ trans('app.tuesday_short') }}.</span>  {{ \Carbon\Carbon::parse($member->schedule->tuesday_start)->format('H:i') }}
                                          - {{ \Carbon\Carbon::parse($member->schedule->tuesday_end)->format('H:i') }}<br/>@endif
                                          @if($member->schedule->wednesday)<span
                                                  class="days-name">{{ trans('app.wednesday_short') }}.</span> {{ \Carbon\Carbon::parse($member->schedule->wednesday_start)->format('H:i') }}
                                          - {{ \Carbon\Carbon::parse($member->schedule->wednesday_end)->format('H:i') }}<br/>@endif
                                          @if($member->schedule->thursday)<span
                                                  class="days-name">{{ trans('app.thursday_short') }}.</span> {{ \Carbon\Carbon::parse($member->schedule->thursday_start)->format('H:i') }}
                                          - {{ \Carbon\Carbon::parse($member->schedule->thursday_end)->format('H:i') }}<br/>@endif
                                          @if($member->schedule->friday)<span
                                                  class="days-name">{{ trans('app.friday_short') }}.</span> {{ \Carbon\Carbon::parse($member->schedule->friday_start)->format('H:i') }}
                                          - {{ \Carbon\Carbon::parse($member->schedule->friday_end)->format('H:i') }}<br/>@endif
                                          @if($member->schedule->saturday)<span
                                                  class="days-name">{{ trans('app.saturday_short') }}.</span> {{ \Carbon\Carbon::parse($member->schedule->saturday_start)->format('H:i') }}
                                          - {{ \Carbon\Carbon::parse($member->schedule->saturday_end)->format('H:i') }}<br/>@endif
                                          @if($member->schedule->sunday)<span
                                                  class="days-name">{{ trans('app.sunday_short') }}.</span> {{ \Carbon\Carbon::parse($member->schedule->sunday_start)->format('H:i') }}
                                          - {{ \Carbon\Carbon::parse($member->schedule->sunday_end)->format('H:i') }} @endif
                                      </p>
                                  </div>
                                  <div class="col-sm col nopad">
                                      @if($member->lunch)<p class="moto-desc">
                                          {{ trans('app.lunch') }}:<br/>
                                          {{ \Carbon\Carbon::parse($member->lunch_start)->format('H:i') }}
                                          - {{ \Carbon\Carbon::parse($member->lunch_end)->format('H:i') }}
                                      </p>@endif
                                      @php
                                          $anchor = 0;
                                      @endphp
                                      @if(!empty($data['non_working_days']))
                                          <p class="moto-desc">
                                              {{ trans('app.non_working_days') }}:<br/>
                                              @foreach($data['non_working_days'] as $non_working_day)
                                                  @php
                                                      $anchor++;
                                                  @endphp
                                                  @if($anchor != $data['nwd_count'])
                                                      {{ \Carbon\Carbon::parse($non_working_day['date'])->format('j M, ') }}
                                                  @else
                                                      {{ \Carbon\Carbon::parse($non_working_day['date'])->format('j M') }}
                                                  @endif
                                              @endforeach
                                          </p>
                                      @endif
                                  </div>
                                  @endif
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="/js/cbpFWTabs.js"></script>
    <script>
        function add_bookmark() {
            $.ajax({
                method: "POST",
                url: "/company/{{ $member->id }}/bookmark",
                data: {"_token": '{{csrf_token()}}'}
            }).done(function (msg) {
                $("#bookmark").hide();
                $("#bookmark_off").show();
            }).fail(function () {
                alert("Ошибка при добавлении в закладки!");
            });
        }

        function remove_bookmark() {
            $.ajax({
                method: "POST",
                url: "/company/{{ $member->id }}/bookmark-remove",
                data: {"_token": '{{csrf_token()}}'}
            }).done(function (msg) {
                $("#bookmark_off").hide();
                $("#bookmark").show();
            }).fail(function () {
                alert("Ошибка!");
            });
        }
    </script>
    <script src="{{ asset('js/bootstrap-rating-input.min.js') }}"></script>
    <script>
      $(".rating").rating({
          iconLib: "fa",
          activeIcon: "fa-heart",
          inactiveIcon: "fa-heart-o",
          clearableIcon: "fa-trash-o",
          min: 1,
          max: 5
      }).change(function () {
          let value = $(this).val();
          let id = $(this).attr('id').substring(5);
          $.ajax({
              url: '/specialist/rate',
              method: 'POST',
              data: {specialist_id: id, value: value},
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              success: function(data) {
                  console.log(data);
              }
          });
      });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
    <script>
        $(".fancy").fancybox({
            showNavArrows : true
//            autoSize: false
        });
    </script>
@stop

@section('css')
    <link href="/css/tab.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
@endsection
