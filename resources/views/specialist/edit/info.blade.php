<div class="row" style="background-color: #F5F5F5;padding-top:40px;">
    <div class="col-sm">
        <div class="form-group">
            <input value="{{ optional($member)->name }}" type="text" id="name" name="name" placeholder="{{ trans('app.title', [], $language->locale) }}" class="form-control add-input" required>
        </div>
        <div class="col-sm nopad norightborder">
            <div class="form-group">
                @include('partials.geo.country', ['countryId' => $member->country_id])
            </div>
        </div>
        <div class="col-sm nopad">
            <div class="form-group">
                @include('partials.geo.city', ['selectedCityId' => $member->city_id, 'countryId' => $member->country_id])
            </div>
        </div>
        <div class="form-group">
            <textarea name="description" cols="6" id="description" placeholder="{{ trans('app.description', [], $language->locale) }}" class="form-control add-textarea" style="margin-top: 10px;" required>{{ optional($member->info)->about }}</textarea>
        </div>
    </div>
    <div class="col-sm">
        <label class="add-label">
            {{ trans('app.photo', [], $language->locale) }}
        </label>
        <div id="hydra_pictures_urls">
            @foreach(optional($member)->pictures as $picture)
                <input type="hidden" name="pictures_urls[]" value="{{ $picture->url }}">
            @endforeach
        </div>
        <div>
            <div id="hydra_avatar_images">
                @foreach(optional($member)->pictures as $picture)
                    <div style="display: inline-block;" class="hydra_uploaded_thumbnails">
                        <canvas width="96" height="110" style="background: url('{{ $picture->url }}');background-size: cover; display: block"></canvas>
                        <a href="#" class="delete_file" onclick="return deleteFileUploaded(this);" data-index="0">{{ trans('app.remove') }}</a>
                    </div>
                @endforeach
            </div>
            <div id="hydra_plus" class="plus-div fileinput-button">
                <input type="hidden" id="pictures_urls" />
                <input id="pictures_fileupload" type="file" name="files[]" multiple>
                <img src="/image/plus.png">
            </div>
        </div>
    </div>
</div>