<div class="add-company margin-top20">
    <div class="row">
        <div class="col-sm nopad" style="background-color: #F5F5F5;">
            <div class="grey-admin">
                <span class="grey-span">{{ trans('app.contacts', [], $language->locale) }}</span>
                <div class="inputwithimg">
                    <img src="/image/con1.png">
                    <input type="tel" name="phone" value="{{ $member->phone }}" placeholder="{{ trans('app.phone', [], $language->locale) }}" class="add-input">
                </div>
                <div class="inputwithimg">
                    <img src="/image/con2.png">
                    <input type="text" name="site" value="{{ $member->site }}" placeholder="{{ trans('app.website_address', [], $language->locale) }}" class="add-input">
                </div>
                <div class="inputwithimg">
                    <img src="/image/con3.png">
                    <input type="email" name="email" value="{{ $member->email }}" placeholder="{{ trans('app.email', [], $language->locale) }}" class="add-input">
                </div>
                <label class="add-label">{{ trans('app.social_networks', [], $language->locale) }}</label>
                <div class="inputwithimg">
                    <img src="/image/soc1.png">
                    <input type="text" name="vk" placeholder="VK" class="add-input" value="{{ $member->vk }}">
                </div>
                <div class="inputwithimg">
                    <img src="/image/soc2.png">
                    <input type="text" name="facebook" placeholder="Facebook" class="add-input" value="{{ $member->facebook }}">
                </div>
                <div class="inputwithimg">
                    <img src="/image/soc3.png">
                    <input type="text" name="twitter" placeholder="Twitter" class="add-input" value="{{ $member->twitter }}">
                </div>
                <div class="inputwithimg">
                    <img src="/image/soc4.png">
                    <input type="text" name="telegram" placeholder="Telegram" class="add-input" value="{{ $member->telegram }}">
                </div>
                <div class="inputwithimg">
                    <img src="/image/soc5.png">
                    <input type="tel" name="whatsapp" placeholder="Whatsapp" class="add-input" value="{{ $member->whatsapp }}">
                </div>
            </div>
        </div>
        <div class="col-sm nopad" style="background-color: #F5F5F5;">
            <div class="grey-admin">
                <span class="grey-span">{{ trans('app.work_time', [], $language->locale) }}</span>
                @php
                    $schedule = $member->schedule ?? (new \App\Schedule());
                @endphp
                @foreach(\App\Service::getWeeks() as $week)
                    <div class="radio-cover2 hydra-cover" style="width: auto; float: none; height: 50px;">
                        <div id="hydra-start-end-{{ $week }}" class="hydra-start-end" style="{{ $schedule->$week || (!$schedule->exists && !\App\Service::isWeekend($week)) ? '': 'display:none' }}">
                            <input type="time" name="{{ $week }}-start" value="{{ $schedule->{$week.'_start'} ?? '10:00' }}" class="add-input hydra_days_start"> -
                            <input type="time" name="{{ $week }}-end" value="{{ $schedule->{$week.'_end'} ?? '20:00' }}" class="add-input hydra_days_end">
                        </div>
                        <label class="checkbox-label">
                            <input type="checkbox" name="{{ $week }}" class="radio" value="1" {{ $schedule->$week || (!$schedule->exists && !\App\Service::isWeekend($week)) ? 'checked' :'' }}
                            onchange="hydra_days_change('{{ $week }}')">
                            <span class="checkbox-icon"></span>
                            <span class="checkbox-text">{{ trans('app.'.$week, [], $language->locale) }}</span>
                        </label>
                    </div>
                    <div class="clear"></div>
                @endforeach
                <div class="hydra-cover" style="margin-bottom: 30px;">
                    <div id="hydra-start-end-lunch" class="hydra-start-end" style="margin-top: 31px; {{ $schedule->lunch ? '': 'display:none' }}">
                        <input type="time" name="lunch-start" value="{{ $schedule->exists ? $schedule->lunch_start : '13:00' }}" class="add-input hydra_days_start"> -
                        <input type="time" name="lunch-end" value="{{ $schedule->exists ? $schedule->lunch_end : '14:00' }}" class="add-input hydra_days_end">
                    </div>
                    <label class="add-label">{{ trans('app.lunch_break', [], $language->locale) }}</label>
                    <div class="radio-cover2 radio-right" style="float: none; width: auto; margin-top: 20px;">
                        <label class="checkbox-label">
                            <input type="checkbox" name="lunch" {{ $schedule->lunch ? '': 'checked' }} class="radio" value="1" onchange="hydra_lunch_change()">
                            <span class="checkbox-icon"></span>
                            <span class="checkbox-text">{{ trans('app.without_lunch', [], $language->locale) }}</span>
                        </label>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>


<script>
    function submitSpecialist() {
        $('form.form').each(function () {
            let form = this;
            $.ajax({
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('.form-group.has-error').removeClass('has-error');
                },
                success: function (data) {
                    window.location.replace("/specialist/profile");
                },
                error: function (data) {
                    let fields = data.responseJSON.errors;
                    var count = 0;
                    $.each(fields, function(index, value) {
                        let selector = $('#'+index);
                        selector.parent().addClass('has-error');
                        if (count === 0) {
                            $('html, body').animate({
                                scrollTop: selector.offset().top
                            }, 300, function () {
                            });
                        }
                        ++count;
                    });
                }
            })
        });
        return false;
    }
</script>
