@extends('partials.app')

@section('content')
    <div class="aksii message zayavki">
        <div class="container">
            <span class="add-span">{{ trans('app.manage_orders') }}</span>
            <div class="calendar-cover">
                <span class="kok-span">{{ trans('app.calendar') }}
                    <a href="#kok-span" onclick="return calendarToggle(this);">
                        <span class="select2-container select2-container--open">
                        <span class="select2-selection--single">
                            <span class="select2-selection__arrow" role="presentation" style="top: -4px; left: -20px;">
                                <b role="presentation"></b>
                            </span>
                        </span>
                        </span>
                    </a>
                </span>
                {!! $calendar->calendar() !!}
                <div class="clear"></div>

            </div>
        </div>

        @section('css')
            <link rel="stylesheet" href="/datepicker/css/bootstrap-datepicker3.min.css">
            <link rel="stylesheet" href="/css/fullcalendar.min.css"/>
        @endsection

        @section('script')
            {!! $calendar->script() !!}
            <script src="/js/moment-with-locales.min.js"></script>
            <script src="/js/fullcalendar.min.js"></script>
            <script src="/js/locale-all.js"></script>
        @endsection
    </div>
@endsection
