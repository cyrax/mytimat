@extends('partials.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="key">Key:</label>
                        <input class="form-control" id="key" type="text" name="key" value="{{ request()->get('key') }}" placeholder="key (example: animals)">
                    </div>
                    <div class="form-group">
                        <label for="text">Text:</label>
                        <input class="form-control" id="text" type="text" name="text" value="{{ request()->get('text') }}" placeholder="text (example: Красота)">
                    </div>
                    <button type="submit">{{ trans('app.send') }}</button>
                </form>
                <hr />
            @foreach($translations as $translation)
                <div class="row">
                    <div class="col-md">
                        <a href="#{{ $translation->item }}" class="collapse_icons" data-toggle="collapse"><i class="fa fa-arrow-down"></i> {{ $translation->item }}</a>
                    </div>
                </div>
                <form role="form" method="POST" class="form-inline collapse" id="{{ $translation->item }}" action="{{ route('translator_update') }}">
                    {{ method_field('PUT') }}
                    {{ csrf_field() }}
                    <input type="hidden" name="item" value="{{ $translation->item }}" />
                    <div class="form-group">
                        <label>Ru</label>
                        <textarea class="form-control" name="text[ru]">{{ $translation->ru_text }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>En</label>
                        <textarea class="form-control" name="text[en]">{{ $translation->en_text }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>It</label>
                        <textarea class="form-control" name="text[it]">{{ $translation->it_text }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Hr</label>
                        <textarea class="form-control" name="text[hr]">{{ $translation->hr_text }}</textarea>
                    </div>
                    <button type="submit">{{ trans('app.save') }}</button>
                </form>
                <hr>
            @endforeach
            </div>
        </div>
        {!! $translations->links() !!}
    </div>

@endsection

@section('script')
    <script>
        var clicks = 0;
        $('.collapse_icons').click(function (e) {
            e.preventDefault();
            if (clicks%2 === 0)
                $(this).find('i').removeClass('fa-arrow-down').addClass('fa-arrow-up');
            else {
                $(this).find('i').addClass('fa-arrow-down').removeClass('fa-arrow-up');
            }
            clicks++;
        });
    </script>
@endsection