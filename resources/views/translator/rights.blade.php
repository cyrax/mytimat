@extends('partials.app')

@section('content')
    <div class="container" id="app">
        <div class="card">
            <div class="card-body">
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="phone">Phone:</label>
                            <input class="form-control" id="phone" type="text" name="phone" value="{{ request()->get('phone') }}" placeholder="phone">
                        </div>
                        <div class="checkbox">
                            <label for="is_user">
                            <input id="is_user" type="checkbox" name="is_user" value="1" {{ request()->has('is_user') ? 'checked' : '' }}>is user:</label>
                        </div>
                        <div class="checkbox">
                            <label for="is_admin">
                            <input id="is_admin" type="checkbox" name="is_admin" value="1" {{ request()->has('is_admin') ? 'checked' : '' }}>is admin:</label>
                        </div>
                        <div class="checkbox">
                            <label for="is_specialist">
                            <input id="is_specialist" type="checkbox" name="is_specialist" value="1" {{ request()->has('is_specialist') ? 'checked' : '' }}>is specialist:</label>
                        </div>
                        <div class="checkbox">
                            <label for="is_techsupport">
                            <input id="is_techsupport" type="checkbox" name="is_techsupport" value="1" {{ request()->has('is_techsupport') ? 'checked' : '' }}>is techsupport:</label>
                        </div>
                        <div class="form-group">
                            <label for="is_translator">
                            <input id="is_translator" type="checkbox" name="is_translator" value="1" {{ request()->has('is_translator') ? 'checked' : '' }}>is translator:</label>
                        </div>
                        <div class="form-group">
                            <label for="is_moderator">
                            <input id="is_moderator" type="checkbox" name="is_moderator" value="1" {{ request()->has('is_moderator') ? 'checked' : '' }}>is moderator:</label>
                        </div>
                        <button type="submit">{{ trans('app.send') }}</button>
                    </form>
                    <hr />
                <div class="row aksii-table-head" style="margin-bottom: 15px;">
                    <div class="col-md-1">
                        <div class="a-t-text">ID</div>
                    </div>
                    <div class="col-md-2">
                        <div class="a-t-text">{{ trans('app.user') }}</div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md">
                                <div class="a-t-text" style="width: auto;">user</div>
                            </div>
                            <div class="col-md">
                                <div class="a-t-text" style="width: auto;">specialist</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md">
                                <div class="a-t-text">administrator</div>
                            </div>
                            <div class="col-md">
                                <div class="a-t-text">techsupport</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md">
                                <div class="a-t-text">translator</div>
                            </div>
                            <div class="col-md">
                                <div class="a-t-text">moderator</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- <div class="row podarki-cover"> -->

                    @foreach($users as $user)
                        <div class="row podarki-cover">
                            <div class="col-md-1">{{ $user->id }}</div>
                            <div class="col-md-2"><b>{{ $user->phone }}</b> {{ $user->name }}</div>
                            <div class="col-md-3 text-center">
                                <div class="row">
                                    <div class="col-md">
                                        <input type="checkbox" class="form-control" name="user" {{ $user->user ? 'checked' : '' }} value="{{ $user->id }}">
                                    </div>
                                    <div class="col-md">
                                        <input type="checkbox" class="form-control" name="specialist" {{ $user->specialist ? 'checked' : '' }} value="{{ $user->id }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md">
                                        <input type="checkbox" class="form-control" name="administrator" {{ $user->administrator ? 'checked' : '' }} value="{{ $user->id }}">
                                    </div>
                                    <div class="col-md">
                                        <input type="checkbox" class="form-control" name="techsupport" {{ $user->techsupport ? 'checked' : '' }} value="{{ $user->id }}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="row">
                                    <div class="col-md">
                                        <input type="checkbox" class="form-control" name="translator" {{ $user->translator ? 'checked' : '' }} value="{{ $user->id }}">
                                    </div>
                                    <div class="col-md">
                                        <input type="checkbox" class="form-control" name="moderator" {{ $user->moderator ? 'checked' : '' }} value="{{ $user->id }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                <!-- </div> -->
            </div>
        </div>
        {!! $users->links() !!}
    </div>


    <script>
        $('input[type=checkbox]').change(function () {
            let name = this.name;
            var values = {user_id: this.value};
            values[name] = +this.checked;
            $.ajax({
                url: '{{ route('translator_rights_update') }}',
                method: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: values,
                success: function (data) {
                    console.log(data);
                }
            });
        });
    </script>
@endsection
