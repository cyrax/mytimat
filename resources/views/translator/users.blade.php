@extends('partials.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <form class="form-inline">
                    <div class="form-group">
                        <label for="phone">{{ trans('app.phone') }}:</label>
                        <input class="form-control" id="phone" type="text" name="phone" value="{{ request()->get('phone') }}" placeholder="Phone">
                    </div>
                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input class="form-control" id="name" type="text" name="name" value="{{ request()->get('name') }}" placeholder="Name">
                    </div>
                    <button type="submit">{{ trans('app.send') }}</button>
                </form>
                <hr />
                @foreach($users as $user)
                    <div class="row">
                        <div class="col-md">
                            <a href="#{{ $user->phone }}" class="collapse_icons" data-toggle="collapse"><i class="fa fa-arrow-down"></i> {{ $user->phone }}</a>
                        </div>
                    </div>
                    <form role="form" method="POST" class="form-inline collapse" id="{{ $user->phone }}" action="{{ route('update-profile') }}">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="name" class="col-md">{{ trans('app.name') }}</label>
                                    <div class="col-md">
                                        <input class="form-control" name="name" id="name" value="{{ $user->name }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="second_name" class="col-md">{{ trans('app.lastname') }}</label>
                                    <input class="form-control" name="second_name" id="second_name" value="{{ $user->second_name }}">
                                </div>
                                <div class="form-group">
                                    <label for="email" class="col-md">{{ trans('app.email') }}</label>
                                    <div class="col-md">
                                        <input class="form-control" name="email" id="email" value="{{ $user->email }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="phone" class="col-md">{{ trans('app.phone') }}</label>
                                    <input class="form-control" name="phone" id="phone" value="{{ $user->phone }}">
                                    <div class="col-md">

                                    </div>
                                </div>
                            </div>
                            <div class="col-md">
                                <div class="form-group">
                                    <label for="language" class="col-md">{{ trans('app.language') }}</label>
                                    <div class="col-md">
                                        <select name="language_id" id="language_id" style="width: 100%;" class="form-control" required>
                                            <option value="">{{ trans('app.choose_language') }}</option>
                                            @foreach($languages as $language)
                                                <option value="{{ $language->id }}" {{ $language->id == $user->language_id ? 'selected' : '' }}>{{ $language->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="country" class="col-md">{{ trans('app.country') }}</label>
                                    <div class="col-md">
                                        @include('partials.geo.country', ['countryId' => $user->country_id])
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="city" class="col-md">{{ trans('app.city') }}</label>
                                    <div class="col-md">
                                        @include('partials.geo.city', ['selectedCityId' => $user->city_id, 'countryId' => $user->country_id])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="add-button" id="accept-1" onclick="accept_order(1)">
                            {{ trans('app.save') }}
                        </button>
                    </form>
                    <hr>
                @endforeach
            </div>
        </div>
        {!! $users->links() !!}
    </div>
    <style>
        .form-group {
            width: 100%;
            margin-bottom: 10px!important;
        }
    </style>
@endsection

@section('script')
    <script>
        // $('#language_id').select2();
    </script>
@endsection
