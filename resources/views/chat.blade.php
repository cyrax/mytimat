@extends('partials.app')

@section('content')

    <div class="container" id="app">
                <div class="card card">
                    <div class="add-span">{{ trans('app.tech_support') }}</div>

                    <div class="card-body">
                        <chat-messages :messages="messages"></chat-messages>
                    </div>
                    <div class="card-footer">
                        <chat-form
                                v-on:messagesent="addMessage"
                                :user="{{ Auth::user() }}"
                        ></chat-form>
                    </div>
                </div>

    </div>
@endsection
@section('script')
    <script src="{{ asset('js/app.js') }}"></script>
@endsection