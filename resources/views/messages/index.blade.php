@extends('partials.app')

@section('content')
    <div class="aksii podarki message">
        <div class="container">
            <span class="add-span">{{ trans('app.messages') }} {{ $paginator->count() }}</span>
            @if(session('deleted'))
                <div class="alert alert-info"><h5>{{ trans('app.sent_to_basket') }}</h5></div>
            @endif
            <div class="aksii-filter">
                <form action="/user/messages" method="get">
                    <div class="row">
                        <div class="col-md col">
                            <select name="type" id="type" style="width: 100%;">
                                <option value="">{{ trans('app.all_messages') }}</option>
                                <option value="user" {{ $request['type'] == 'user' ? 'selected' : '' }}>{{ trans('app.from_users') }}</option>
                                <option value="company" {{ $request['type'] == 'company' ? 'selected' : '' }}>{{ trans('app.reserve_services') }}</option>
                                <option value="system" {{ $request['type'] == 'system' ? 'selected' : '' }}>{{ trans('app.system_notifications') }}</option>
                            </select>
                        </div>
                        <div class="col-md col">
                            <select name="status" id="status" style="width: 100%;">
                                <option value="">{{ trans('app.all_statuses') }}</option>
                                <option value="non_read" {{ $request['status'] == 'non_read' ? 'selected' : '' }}>{{ trans('app.not_read') }}</option>
                                <option value="read" {{ $request['status'] == 'read' ? 'selected' : '' }}>{{ trans('app.read') }}</option>
                            </select>
                        </div>
                        <div class="col-md col" style="padding-right: 0">
                            <div class="input-daterange input-group" id="datepicker1" class="datepicker">
                                <input type="text" class="input-sm form-control" name="start" value="{{ $request['start'] }}" placeholder="{{ trans('app.from') }}">
                            </div>
                        </div>
                        <div class="col-md col" style="padding-left: 0">
                            <div class="input-daterange input-group" id="datepicker2" class="datepicker">
                                <input type="text" class="input-sm form-control" name="end" value="{{ $request['end'] }}" placeholder="{{ trans('app.to') }}" />
                            </div>
                        </div>

                        <div class="col-md col">
                            <input name="text" class="add-input" value="{{$request['text']}}" placeholder="{{ trans('app.search') }}">
                        </div>
                        <div class="col-md col" style="text-align: center">
                            <button class="aksii-search" type="submit">
                                <i class="fa fa-search" aria-hidden="true"></i>{{ trans('app.find') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            @if($paginator->count())
                <form action="/user/messages" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="DELETE">
                    <div class="row aksii-table-head">
                        <div class="col-md col">
                            <div class="a-t-text">{{ trans('app.date') }}</div>
                        </div>
                        <div class="col-md col">
                            <div class="a-t-text">{{ trans('app.theme') }}</div>
                        </div>
                        <div class="col-md col">
                            <div class="a-t-text">{{ trans('app.companion') }}</div>
                        </div>
                        <div class="col-md d-none d-sm-block">
                            <div class="a-t-text">{{ trans('app.messages') }}</div>
                        </div>
                    </div>
                    @foreach($messages as $message)
                        <div class="row message-cover">
                            <div class="col-md-2 col nopad">
                                <label class="checkbox-label">
                                    <input type="checkbox" name="subjects[{{ $message->subject_id }}]" class="radio delete-subjects" value="1">
                                    <span class="checkbox-icon"></span>
                                </label>
                                <span class="messpan0">
                                    <i class="fa fa-envelope{{ $message->read_at ? '-open' : '' }} hidden-sm d-none d-sm-block" aria-hidden="true"></i>
                                    {{ \Carbon\Carbon::parse($message->created_at)->format('d.m.Y H:i') }}
                                </span>
                            </div>
                            <div class="col-md-3 col">
                                <a href="/user/messages/subject/{{ $message->id }}/{{ $message->subject_id }}">
                                  <span class="messpan{{ $message->read_at ? '' : 'b' }}" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{ $message->subject }}</span>
                                </a>
                            </div>
                            <div class="col-md-2 col">
                                <span class="messpan" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">
                                    @if($message->type == 'user')
                                        <a href="/profile/{{ $message->companion->id }}">
                                            {{ $message->companion->getFullName() }}
                                        </a>
                                    @elseif($message->type == 'system') {{ trans('app.website_administrator') }}
                                    @else
                                        <a href="/profile/{{ $message->companion->id }}">
                                            @php
                                                $specialist = $message->companion()->first()
                                            @endphp
                                            {{ $message->companion->getFullName() }}</a> |
                                        <a href="/company/{{ $specialist->company->id }}">
                                            {{ $specialist->company->name }}</a>
                                    @endif
                                </span>
                            </div>
                            <div class="col-md-5">
                                <a href="/user/messages/subject/{{ $message->id }}/{{ $message->subject_id }}"><span class="messpan4" style="text-overflow: ellipsis; white-space: nowrap; overflow: hidden;">{{ $message->text }}</span></a>
                            </div>
                            <div class="col-md col">
                                <a href="/user/messages/subject/{{ $message->id }}/{{ $message->subject_id }}"><button class="read-but" type="button">{{ trans('app.read') }}</button></a>
                            </div>
                        </div>
                    @endforeach
                    <div style="float: right;">
                        {{ $paginator->appends([
                            'start' => $request['start'],
                            'end' => $request['end'],
                            'status' => $request['status'],
                            'type' => $request['type'],
                            'text' => $request['text']
                            ])
                            ->links() }}
                    </div>
                    <button class="del-check" type="submit">{{ trans('app.remove_selected') }}</button>
                </form>
            @else
                <h2 style="color: #111111; text-align: center; margin: 50px 0 250px;">{{ trans('app.not_found') }}</h2> @endif
        </div>
    </div>
@endsection

@section('css')
    <link rel="stylesheet" href="/datepicker/css/bootstrap-datepicker3.min.css">
@endsection

@section('script')
    <script src="/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script>
        $(function () {
            $('#type').select2();
            $('#status').select2();
            $('.datepicker').datepicker({
                format: "d-M-yyyy",
                endDate: "today",
                maxViewMode: 1,
                todayBtn: "linked",
                todayHighlight: true
            });
        });
    </script>
@endsection
