@extends('partials.app')

@section('content')
    <div class="aksii message sup-ser-div">
        <div class="container">
            <span class="add-span">{{ trans('app.messages') }}</span>
            <div class="clear"></div>
            <div class="card">
                <div class="card-body sub-card-body aksii-sub-card-body grey-admin3">
                    <p>@if($companion == 'system') {{ trans('app.system_message') }} @else {{ trans('app.companion') }}: {{ $companion->getFullName() }}@endif</p>
                    @if($conversation['type'] == 'company')<p> {{ trans('app.company') }}: {{ $companion->company()->first()['name'] }}</p>@endif
                    <div class="clear"></div>
                    <form action="/user/messages/subject/{{ $id }}/send" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input type="hidden" name="subject" value="{{ $subject }}">
                        <input type="hidden" name="companion" value="{{ $companion == 'system' ? 0 : $companion->id }}">
                        <div class="col-sm col-sm-offset-3">
                            <textarea class="add-textarea" name="text" placeholder="{{ trans('app.enter_your_message') }}" required></textarea>
                        </div>
                        <div class="clear"></div>
                        <div class="otr-soobw">
                            <button class="greenbutton">{{ trans('app.response') }}</button>
                            <input type="file" name="pictures[]" multiple>
                        </div>
                    </form>
                    @foreach($messages as $message)
                        <div class="chat-{{ $message->from == 'owner' ? 'to' : 'from' }}">
                            <img src="{{ $message->from == 'owner' ? $avatars['owner'] : $avatars['companion']}}" class="ava-chat">
                            <p>{{ $message->text }}</p>
                            @php
                                $attachments = $message->attachments($message->from)->get()->toArray();
                            @endphp
                            @if(!empty($attachments))
                            <p>
                                @foreach($attachments as $attachment)<img class="att-img" src="/images/attachment/thumbnail/{{ $attachment['url'] }}.jpg">@endforeach
                            </p>
                            @endif
                            <div class="clear"></div>
                            <span>{{ $message->created_at->format('M j, Y - H:i:s') }}</span>
                        </div>
                        <div class="clear"></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')

@endsection

@section('script')

@endsection
