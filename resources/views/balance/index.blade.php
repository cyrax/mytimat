@extends('partials.app')

@section('content')
    <div class="aksii">
        <div class="container">
            <span class="add-span">Баланс</span>
            <div class="balance-div">
                <span class="vash-balance">Ваш баланс: <span>{{ Auth::user()->tims }}</span> Tim</span>
                <div class="balance-popolnit-div">
                    <div class="col nopad"><span class="balance-span">Пополнить баланс на:</span></div>
                    <div class="col nopad"><input id="sum_tims" type="number" name="sum" class="add-input" value="0" onchange="change_the_totaltims()"></div>
                    <div class="col"><span class="balance-span">Тимов</span></div>
                    <div class="clear"></div>
                    <div class="col nopad"><span class="balance-span">Баланс после пополнения:</span></div>
                    <div class="col nopad"><input id="total_tims" type="number" class="add-input" disabled value="{{ Auth::user()->tims }}"></div>
                    <div class="col"><span class="balance-span">Тимов</span></div>
                    <div class="clear"></div>
                </div>
                <div class="balance-popolnit-div2">
                    <div class="col nopad">
            <span class="balance-span">
              Сумма к оплате: <b>4500</b> руб.
            </span>
                    </div>
                    <div class="col">
                        <button class="aksii-search">Оплатить</button>
                    </div>
                    <div class="clear"></div>
                </div>
                <span class="span30px">Способы оплаты</span>
                <div id="tabs" class="tabs">
                    <nav class="white-nav">
                        <ul>
                            <li><a href="#section-1"><span>Банковские карты</span></a></li>
                            <li><a href="#section-2"><span>Электронные деньги</span></a></li>
                            <li><a href="#section-3"><span>Платежные системы</span></a></li>
                        </ul>
                    </nav>
                    <div class="content textleft">
                        <section id="section-1">
                            <div class="col"><label class="add-label">Имя</label></div>
                            <div class="col"><input type="" name="" class="add-input"></div>
                            <div class="clear"></div>
                            <div class="col"><label class="add-label">ФИО</label></div>
                            <div class="col"><input type="" name="" class="add-input"></div>

                            <div class="clear"></div>
                            <button class="oplatit-balance">Оплатить</button>
                        </section>
                        <section id="section-1">
                            <div class="col"><label class="add-label">Имя</label></div>
                            <div class="col"><input type="" name="" class="add-input"></div>
                            <div class="clear"></div>
                            <button class="oplatit-balance">Оплатить</button>
                        </section>
                        <section id="section-1">
                            <div class="col"><label class="add-label">Имя</label></div>
                            <div class="col"><input type="" name="" class="add-input"></div>
                            <div class="clear"></div>
                            <div class="col"><label class="add-label">ФИО</label></div>
                            <div class="col"><input type="" name="" class="add-input"></div>
                            <div class="clear"></div>
                            <div class="col"><label class="add-label">Номер карты</label></div>
                            <div class="col"><input type="" name="" class="add-input"></div>
                            <div class="clear"></div>
                            <button class="oplatit-balance">Оплатить</button>
                        </section>
                    </div>
                </div>
                <script>
                    jQuery(document).ready(function($){
                        new CBPFWTabs( document.getElementById( 'tabs' ) );
                    });
                </script>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <link href="/css/tab.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('file_upload/css/jquery.fileupload.css') }}">
@endsection

@section('script')
    <script src="/js/cbpFWTabs.js"></script>
    <script>
        const current_tims = {{ Auth::user()->tims }};
        function change_the_totaltims() {
            $("#total_tims").val(parseInt($("#sum_tims").val()) + current_tims);
        }
    </script>
@endsection