@php
$content = 'content_'.$currentLanguage->locale;
@endphp

@extends('partials.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                {!! $page->$content !!}
            </div>
        </div>
    </div>
@endsection