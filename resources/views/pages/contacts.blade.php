@extends('partials.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <p>{{ trans('app.customer_support') }}: <a href="mailto:help@bukutime.com">help@bukutime.com</a></p>
                <p>{{ trans('app.cooperation') }}: <a href="mailto:cooperation@bukutime.com">cooperation@bukutime.com</a></p>
                <p>{{ trans('app.general_inquiries') }}: <a href="mailto:general@bukutime.com">general@bukutime.com</a></p>
            </div>
        </div>
    </div>
@endsection