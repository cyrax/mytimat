@extends('partials.app')

@section('content')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <p>{{ trans('app.about_site_page') }}</p>
            </div>
        </div>
    </div>
@endsection