<div class="dm-overlay login_register" id="login_register">
    <div class="dm-table">
        <div class="dm-cell">
            <div class="dm-modal">
                <div id="hydra-login-tab" class="hydra_tab hydra_tab_active"
                     onclick="change_the_login_block('login', ['register', 'restore'])">{{ trans('app.login') }}</div>
                <div id="hydra-register-tab" class="hydra_tab"
                     onclick="change_the_login_block('register', ['login', 'restore'])">{{ trans('app.registration') }}</div>
                <div id="hydra-restore-tab" class="hydra_tab"
                     onclick="change_the_login_block('restore', ['login', 'register'])">{{ trans('app.restore_password') }}</div>
                <div class="hydra_login_block">
                    <div class="hydra_login">
                        @if (isset($errors) && $errors->has('login_phone_error'))
                            <div>
                                <span class="help-block">
                                    <strong>{{ $errors->first('login_phone_error') }}</strong>
                                </span>
                            </div>
                        @endif
                        <form class="form-horizontal" id="login_form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}
                            <div style="display: none;"><input type="checkbox" name="remember" checked></div>
                            <p>

                            <input type="text" placeholder="{{ trans('app.enter_phone') }}"
                                       class="hydra_login_input hydra_login_input_left {{ isset($errors) && $errors->has('login_phone_error') ? ' hydra_login_error' : '' }}"
                                       name="phone" value="{{ old('phone') }}" required>

                                <input type="password" placeholder="{{ trans('app.password') }}"
                                       class="hydra_login_input hydra_login_input_right {{ isset($errors) && $errors->has('password') ? ' hydra_login_error' : '' }}"
                                       name="password" required>
                            </p>
                            <p id="login_error" style="color:#F00;display: none;"></p>
                            <p>
                                <button type="submit" class="aksii-search login_submit" onclick="return login();">{{ trans('app.login') }}</button>
                            </p>
                            <p>
                                <a href="#close" style="float: right;">{{ trans('app.cancel') }}</a>
                                <a href="#" onclick="change_the_login_block('restore', ['login', 'register']); return false;">{{ trans('app.restore_password') }}
                                    <i class="fa fa-caret-down"></i></a>
                                <a href="#" onclick="change_the_login_block('kit', ['restore', 'register'])"></a>
                            </p>
                        </form>
                    </div>
                    {{--<div class="hydra_kit" style="display: none;">--}}
                        {{--<input value="+7" placeholder="phone number" id="phone_number"/>--}}
                        {{--<button onclick="smsLogin();">{{ trans('app.login') }}</button>--}}
                    {{--</div>--}}
                    <div class="hydra_register" style="display: none;">
                        <p class="hydra_register_1_p">{{ trans('app.enter_phone') }}</p>
                        <p><input type="text" placeholder="{{ trans('app.enter_phone') }}"
                                  class="hydra_login_input hydra_login_input_left" id="register_phone" value="+"
                                  required> <span class="hydra_wewillsend">— {{ trans('app.we_will_send_sms') }}</span></p>
                        <p style="margin-top: 20px;">
                            <button type="button" class="aksii-search register_submit"
                                    onclick="hydra_send_sms()">{{ trans('app.next') }} <i class="fa fa-arrow-right"></i>
                            </button>
                            <label><input type="checkbox" id="register_checkbox"> {{ trans('app.i_accept') }}
                                <a href="{{ route('pages-view', ['url' => 'user-agreement']) }}" target="_blank">{{ trans('app.user_agreement') }}</a>
                                <br>&#8195;{{ trans('app.and') }}
                                <a href="{{ route('pages-view', ['url' => 'privacy-policy']) }}" target="_blank">{{ trans('app.privacy_policy') }}</a></label></p>
                        <p style="margin-top: 30px; text-align: right; margin-bottom: 0;">
                            <a href="#close">{{ trans('app.cancel') }}</a>
                        </p>
                    </div>
                    <div class="hydra_register_error" style="display: none;">
                        <p style="font-size: 24px; color: red;">{{ trans('error.sending_sms') }}</p>
                        <p>
                            <button type="button" class="aksii-search login_submit"
                                    onclick="hydra_again()">{{ trans('app.try_again') }} <i
                                        class="fa fa-arrow-circle-left"></i></button>
                        </p>
                    </div>
                    <div class="hydra_restore_error" style="display: none;">
                        <p style="font-size: 24px; color: red;">{{ trans('error.sending_sms') }}</p>
                        <p>
                            <button type="button" class="aksii-search login_submit"
                                    onclick="hydra_restore_again()">{{ trans('app.try_again') }}
                                <i class="fa fa-arrow-circle-left"></i>
                            </button>
                        </p>
                    </div>
                    <div class="hydra_register_second" style="display: none; width: 720px;">
                        <p class="hydra_register_1_p">{{ trans('app.enter_sms_code') }}</p>
                        <p><input type="password" class="hydra_login_input" id="sms-confirmation" required></p>
                        <p style="margin-top: 20px; clear: both;">
                            <button type="button" class="aksii-search register_submit"
                                    onclick="hydra_confirm_sms()">{{ trans('app.next') }}
                                <i class="fa fa-arrow-right"></i>
                            </button>
                            <a style="cursor: pointer;" onclick="hydra_send_sms_again()">{{ trans('app.enter_code_again') }}</a>
                        </p>
                        <p style="margin-top: 45px; clear: both; text-align: right; margin-bottom: 0;">
                            <a href="#close">{{ trans('app.cancel') }}</a>
                        </p>
                    </div>
                    <div class="hydra_restore" style="display: none;">
                        <p class="hydra_register_1_p">{{ trans('app.enter_phone') }}</p>
                        <p>
                            <input type="text" placeholder="{{ trans('app.enter_phone') }}"
                                   class="hydra_login_input hydra_login_input_left" id="restore_phone" value="+"
                                   required>
                            <button type="button" class="aksii-search register_submit" onclick="hydra_send_restore_sms()" style="float: none;margin:0;">{{ trans('app.next') }}
                                <i class="fa fa-arrow-right"></i>
                            </button>
                        </p>
                        <p style="margin-top: 30px; text-align: right; margin-bottom: 0;">
                            <a href="#close">{{ trans('app.cancel') }}</a>
                        </p>
                    </div>
                    <div class="hydra_restore_second" style="display: none; width: 720px;">
                        <p class="hydra_register_1_p">{{ trans('app.enter_sms_code') }}</p>
                        <p><input type="password" class="hydra_login_input" id="sms-restore-confirmation" required></p>
                        <p class="hydra_register_1_p">{{ trans('app.password') }}</p>
                        <p><input type="text" class="hydra_login_input" id="restore-password" required></p>
                        <p style="margin-top: 20px; clear: both;">
                            <button type="button" class="aksii-search register_submit"
                                    onclick="hydra_confirm_restore_sms()">{{ trans('app.next') }}
                                <i class="fa fa-arrow-right"></i>
                            </button>
                            <a style="cursor: pointer;"
                               onclick="hydra_send_sms_again()">{{ trans('app.enter_code_again') }}</a>
                        </p>
                        <p style="margin-top: 45px; clear: both; text-align: right; margin-bottom: 0;">
                            <a href="#close">{{ trans('app.cancel') }}</a>
                        </p>
                    </div>
                    <div class="hydra_restore_third" style="display: none; width: 720px;">
                        <h5>{{ trans('app.success_restore') }}</h5>
                    </div>
                    <div class="hydra_register_third" style="display: none;">
                        <form class="form-horizontal" method="POST" action="/register-complete" id="hydra_register_third_form">
                            {{ csrf_field() }}
                            <input type="hidden" name="phone" id="last_phone">
                            <input type="hidden" name="password" id="last_password">
                            <p class="hydra_register_1_p">{{ trans('app.private_info') }}</p>
                            <p>
                                <input type="text" name="name" class="hydra_login_input"
                                       placeholder="{{ trans('app.name') }}" required>
                                <input type="text" name="second_name" class="hydra_login_input"
                                       placeholder="{{ trans('app.lastname') }}" required>
                            </p>
                            <p>
                                <input type="text" name="email" class="hydra_login_input" placeholder="E-mail" required>
                                {{--<input type="text" name="city" class="hydra_login_input"--}}
                                       {{--placeholder="{{ trans('app.city') }}" required>--}}
                            </p>
                            <p style="margin-top: 40px; clear: both;">
                                <button type="submit" onclick="return hydra_register_third();" class="aksii-search register_submit">{{ trans('app.register') }}
                                    <i class="fa fa-arrow-right"></i></button>
                            </p>
                            <div class="clear"></div>
                            <p style="margin-top: 30px; clear: both; text-align: right; margin-bottom: 0;">
                                <a href="#close">{{ trans('app.cancel') }}</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var hydra_tab = 'login';
    var phone = '';

    function change_the_login_block(active, passive) {
        if (hydra_tab !== active) {
            for (var i = 0; i < passive.length; i++) {
                $("#hydra-" + passive[i] + "-tab").removeClass('hydra_tab_active');
                $(".hydra_" + passive[i]).hide();
            }
            $("#hydra-" + active + "-tab").addClass('hydra_tab_active');
            $(".hydra_" + active).show();
            $(".hydra_register_error").hide();
            $(".hydra_register_second").hide();
            $(".hydra_register_third").hide();

            $(".hydra_restore_error").hide();
            $(".hydra_restore_second").hide();
            $(".hydra_restore_third").hide();

            hydra_tab = active;
        }
    }

    function hydra_send_sms() {
        if ($("#register_checkbox").prop('checked')) {
            $.ajax({
                method: "POST",
                url: "/send-sms",
                data: {phone: $("#register_phone").val()},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (msg) {
                if (msg === 'error') {
                    $(".hydra_register").hide();
                    $(".hydra_register_error").show();
                    $(".hydra_register_second").hide();
                    $(".hydra_register_third").hide();
                } else {
                    $(".hydra_register").hide();
                    $(".hydra_register_second").show();
                    $(".hydra_register_third").hide();
                    $(".hydra_register_error").hide();

                    phone = msg;

                    var seconds = 59;
                    var timerId = setInterval(function () {
                        seconds--;
                        $("#seconds_timer").text(seconds);
                        if (seconds <= 0) {
                            clearInterval(timerId);
                        }
                    }, 1000);

                }
            }).fail(function (msg) {
                alert("{{ trans('error.sending_sms') }}");
            });
        } else {
            alert("{{ trans('error.agree_with_conditions') }}");
        }
    }

    function hydra_send_restore_sms() {
        $.ajax({
            method: "POST",
            url: "/send-restore-sms",
            data: {
                phone: $("#restore_phone").val()
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (msg) {
            if (msg === 'error') {
                $(".hydra_restore").hide();
                $(".hydra_restore_error").show();
                $(".hydra_restore_second").hide();
                $(".hydra_restore_third").hide();
            } else {
                $(".hydra_restore").hide();
                $(".hydra_restore_second").show();
                $(".hydra_restore_third").hide();
                $(".hydra_restore_error").hide();

                phone = msg;
            }
        }).fail(function (msg) {
            alert("{{ trans('error.sending_sms') }}");
        });
    }
    
    function hydra_send_sms_again() {
        $.ajax({
            method: "POST",
            url: "/send-sms",
            data: {
                phone: $("#register_phone").val(),
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (msg) {
            if (msg === 'error') {
                $(".hydra_register").hide();
                $(".hydra_register_error").show();
                $(".hydra_register_second").hide();
                $(".hydra_register_third").hide();
            } else {
                $(".hydra_register").hide();
                $(".hydra_register_second").show();
                $(".hydra_register_third").hide();
                $(".hydra_register_error").hide();

                phone = msg;

                var seconds = 59;
                var timerId = setInterval(function () {
                    seconds--;
                    $("#seconds_timer").text(seconds);
                    if (seconds <= 0) {
                        clearInterval(timerId);
                    }
                }, 1000);

            }
        }).fail(function (msg) {
            alert("{{ trans('error.sending_sms') }}");
        });
    }

    function hydra_confirm_sms() {
        $.ajax({
            method: "POST",
            url: "/confirm-sms",
            data: {'sms': $("#sms-confirmation").val(), 'phone': phone},
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (msg) {
            if (msg === 'error') {
                $("#sms-confirmation").val('');
                $("#sms-confirmation").focus();
                alert('{{ trans('error.sms_code') }}');
            } else {
                $(".hydra_register").hide();
                $(".hydra_register_second").hide();
                $(".hydra_register_third").show();
                $(".hydra_register_error").hide();
                $("#last_phone").val(phone);
                $("#last_password").val($("#sms-confirmation").val());
            }
        });
    }

    function hydra_confirm_restore_sms() {
        $.ajax({
            method: "POST",
            url: "/confirm-restore-sms",
            data: {
                'sms': $("#sms-restore-confirmation").val(),
                'password': $('#restore-password').val(),
                'phone': phone
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (msg) {
            if (msg === 'error') {
                $("#sms-confirmation").val('');
                $("#sms-confirmation").focus();
                alert('{{ trans('error.sms_code') }}');
            } else {
                $(".hydra_restore").hide();
                $(".hydra_restore_second").hide();
                $(".hydra_restore_third").show();
                $(".hydra_restore_error").hide();
                $("#last_phone").val(phone);
                $("#last_password").val($("#sms-restore-confirmation").val());
            }
        });
    }

    function hydra_register_third() {
        let form = $('#hydra_register_third_form');
        $.ajax({
           url: form.attr('action'),
            data: form.serialize(),
            method: 'POST',
            success: function () {
                if ($('#make-order').length > 0) {
                    $('#make-order').submit();
                }
                else {
                    window.location = '{{ route('profile_index') }}';
                }
            }
        });
        return false;
    }

    function hydra_again() {
        $(".hydra_register").show();
        $(".hydra_register_error").hide();
    }
    function hydra_restore_again() {
        $(".hydra_restore").show();
        $(".hydra_restore_error").hide();
    }

    function register() {
        $("#hydra-login-tab").removeClass('hydra_tab_active');
        $("#hydra-register-tab").addClass('hydra_tab_active');
        $(".hydra_login").hide();
        $(".hydra_register").show();
        $(".hydra_register_error").hide();
        $(".hydra_register_second").hide();
        $(".hydra_register_third").hide();
        hydra_tab = 'register';
    }
    
    function login() {
        $('#login_error').hide();
        $.ajax({
            url: '{{ route('login') }}',
            method: 'POST',
            data: $('#login_form').serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                if ($('#make-order').length > 0) {
                    $('#make-order').submit();
                }
                else {
                    window.location.reload();
                }
            },
            error: function (data) {
                $('#login_error').show().html(data.responseJSON.errors.phone[0]);
            }
        });
        return false;
    }
</script>


<script>
    // initialize Account Kit with CSRF protection
    AccountKit_OnInteractive = function(){
        AccountKit.init(
            {
                appId:"{{ env('ACCOUNTKIT_APP_ID') }}",
                state:"{{ csrf_token() }}",
                version:"{{ env('ACCOUNTKIT_VERSION')  }}",
                fbAppEventsEnabled:true,
                redirect: "https://bukutime.com/login"
            }
        );
    };

    // login callback
    function loginCallback(response) {
        if (response.status === "PARTIALLY_AUTHENTICATED") {
            let code = response.code;
            let csrf = response.state;
            $.post({
                url: '/login',
                data: {code: code},
                success: function(data) {
                    if (data.success) {
                        location.href = '/';
                    }
                }
            })
            // Send code to server to exchange for access token
        }
        else if (response.status === "NOT_AUTHENTICATED") {
            // handle authentication failure
        }
        else if (response.status === "BAD_PARAMS") {
            // handle bad parameters
        }
    }

    // phone form submission handler
    function smsLogin() {
        let phoneNumber = document.getElementById("phone_number").value;
        AccountKit.login(
            'PHONE',
            {countryCode: phoneNumber.substr(0,1), phoneNumber: phoneNumber.substr(1)}, // will use default values if not specified
            loginCallback
        );
    }
</script>

<style>
    @media screen and (min-width: 920px) {
        .hydra_login, .hydra_restore {
            height: 237px;
        }
        .hydra_register, .hydra_restore {
            width: 722px;
        }
    }
</style>