@extends('layouts.app')

@section('content')



<div class="container">
    <div class="row">
        <div class="col-md col-md-offset-2">
            <h1>Регистрация</h1>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md"><button id="btn_1" class="btn btn-success btn-block">Я пользователь</button></div>
                <div class="col-md"><button id="btn_2" class="btn btn-deafult btn-block">Хочу зарегистрировать компанию или специалиста</button></div>
            </div>
            <div class="jumbotron"><div id="registration1">Регистрируйся и зарабатывай с гарантией оплаты проектов
                от более чем 10 000 лучших заказчиков рунета,
                увеличивай свой рейтинг и доход на постоянном потоке заказов.</div>
                <div id="registration2" style="display: none;">Зарегистрируйся и размести проект:
                    мы подберем тебе лучших исполнителей с гарантией
                    выполнения работы в срок через безопасную сделку!</div>
            </div>

    </div>
    <div class="row" style="margin-bottom: 200px;">
        <div class="col-md col-md-offset-2">
            <div id="card1" class="card card">
                <div class="card-body">
                    <br>
                    <form class="form-horizontal" method="POST" action="{{ url('register-user') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md control-label">Ваше имя</label>

                            <div class="col-md">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('second_name') ? ' has-error' : '' }}">
                            <label for="second_name" class="col-md control-label">Ваша фамилия</label>

                            <div class="col-md">
                                <input id="second_name" type="text" class="form-control" name="second_name" value="{{ old('second_name') }}" required autofocus>

                                @if ($errors->has('second_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('second_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md control-label">Ваш номер</label>

                            <div class="col-md">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
            <div id="card2" class="card card" style="display: none;">
                <br>
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md control-label">Название компании</label>

                            <div class="col-md">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="name" class="col-md control-label">Номер телефона</label>

                            <div class="col-md">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md control-label">E-Mail Address</label>

                            <div class="col-md">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md control-label">Password</label>

                            <div class="col-md">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md control-label">Confirm Password</label>

                            <div class="col-md">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){
            var btn1 = $("#btn_1");
            var btn2 = $("#btn_2");
            btn1.click(function(){
                $("#registration1").show();
                $("#registration2").hide();
                btn1.addClass("btn-success");
                btn1.removeClass("btn-default");
                btn2.addClass("btn-default");
                btn2.removeClass("btn-primary");
                $("#card1").show();
                $("#card2").hide();
            });
            btn2.click(function(){
                $("#registration1").hide();
                $("#registration2").show();
                btn2.addClass("btn-primary");
                btn2.removeClass("btn-default");
                btn1.addClass("btn-default");
                btn1.removeClass("btn-success");
                $("#card1").hide();
                $("#card2").show();
            });
        });
    </script>
@endsection
