@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row" style="margin-bottom: 200px;">
        <div class="col-md col-md-offset-2">
            <div class="card card">
                <div class="card-header">{{ trans('app.enter_sms_it_is_password') }}</div>
                <div class="card-body">
                    <br>
                    <form class="form-horizontal" method="POST" action="{{ url('register-user-complete') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md control-label">{{ trans('app.enter_code_from_sms') }}</label>

                            <div class="col-md">
                                <input type="hidden" name="phone" value="{{ $phone }}">

                                <input id="password" type="password" class="form-control" name="password" value="{{ old('password') }}" required autofocus>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ trans('app.register') }}
                                </button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            </div>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){
            var btn1 = $("#btn_1");
            var btn2 = $("#btn_2");
            btn1.click(function(){
                $("#registration1").show();
                $("#registration2").hide();
                btn1.addClass("btn-success");
                btn1.removeClass("btn-default");
                btn2.addClass("btn-default");
                btn2.removeClass("btn-primary");
                $("#card1").show();
                $("#card2").hide();
            });
            btn2.click(function(){
                $("#registration1").hide();
                $("#registration2").show();
                btn2.addClass("btn-primary");
                btn2.removeClass("btn-default");
                btn1.addClass("btn-default");
                btn1.removeClass("btn-success");
                $("#card1").hide();
                $("#card2").show();
            });
        });
    </script>
@endsection
