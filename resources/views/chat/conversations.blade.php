@extends('partials.app')

@section('content')
    <div class="container clearfix body">
        @include('chat.partials.peoplelist')
        <div class="chat" style="width: 75%">

            <div class="chat-history">
                <ul id="talkMessages">
                    @foreach($messages as $message)
                        @if($message->sender->id == auth()->user()->id)
                            <li class="clearfix" id="message-{{$message->id}}">
                                <div class="message-data align-right">
                                    <span class="message-data-time" >{{$message->humans_time}} ago</span> &nbsp; &nbsp;
                                    <span class="message-data-name" >{{$message->sender->name}}</span>
                                </div>
                                <div class="message other-message float-right">
                                    {{$message->message}}
                                </div>
                            </li>
                        @else

                            <li id="message-{{$message->id}}">
                                <div class="message-data">
                                    <span class="message-data-name">{{$message->sender->name}}</span>
                                    <span class="message-data-time">{{$message->humans_time}} ago</span>
                                </div>
                                <div class="message my-message">
                                    {{$message->message}}
                                </div>
                            </li>
                        @endif
                    @endforeach


                </ul>

            </div> <!-- end chat-history -->

            <div class="chat-message clearfix">
                <form action="" method="post" id="talkSendMessage">
                    <textarea name="message-data" id="message-data" placeholder ="Type your message" rows="3"></textarea>
                    <input type="hidden" name="_id" value="{{@request()->route('id')}}">
                    <button type="submit">Send</button>
                </form>

            </div> <!-- end chat-message -->
        </div> <!-- end chat -->
    </div>
@endsection

@section('script')
    <script>
        var __baseUrl = "{{url('/')}}"
    </script>
    <script src='{{ asset('js/handlebars.min.js') }}'></script>
    <script src='{{ asset('js/list.min.js') }}'></script>
    <script>
        $('textarea').keypress(function(e){
            var code = e.keyCode || e.which;

            if( code === 13 ) {
                e.preventDefault();
                $( "button[type=submit]" ).click();
            }
        });
    </script>
@endsection