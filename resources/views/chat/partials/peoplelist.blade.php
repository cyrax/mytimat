<div class="people-list" id="people-list" style="width: 25%;">
    <div class="search" style="text-align: center">
        <a href="#" style="font-size:16px; text-decoration:none; color: white;"><i class="fa fa-user"></i> {{auth()->user()->name}}</a>
    </div>
    <ul class="list">
        @foreach($threads as $inbox)
            @if(!is_null($inbox->thread))
                <li class="clearfix {{ request()->route('id') == optional($inbox->withUser)->id ? 'current' : '' }}">
                    <a href="{{ optional($inbox->withUser)->id }}">
                        <span style="width: 100px;float: left;">
                            @if (isset($inbox->withUser->avatar))
                                <img src="/images/user/{{optional($inbox->withUser)->avatar}}.jpg" alt="avatar" />
                            @else
                                <img src="/image/myphoto.png" alt="avatar" />
                            @endif
                        </span>
                        <div class="about">
                            <div class="name">{{optional($inbox->withUser)->name}}</div>
                            <div class="status">
                                @if(auth()->user()->id == $inbox->thread->sender->id)
                                    <span class="fa fa-reply"></span>
                                @endif
                                <span>{{substr($inbox->thread->message, 0, 20)}}</span>
                            </div>
                        </div>
                    </a>
                </li>
            @endif
        @endforeach

    </ul>
</div>
