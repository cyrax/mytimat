@extends('partials.app')

@section('content')
    <header>
        <div class="container">
          <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/">
                <img src="/image/logo.png" class="logo">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
                @include('partials.topmenu')
            </nav>
            <hr/>
            <div class="header__body">
                <span class="company_name">Book Your Time</span>
                <h1>{{ trans('app.main_title') }}</h1>
                <span class="title">{{ trans('app.sub_main_title') }}</span>
                @include('partials.poisk')
                <div class="clear"></div>
                @if(!Auth::check())
                    <a class="zaregistririvat" href="#login_register" onclick="register()">{{ trans('app.register') }}</a>
                @endif
            </div>
        </div>
    </header>
    <div class="uslugi">
        <div class="container">
          <div class="row">
            @foreach($categories as $category)
                <div class="col-sm">
                    <a href="{{ route('category', ['id' => $category->url]) }}">
                        <img src="{{ isset($category->image) ? '/uploads/'.$category->image: '/image/icon1.png' }}" width="38px">
                        <p>{{ $category->name }}</p>
                    </a>
                </div>
            @endforeach
          </div>
        </div>
    </div>
    <div class="container">
        <a href="{{ route('categories') }}"><div class="vse-uslugi">{{ trans('app.all_services') }}</div></a>
    </div>
    <div class="notification"></div>
    @include('partials.how_to_work')
@endsection
