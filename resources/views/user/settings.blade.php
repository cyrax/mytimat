@extends('partials.app')

@section('content')
<div class="aksii studio">
    <div class="container">
        <span class="add-span">{{ trans('app.settings') }}</span>
        <form id="setting-form" method="post">
            {{ csrf_field() }}
            <div id="setting-alert" class="alert"></div>
            <div class="form-group">
                <input id="email" name="email" type="checkbox" class="form-check-input" {{ $data['setting']['display_email'] ? 'checked' : '' }} value="{{ $data['setting']['display_email'] }}">
                <label for="email" class="form-check-label">{{ trans('app.display_email') }}</label>
            </div>
            <div class="form-group">
                <input id="phone" name="phone" type="checkbox" class="form-check-input" {{ $data['setting']['display_phone'] ? 'checked' : '' }} value="{{ $data['setting']['display_phone'] }}">
                <label for="phone" class="form-check-label">{{ trans('app.display_phone') }}</label>
            </div>
            <div class="form-group">
                <input id="social" name="social" type="checkbox" class="form-check-input" {{ $data['setting']['display_social'] ? 'checked' : '' }} value="{{ $data['setting']['display_social'] }}">
                <label for="social" class="form-check-label">{{ trans('app.display_social') }}</label>
            </div>
            <button onclick="return updateUserSettings()" type="submit" class="aksii-more more-z">{{ trans('app.submit') }}</button>
        </form>
    </div>
</div>
@endsection
@section('script')
    <script>
        function updateUserSettings() {
            $.ajax({
                method: 'POST',
                data: $('#setting-form').serialize(),
                success: function (data) {
                    $('#setting-alert').addClass('alert-success').text(data);
                }
            });
            return false;
        }
    </script>
@endsection