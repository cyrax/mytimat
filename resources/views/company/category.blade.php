@extends('partials.app')

@section('content')
    <div class="bread">
        <div class="container">
            <a href="/category">{{ trans('app.services') }}</a> »@foreach($parents as $parent) <a href="/category/{{ $parent['url'] }}">{{ $parent['name'] }}</a> »@endforeach <span>{{ $category['name'] }}</span>
        </div>
    </div>
    <div class="aksii">
        <div class="container">
            <span class="add-span">{{ $category['name'] }}</span>
                @foreach($children as $child) <a href="/category/{{ $child['url'] }}" style="margin-right: 20px;">{{ $child['name'] }}</a> @endforeach
            <div class="aksii-container auto-moto">
                @if(!empty($data['companies']))
            @foreach($data['companies'] as $company)
                    <div class="col-sm">
                    <div class="aksii-text-cover">
                        <p><a href="{{ route('company-view', ['id' => $company->id]) }}"> {{ $company->name }}</a></p>
                        {{--<span>Бонус за бронирование услуги</span>--}}
                        <div style="position: relative;font-size: 14px;">
                          <input type="number" id="star-{{ $company->id }}" name="rating" class="rating rating-loading" value="{{ intval($company->averageRating) }}" {{ $company->userSumRating > 0 ? 'data-readonly' : '' }}>
                          {{ round($company->averageRating, 2) }}
                        </div>
                        @php
                            $bookmark = $company->bookmarks()->where('user_id', Auth::id())->first();
                            $bookmark = empty($bookmark) ? 0 : 1;
                        @endphp
                        <span id="bookmark_{{ $company->id }}" onclick="add_bookmark({{ $company->id }})" class="heart-button opacity50" @if($bookmark) style="display: none;" @endif>
                            <img src="/image/heart-button.png">
                        </span>
                        <span id="bookmark_off_{{ $company->id }}" onclick="remove_bookmark({{ $company->id }})" class="heart-button" @if(!$bookmark) style="display: none;" @endif>
                            <img src="/image/heart-button.png">
                        </span>
                    </div>
                    @php
                        $opened = $company[$day_of_week];
                        if($company->nonworkingdays()->where('date', \Carbon\Carbon::now()->toDateString())->first()){
                            $opened = 0;
                        } elseif($company[$day_of_week.'_end'] < \Carbon\Carbon::now()->toTimeString()) {
                            $opened = 0;
                        }
                    @endphp
                    <div class="aksii-cover">
                        <p class="auto-moto-p">
                            {{ $company->address }}<br/>
                            @if(!$opened) {{ trans('app.closed') }} @else {{ trans('app.today') }} с {{ \Carbon\Carbon::parse($company[$day_of_week.'_start'])->format('H:i') }} до {{ \Carbon\Carbon::parse($company[$day_of_week.'_end'])->format('H:i') }} @endif <br/>
                            {{ $company->phone }}  {{--<span>смотреть телефон</span>--}}
                        </p>
                        <span class="moto-country">
                            <img src="/image/ru.png">
                        </span>
                        <div class="aksii-img" style="background-image: url('{{ $company->pictures()->where('main', 1)->first()->url}}'); background-size: cover;">
                        </div>
                        <p class="moto-desc">{{ substr($company->features, 0, 500) }}</p>
                        <a href="/company/{{ $company->id }}"><button>{{ trans('app.order') }}</button></a>
                        @if($company->adult)<div class="check-img">
                            <img src="/image/check2.png" alt="{{ trans('app.from_18') }}" title="{{ trans('app.from_18') }}">
                        </div>@endif
                        @if($company->departure)<div class="check-img">
                            <img src="/image/check1.png" alt="{{ trans('app.with_out') }}" title="{{ trans('app.with_out') }}">
                        </div>@endif
                        @if($company->animals)<div class="check-img">
                            <img src="/image/check0.png" alt="{{ trans('app.with_animals') }}" title="{{ trans('app.with_animals') }}">
                        </div>@endif
                        <div class="clear"></div>
                    </div>
                </div>
                @endforeach
                    <div class="clear"></div>

                    {{ $services->links() }}
                {{--<button class="aksii-more">Показать еще</button>--}}
                @else
                    <h2 style="color: #111111; text-align: center; margin-bottom: 250px;">{{ trans('app.nothing_found') }}</h2>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('css')

@endsection

@section('script')
    <script>
        function add_bookmark(id) {
            $.ajax({
                method: "POST",
                url: "/company/"+ id +"/bookmark",
                data: { "_token": '{{csrf_token()}}' }
            }).done(function( msg ) {
                $("#bookmark_" + id).hide();
                $("#bookmark_off_" + id).show();
            }).fail(function() {
                alert("{{ trans('error.adding_to_bookmarks') }}");
            });
        }

        function remove_bookmark(id) {
            $.ajax({
                method: "POST",
                url: "/company/"+ id +"/bookmark-remove",
                data: { "_token": '{{csrf_token()}}' }
            }).done(function( msg ) {
                $("#bookmark_off_" + id).hide();
                $("#bookmark_" + id).show();
            }).fail(function() {
                alert("{{ trans('app.error') }}");
            });
        }
    </script>
    <script src="{{ asset('js/bootstrap-rating-input.min.js') }}"></script>
    <script>
        $(".rating").rating({
            iconLib: "fa",
            activeIcon: "fa-heart",
            inactiveIcon: "fa-heart-o",
            clearableIcon: "fa-trash-o",
            min: 1,
            max: 5
        }).change(function () {
            let value = $(this).val();
            let id = $(this).attr('id').substring(5);
            $.ajax({
                url: '/company/rate',
                method: 'POST',
                data: {company_id: id, value: value},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    console.log(data);
                }
            });
        });
    </script>
@endsection
