@extends('partials.app')
@section('head_script')
<script type="text/javascript" src="https://vk.com/js/api/share.js?95" charset="windows-1251"></script>
@endsection
@php
    $lang = UriLocalizer::localeFromRequest();
    $startTime = strtolower(date('l')).'_start';
    $endTime = strtolower(date('l')).'_end';

@endphp
<script>
    var disabledTimeRanges = [];
    var specialistTimes = [];
    var companyTime = [];
    companyTime['monday'] = ['{{ $company->monday_start }}', '{{ $company->monday_end }}'];
    companyTime['tuesday'] = ['{{ $company->tuesday_start }}', '{{ $company->tuesday_end }}'];
    companyTime['wednesday'] = ['{{ $company->wednesday_start }}', '{{ $company->wednesday_end }}'];
    companyTime['thursday'] = ['{{ $company->thursday_start }}', '{{ $company->thursday_end }}'];
    companyTime['friday'] = ['{{ $company->friday_start }}', '{{ $company->friday_end }}'];
    companyTime['saturday'] = ['{{ $company->saturday_start }}', '{{ $company->saturday_end }}'];
    companyTime['sunday'] = ['{{ $company->sunday_start }}', '{{ $company->sunday_end }}'];
    @foreach ($data['specialists'] as $specialist)
        var specialistTime = [];
        specialistTime['monday'] = ['{{ $specialist['schedule']['monday_start']}}', '{{ $specialist['schedule']['monday_end']}}'];
        specialistTime['tuesday'] = ['{{  $specialist['schedule']['tuesday_start'] }}', '{{ $specialist['schedule']['tuesday_end']}}'];
        specialistTime['wednesday'] = ['{{ $specialist['schedule']['wednesday_start'] }}', '{{ $specialist['schedule']['wednesday_end']}}'];
        specialistTime['thursday'] = ['{{ $specialist['schedule']['thursday_start'] }}', '{{ $specialist['schedule']['thursday_end']}}'];
        specialistTime['friday'] = ['{{ $specialist['schedule']['friday_start'] }}', '{{ $specialist['schedule']['friday_end']}}'];
        specialistTime['saturday'] = ['{{ $specialist['schedule']['saturday_start'] }}', '{{ $specialist['schedule']['saturday_end']}}'];
        specialistTime['sunday'] = ['{{ $specialist['schedule']['sunday_start'] }}', '{{ $specialist['schedule']['sunday_end']}}'];
        specialistTimes['{{ $specialist['id'] }}'] = specialistTime;
    @endforeach

</script>

@section('content')
    <div class="aksii studio">
        <div class="container">
            @if(session('services_added'))
                <div class="alert alert-success" role="alert" style="text-align: center;">
                    <p><strong>{{ trans('app.application_received') }}</strong><br>{{ trans('app.expect_response_company') }}</p>
                    <p><br>
                        {{--<button class="btn btn-primary">{{ trans('app.go') }} {{ trans('app.to_list_of_requests') }}</button>--}}
                    </p>
                </div>
            @endif
            @if ($errors->any())
                <div class="alert alert-danger">{{ implode('', $errors->all(':message')) }}</div>
            @endif
            <span class="add-span">
                <span class="span-category">{{ $company->category->name }}</span><br/>
                {{ $company->name }}
                <img id="bookmark" onclick="add_bookmark()" src="/image/heart-button.png" class="hydra-like"
                     @if($data['bookmark']) style="display: none;" @endif >
                <img id="bookmark_off" onclick="remove_bookmark()" src="/image/heart-button.png"
                     class="hydra-like hydra-liked"
                     @if(!$data['bookmark']) style="display: none;" @endif >
                @if (Auth::check() && auth()->user()->company_id == $company->id)
                    <a href="/company/{{ $company->id }}/edit" style="font-size: 20px;"><i class="fa fa-edit"></i></a>
                @endif
                <div style="position: relative;font-size: 14px;">
                    <input type="number" id="star-{{ $company->id }}" name="rating" class="rating rating-loading" value="{{ intval($company->averageRating) }}" {{ $company->userSumRating > 0 ? 'data-readonly' : '' }}>
                    {{ round($company->averageRating, 2) }}
                </div>
            </span>
          <div class="row">
            <div class="col-sm">
                <ul class="slickslide">
                    @foreach($data['pictures'] as $picture)
                        <a class="fancy" data-fancybox="images" href="{{ $picture['url'] }}" style="">
                            <img src="{{ $picture['url'] }}" title="img" alt="img" width="95%"/>
                        </a>
                    @endforeach
                </ul>
            </div>
            <div class="col-sm studio-small">
                <div class="row">
                    <div class="col-sm col nopad">
                        <p class="moto-desc">
                            <b>{{ trans('app.address') }}:</b> <span id="address" class="kok-color">{{ $company->address }}</span><br/>
                            @if(!$data['opened']) {{ trans('app.closed') }} @else {{ trans('app.today') }}
                            {{ trans('app.from') }} {{ \Carbon\Carbon::parse($company[$data['day_of_week'].'_start'])->format('H:i') }}
                            {{ trans('app.to') }} {{ \Carbon\Carbon::parse($company[$data['day_of_week'].'_end'])->format('H:i') }} @endif
                        </p>
                        <a href="/chat-with-company/{{ $company->id }}/{{ $company->user_id }}" class="btn icons_email"><img src="/image/icons_email.png">{{ trans('app.contact_company') }}</a>
                    </div>
                    <div class="col-sm col nopad">
                        @if($company->adult)
                            <div class="check-img">
                                <img src="/image/check2.png" alt="{{ trans('app.from_18') }}" title="{{ trans('app.from_18') }}">
                            </div>
                        @endif
                        @if($company->departure)
                            <div class="check-img">
                                <img src="/image/check1.png" alt="{{ trans('app.with_out') }}" title="{{ trans('app.with_out') }}">
                            </div>
                        @endif
                        @if($company->animals)
                            <div class="check-img">
                                <img src="/image/check0.png" alt="{{ trans('app.with_animals') }}" title="{{ trans('app.with_animals') }}">
                            </div>
                        @endif
                            <div class="fb-like"
                                 data-href="{{ request()->url() }}"
                                 data-layout="button_count"
                                 data-action="like"
                                 data-show-faces="true"
                                 style="vertical-align: top;">
                            </div>
                            <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                            <script type="text/javascript">document.write(VK.Share.button(false,{type: "round", text: "{{ trans('app.save') }}"}))</script>
                        {{--<img src="/image/layer-4.png" style="float: right;margin-top: 12px;">--}}
                    </div>
                </div>
                    <div class="clear"></div>
                <div class="row">
                  <p class="moto-desc margintop30"><b>{{ trans('app.company_features') }}: </b>{{ substr($company->features, 0, 200) }}</p>
                </div>
                <div class="card-group" style="margin-top: 30px;">
                    <div class="">
                        <div class="card-body sub-card-body">
                            <div class="row">
                                <div class="col-sm col nopad">
                                    <p class="moto-desc">
                                        <b>{{ trans('app.phone') }}:</b><br/>
                                        {{ $company->phone }}<br/>
                                    </p>
                                </div>
                                <div class="col-sm col nopad">
                                    <p class="moto-desc">
                                        @if($company->email)<b>E-mail:</b> <a href="mailto:{{ $company->email }}"
                                                                              target="_blank"><span
                                                    class="kok-color">{{ $company->email }}</span></a><br/>@endif
                                        @if($company->site)<b>{{ trans('app.website') }}:</b> <a href="{{ $company->site }}/" target="_blank"><span
                                                    class="kok-color">{{ $company->site }}</span></a>@endif
                                    </p>
                                </div>
                                @if($company->vk || $company->facebook || $company->twitter)
                                    <div class="col nopad">
                                        <p class="moto-desc"><b>{{ trans('app.social_network_pages') }}</b></p>
                                        @if($company->vk)<a href="//{{ $company->vk }}"><img class="soc-img" src="/image/soc1.png"></a>@endif
                                        @if($company->facebook)<a href="//{{ $company->facebook }}"><img
                                                    class="soc-img" src="/image/soc2.png"></a>@endif
                                        @if($company->twitter)<a href="//{{ $company->twitter }}"><img
                                                    class="soc-img" src="/image/soc3.png"></a>@endif
                                    </div>@endif
                            </div>
                            <div class="row">
                                <div class="col nopad">
                                    <p class="moto-desc"><b>{{ trans('app.schedule') }}</b></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm col nopad">
                                    <p class="moto-desc">
                                        @if($company->monday)<span
                                                class="days-name">{{ trans('app.monday_short') }}.</span> {{ \Carbon\Carbon::parse($company->monday_start)->format('H:i') }}
                                        - {{ \Carbon\Carbon::parse($company->monday_end)->format('H:i') }}<br/>@endif
                                        @if($company->tuesday)<span
                                                class="days-name">{{ trans('app.tuesday_short') }}.</span>  {{ \Carbon\Carbon::parse($company->tuesday_start)->format('H:i') }}
                                        - {{ \Carbon\Carbon::parse($company->tuesday_end)->format('H:i') }}<br/>@endif
                                        @if($company->wednesday)<span
                                                class="days-name">{{ trans('app.wednesday_short') }}.</span> {{ \Carbon\Carbon::parse($company->wednesday_start)->format('H:i') }}
                                        - {{ \Carbon\Carbon::parse($company->wednesday_end)->format('H:i') }}<br/>@endif
                                        @if($company->thursday)<span
                                                class="days-name">{{ trans('app.thursday_short') }}.</span> {{ \Carbon\Carbon::parse($company->thursday_start)->format('H:i') }}
                                        - {{ \Carbon\Carbon::parse($company->thursday_end)->format('H:i') }}<br/>@endif
                                        @if($company->friday)<span
                                                class="days-name">{{ trans('app.friday_short') }}.</span> {{ \Carbon\Carbon::parse($company->friday_start)->format('H:i') }}
                                        - {{ \Carbon\Carbon::parse($company->friday_end)->format('H:i') }}<br/>@endif
                                        @if($company->saturday)<span
                                                class="days-name">{{ trans('app.saturday_short') }}.</span> {{ \Carbon\Carbon::parse($company->saturday_start)->format('H:i') }}
                                        - {{ \Carbon\Carbon::parse($company->saturday_end)->format('H:i') }}<br/>@endif
                                        @if($company->sunday)<span
                                                class="days-name">{{ trans('app.sunday_short') }}.</span> {{ \Carbon\Carbon::parse($company->sunday_start)->format('H:i') }}
                                        - {{ \Carbon\Carbon::parse($company->sunday_end)->format('H:i') }} @endif
                                    </p>
                                </div>
                                <div class="col-sm col nopad">
                                    @if($company->lunch)<p class="moto-desc">
                                        {{ trans('app.lunch') }}:<br/>
                                        {{ \Carbon\Carbon::parse($company->lunch_start)->format('H:i') }}
                                        - {{ \Carbon\Carbon::parse($company->lunch_end)->format('H:i') }}
                                    </p>@endif
                                    @php
                                        $anchor = 0;
                                    @endphp
                                    @if(!empty($data['non_working_days']))
                                        <p class="moto-desc">
                                            {{ trans('app.non_working_days') }}:<br/>
                                            @foreach($data['non_working_days'] as $non_working_day)
                                                @php
                                                    $anchor++;
                                                @endphp
                                                @if($anchor != $data['nwd_count'])
                                                    {{ \Carbon\Carbon::parse($non_working_day['date'])->format('j M, ') }}
                                                @else
                                                    {{ \Carbon\Carbon::parse($non_working_day['date'])->format('j M') }}
                                                @endif
                                            @endforeach
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
            <div id="map"></div>
            <p class="moto-desc margintop30"><b>{{ trans('app.description') }}:</b> {{ $company->description }}</p>
            <span class="vibor-uslugi">{{ trans('app.specialists') }}</span>
            <div class="" id="accordion2">
                <div class="card">
                    <div class="card-body sub-card-body" style="padding-top:15px;">
                        <div class="row form-group">
                            <div class="col-md"></div>
                            <div class="col-md"></div>
                            <div class="col-md"></div>
                            <div class="col-md"></div>
                            <div class="col-md"></div>
                        </div>
                        @foreach($data['specialists'] as $specialist)
                            <div class="row form-group">
                                <div class="col-md">
                                    <img style="max-width: 50px" src="{{ $specialist['pictures'][0]['url'] ?? '/image/user.png' }}" />
                                </div>
                                <div class="col-md">
                                    <a href="{{ route('specialist-profile', ['id' => $specialist['id']]) }}">{{ $specialist['name'] }} {{ $specialist['last_name'] }}</a>
                                </div>
                                <div class="col-md">
                                    {{ $specialist['country']['title_'.$lang] }}
                                </div>
                                <div class="col-md">
                                    {{ $specialist['city']['title_'.$lang] }}
                                </div>
                                <div class="col-md">
                                    {{ $specialist['accepts'] ? trans('app.accepts_orders') : trans('app.does_not_accept') }}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @if($services['data']->isNotEmpty())
                <span class="vibor-uslugi">{{ trans('app.select_service') }}</span>
                <div class="row">
                    <div class="col">
                        <b>{{ trans('app.service') }}</b>
                    </div>
                    <div class="col">
                        <b>{{ trans('app.duration') }}</b>
                    </div>
                    <div class="col">
                        <b>{{ trans('app.price') }}</b>
                    </div>
                </div>

                    @php
                        $category = 0;
                    @endphp

                    @foreach($services['data'] as $service)
                        @php $duration = \App\Http\Controllers\CompaniesController::get_duration($service->days, $service->hours, $service->minutes);@endphp
                        @if($category != $service['category_id'])
                            @php
                                $category = $service['category_id'];
                            @endphp
                                <div class="d-flex justify-content-between center align-items-center">
                                    <h4 class="card-title">
                                        {{ $service->category()->first()->name}}
                                    </h4>
                                    <div>
                                        <p class="studio-span">{{ trans('app.price') }} {{ trans('app.from') }} <span
                                                    class="j-tus">{{ $services['data']->where('category_id', $category)->min('price') }}</span>
                                        </p>
                                        <p class="studio-span">{{ trans('app.amount_short') }}
                                            {{ trans('app.of_services') }}: {{ $services['data']->where('category_id', $category)->count() }}</p>
                                    </div>
                                </div>
                        @endif
                        <div class="row" id="accordion3">
                                    <div class="col">
                                        <p class="st-p">{{ $service->name }}</p>
                                    </div>
                                    <div class="col">
                                        <p class="st-p"> {{ $duration }} </p>
                                    </div>
                                    <div class="col">
                                        <p class="st-p">
                                          <span class="j-tus">{{ $service->price }}</span>
                                          <button id="select_a_service_{{ $service->id }}"
                                                  onclick="add_a_service({{ $service->id }}, '{{ addslashes($service->name) }}', '{{ addslashes($duration) }}', '{{ $service->price }}')"
                                                  class="plus-but float-right"><i class="fa fa-plus"></i> {{ trans('app.choose_service') }}
                                          </button>
                                          <p id="service_selected_checked_{{ $service->id }}" class="st-p" style="display: none;">
                                            <span class="j-tus"><i class="fa fa-check"></i></span> {{ trans('app.selected') }}</p>
                                        </p>
                                    </div>
                                    <script>
                                        @foreach($data['specialists'] as $specialist)
                                            @foreach ($specialist['orders'] as $order)
                                                disabledTimeRanges.push(['{{ $order['specialist_id'] }}','{{ $order['date'] }}', '{{ $order['time'] }}', '{{ date('H:i', strtotime($order['date'].' '.$order['time'] . ' +'.$service->hours.' hours, +'.$service->minutes.' minutes')) }}']);
                                            @endforeach
                                        @endforeach
                                    </script>
                                  </div>
                                  @endforeach

                            </div>

                          </div>
    @else
        <span class="vibor-uslugi">{{ trans('app.company_not_added_services') }}<br>{{ trans('app.contact_above_contacts') }}</span>
    @endif
    <span class="vibor-uslugi">{{ trans('app.selected_services') }}</span>
    <form id="make-order" action="/company/{{ $company->id }}/order" method="POST">
        {{ csrf_field() }}
        <div class="row aksii-table-head s-a">
            <div class="a-t-text a-t-names">{{ trans('app.services') }}</div>
            <div class="a-t-text a-t-spec">{{ trans('app.specialist') }}</div>
            <div class="a-t-text" style="width: 18%">{{ trans('app.date') }}</div>
            <div class="a-t-text">{{ trans('app.time') }}</div>
            <div class="a-t-text">{{ trans('app.duration') }}.</div>
            <div class="a-t-text">{{ trans('app.price') }}</div>
            <div class="a-t-text">{{ trans('app.delete') }}</div>
        </div>
        <div id="selected_placeholder" style="text-align: center; font-size: 1.5em; padding: 50px 0;"></div>
        <div id="sel_block" style="display: none;">
            <div id="selected_services_block"></div>
            <div class="clear"></div>
            <button onclick="return send_order();" class="aksii-more more-z" type="submit">{{ trans('app.send_application') }}</button>
        </div>
    </form>
    <!-- Load Facebook SDK for JavaScript -->
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>
@endsection

@section('script')
    <script src="/js/cbpFWTabs.js"></script>
    <script>
        var services_count = 0;
        var weekday = new Array(7);
        weekday[0] =  "sunday";
        weekday[1] = "monday";
        weekday[2] = "tuesday";
        weekday[3] = "wednesday";
        weekday[4] = "thursday";
        weekday[5] = "friday";
        weekday[6] = "saturday";

        function add_a_service(id, name, duration, price) {
            services_count++;
            $("#select_a_service_" + id).hide();
            $("#service_selected_checked_" + id).show();
            $("#selected_placeholder").hide();
            $("#sel_block").show();
            $("#selected_services_block").append("<div id=\"selected_service_" + id + "\" class=\"aksii-table-head white-back-table s-a\">" +
                "<input type='hidden' name='services[" + id + "][id]' value='" + id + "' id='service_id' required>" +
                "<div class=\"a-t-text a-t-names\">" + name + "</div>" +
                "<div class=\"a-t-text\" id='date__block' style='width: 18%;white-space: nowrap;'>\n" +
                "<span class=\"j-tus\"><i class=\"fa fa-calendar\" aria-hidden=\"true\"></i></span> <input type='date' onchange='dateChanged(this)' name='services[" + id + "][date]' min='{{\Carbon\Carbon::now()->toDateString()}}' required>\n" +
                "</div><div class=\"a-t-text\"><input class='services_time' name='services[" + id + "][time]'></div>\n" +
                "<div class=\"a-t-text a-t-spec\" style='position:relative;'>\n" +
                "<select name=\"services[" + id + "][specialist]\" id=\"services_specialist_" + id + "\" style=\"width: 100%;\" onchange='specialist_changed(this)' required>\n" +
                "<option value=\"0\">{{ trans('app.any') }}</option>\n" +
                    @foreach($data['specialists'] as $specialist)
                        "<option value=\"{{ $specialist['id'] }}\">{{ $specialist['name'] }} {{ $specialist['last_name'] }}</option>\n" +
                    @endforeach
                        "</select>" +
                    @foreach($data['specialists'] as $specialist)
                        "<a href='{{ route('specialist-profile', ['id' => $specialist['id']]) }}'><img class='spec-images' id='s__img-{{ $specialist['id'] }}' style=\"max-width: 50px;position:absolute;display:none;\" src=\"{{ $specialist['pictures'][0]['url'] ?? '/image/user.png' }}\" /></a>"+
                    @endforeach
                        "</div>" +
                "<div class=\"a-t-text\">" + duration + "</div>\n" +
                "<div class=\"a-t-text\">" + price + "</div>" +
                "<div class=\"a-t-text\">\n" +
                "<span class=\"k-tus\"><i style='cursor: pointer;' class=\"fa fa-close\" onclick='delete_selected_service(" + id + ")'></i></span>\n" +
                "</div></div>");

            $("#services_specialist_" + id).select2();

            $('.services_time').timepicker({
                'minTime': '{{ $company->$startTime }}',
                'maxTime': '{{ $company->$endTime }}',
		            'timeFormat': 'H:i:s',
		            'step': {{ $service->interval > 0 ? $service->interval : 5 }},
            }).on('changeTime', function() {
                let time_picker = this;
                let dateValue = $(this).parents('.aksii-table-head').find('[type=date]').val();
                let timeValue = $(this).val();
                if (dateValue.length > 0 && timeValue.length > 0) {
                    let d = new Date(dateValue);
                    let week_name = weekday[d.getDay()];
                    specialistTimes.forEach(function (item, id){
                        console.log(item[week_name][0]);
                        if (item[week_name][0].length === 0 || item[week_name][0] > timeValue || item[week_name][1] < timeValue) {
                            $(time_picker).parents('.aksii-table-head').find('.a-t-spec').find('option[value='+id+']').attr('disabled', 'disabled');
                        }
                        else {
                            console.log('not');
                            $(time_picker).parents('.aksii-table-head').find('.a-t-spec').find('option[value='+id+']').removeAttr('disabled');
                        }
                    })
                }
            });
            $('.ui-timepicker-wrapper').css('width', $('.ui-timepicker-input').width()+5);
        }

        function delete_selected_service(id) {
            services_count--;
            $("#select_a_service_" + id).show();
            $("#service_selected_checked_" + id).hide();
            if (services_count === 0) {
                $("#selected_placeholder").show();
                $("#sel_block").hide();
            }
            $("#selected_service_" + id).hide('50', function () {
                $(this).remove();
            });
        }


        function add_bookmark() {
            $.ajax({
                method: "POST",
                url: "/company/{{ $company->id }}/bookmark",
                data: {"_token": '{{csrf_token()}}'}
            }).done(function (msg) {
                $("#bookmark").hide();
                $("#bookmark_off").show();
            }).fail(function () {
                alert("{{ trans('error.adding_to_bookmarks') }}");
            });
        }

        function remove_bookmark() {
            $.ajax({
                method: "POST",
                url: "/company/{{ $company->id }}/bookmark-remove",
                data: {"_token": '{{csrf_token()}}'}
            }).done(function (msg) {
                $("#bookmark_off").hide();
                $("#bookmark").show();
            }).fail(function () {
                alert("{{ trans('app.error') }}");
            });
        }
        function specialist_changed(spec) {
            $('.spec-images').hide();
            $('#s__img-'+$(spec).val()).show();

            let dateValue = $(spec).parents('.aksii-table-head').find('[type=date]').val();
            if (dateValue.length > 0) {
                let d = new Date(dateValue);
                let selectedSpecialist = $(spec).val();
                console.log(weekday[d.getDay()]);
                if (selectedSpecialist > 0) {
                    let specialist = specialistTimes[selectedSpecialist];
                    setMinAndMaxTime($(spec).parents('.aksii-table-head').find('.services_time'), specialist[weekday[d.getDay()]]);
                }
                else {
                    setMinAndMaxTime($(spec).parents('.aksii-table-head').find('.services_time'), companyTime[weekday[d.getDay()]])
                }
            }
        }
        function setMinAndMaxTime(input, times) {
            input.timepicker('option', { minTime: times[0] });
            input.timepicker('option', { maxTime: times[1] });
        }

        function dateChanged(date) {
            if (date.value.length > 0) {
                let d = new Date(date.value);
                let selectedSpecialist = $(date).parent().parent().find('.a-t-spec').find('select').val();
                console.log(weekday[d.getDay()]);
                if (selectedSpecialist > 0) {
                    let specialist = specialistTimes[selectedSpecialist];
                    $(date).parent().next().find('input').timepicker('option', {minTime: specialist[weekday[d.getDay()]][0]});
                    $(date).parent().next().find('input').timepicker('option', {maxTime: specialist[weekday[d.getDay()]][1]});
                }
                else {
                    $(date).parent().next().find('input').timepicker('option', {minTime: companyTime[weekday[d.getDay()]][0]});
                    $(date).parent().next().find('input').timepicker('option', {maxTime: companyTime[weekday[d.getDay()]][1]});
                }
            }
        }
        $(function () {

        })
    </script>
    <script src="{{ asset('js/bootstrap-rating-input.min.js') }}"></script>
    <script>
        $(".rating").rating({
            iconLib: "fa",
            activeIcon: "fa-heart",
            inactiveIcon: "fa-heart-o",
            clearableIcon: "fa-trash-o",
            min: 1,
            max: 5
        }).change(function () {
            let value = $(this).val();
            let id = $(this).attr('id').substring(5);
            $.ajax({
                url: '/company/rate',
                method: 'POST',
                data: {company_id: id, value: value},
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function(data) {
                    console.log(data);
                }
            });
        });
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.js"></script>
    <script src="{{ asset('js/jquery.timepicker.min.js') }}"></script>
    <script>
        $(".fancy").fancybox({
            showNavArrows : true,
            autoSize: true
        });
        function send_order() {
            @if (!Auth::check())
                $('#login_register').modal();
                $('.modal-backdrop').hide();
                return false;
            @else
                return true;
            @endif
        }
    </script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDV61DqeVe7pKi5H5cQNmRZbHvX-DPQ59I"></script>
    <script>
        var almaty = {lat: 43.2467582, lng: 76.871403};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: almaty
        });
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode( { 'address': $('#address').text() }, function(results, status) {
            if (status === 'OK') {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
            } else {
                // alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    </script>
@stop

@section('css')
    <link href="/css/tab.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.2.5/jquery.fancybox.min.css" />
    <link rel="stylesheet" href="{{ asset('css/jquery.timepicker.css') }}">
    <style>
        #map {
            width: 100%;
            height: 200px;
            background-color: grey;
        }
        .slickslide .fancy:not(:first-child){
            width: 20%;
            float: left;
            margin-top: 10px;
        }
        .slickslide img{
            max-height: 600px;
            max-width: 100%;
        }
    </style>
@endsection
