<div class="add-company margin-top20 row">
      <div class="col-sm nopad" style="background-color: #F5F5F5;">
          <div class="grey-admin">
              <span class="grey-span">{{ trans('app.contacts', [], $language->locale) }}</span>
              <div class="inputwithimg">
                  <img src="/image/con1.png">
                  <input type="tel" name="phone" value="{{ $company->phone }}" placeholder="{{ trans('app.phone', [], $language->locale) }}" class="add-input">
              </div>
              <div class="inputwithimg">
                  <img src="/image/con2.png">
                  <input type="text" name="site" value="{{ $company->site }}" placeholder="{{ trans('app.website_address', [], $language->locale) }}" class="add-input">
              </div>
              <div class="inputwithimg">
                  <img src="/image/con3.png">
                  <input type="email" name="email" value="{{ $company->email }}" placeholder="{{ trans('app.email', [], $language->locale) }}" class="add-input">
              </div>
              <label class="add-label">{{ trans('app.social_networks', [], $language->locale) }}</label>
              <div class="inputwithimg">
                  <img src="/image/soc1.png">
                  <input type="text" name="vk" placeholder="VK" class="add-input" value="{{ $company->vk }}">
              </div>
              <div class="inputwithimg">
                  <img src="/image/soc2.png">
                  <input type="text" name="facebook" placeholder="Facebook" class="add-input" value="{{ $company->facebook }}">
              </div>
              <div class="inputwithimg">
                  <img src="/image/soc3.png">
                  <input type="text" name="twitter" placeholder="Twitter" class="add-input" value="{{ $company->twitter }}">
              </div>
              <div class="inputwithimg">
                  <img src="/image/soc4.png">
                  <input type="text" name="telegram" placeholder="Telegram" class="add-input" value="{{ $company->telegram }}">
              </div>
              <div class="inputwithimg">
                  <img src="/image/soc5.png">
                  <input type="tel" name="whatsapp" placeholder="Whatsapp" class="add-input" value="{{ $company->whatsapp }}">
              </div>
          </div>
      </div>
      <div class="col-sm nopad" style="background-color: #F5F5F5;">
          <div class="grey-admin">
              <span class="grey-span">{{ trans('app.work_time', [], $language->locale) }}</span>
              @foreach(\App\Service::getWeeks() as $week)
                  <div class="radio-cover2 hydra-cover" style="width: auto; float: none; height: 50px;">
                      <div id="hydra-start-end-{{ $week }}" class="hydra-start-end" style="{{ $company->$week || (!$company->exists && !\App\Service::isWeekend($week)) ? '': 'display:none' }}">
                          <input type="time" name="{{ $week }}-start" value="{{ $company->{$week.'_start'} ?? '10:00' }}" class="add-input hydra_days_start"> -
                          <input type="time" name="{{ $week }}-end" value="{{ $company->{$week.'_end'} ?? '20:00' }}" class="add-input hydra_days_end">
                      </div>
                      <label class="checkbox-label">
                          <input type="checkbox" name="{{ $week }}" class="radio" value="1" {{ $company->$week || (!$company->exists && !\App\Service::isWeekend($week)) ? 'checked' :'' }}
                                  onchange="hydra_days_change('{{ $week }}')">
                          <span class="checkbox-icon"></span>
                          <span class="checkbox-text">{{ trans('app.'.$week, [], $language->locale) }}</span>
                      </label>
                  </div>
                  <div class="clear"></div>
              @endforeach
              <div class="hydra-cover" style="margin-bottom: 30px;">
                  <div id="hydra-start-end-lunch" class="hydra-start-end" style="margin-top: 31px; {{ $company->lunch ? '': 'display:none' }}">
                      <input type="time" name="lunch-start" value="{{ $company->exists ? $company->lunch_start : '13:00' }}" class="add-input hydra_days_start"> -
                      <input type="time" name="lunch-end" value="{{ $company->exists ? $company->lunch_end : '14:00' }}" class="add-input hydra_days_end">
                  </div>
                  <label class="add-label">{{ trans('app.lunch_break', [], $language->locale) }}</label>
                  <div class="radio-cover2 radio-right" style="float: none; width: auto; margin-top: 20px;">
                      <label class="checkbox-label">
                          <input type="checkbox" name="lunch" {{ $company->lunch ? '': 'checked' }} class="radio" value="1" onchange="hydra_lunch_change()">
                          <span class="checkbox-icon"></span>
                          <span class="checkbox-text">{{ trans('app.without_lunch', [], $language->locale) }}</span>
                      </label>
                  </div>
              </div>
              <div class="clear"></div>
              <label class="add-label"><b>{{ trans('app.weekends', [], $language->locale) }}</b></label>
              <div class="clear"></div>
              <div id="inputs_non_days">
                  @foreach ($company->nonworkingdays()->get() as $nonworkingday)
                      <input type="hidden" name="non_working_days[]" value="{{ $nonworkingday->date }}">
                  @endforeach
              </div>
              <div id="list_of_non_days">
                  @foreach ($company->nonworkingdays()->get() as $nonworkingday)
                      <span id="holiday-{{ $nonworkingday->id }}" class="holiday-day">{{ $nonworkingday->date }}<img src="/image/h-close.png" onclick="delete_a_holiday('{{ $nonworkingday->id }}')"></span>
                  @endforeach
              </div>
              <div class="clear"></div>
              <a href="#add_non_working_date">
                  <button type="button" class="add-button" >
                      {{ trans('app.add', [], $language->locale) }}
                  </button>
              </a>
          </div>
      </div>
</div>
