<div class="row" style="background-color: #F5F5F5;padding-top:40px;">
    <div class="col-sm">
        <div class="form-group">
            <input value="{{ optional($company)->name }}" type="text" id="name" name="name" placeholder="{{ trans('app.title', [], $language->locale) }}" class="form-control add-input" required>
        </div>
        <div class="col-sm nopad norightborder">
            <div class="form-group">
                @include('partials.geo.country', ['countryId' => $company->country_id])
            </div>
        </div>
        <div class="col-sm nopad">
            <div class="form-group">
                @include('partials.geo.city', ['countryId' => $company->country_id, 'selectedCityId' => $company->city_id])
            </div>
        </div>
        <div class="form-group">
            <input type="text" value="{{ optional($company)->address }}" id="address" name="address" placeholder="{{ trans('app.address', [], $language->locale) }}" class="form-control add-input" style="margin-top: 10px;" required>
        </div>
        <div class="form-group">
            <div id="map_{{ $language->locale }}" class="map"></div>
        </div>
        <div class="form-group">
            <select name="category_id" id="category_id" class="form-control category" style="width: 100%;">
                @foreach($categories as $category)
                    <option value="{{ $category->id }}" {{ (int)$category->id === (int)$company->category_id ? 'selected' : '' }}>{{ $category->name }}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <textarea name="description" cols="6" id="description" placeholder="{{ trans('app.description', [], $language->locale) }}"
                      class="form-control add-textarea" style="margin-top: 10px;" required>{{ optional($company)->description }}</textarea>
        </div>
        <div class="form-group">
            <label class="add-label mrgbot10">{{ trans('app.company_rights_confirm', [], $language->locale) }}</label>
            <div id="hydra_rights_urls">
                @foreach(optional($company)->rights as $right)
                    <input type="hidden" name="rights_urls[]" value="{{ $right->url }}">
                @endforeach
            </div>
            <div id="files" class="files" style="display: inline-block; float: left;">
                @foreach(optional($company)->rights as $right)
                    <span style="display: inline-block;">
                        <span style="margin-right: 10px;">
                            <canvas width="96" height="110" style="background: url('{{ $right->url }}');background-size: cover;"></canvas>
                        </span>
                    </span>
                @endforeach
            </div>
            <div class="plus-div fileinput-button" style="display: inline-block;">
                <input type="hidden" id="rights_urls">
                <img src="/image/plus.png">
                <input id="rights_fileupload" type="file" name="files[]" multiple>
            </div>
            <div style="clear: both;"></div>
            <div id="progress" class="progress" style="margin-top: 20px;">
                <div class="progress-bar progress-bar-success"></div>
            </div>
        </div>
    </div>
    <div class="col-sm">
        <label class="add-label">
            {{ trans('app.photo', [], $language->locale) }}
        </label>
        <div id="hydra_pictures_urls">
            @foreach(optional($company)->pictures as $picture)
                <input type="hidden" name="pictures_urls[]" value="{{ $picture->url }}">
            @endforeach
        </div>
        <div>
            <div id="hydra_avatar_images">
                @php $count = 1; @endphp
                @foreach(optional($company)->pictures as $picture)
                    <span style="display: inline-block;">
                        <span style="margin-right: 10px;">
                            <canvas width="96" height="110" style="background: url('{{ $picture->url }}');background-size: cover;"></canvas>
                            <a href="#" class="delete_file" onclick="return deleteFileUploaded(this, 1);" data-index="{{ $count }}">{{ trans('app.remove') }}</a>
                        </span>
                    </span>
                    @php ++$count; @endphp
                @endforeach
            </div>
            <div id="hydra_plus" class="plus-div fileinput-button">
                <input type="hidden" id="pictures_urls" />
                <input id="pictures_fileupload" type="file" name="files[]" multiple>
                <img src="/image/plus.png">
            </div>
        </div>
        <div style="clear: both;"></div>
        <small class="form-text text-muted">{{ trans('app.recommended_picture') }}</small>
        <div id="progress_picture" class="progress" style="margin-top: 20px; margin-bottom: 20px;">
            <div class="progress-bar progress-bar-success"></div>
        </div>

        <div class="form-group row">
          <div class="col-sm-3">
            <label class="checkbox-label" >
                <input type="checkbox" name="permissions[animals]" class="radio" value="1" id="animals" {{ optional($company)->animals? 'checked':'' }} >
                <span class="checkbox-icon"></span>
                <span class="checkbox-text">
                    <div class="check-img">
                        <img src="/image/check0.png">
                    </div>
                    {{ trans('app.with_animals', [], $language->locale) }}
                </span>
            </label>
        </div>
          <div class="col-sm-3">
            <label class="checkbox-label" >
                <input type="checkbox" name="permissions[departure]" class="radio" value="1" id="departure" {{ optional($company)->departure? 'checked':'' }}>
                <span class="checkbox-icon"></span>
                <span class="checkbox-text">
                    <div class="check-img">
                        <img src="/image/check1.png">
                    </div>
                    {{ trans('app.with_out', [], $language->locale) }}
                </span>
            </label>
        </div>
          <div class="col-sm-3">
            <label class="checkbox-label" >
                <input type="checkbox" name="permissions[adult]" class="radio" value="1" id="adult" {{ optional($company)->adult ? 'checked':'' }}>
                <span class="checkbox-icon"></span>
                <span class="checkbox-text">
                    <div class="check-img">
                        <img src="/image/check2.png">
                    </div>
                    {{ trans('app.from_18', [], $language->locale) }}
                </span>
            </label>
        </div>
        </div>
        <div class="form-group">
            <label class="add-label">{{ trans('app.company_features', [], $language->locale) }}:</label>
            <textarea name="features" id="features" cols="6" class="form-control add-textarea marbot40" required>{{ optional($company)->features }}</textarea>
        </div>
    </div>
</div>
