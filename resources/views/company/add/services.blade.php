@php
    $intervals = [5, 10, 15, 20, 30];

@endphp
<div class="add-company uslugi-kompanii row" style="background-color: #F5F5F5;">
      <span class="grey-span">{{ trans('app.company_services', [], $language->locale) }}</span>
      <div class="just-grey just-grey-hydra">
          <button type="button" class="bluebutton" onclick="add_a_service_{{ $language->locale }}('{{ $language->locale }}')">{{ trans('app.add_service', [], $language->locale) }}</button>
          <div class="radio-cover2 radio-right2" style="width: auto;">
              <label class="checkbox-label">
                  <input type="checkbox" name="self-decision" class="radio" value="1">
                  <span class="checkbox-icon"></span>
                  <span class="checkbox-text">
                {{ trans('app.allow_ask_self_service', [], $language->locale) }}
              </span>
              </label>
          </div>
      </div>
      <div id="services_block_{{ $language->locale }}" class="card-group">
          @foreach ($company->services as $service)
          <div id="service_{{ $service->id }}" class="card card">
              <div class="card" style="background: #f5f5f5; margin-top: 30px">
                  <input type="hidden" name="services[{{ $service->id }}][id]" value="{{ $service->id }}" />
                  <div class="card-body">
                    <div class="row">
                      <div class="col-sm">
                          <div class="form-group">
                              <label class="add-label">{{ trans('app.title', [], $language->locale) }}</label>
                              <input type="text" name="services[{{ $service->id }}][name]" value="{{ $service->name }}" class="add-input" required>
                          </div>
                          <div class="form-group">
                              <label class="add-label">{{ trans('app.description', [], $language->locale) }}</label>
                              <textarea name="services[{{ $service->id }}][description]" cols="6" class="add-textarea marbot40">{{ $service->description }}</textarea>
                          </div>
                          <div class="form-group">
                            <label class="add-label">{{ trans('app.interval', [], $language->locale) }}</label>
                      			<select name="services[{{ $service->id }}][interval]" class="add-input">
                      				@foreach($intervals as $interval)
                      				   <option value='{{ $interval }}' {{ $service->interval == $interval ? 'selected' : '' }}>{{ $interval }}</option>
                      			   @endforeach
                      			</select>
                          </div>
                      </div>
                      <div class="col-sm">
                          <div class="form-group">
                              <label class="add-label">{{ trans('app.price', [], $language->locale) }}</label>
                              <input type="number" name="services[{{ $service->id }}][price]" value="{{ $service->price }}" class="add-input" required>
                          </div>
                          <label class='add-label'>{{ trans('app.service_duration', [], $language->locale) }}</label>
                          <div class="form-group row">
                              <div class="col-sm-3">
                                  <select class='form-control' name="services[{{ $service->id }}][duration_days]" id="duration_days_{{ $language->locale }}">
                                    @for($i = 0; $i < 50; $i++)
                                      <option value='0'>{{ trans('app.days') }}</option>
                                      <option value='{{ $i }}' {{ $i == $service->days ? 'selected' : '' }}>{{ $i }}</option>
                                    @endfor
                                  </select>
                              </div>
                              <div class="col-sm-3">
                                  <select class="form-control" name="services[{{ $service->id }}][duration_hours]" id="duration_hours_{{ $language->locale }}">
                                      <option value='0'>{{ trans('app.hours') }}</option>
                                      @for($i = 0; $i < 24; $i++)
                                          <option value='{{ $i }}' {{ $i == $service->hours ? 'selected' : '' }}>{{ $i }}</option>
                                      @endfor
                                  </select>
                              </div>
                              <div class="col-sm-3">
                                  <select name="services[{{ $service->id }}][duration_minutes]" id="duration_minutes_" class='form-control'>
                                      <option value='0'>{{ trans('app.minutes') }}</option>
                                      @for($i = 0; $i < 60; $i+=5)
                                          <option value='{{ $i }}' {{ $i == $service->minutes ? 'selected' : '' }}>{{ $i }}</option>
                                      @endfor
                                  </select>
                              </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-sm-3">
                                <label class="checkbox-label">
                                    <input type="checkbox" name="services[{{ $service->id }}][permissions][animals]" class="radio" value="1" {{ $service->animals ? 'checked' : ''}}>
                                    <span class="checkbox-icon"></span>
                                    <span class="checkbox-text">
                                        <div class="check-img">
                                            <img src="/image/check0.png">
                                        </div> {{ trans('app.with_animals', [], $language->locale) }}
                                    </span>
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <label class="checkbox-label">
                                    <input type="checkbox" name="services[{{ $service->id }}][permissions][departure]" class="radio" value="1" {{ $service->departure ? 'checked' : '' }}>
                                    <span class="checkbox-icon"></span>
                                    <span class="checkbox-text">
                                        <div class="check-img">
                                            <img src="/image/check1.png">
                                        </div> {{ trans('app.with_out', [], $language->locale) }}
                                    </span>
                                </label>
                            </div>
                            <div class="col-sm-3">
                                <label class="checkbox-label">
                                    <input type="checkbox" name="services[{{ $service->id }}][permissions][adult]" class="radio" value="1" {{ $service->adult ? 'checked' : '' }}>
                                    <span class="checkbox-icon"></span>
                                    <span class="checkbox-text">
                                        <div class="check-img"><img src="/image/check2.png">
                                        </div> {{ trans('app.from_18', [], $language->locale) }} </span>
                                </label>
                            </div>
                          </div>
                      </div>
                      <div class="right-card">
                          <img src="/image/h-close.png" class="close-uslugi" onclick='delete_a_service("{{ $service->id }}")'>
                      </div>
                    </div>
                  </div>
              </div>
          </div>
          @endforeach
    </div>
</div>
<div id="service-template" style="display:none;">
<div id="service_#service_id#" class="card card">
    <div class="card" style="background: #f5f5f5; margin-top: 30px">
        <input type="hidden" name="services[#service_id#][id]" value="#service_id#" />
        <div class="card-body">
          <div class="row">
            <div class="col-sm">
                <div class="form-group">
                    <label class="add-label">{{ trans('app.title', [], $language->locale) }}</label>
                    <input type="text" name="services[#service_id#][name]" class="add-input" required>
                </div>
                <div class="form-group">
                    <label class="add-label">{{ trans('app.description', [], $language->locale) }}</label>
                    <textarea name="services[#service_id#][description]" cols="6" class="add-textarea marbot40"></textarea>
                </div>
                <div class="form-group">
                  <label class="add-label">{{ trans('app.interval', [], $language->locale) }}</label>
                  <select name="services[#service_id#][interval]" class="add-input">
                    @foreach($intervals as $interval)
                       <option value='{{ $interval }}'>{{ $interval }}</option>
                     @endforeach
                  </select>
                </div>
            </div>
            <div class="col-sm">
                <div class="form-group">
                    <label class="add-label">{{ trans('app.price', [], $language->locale) }}</label>
                    <input type="number" name="services[#service_id#][price]" value="" class="add-input" required>
                </div>
                <label class='add-label'>{{ trans('app.service_duration', [], $language->locale) }}</label>
                <div class="form-group row">
                    <div class="col-sm-3">
                        <select class='form-control' name="services[#service_id#][duration_days]" id="duration_days_{{ $language->locale }}">
                          @for($i = 0; $i < 50; $i++)
                            <option value='0'>{{ trans('app.days') }}</option>
                            <option value='{{ $i }}'>{{ $i }}</option>
                          @endfor
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="services[#service_id#][duration_hours]" id="duration_hours_{{ $language->locale }}">
                            <option value='0'>{{ trans('app.hours') }}</option>
                            @for($i = 0; $i < 24; $i++)
                                <option value='{{ $i }}'>{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <select name="services[#service_id#][duration_minutes]" id="duration_minutes_" class='form-control'>
                            <option value='0'>{{ trans('app.minutes') }}</option>
                            @for($i = 0; $i < 60; $i+=5)
                                <option value='{{ $i }}'>{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                  <div class="col-sm-3">
                      <label class="checkbox-label">
                          <input type="checkbox" name="services[#service_id#][permissions][animals]" class="radio" value="1">
                          <span class="checkbox-icon"></span>
                          <span class="checkbox-text">
                              <div class="check-img">
                                  <img src="/image/check0.png">
                              </div> {{ trans('app.with_animals', [], $language->locale) }}
                          </span>
                      </label>
                  </div>
                  <div class="col-sm-3">
                      <label class="checkbox-label">
                          <input type="checkbox" name="services[#service_id#][permissions][departure]" class="radio" value="1">
                          <span class="checkbox-icon"></span>
                          <span class="checkbox-text">
                              <div class="check-img">
                                  <img src="/image/check1.png">
                              </div> {{ trans('app.with_out', [], $language->locale) }}
                          </span>
                      </label>
                  </div>
                  <div class="col-sm-3">
                      <label class="checkbox-label">
                          <input type="checkbox" name="services[#service_id#][permissions][adult]" class="radio" value="1">
                          <span class="checkbox-icon"></span>
                          <span class="checkbox-text">
                              <div class="check-img"><img src="/image/check2.png">
                              </div> {{ trans('app.from_18', [], $language->locale) }} </span>
                      </label>
                  </div>
                </div>
            </div>
            <div class="right-card">
                <img src="/image/h-close.png" class="close-uslugi" onclick='delete_a_service("#service_id#")'>
            </div>
          </div>
        </div>
    </div>
</div>
</div>
<script>
    function add_a_service_{{ $language->locale }}(lang) {
        var services_counter = {{ $service ? $service->id : 0 }} + 1;
        $("#services_block_"+lang).append($('#service-template').html().replace(/#service_id#/g, services_counter));

        $('#duration_days_' + lang + services_counter).select2({placeholder: "{{ trans('app.days', [], $language->locale) }}"});
        $('#duration_hours_' + lang + services_counter).select2({placeholder: "{{ trans('app.hours', [], $language->locale) }}"});
        $('#duration_minutes_' + lang + services_counter).select2({placeholder: "{{ trans('app.minutes', [], $language->locale) }}"});
        services_counter++;
    }

    function delete_a_service(number) {
        $("#service_" + number).hide('slow', function(){ $("#service_" + number).remove(); });
    }

    function submitCompany() {
        $('form.form').each(function () {
            var form = this;
            $.ajax({
                method: 'POST',
                data: $(form).serialize(),
                beforeSend: function() {
                    $('.form-group.has-error').removeClass('has-error');
                },
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    window.location.href = {!! $company->exists ? "'/company/".$company->id."'" : '/profile' !!};
                },
                error: function (data) {
                    var fields = data.responseJSON.errors;
                    var count = 0;
                    $.each(fields, function(index, value) {
                        var selector = $('#'+index);
                        selector.parent().addClass('has-error');
                        if (count === 0) {
                            $('html, body').animate({
                                scrollTop: selector.offset().top
                            }, 300, function () {
                            });
                        }
                        ++count;
                    });
                }
            })
        });
        return false;
    }
</script>
