@extends('partials.app')

@section('content')
    <div class="aksii">
        <div class="container">
            <span class="add-span">{{ trans('app.service_catalog') }}</span>
            <div class='row'>
              <div class="col-sm nopad">
                  @php $counter = 0; @endphp
                  @foreach($categories as $category)
                  @php $counter++; @endphp
                  @continue($counter % 4 != 1)
                  <div class="cu-div">
                      @if(empty($category->image2))
                          <img src="/image/cu-3.png" />
                      @else
                          <img style="width: 40px" src="/uploads/{{ $category->image2 }}">
                      @endif
                      <a href="/category/{{$category->url}}"><span class="cu-span">{{ $category->name }}</span></a>
                      <p>
                          @foreach($sub_categories[$category->id] as $sub_category)
                              <a href="/category/{{$sub_category->url}}">{{$sub_category->name}}</a>
                              <br/>
                          @endforeach
                      </p>
                  </div>
                  @endforeach
              </div>
              <div class="col-sm nopad">
                  @php $counter = 0; @endphp
                  @foreach($categories as $category)
                      @php $counter++; @endphp
                      @continue($counter % 4 != 2)
                      <div class="cu-div">
                          @if(empty($category->image2))
                              <img src="/image/cu-3.png" />
                          @else
                              <img style="width: 40px" src="/uploads/{{ $category->image2 }}">
                          @endif
                          <a href="/category/{{$category->url}}"><span class="cu-span">{{ $category->name }}</span></a>
                          <p>
                              @foreach($sub_categories[$category->id] as $sub_category)<a href="/category/{{$sub_category->url}}">{{$sub_category->name}}</a><br/>@endforeach
                          </p>
                      </div>
                  @endforeach
              </div>
              <div class="col-sm nopad">
                  @php $counter = 0; @endphp
                  @foreach($categories as $category)
                      @php $counter++; @endphp
                      @continue($counter % 4 != 3)
                      <div class="cu-div">
                          @if(empty($category->image2))
                              <img src="/image/cu-3.png" />
                          @else
                              <img style="width: 40px" src="/uploads/{{ $category->image2 }}">
                          @endif
                          <a href="/category/{{$category->url}}"><span class="cu-span">{{ $category->name }}</span></a>
                          <p>
                              @foreach($sub_categories[$category->id] as $sub_category)<a href="/category/{{$sub_category->url}}">{{$sub_category->name}}</a><br/>@endforeach
                          </p>
                      </div>
                  @endforeach
              </div>
              <div class="col-sm nopad">
                  @php $counter = 0; @endphp
                  @foreach($categories as $category)
                      @php $counter++; @endphp
                      @continue($counter % 4 != 0)
                      <div class="cu-div">
                          @if(empty($category->image2))
                              <img src="/image/cu-3.png" />
                          @else
                              <img style="width: 40px" src="/uploads/{{ $category->image2 }}">
                          @endif
                          <a href="/category/{{$category->url}}"><span class="cu-span">{{ $category->name }}</span></a>
                          <p>
                              @foreach($sub_categories[$category->id] as $sub_category)<a href="/category/{{$sub_category->url}}">{{$sub_category->name}}</a><br/>@endforeach
                          </p>
                      </div>
                  @endforeach
              </div>
            </div>
        </div>
    </div>
    @include('partials.how_to_work')
@endsection

@section('css')

@endsection

@section('script')
    <script>
        $(".start").click(function () {
            $('html, body').animate({scrollTop:0}, 'slow', function() {
                $("#search").focus();
            });
        });
    </script>

@endsection
