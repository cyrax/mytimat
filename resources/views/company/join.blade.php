@extends('partials.app')

@section('content')
    <div class="add-company">
        <div class="container">
            <h3 class="text-center">
                {{ trans('app.contact_company_administrator') }}
            </h3>
        </div>
    </div>
@endsection