@extends('partials.app')

@section('content')
    <div class="aksii marbot50">
        <div class="container">
            <span class="add-span">{{ trans('app.companies') }}</span>
            @foreach($companies as $company)
            <div class="cies">
                <div class="col">
                    <p><a href="/company/{{ $company->id }}" target="_blank">{{ $company->name }}</a> <span>ID: {{ $company->id }}</span>
                        <span>{{ trans('app.'.$company->getStatus()) }}</span>
                    </p>
                </div>
                <div class="col textright">
                    <span class="quan-cies">{{ trans('app.count_of_services') }}: {{ $company->services()->count() }}</span>
                    <a href="{{ route('company-edit', ['id' => $company->id]) }}">{{ trans('app.edit') }}</a>
                </div>
            </div>
            @endforeach
            {{ $companies->links() }}
        </div>
    </div>
@endsection

@section('css')

@endsection

@section('script')

@endsection