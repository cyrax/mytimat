@extends('partials.app')

@section('content')
    <div class="container">
        <div class="card card-danger">
            <div class="card-header">
                {{ trans('app.company_not_approved') }}
            </div>
        </div>
    </div>
@endsection