@extends('partials.app')

@section('content')

@php
$languages = [];
$selectableLanguages[] = $currentLanguage;
$languages = $selectableLanguages->reverse();
$currentLanguages = [$currentLanguage];
@endphp

<div class="add-company">
    <div class="container">
        <span class="add-span">{{ trans('app.add_company') }}</span>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="grey-admin">
            <span class="grey-span">{{ trans('app.description') }}</span>
            <div id="tabs" class="tabs">
                <nav>
                    <ul>
                        @foreach($languages as $language)
                            <li id="tab_{{ $language->locale }}" class="{{ $language->locale === $currentLanguage->locale ? 'tab-current' : '' }}">
                                <a href="/{{ $language->locale }}/company/{{ !$company->exists ? 'add' : $company->id.'/edit' }}">
                                    <span>
                                        <img src="/image/{{ $language->locale }}.png">{{ $language->name }}
                                    </span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </nav>
                <div class="content">
                    @foreach($currentLanguages as $language)
                        <section id="section-{{ $language->locale }}" class="content-current">
                            <form method="POST" enctype="multipart/form-data" class="form" id="form-{{ $language->locale }}">
                                {{csrf_field()}}
                                <input type="hidden" name="lang" value="{{ $language->locale }}">
                                @if (Auth::user()->administrator)
                                    <div class="card" style="background:transparent">
                                        <div class="card-body">
                                            <label class="radio-inline">
                                                <input type="radio" name="approved" value="0" {{ $company->approved == 0 ? 'checked' : '' }}>{{ trans('app.not_accepted') }}
                                            </label>
                                            <label class="radio-inline">
                                                <input type="radio" name="approved" value="1" {{ $company->approved == 1 ? 'checked' : '' }}>{{ trans('app.accepted') }}
                                            </label>
                                        </div>
                                    </div>
                                @endif
                                {!! view('company.add.info', compact('countries', 'languages', 'categories', 'language', 'company')) !!}
                                {!! view('company.add.contacts', compact('language', 'company'))  !!}
                                {!! view('company.add.services', compact('language', 'categories', 'company')) !!}
                            </form>
                            <button type="submit" onclick="return submitCompany()" class="oplatit-balance">{{ trans('app.submit', [], $language->locale) }}</button>
                        </section>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

<div class="dm-overlay" id="add_non_working_date">
    <div class="dm-table">
        <div class="dm-cell">
            <div class="dm-modal">
                <a href="#close" class="close"></a>
                <p style="font-size: 18px; color: #111;">{{ trans('app.choose_weekends') }}</p>
                <p>
                    <input id="non_working_day" type="date" class="add-input" min="{{ \Carbon\Carbon::now()->toDateString() }}" autofocus>
                </p>
                <p><button type="button" class="aksii-search" style="float: none;" onclick="add_non_working_day()">{{ trans('app.add') }}</button></p>
                <div id="temp_non_days"></div>
                <p><a href="#close"><button type="button" class="aksii-search aksii-search-select"><i class="fa fa-check"></i> {{ trans('app.save') }}</button></a></p>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="/js/cbpFWTabs.js"></script>
    <script>
        $('#the_city').select2();
        $('.category').select2({ placeholder: "{{ trans('app.choose_category') }}" });
        $('.country').select2({placeholder: "{{ trans('app.choose_country') }}"});
        $('.city').select2({placeholder: "{{ trans('app.choose_city') }}"});

        $("#country_id").change(function () {
            var country = $("#country_id").val();
            if(country === 0) {
                $("#city_id").html("<option value=\"\">{{ trans('app.choose_city') }}</option>");
            } else {
                $.ajax({
                    url: "/get-cities/"+ country
                }).done(function( msg ) {
                    $("#city_id").html("<option value=\"\">{{ trans('app.choose_city') }}</option>");
                    if(msg){
                        msg = $.parseJSON(msg);
                        msg.forEach(function (t) {
                            $("#city_id").append("<option value=\"" + t.id + "\">" + t.name + "</option>");
                        });
                    } else {
                        $("#city_id").html("<option value=\"0\">{{ trans('app.choose_city') }}</option>");
                        alert("{{ trans('app.no_city_in_this_country') }}");
                    }
                }).fail(function() {
                    alert('{{ trans('app.error') }}');
                });
            }
        });

        function hydra_days_change(day) {
            if($("input[name="+day+"]").is(':checked')){
                $("#hydra-start-end-"+day).show();
            } else {
                $("#hydra-start-end-"+day).hide();
            }
        }

        function hydra_lunch_change(day) {
            if($("input[name=lunch]").is(':checked')){
                $("#hydra-start-end-lunch").hide();
            } else {
                $("#hydra-start-end-lunch").show();
            }
        }

        var non_working_days = [];
        @foreach($company->nonworkingdays()->get() as $nonworkingday)
                non_working_days.push('{{ $nonworkingday->date }}');
        @endforeach

        function delete_a_holiday(ind) {
            non_working_days.splice(ind, 1);
            refresh_non_working_days();
        }


        function add_non_working_day() {
            var a_day = $("#non_working_day").val();
            if( a_day === false ) {
                alert('{{ trans('app.choose_date') }}');
                return false;
            }
            if( a_day < '{{ \Carbon\Carbon::now()->toDateString() }}' ) {
                alert('{{ trans('app.chosen_date_already_passed') }}');
                return false;
            }
            if(non_working_days.indexOf( a_day ) === -1 ) {
                non_working_days.push(a_day);
                non_working_days = non_working_days.sort();
                refresh_non_working_days(a_day);
            } else {
                alert('{{ trans('app.already_chase_date') }}');
            }
        }

        function refresh_non_working_days() {
            var the_list_non = '';
            var the_temp_list_non = '';
            var the_hidden_input_non = '';
            non_working_days.forEach(function (data, index) {
                the_list_non = the_list_non + '<span id="holiday-'+index+'" class="holiday-day">'+data+'<img src="/image/h-close.png" onclick="delete_a_holiday(\''+index+'\')"></span>';
                the_temp_list_non = the_temp_list_non + '<span id="temp-holiday-'+index+'" class="holiday-day">'+data+'<img src="/image/h-close.png" onclick="delete_a_holiday(\''+index+'\')"></span><br>';
                the_hidden_input_non = the_hidden_input_non + '<input type="hidden" name="non_working_days[]" value="'+data+'">';
            });
            $("#temp_non_days").html(the_temp_list_non);
            $("#inputs_non_days").html(the_hidden_input_non);
            $("#list_of_non_days").html(the_list_non);
        }
    </script>


    <script src="/file_upload/js/vendor/jquery.ui.widget.js"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="/js/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="/file_upload/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="/file_upload/js/jquery.fileupload.js"></script>
    <!-- The File Upload processing plugin -->
    <script src="/file_upload/js/jquery.fileupload-process.js"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="/file_upload/js/jquery.fileupload-image.js"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="/file_upload/js/jquery.fileupload-audio.js"></script>
    <!-- The File Upload video preview plugin -->
    <script src="/file_upload/js/jquery.fileupload-video.js"></script>
    <!-- The File Upload validation plugin -->
    <script src="/file_upload/js/jquery.fileupload-validate.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&key=AIzaSyDV61DqeVe7pKi5H5cQNmRZbHvX-DPQ59I"></script>
    <script>
        /*jslint unparam: true, regexp: true */
        /*global window, $ */
        $(function () {
            'use strict';
            // Change this to the location of your server-side upload handler:
            var url = '/company/rights-picture';
            var $rights = $('#rights_fileupload');
            $rights.fileupload({
                url: url,
                dataType: 'json',
                autoUpload: true,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 999000,
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                disableImageResize: false, ///Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                previewMaxWidth: 96,
                previewMaxHeight: 110,
                imageMaxWidth: 600,
                imageMaxHeight: 400,
//                imageCrop: true,
                previewCrop: true,
                maxNumberOfFiles: 6
            }).on('fileuploadadd', function (e, data) {
                data.context = $('<span/>').appendTo('#files');
                $.each(data.files, function (index, file) {
                    var node = $('<span style="margin-right: 10px;display:inline-block;" />');
                    node.append('<a href="#" class="delete_file" style="display:block;" onclick="return deleteFileUploaded(this);" data-index="'+index+'">Remove</a>');
                    node.appendTo(data.context);
                });
            }).on('fileuploadprocessalways', function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.error) {
                    alert("{{ trans('error.uploading_file') }}: \n"+file.error+"\n\n {{ trans('app.filename') }}: \n"+file.name);
                } else if (file.preview) {
                    node.prepend(file.preview);
                }
                if (index + 1 === data.files.length) {
                    data.context.find('button')
                        .text('Upload')
                        .prop('disabled', !!data.files.error);
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }).on('fileuploaddone', function (e, data) {
                $.each(data.result.files, function (index, file) {
                    if (file.url) {
                        $("#hydra_rights_urls").append($('<input type="hidden" name="rights_urls[]" value="'+file.url+'"/>'));
                    } else if (file.error) {
                        alert(file.error);
                        $(data.context).html('');
                    }
                });
            }).on('fileuploadfail', function (e, data) {
                $.each(data.files, function () {
                    $(data.context).html('');
                    alert("{{ trans('error.uploading_file') }}: \n" + data.files[0].name);
                });
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });

        $(function () {
            'use strict';
            // Change this to the location of your server-side upload handler:
            var url_picture = '/company/pictures-upload';
            var the_pictures_count = 0;
            $('#pictures_fileupload').fileupload({
                url: url_picture,
                dataType: 'json',
                autoUpload: true,
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 99900000,
                // Enable image resizing, except for Android and Opera,
                // which actually support image resizing, but fail to
                // send Blob objects via XHR requests:
                disableImageResize: /Android(?!.*Chrome)|Opera/
                    .test(window.navigator.userAgent),
                previewMaxWidth: 500,
                previewMaxHeight: 340,
                previewCrop: true,
                maxNumberOfFiles: 10,
                imageMaxWidth: 600,
                imageMaxHeight: 400,
                imageCrop: true,
            }).on('fileuploadadd', function (e, data) {
                var newFileDiv = $('<div class="hydra_uploaded_thumbnails"/>').appendTo('#hydra_avatar_images');

                $.each(data.files, function (index, file) {
                    var node = $('<span/>').append('<a href="#" class="delete_file" onclick="return deleteFileUploaded(this);" data-index="'+index+'">{{ trans('app.remove') }}</a>');
                    node.appendTo(newFileDiv);
                });

                data.context = newFileDiv
            }).on('fileuploadprocessalways', function (e, data) {
                var index = data.index,
                    file = data.files[index],
                    node = $(data.context.children()[index]);
                if (file.error) {
                    alert("{{ trans('error.uploading_file') }}: \n"+file.error+"\n\n {{ trans('app.filename') }}: \n"+file.name);
                } else if (file.preview) {
                    node.prepend(file.preview);
                }
                the_pictures_count = the_pictures_count + 1;
                if (the_pictures_count > 4) {
                    $('#hydra_plus').remove();
                }
            }).on('fileuploadprogressall', function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress_picture .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }).on('fileuploaddone', function (e, data) {
                $.each(data.result.files, function (index, file) {
                    if (file.url) {
                        $("#hydra_pictures_urls").append($('<input type="hidden" name="pictures_urls[]" value="'+file.url+'"/>'));
                    } else if (file.error) {
                        alert(file.error);
                        $(data.context).html('');
                    }
                });
            }).on('fileuploadfail', function (e, data) {
                $.each(data.files, function () {
                    $(data.context).html('');
                    alert("{{ trans('error.uploading_file') }}: \n" + data.files[0].name);
                });
            }).prop('disabled', !$.support.fileInput)
                .parent().addClass($.support.fileInput ? undefined : 'disabled');
        });
        function deleteFileUploaded(context, is_new = 0) {
            let index = $(context).data('index');
            $('#hydra_pictures_urls').find('input:nth-child('+index+')').remove();
            if (!is_new)
                $(context).parents('.hydra_uploaded_thumbnails').remove();
            else {
                $('#hydra_avatar_images > span:nth-child('+index+')').remove();
            }
            return false;
        }
    </script>
    <script>
        var almaty = {lat: 43.2467582, lng: 76.871403};
        var map = new google.maps.Map(document.getElementById('map_{{ $language->locale }}'), {
            zoom: 10,
            center: almaty
        });
        var geocoder = new google.maps.Geocoder();

        var marker = new google.maps.Marker({
            position: almaty,
            map: map
        });
        var markersArray = [marker];
        $('#form-{{ $language->locale }} [name=address]').on('input', function () {
            changeMap();
        });
        function changeMap()
        {
            clearOverlays();
            geocoder.geocode( { 'address': $('#city_id :selected').text()+', ' + $("[name=address]").val()}, function(results, status) {
                if (status === 'OK') {
                    map.setCenter(results[0].geometry.location);
                    var marker = new google.maps.Marker({
                        map: map,
                        position: results[0].geometry.location
                    });
                    markersArray.push(marker);
                } else {
                    // alert('Geocode was not successful for the following reason: ' + status);
                }
            });
        }
        function clearOverlays() {
            for (var i = 0; i < markersArray.length; i++ ) {
                markersArray[i].setMap(null);
            }
            markersArray.length = 0;
        }
        google.maps.event.addListener(map, 'click', function(event) {
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        var address = results[0].formatted_address;
                        address = address.substring(0, address.lastIndexOf(","));
                        address = address.substring(0, address.lastIndexOf(","));
                        $('[name=address]').val(address);
                    }
                }
            });
        });
        @if ($company->exists)
            changeMap();
        @endif
    </script>
@stop

@section('css')
    <link href="/css/tab.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('file_upload/css/jquery.fileupload.css') }}">
    <style>
        .map {
            width: 100%;
            height: 400px;
            background-color: grey;
        }
        .has-error .select2-selection {
            border-color: rgb(185, 74, 72) !important;
        }
        .has-error.plus-div {
            border-color: rgb(185, 74, 72) !important;
        }
    </style>
@endsection
