@extends('partials.app')

@section('content')
    <div class="aksii podarki members">
        <div class="container">
            <form action="/company/members" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <span class="add-span">{{ trans('app.employee_management') }}</span>
                @if(session('no_changes'))
                    <div class="alert alert-warning"><b>{{ trans('app.no_changes') }}</b></div>@endif
                @if(session('changes_saved'))
                    <div class="alert alert-success"><b>{{ trans('app.changes_saved') }}</b></div>@endif
                @if(session('member_added'))
                    <div class="alert alert-success"><b>{{ trans('app.employee_added') }}</b></div>@endif
                @if(session('member_adding_error'))
                    <div class="alert alert-warning"><b>{{ trans('error.technical_problems') }} {{ trans('error.member_not_added') }} {{ trans('error.contact_tech_support') }}</b></div>@endif
                <div class="row aksii-table-head">
                    <div class="col-md col">
                        <div class="a-t-text">{{ trans('app.name') }}</div>
                    </div>
                    <div class="col-md col">
                        <div class="a-t-text">{{ trans('app.phone') }}</div>
                    </div>
                    <div class="col-md d-none d-sm-block">
                        <div class="a-t-text">{{ trans('app.group') }}</div>
                    </div>
                    <div class="col-md d-none d-sm-block">
                        <div class="a-t-text">{{ trans('app.role') }}</div>
                    </div>
                    <div class="col-md col">
                        <div class="a-t-text">{{ trans('app.accepts_orders') }}</div>
                    </div>
                    <div class="col d-none d-sm-block">
                        <div class="a-t-text">{{ trans('app.actions') }}</div>
                    </div>
                </div>
                <div class="podarki-cover">
                    @foreach($data['specialists'] as $specialist)
                        @php
                            $memberGroups = \App\Member::getMemberGroups($specialist->id);
                        @endphp
                        <div class="podarki-list row" id="specialist_block_{{ $specialist->id }}">
                            <div class="col-md col name-pod-list @if(\Carbon\Carbon::parse($specialist->updated_at)->diffInMinutes() > 3) @php $offline = 1; @endphp offline-pod-list @else @php $offline = 0; @endphp @endif">
                                <span></span><a href="{{ route('specialist-profile', ['id' => $specialist->id]) }}">{{ $specialist->name }} {{ $specialist->last_name }}</a>
                            </div>
                            <div class="col-md col nopad">
                                {{ optional($specialist->user)->phone }}
                            </div>
                            <div class="col-md d-none d-sm-block">
                                @foreach($memberGroups as $memberGroup)
                                    {{ \App\Member::getMemberGroup($memberGroup) }}
                                @endforeach
                            </div>
                            <div class="col-md col">
                                {{ optional($specialist->roles)->name }}
                            </div>
                            <div class="col-md col">
                                {{ App\Member::isAccepts($specialist->accepts) }}
                            </div>
                            <div class="col-md col actions">
                                <button type="button" class="btn btn-primary"
                                        onclick="edit_user({{ $specialist->id }}, {{ addslashes($offline) }},'{{ addslashes($specialist->name) }}', '{{ addslashes($specialist->last_name) }}', '{{ addslashes($specialist->user->phone) }}', '{{ addslashes(implode(',', $memberGroups)) }}', {{ addslashes($specialist->accepts) }}, {{ addslashes($specialist->role) }})">
                                    <i class="fa fa-pencil"></i> {{ trans('app.edit') }}
                                </button>
                                <button type="button" class="btn btn-danger"
                                        onclick="delete_member({{ $specialist->id }}, '{{ addslashes($specialist->name) }} {{ addslashes($specialist->second_name) }}')">
                                    <i class="fa fa-times"></i> {{ trans('app.delete') }}
                                </button>
                            </div>
                        </div>
                    @endforeach
                    <div class="mem-add-but-cov">
                        <button class="add-button" type="submit">
                            {{ trans('app.save') }}
                        </button>
                        <a href="#add_member" class="float-right">
                            <button class="add-podarok" type="button">{{ trans('app.add_employee') }}</button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="aksii podarki members">
        <div class="container">
            <form action="/company/roles" method="post">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="PUT">
                <span class="add-span">{{ trans('app.manage_roles') }}</span>
                <div class="row aksii-table-head">
                    <div class="col-md col">
                        <div class="a-t-text">{{ trans('app.roles') }}</div>
                    </div>
                </div>
                <div class="podarki-cover">
                    @foreach($data['roles'] as $role)
                        <div class="podarki-list row" id="roles_block_{{ $role->id }}">
                            <div class="col-md">
                                {{ $role->name }}
                            </div>
                            <div class="col-md col actions">
                                <button type="button" class="btn btn-primary"
                                        onclick="edit_role({{ $role->id }},'{{ addslashes($role->name) }}')">
                                    <i class="fa fa-pencil"></i> {{ trans('app.edit') }}
                                </button>
                                <button type="button" class="btn btn-danger"
                                        onclick="delete_role({{ $role->id }}, '{{ $role->name }}')">
                                    <i class="fa fa-times"></i> {{ trans('app.delete') }}
                                </button>
                            </div>
                        </div>
                    @endforeach
                    <div class="mem-add-but-cov">
                        <button class="add-button" type="submit">
                            {{ trans('app.save') }}
                        </button>
                        <a href="#add_role" class='float-right'>
                            <button class="add-podarok" style="margin-right: 15px;" type="button">{{ trans('app.add') }} {{ trans('app.role') }}</button>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="dm-overlay" id="add_member">
        <div class="dm-table">
            <div class="dm-cell">
                <form action="/company/members/store" method="post">
                    {{ csrf_field() }}
                    <div class="dm-modal" style="color: #111111; text-align: left;">
                      <a href="#close" class="close"></a>
                        <h3>{{ trans('app.add_employee') }}</h3>
                        @if ($errors->add_member->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->add_member->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <p>
                            <small>{{ trans('app.sms_password_entered_number') }}<br>{{ trans('app.user_access_using_that_number') }}
                            </small>
                        </p>
                        <p><input @if ($errors->add_member->has('phone')) style="border: 1px red solid;"
                                  @endif type="tel" class="add-input" name="phone" placeholder="{{ trans('app.enter_employee_number') }}"
                                  value="{{ old('phone') }}"></p>
                        <p><input @if ($errors->add_member->has('name')) style="border: 1px red solid;"
                                  @endif type="text" class="add-input" name="name" placeholder="{{ trans('app.name') }}"
                                  value="{{ old('name') }}"></p>
                        <p><input @if ($errors->add_member->has('surname')) style="border: 1px red solid;"
                                  @endif type="text" class="add-input" name="surname" placeholder="{{ trans('app.lastname') }}"
                                  value="{{ old('surname') }}"></p>
                        <p>
                            <select name="group_type" required id="group_type"
                                    style="width: 100%; @if ($errors->add_member->has('group_type')) border: 1px red solid; @endif ">
                                <option value="">{{ trans('app.group') }}</option>
                                <option value="master" @if(old('group_type') === "master") selected @endif >{{ trans('app.specialist') }}
                                </option>
                                <option value="manage_specialist"
                                        @if(old('group_type') === "manage_specialist") selected @endif >{{ trans('app.manager') }}
                                </option>
                                @if(Auth::user()->group == 'company_admin')
                                    <option value="the_site_admin"
                                            @if(old('group_type') === "the_site_admin") selected @endif >{{ trans('app.administrator') }}
                                    </option>@endif
                            </select>
                        </p>
                        <p>
                            <select name="specialist" required id="specialist_select"
                                    style="width: 100%; @if ($errors->add_member->has('specialist')) border: 1px red solid; @endif ">
                                <option value="1" @if(old('specialist') === "1") selected @endif>{{ trans('app.accepts_orders') }}
                                </option>
                                <option value="0" @if(old('specialist') === "0") selected @endif>{{ trans('app.does_not_accept') }}</option>
                            </select>
                        </p>
                        <p>
                            <button type="submit" class="aksii-search aksii-search-select"><i class="fa fa-check"></i>
                                {{ trans('app.add') }}
                            </button>
                        </p>
                        <p style="margin: 15px 0 0;"><a href="#close">{{ trans('app.cancel') }}</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="dm-overlay" id="add_role">
        <div class="dm-table">
            <div class="dm-cell">
                <form action="/company/role/store" method="post">
                    {{ csrf_field() }}
                    <div class="dm-modal" style="color: #111111; text-align: left;">
                      <a href="#close" class="close"></a>
                        <h3>{{ trans('app.add_role') }}</h3>
                        @if ($errors->add_member->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->add_member->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <p>
                            <input type="text" class="add-input" name="role" placeholder="{{ trans('app.role') }}"
                                   value="{{ old('role') }}"/>
                        </p>

                        <p>
                            <button type="submit" class="aksii-search aksii-search-select"><i class="fa fa-check"></i>
                                {{ trans('app.add') }}
                            </button>
                        </p>
                        <p style="margin: 15px 0 0;"><a href="#close">{{ trans('app.cancel') }}</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('css')

@endsection

@section('script')
    <script>
        $("#group_type").select2();
        $("#specialist_select").select2();

        function edit_role(id, name) {
            $("#default_block_" + id).hide();

            var block = "<div id=\"roles_edit_block_" + id + "\">" +
                "<input type='hidden' class='add-input text-pad-in' name='roles[" + id + "][id]' value='" + id + "'>" +
                "<div class='col name-pod-list'>" +
                "<input name='roles[" + id + "][name]' type='text' value='"+name+"' /></div>";

            block = block +
                "<div class=\"col\">" +
                "<button type=\"button\" class=\"btn btn-warning\" onclick=\"cancel_edit_role(" + id + ", '" + name + "')\">" +
                "<i class=\"fa fa-ban\"></i> {{ trans('app.cancel') }}" +
                "</button>" +
                "</div></div>";

            $("#roles_block_" + id).append(block);
        }

        function edit_user(id, offline, name, second_name, phone, group, specialist, role) {
            $("#default_block_" + id).hide();

            var block = "<div id=\"members_edit_block_" + id + "\"><input type='hidden' name='specialists[" + id + "][id]' value='" + id + "'><div class=\"col name-pod-list";
            if (offline) {
                block = block + " offline-pod-list";
            }
            block = block + "\"><span></span>" + name + " " + second_name + "</div>\n" +
                "<div class=\"col\"><input type=\"tel\" name=\"specialists[" + id + "][phone]\" value=\"" + phone + "\" class=\"add-input text-pad-in\" required></div>\n" +
                "<div class=\"col\"><select name=\"specialists[" + id + "][group][]\" id=\"user_group_" + id + "\" style=\"width: 100%;\" multiple='multiple'>\n" +
                "<option value=\"company_admin\"";
            if (group.indexOf('company_admin') !== -1) {
                block = block + " selected";
            }
            block = block + ">{{ trans('app.administrator')}}</option><option value=\"company_manager\"";
            if (group.indexOf('company_manager') !== -1) {
                block = block + " selected";
            }
            block = block + ">{{ trans('app.manager') }}</option><option value=\"company_specialist\"";
            if (group.indexOf('company_specialist') !== -1) {
                block = block + " selected";
            }
            block = block + ">{{ trans('app.specialist') }}</option></select></div>";

            block = block + "<div class='col'><select class='' name='specialists["+id+"][role]' id='specialists_"+id+"_role'>";
            @foreach(\App\MemberRole::where('company_id', auth()->user()->company_id)->get() as $role)
                block = block + "<option value='{{ $role->id }}'>{{ $role->name }}</option>";
            @endforeach
                block = block + '</select></div>';

            block = block +
                "<div class=\"col\"><select name=\"specialists[" + id + "][specialist]\" id=\"user_specialist_" + id + "\" style=\"width: 100%;\">\n" +
                "<option value=\"1\"";

            if (specialist == 1) {
                block = block + " selected";
            }
            block = block + ">{{ trans('app.accepts_orders') }}</option><option value=\"0\"";
            if (specialist != 1) {
                block = block + " selected";
            }
            block = block + ">{{ trans('app.does_not_accept') }}</option></select></div>\n" +
                "<div class=\"col\">" +
                "<button type=\"button\" class=\"btn btn-warning\" onclick=\"cancel_edit_member(" + id + ", '" + name + "')\">" +
                "<i class=\"fa fa-ban\"></i> {{ trans('app.cancel') }}" +
                "</button>" +
                "</div></div>";

            $("#specialist_block_" + id).append(block);

            $("#user_group_" + id).select2();
            $("#user_specialist_" + id).select2();
            $("#specialists_" + id + '_role').select2({width:'100%'});
            $("#specialists_" + id + '_role').select2('val', String(role));
        }

        function cancel_edit_member(id, name) {
            if (confirm('{{ trans('app.cancel_edit_question') }}\n\n {{ trans('app.name') }}: ' + name)) {
                $("#members_edit_block_" + id).remove();
                $("#default_block_" + id).show();
            }
        }

        function cancel_edit_role(id) {
            $("#roles_edit_block_" + id).remove();
            $("#default_block_" + id).show();
        }

        function delete_role(id, name) {
            if (confirm('{{ trans('app.delete_role') }} ' + name + '?\n\n')) {
                $.ajax({
                    method: "POST",
                    url: "/company/roles/delete",
                    data: {
                        "_method": 'DELETE',
                        "_token": '{{csrf_token()}}',
                        role: id
                    }
                }).done(function (msg) {
                    $("#roles_block_" + id).remove();
                }).fail(function () {
                    console.log("{{ trans('app.error') }}");
                });
            }
        }

        function delete_member(id, name) {
            if (confirm('{{ trans('app.remove_user_question') }} ' + name + '?\n\n {{ trans('app.remove_member_notice') }}')) {
                $.ajax({
                    method: "POST",
                    url: "/company/members/delete",
                    data: {
                        "_method": 'DELETE',
                        "_token": '{{csrf_token()}}',
                        user: id,
                        company: '{{ $data['company']->id }}'
                    }
                }).done(function (msg) {
                    alert("{{ trans('app.user_removed') }}!");
                    $("#specialist_block_" + id).remove();
                }).fail(function () {
                    alert("{{ trans('app.error') }}");
                });
            }
        }
    </script>
@endsection
