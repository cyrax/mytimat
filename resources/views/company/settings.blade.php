@extends('partials.app')

@section('content')
    <div class="aksii studio">
        <div class="container">
            <span class="add-span">{{ trans('app.settings') }}</span>
            <form id="setting-form" method="post">
                {{ csrf_field() }}
                <div id="setting-alert" class="alert"></div>
                <div class="form-group">
                    <input id="is_active" name="is_active" type="checkbox" class="form-check-input" {{ $data['setting']['display_email'] ? 'checked' : '' }} value="{{ $data['setting']['is_active'] }}">
                    <label for="is_active" class="form-check-label">{{ trans('app.is_active') }}</label>
                </div>
                <div class="form-group">
                    <input id="chat_with_company" name="chat_with_company" type="checkbox" class="form-check-input" {{ $data['setting']['chat_with_company'] ? 'checked' : '' }} value="{{ $data['setting']['chat_with_company'] }}">
                    <label for="chat_with_company" class="form-check-label">{{ trans('app.chat_with_company') }}</label>
                </div>
                <button onclick="return updateCompanySettings()" type="submit" class="aksii-more more-z">{{ trans('app.submit') }}</button>
            </form>
        </div>
    </div>
@endsection
@section('script')
    <script>
        function updateCompanySettings() {
            $.ajax({
                method: 'POST',
                data: $('#setting-form').serialize(),
                success: function (data) {
                    $('#setting-alert').addClass('alert-success').text(data);
                }
            });
            return false;
        }
    </script>
@endsection