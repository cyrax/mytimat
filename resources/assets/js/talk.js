$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('#talkSendMessage').on('submit', function(e) {
        e.preventDefault();
        var url, request, tag, data;
        tag = $(this);
        url = __baseUrl + '/ajax/message/send';
        data = tag.serialize();

        request = $.ajax({
            method: "post",
            url: url,
            data: data
        });

        request.done(function (response) {
            if (response.status == 'success') {
                $('#talkMessages').append(response.html);
                tag[0].reset();
            }
        });

    });
});

var show = function(data) {
    alert(data.sender.name + " - '" + data.message + "'");
};

var msgshow = function(data) {
    var html = '<li id="message-' + data.id + '">' +
        '<div class="message-data">' +
        '<span class="message-data-name">' + data.sender.name + '</span>' +
        '<span class="message-data-time">1 Second ago</span>' +
        '</div>' +
        '<div class="message my-message">' +
        data.message +
        '</div>' +
        '</li>';

    $('#talkMessages').append(html);
}
