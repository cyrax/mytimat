
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
// global.$ = global.jQuery = require('jquery');
// const autosize = require('autosize');
// const autosize = require('autosize/dist/autosize');
require('./bootstrap');
window.autosize = require('autosize');
// require('jquery-ui');
// require('jquery-ui-bundle');
require('./jquery.auto-complete');
require('scroll-js');
require('select2');
require('./toastr.min');
require('./talk');

autosize(document.querySelectorAll('textarea'));

$('#the_city').select2();
$('#the_country').select2();
let select3 = $('#category').select2({ width: '100%'});
select3.data('select2').$selection.css({height:'34px'});
$( "#search" ).autoComplete({
    source: function( request, response ) {
        $.ajax({
            url: "/company/search-ajax",
            data: {
                search_text: request.term
            },
            success: function( data ) {
                return response(data);
            },
        });
    },
    minChars: 2,
    onSelect: function( event, ui ) {
        window.location = ui.item.url;
    }
});
