<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class Category
 *
 * @package App
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property integer $parent_id
 * @property string $image
 * @property string $name_translation
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Category[] $categories
 * @property-read \App\Category $category
 * @property-read mixed $raw
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Service[] $services
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category search($search, $threshold = null, $entireText = false, $entireTextOnly = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category searchRestricted($search, $restriction, $threshold = null, $entireText = false, $entireTextOnly = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereNameTranslation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Category whereUrl($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    use \Waavi\Translation\Traits\Translatable;
    use SearchableTrait;

    protected $translatableAttributes = ['name'];

    protected $table = 'categories';

    protected $fillable = [
        'name',
        'name_translation',
        'url',
        'parent_id',
    ];

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'parent_id');
    }

    public function categories()
    {
        return $this->hasMany('App\Category', 'parent_id');
    }

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'name' => 1,
            'name_translation' => 2,
        ]
    ];
}
