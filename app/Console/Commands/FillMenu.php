<?php

namespace App\Console\Commands;

use Encore\Admin\Auth\Database\Menu;
use Illuminate\Console\Command;

class FillMenu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fill:menu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fill admin Menu';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $menuItems = [
            ['title' => 'Languages', 'uri' => 'languages', 'icon' => 'fa-language'],
            ['title' => 'Companies', 'uri' => 'companies', 'icon' => 'fa-company']
        ];

        foreach ($menuItems as $menuItem) {
            $this->createMenu($menuItem['title'], $menuItem['uri'], $menuItem['icon']);
        }
    }

    function createMenu($title, $uri, $icon = 'fa-bars')
    {
        Menu::create([
            'parent_id' => 0,
            'order'     => 0,
            'title'     => $title,
            'icon'      => $icon,
            'uri'       => $uri,
        ]);
    }
}
