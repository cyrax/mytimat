<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Pusher\Pusher;

class UserController extends Controller
{
    public function settings(Request $request)
    {
        if (!\Auth::check()) {
            abort(403, 'Forbidden');
        }
        $data['setting'] = auth()->user()->setting;
        if ($request->isMethod('post')) {
            Setting::saveUserSettings($request);
            return trans('app.data_updated', [], \UriLocalizer::localeFromRequest());
        }
        return view('user.settings', compact('data'));
    }
}