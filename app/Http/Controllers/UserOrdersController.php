<?php


namespace App\Http\Controllers;


use App\Member;
use App\Order;
use App\UserMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Waavi\Translation\Facades\UriLocalizer;

class UserOrdersController extends Controller
{
    private $permitted = ['user', 'company_admin'];

    public function index(Request $request)
    {
        if(!Auth::check()) {
            abort(403, 'Unauthorized action.');
        }

        $data['orders'] = Order::with(['service', 'specialist', 'user'])->where('user_id', Auth::id())->get();
        $data['specialists'] = Member::all();

        $events = [];
        foreach ($data['orders'] as $order) {
            $service = $order->service;
            $color = '#4CAF50';
            switch ($order->status) {
                case Order::STATUS_NEW:
                    $color = '#3a87ad';
                    break;
                case ORDER::STATUS_REJECTED:
                    $color = '#F00';
                    break;
            }
            $events[] = \Calendar::event(
                optional($service)->name . " \n". $order->company->name . "\n" . optional($order->specialist)->last_name,
                false,
                $order->date . ' ' . $order->time,
                date('H:i:s', strtotime('+' . $service->days . ' days +' . $service->hours . ' hours +' . $service->minutes . ' minutes', strtotime($order->date . ' ' . $order->time))),
                0, // event ID
                [
                    'color' => $color,
                    'url' => '/order/' . $order->id,
                    'slotMinutes' => '5'
                ]

            );
        }

        $calendar = \Calendar::addEvents($events) //add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            'defaultView' => 'agendaWeek',
            'aspectRatio' => '1.5',
            'allDaySlot' => false,
            'locale' => UriLocalizer::localeFromRequest()
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'dayClick' => 'function(date) {}'
        ]);

        return view('user_orders.index', compact('data', 'request', 'calendar'));
    }

    public function accept($id, Request $request)
    {
        $order = Order::find($id);
        //if ($order->status === Order::STATUS_NEW)
        $order->status = Order::STATUS_ACCEPTED;
        $order->save();

        UserMessage::orderAcceptedByUser($order->id, $order->user_id);

        return Redirect::back()->with('message', 'Order Accepted');
    }

    public function reject(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reject_status' => 'required|max:255',
            'reject_text' => 'required|max:255',
            'order_id' => 'required|max:255|integer',
        ]);

        if ($validator->fails()) {
            abort(422, 'Request failed');
        }

        $order = Order::find($request['order_id']);
        $order->reject_status = $request['reject_status'];
        $order->reject_text = $request['reject_text'];
        $order->status = Order::STATUS_REJECTED;
        $order->save();

        UserMessage::orderRejected($order);
        return 'Order rejected';
        //return Redirect::back()->with('message', 'Order Rejected');
    }

    public function editTime($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|max:255',
            'time' => 'required|max:255',
            'specialist' => 'required'
        ]);

        if ($validator->fails()) {
            abort(422, 'Request failed');
        }

        $order = Order::with('company')->find($id);
        $order->date = date('Y-m-d', strtotime($request['date']));
        $order->time = $request['time'];
        $order->specialist_id = $request['specialist'];
        $order->status = Order::STATUS_REACCEPT_CHANGES;
        $order->save();

        UserMessage::needToAcceptChangesByUser(Auth::user()->getFullName(), $order->company->user_id);
    }

}
