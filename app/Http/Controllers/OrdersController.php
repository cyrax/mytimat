<?php

namespace App\Http\Controllers;

use App\Company;
use App\Member;
use App\Order;
use App\Service;
use App\UserMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Waavi\Translation\Facades\UriLocalizer;

class OrdersController extends Controller
{
    private $permitted = ['company_admin', 'company_manager', 'company_specialist'];

    public function index($id, Request $request)
    {
        if (!Auth::check()) {
            abort(403, 'Unauthorized action.');
        }

        if (!in_array(Auth::user()->group, $this->permitted)) {
            abort(403, 'Unauthorized action.');
        }

        if(Auth::user()->company_id != $id && !Auth::user()->administrator) {
            abort(403, 'Unauthorized action.');
        }

        $data['company'] = Company::findOrFail($id);

        if (!empty($request['month'])) {
            try {
                $data['carbon'] = Carbon::parse($request['month']);
                $data['carbon']->day = 1;
                $data['holidays'] = Carbon::parse($request['month']);
                $data['holidays']->day = 1;
            } catch (\Exception $err) {
                $data['carbon'] = Carbon::createFromDate(null, null, 1);
                $data['holidays'] = Carbon::createFromDate(null, null, 1);
            }
        } else {
            $data['carbon'] = Carbon::createFromDate(null, null, 1);
            $data['holidays'] = Carbon::createFromDate(null, null, 1);
        }

        $data['max_date'] = $data['carbon']->daysInMonth;
        // get new orders
        $data['new_orders'] = Order::with(['service', 'company', 'specialist'])
                                    ->where(['company_id' => $data['company']->id])
                                    ->whereIn('status', [Order::STATUS_NEW, Order::STATUS_REACCEPT_CHANGES])
                                    ->select('id', 'group', 'created_at', 'user_id', 'date', 'time', 'service_id', 'specialist_id', 'company_id')
                                    ->distinct()
                                    ->latest()
                                    ->get();

        // get services
        $data['services'] = Service::where('company_id', $id)->get();
        // get specialists
        $data['specialists'] = $data['company']->members()->get();
        // get roles
        $data['roles'] = $data['company']->roles()->get();

        $events = [];
        $orders = Order::where('company_id', $id);
        if (!empty($request['specialists']))
            $orders->where('specialist_id', $request['specialists']);

        if (!empty($request['roles'])) {
            $orders->whereIn('specialist_id', Member::where('role', $request['roles'])->select('id'));
        }
        $orders = $orders->get();

        foreach ($orders as $order) {
            $service = $order->service;
            $color = '#4CAF50';
            switch ($order->status) {
                case Order::STATUS_NEW:
                    $color = '#3a87ad';
                    break;
                case ORDER::STATUS_REJECTED:
                    $color = '#F00';
                    break;
            }
            $events[] = \Calendar::event(
                $service->name . ' ' . optional($order->specialist)->name . ' ' . optional($order->specialist)->last_name,
                false, //full day event?
                $order->date . ' ' . $order->time, //start time
                date('H:i:s', strtotime('+' . $service->days . ' days +' . $service->hours . ' hours +' . $service->minutes . ' minutes', strtotime($order->date . ' ' . $order->time))), //end time
                0, // event ID
                [
                    'color' => $color,
                    'url' => '/order/' . $order->id,
                    'slotMinutes' => '5'
                ]
            );
        }
        $calendar = \Calendar::addEvents($events)//add an array with addEvents
        ->setOptions([ //set fullcalendar options
            'firstDay' => 1,
            'defaultView' => 'agendaWeek',
            'minTime' => $data['company']->getMinStartTime(),
            'maxTime' => $data['company']->getMaxEndTime(),
            'allDaySlot' => false,
            'locale' => UriLocalizer::localeFromRequest(),
            'contentHeight' => 'auto',
        ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
            'dayClick' => 'function(date) {
                $("#chosenTime").text(date.format());
                $("#order_date").val(date.format());
                $("#new-order-modal").modal().show();
                $(".modal-backdrop").remove();
            }'
        ]);

        return view('orders.index', compact('data', 'request', 'calendar', 'orders'));
    }

    public function accept($id, Request $request)
    {
        $order = Order::find($id);
        //if ($order->status === Order::STATUS_NEW)
        $order->status = Order::STATUS_ACCEPTED;
        if ($order->save()) {
            UserMessage::orderAccepted($order);
//            return;
            if (!$request->ajax())
              return Redirect::back()->with('message', 'Order Accepted');
        }
        // dd($order);
    }

    public function reject(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'reject_status' => 'required|max:255',
            'reject_text' => 'required|max:255',
            'order_id' => 'required|max:255|integer',
        ]);

        if ($validator->fails()) {
            abort(422, 'Request failed');
        }

        $order = Order::find($request['order_id']);
        $order->reject_status = $request['reject_status'];
        $order->reject_text = $request['reject_text'];
        $order->status = Order::STATUS_REJECTED;
        $order->save();

        UserMessage::orderRejected($order);
        return 'Order rejected';
        //return Redirect::back()->with('message', 'Order Rejected');
    }

    public function editTime($id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'date' => 'required|max:255',
            'time' => 'required|max:255',
            'specialist' => 'required'
        ]);

        if ($validator->fails()) {
            abort(422, 'Request failed');
        }

        $order = Order::with('company')->find($id);
        $order->date = date('Y-m-d', strtotime($request['date']));
        $order->time = $request['time'];
        $order->specialist_id = $request['specialist'];
        $order->status = 'need_accept_changes';
        $order->save();

        UserMessage::needToAcceptChangesByCompany($order->company->name, $order->user_id);
    }

    public function view($id, Request $request)
    {
        $order = Order::findOrFail($id);

        return view('orders.view', compact('order'));
    }
}
