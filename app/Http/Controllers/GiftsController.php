<?php

namespace App\Http\Controllers;

use App\Gift;
use App\GiftCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class GiftsController extends Controller
{
    private $permitted = ['administrator', 'manager'];

    public function index($id = 0) {
        $data['categories'] = GiftCategory::latest()->get();
        if($id == 0) {
            $data['services'] = Gift::latest()->get();
        } else {
            $data['services'] = Gift::where('category_id', $id)->latest()->get();
        }

        return view('gifts.index', compact('id', 'data'));
    }

    public function administrate() {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(!in_array(Auth::user()->group, $this->permitted)) {
            abort(403, 'Unauthorized action.');
        }

        $data['categories'] = GiftCategory::latest()->get();

        return view('gifts.manage', compact('data'));
    }

    public function delete_gift(Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(!in_array(Auth::user()->group, $this->permitted)) {
            abort(403, 'Unauthorized action.');
        }

        $gift = Gift::find($request->gift);
        if (file_exists('images/gifts/'.$gift->picture.'.jpg'))
        {
            unlink('images/gifts/'.$gift->picture.'.jpg');
        }
        if (file_exists('images/gifts/thumbnail/'.$gift->picture.'.jpg'))
        {
            unlink('images/gifts/thumbnail/'.$gift->picture.'.jpg');
        }
        $gift->delete();

        return 'success';
    }

    public function add_category(Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(!in_array(Auth::user()->group, $this->permitted)) {
            abort(403, 'Unauthorized action.');
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:gift_categories|max:255',
        ]);


        if ($validator->fails()) {
            $request->session()->flash('category_exists', 'yes');
            return redirect('gift/manage');
        }


        GiftCategory::create($request->all());

        $request->session()->flash('category_added', 'yes');

        return redirect('gift/manage');
    }

    public function delete_category(Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(!in_array(Auth::user()->group, $this->permitted)) {
            abort(403, 'Unauthorized action.');
        }

        $category = GiftCategory::findOrFail($request->category);

        foreach ($category->gifts()->get() as $gift) {
            if (file_exists('images/gifts/'.$gift->picture.'.jpg'))
            {
                unlink('images/gifts/'.$gift->picture.'.jpg');
            }
            if (file_exists('images/gifts/thumbnail/'.$gift->picture.'.jpg'))
            {
                unlink('images/gifts/thumbnail/'.$gift->picture.'.jpg');
            }
            $gift->delete();
        }

        $category->delete();

        $request->session()->flash('category_deleted', 'yes');

        return redirect('gift/manage');
    }

    public function store_gift (Request $request) {

        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(!in_array(Auth::user()->group, $this->permitted)) {
            abort(403, 'Unauthorized action.');
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'text' => 'required',
            'picture' => 'required|image',
            'category_id' => 'required|numeric',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);


        if ($validator->fails()) {
            $request->session()->flash('gift_add_error', 'yes');
            return redirect('gift/manage#add-gift')
                ->withErrors($validator)
                ->withInput();
        }

        $name=time().mt_rand(1000,9999);

        $filename = $request->file('picture') . '';

        if (!file_exists('images/gifts/thumbnail/')) {
            mkdir('images/gifts/thumbnail/', 0777, true);
        }

        if (!$this->store_thumbnail($filename,$request->file('picture')->extension(),'images/gifts/thumbnail/'.$name.'.jpg', 50, 50)){
            $upload_error='Ошибка загрузки изображения';
            $request->session()->flash('gift_add_error', 'yes');
            return redirect('gift/manage#add-gift')
                ->withInput($request->input())
                ->withErrors($upload_error);
        }

        if (!$this->store_resize($filename,$request->file('picture')->extension(),'images/gifts/'.$name.'.jpg')){
            $upload_error='Ошибка загрузки изображения';
            $request->session()->flash('gift_add_error', 'yes');
            return redirect('gift/manage#add-gift')
                ->withInput($request->input())
                ->withErrors($upload_error);
        }

        $data = $request->all();

        $data['picture'] = $name;

        Gift::create($data);

        $request->session()->flash('gift_added', 'yes');

        return redirect('gift/manage');
    }



    public function edit_gift(Request $request) {

        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(!in_array(Auth::user()->group, $this->permitted)) {
            abort(403, 'Unauthorized action.');
        }

        $gift = Gift::findOrFail($request['id']);
        $validator = Validator::make($request->all(), [
            'id' => 'required|max:255',
            'name' => 'required|max:255',
            'text' => 'required',
            'picture' => 'image',
            'category_id' => 'required|numeric',
            'price' => 'required|numeric',
            'quantity' => 'required|numeric',
        ]);

        $id = $request['id'];

        $data['name_'.$id] = $request['name'];
        $data['text_'.$id] = $request['text'];
        $data['category_id_'.$id] = $request['category_id'];
        $data['price_'.$id] = $request['price'];
        $data['quantity_'.$id] = $request['quantity'];


        if ($validator->fails()) {
            $request->session()->flash('gift_edit_error_'.$id, 'yes');
            return redirect('gift/manage#edit-gift-'.$id)
                ->withErrors($validator)
                ->withInput($data);
        }

        unset($data);

        $data = $request->all();

        if ($request->hasFile('picture')) {
            $name=time().mt_rand(1000,9999);

            $filename = $request->file('picture') . '';

            if (!file_exists('images/gifts/thumbnail/')) {
                mkdir('images/gifts/thumbnail/', 0777, true);
            }

            if (!$this->store_thumbnail($filename,$request->file('picture')->extension(),'images/gifts/thumbnail/'.$name.'.jpg', 50, 50)){
                $upload_error='Ошибка загрузки изображения';
                $request->session()->flash('gift_edit_error_'.$id, 'yes');
                return redirect('gift/manage#edit-gift-'.$id)
                    ->withInput($request->input())
                    ->withErrors($upload_error);
            }

            if (!$this->store_resize($filename,$request->file('picture')->extension(),'images/gifts/'.$name.'.jpg')){
                $upload_error='Ошибка загрузки изображения';
                $request->session()->flash('gift_edit_error_'.$id, 'yes');
                return redirect('gift/manage#edit-gift-'.$id)
                    ->withInput($request->input())
                    ->withErrors($upload_error);
            }

            if (file_exists('images/gifts/'.$gift->picture.'.jpg'))
            {
                unlink('images/gifts/'.$gift->picture.'.jpg');
            }
            if (file_exists('images/gifts/thumbnail/'.$gift->picture.'.jpg'))
            {
                unlink('images/gifts/thumbnail/'.$gift->picture.'.jpg');
            }

            $data['picture'] = $name;
        } else {
            $data['picture'] = $gift->picture;
        }

        $gift->update($data);

        $request->session()->flash('gift_edited', 'yes');

        return redirect('gift/manage');
    }




    public function store_thumbnail($uploadedfile,$extension,$needed_name,$needed_width,$needed_height) {
        if($extension!='png' && $extension!='jpg' && $extension!='gif' && $extension!='jpeg')
        {
            return false;
        }
        if($extension=='gif') {
            $src = imagecreatefromgif($uploadedfile); //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='png') {
            $src = imagecreatefrompng($uploadedfile) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='jpg' || $extension=='jpeg') {
            $src = imagecreatefromjpeg($uploadedfile); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        list($width,$height)=getimagesize($uploadedfile);
        $tmp=imagecreatetruecolor($needed_width,$needed_height);
        $wasd=$width/$needed_width;
        $hasd=$height/$needed_height;
        imagecopyresampled($tmp,$src,0,0,$wasd>$hasd?intval(($width-$needed_width*$hasd)/2):0,$width<$height?intval(($height-$needed_height*$wasd)/2):0,$needed_width,$needed_height,$wasd>$hasd?intval($needed_width*$hasd):$width,$wasd<$hasd?intval($needed_height*$wasd):$height);
        $filename = $needed_name;
        if (!imagejpeg($tmp,$filename,100)){
            imagedestroy($src);
            imagedestroy($tmp);
            return false;
        }
        imagedestroy($src);
        imagedestroy($tmp);
        return true;
    }

    public function store_resize($uploadedfile,$extension,$needed_name) {
        if($extension!='png' && $extension!='jpg' && $extension!='gif' && $extension!='jpeg')
        {
            return false;
        }
        if($extension=='gif') {
            $src = imagecreatefromgif($uploadedfile); //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='png') {
            $src = imagecreatefrompng($uploadedfile) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='jpg' || $extension=='jpeg') {
            $src = imagecreatefromjpeg($uploadedfile); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        list($width,$height)=getimagesize($uploadedfile);
        if($width <=600 && $height <=600) {
            $new_width = $width;
            $new_height = $height;
        } elseif ($width > $height) {
            $new_width = 600;
            $new_height = $height*600/$width;
        } elseif ($width < $height) {
            $new_width = $width*600/$height;
            $new_height = 600;
        } else {
            $new_width = 600;
            $new_height = 600;
        }
        $tmp=imagecreatetruecolor($new_width,$new_height);
        imagecopyresampled($tmp,$src,0,0,0,0,$new_width,$new_height,$width,$height);

        $filename = $needed_name;
        if (!imagejpeg($tmp,$filename,100)){
            imagedestroy($src);
            imagedestroy($tmp);
            return false;
        }
        imagedestroy($src);
        imagedestroy($tmp);
        return true;
    }
}
