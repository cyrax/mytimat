<?php
namespace App\Http\Controllers;

use App\Language;
use App\Member;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Waavi\Translation\Models\Translation;

class TranslatorController extends Controller
{
    public function __construct()
    {
        $this->middleware('translator');
    }

    public function ui_elements(Request $request)
    {
        $translations = DB::table('translator_translations as t1')
            ->select(['t1.text as ru_text', 't2.text as en_text', 't3.text as it_text', 't4.text as hr_text', 't1.item'])
            ->leftJoin('translator_translations as t2', function ($join) {
                $join->on('t1.item', '=', 't2.item');
                $join->on('t2.locale', 'like', DB::raw("'en'"));
            })
            ->leftJoin('translator_translations as t3', function ($join) {
                $join->on('t1.item', '=', 't3.item');
                $join->on('t3.locale', 'like', DB::raw("'it'"));
            })
            ->leftJoin('translator_translations as t4', function ($join) {
                $join->on('t1.item', '=', 't4.item');
                $join->on('t4.locale', 'like', DB::raw("'hr'"));
            })
            ->where('t1.locale', 'ru');

        if ($request->has('key'))
            $translations->where('t1.item', 'LIKE', '%'.$request['key'].'%');

        if ($request->has('text'))
            $translations->whereRaw('t1.text like ? or t2.text like ? or t3.text like ? or t4.text like ?',
                [
                    '%'.$request['text'].'%',
                    '%'.$request['text'].'%',
                    '%'.$request['text'].'%',
                    '%'.$request['text'].'%',
                ]);

        $translations = $translations->paginate(12);
        return view('translator.ui_elements', compact('translations'));
    }

    public function update(Request $request)
    {
        $item = $request['item'];
        foreach ($request['text'] as $locale => $text) {
            if (empty($text))
                continue;
            $translations = Translation::firstOrNew(['item' => $item, 'locale' => $locale]);
            $translations->group = 'app';
            $translations->namespace = '*';
            $translations->text = $text;
            $translations->save();
        }

        return Redirect::back()->with(['success', 'Stored']);
    }

    public function rights(Request $request)
    {
        $users = User::whereRaw('1=1');
        if ($request->has('phone')){
            $users->where('phone', 'like', '%'.$request['phone'].'%');
        }
        if ($request->has('is_user')) {
            $users->orWhere('user', 1);
        }
        if ($request->has('is_admin')) {
            $users->orWhere('administrator', 1);
        }
        if ($request->has('is_specialist')) {
            $users->orWhere('specialist', 1);
        }
        if ($request->has('is_techsupport')) {
            $users->orWhere('techsupport', 1);
        }
        if ($request->has('is_translator')) {
            $users->orWhere('translator', 1);
        }
        if ($request->has('is_moderator')) {
            $users->orWhere('moderator', 1);
        }

        $users = $users->paginate(12);
        return view('translator.rights', compact('users'));
    }

    public function rightsUpdate(Request $request)
    {
        $changedAttrs = ['user', 'moderator', 'administrator', 'specialist', 'techsupport', 'translator'];
        foreach ($changedAttrs as $changedAttr) {
            if ($request->has($changedAttr)) {
                if ($changedAttr == 'specialist') {
                    $member = Member::firstOrNew(['user_id' => $request['user_id']]);
                    if (!$member->exists) {
                        $user = User::findOrFail($request['user_id']);
                        $member->company_id = 0;
                        $member->name = $user->name;
                        $member->last_name = $user->second_name;
                        $member->save();
                    }
                }
                User::find($request['user_id'])->update([$changedAttr => (int)$request[$changedAttr]]);
                return 'Updated';
            }
        }
        return '';
    }

    public function users(Request $request)
    {
        $users = User::whereRaw('1=1');
        if ($request->has('phone')) {
            $users->where('phone', 'like', '%'.$request['phone'].'%');
        }
        if ($request->has('name')) {
            $users->where('name', 'like', '%'.$request['name'].'%');
        }
        $users = $users->paginate(12);
        $languages = Language::all();
        return view('translator.users', compact('users', 'languages'));
    }

    public function usersUpdate()
    {

    }
}