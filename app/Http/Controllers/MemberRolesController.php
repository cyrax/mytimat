<?php

namespace App\Http\Controllers;


use App\MemberRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MemberRolesController extends Controller
{
    public function update(Request $request)
    {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

//        if(Auth::user()->group != 'company_admin') {
//            abort(403, 'Unauthorized action.');
//        }

        $data = $request->all();
        if(empty($data['roles'])) {
            $request->session()->flash('no_changes', 'yes');
            return redirect('company/members');
        }

        // $company = Auth::user()->company()->first();

        foreach ($data['roles'] as $role) {
            /** @var Member $member */
            $memberRole = MemberRole::find($role['id']);
            $memberRole->name = $role['name'];
            $memberRole->save();
        }

        $request->session()->flash('changes_saved', 'yes');
        return redirect('company/members');
    }
}