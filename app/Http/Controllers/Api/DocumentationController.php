<?php
/**
 * Created by PhpStorm.
 * User: andreysmac
 * Date: 22.03.2018
 * Time: 20:58
 */

namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use M165437\BlueprintDocs\BlueprintDocs;

class DocumentationController extends Controller
{
    public function index(BlueprintDocs $blueprintDocs)
    {
        $api = $blueprintDocs
            ->parse(config('blueprintdocs.blueprint_file'))
            ->getApi();

        dd($api);
    }
}