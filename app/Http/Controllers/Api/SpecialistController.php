<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Member;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;

/**
 * @Resource("Specialists", uri="/specialists")
 */
class SpecialistController extends Controller
{
    /**
     * Get available specialists
     *
     * Get available specialists in company in particular date and time
     *
     * @Get("/available")
     * @Versions({"v1"})
     * @Transaction({
     *     @Request({"username": "foo", "password": "bar"}),
     *     @Response(200, body={"id": 10, "username": "foo"}),
     *     @Response(422, body={"error": {"username": {"Username is already taken."}}})
     * })
     * @Parameters({
     *     @Parameter("token", type="string", required=true, description="API Token", default=null),
     *     @Parameter("company_id", type="integer", required=true, description="Company ID"),
     *     @Parameter("date", type="date", required=true, description="Date"),
     *     @Parameter("time", type="string", required=true, description="Time")
     * })
     */
    public function available($company_id, Request $request)
    {
        $rules = [
            'company_id' => ['required'],
            'date' => ['required'],
            'time' => ['required']
        ];

        $payload = app('request')->only('date', 'time');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            throw new StoreResourceFailedException($validator->errors());
        }

        $date = $request->get('date');
        $time = $request->get('time').':00';
        $dateTime = $date.' '.$time;

        $members = Member::whereHas('schedule', function ($query) use ($dateTime, $time) {
            $weekName = strtolower(date('l', strtotime($dateTime)));
            $query->where($weekName, 1);
            $query->where($weekName.'_start', '<=', $time);
            $query->where($weekName.'_end', '>', $time);
        })
            ->select(['id', 'name', 'last_name'])
            ->where('company_id', $company_id)
            ->get()
            ->toArray();

        return $members;

    }

    /**
     * Show all specialists
     *
     * Get a JSON representation of all the registered specialists.
     *
     * @Get("/{?page,limit}")
     * @Versions({"v1"})
     * @Transaction({
     *      @Request({"username": "foo", "password": "bar"}),
     *      @Response(200, body={"id": 10, "username": "foo"}),
     *      @Response(422, body={"error": {"username": {"Username is already taken."}}})
     * })
     * @Parameters({
     *      @Parameter("page", description="The page of results to view.", default=1),
     *      @Parameter("limit", description="The amount of results per page.", default=10)
     * })
     */
    public function index()
    {
        return [];
    }
}