<?php


namespace App\Http\Controllers;


use App\Company;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Nahid\Talk\Conversations\Conversation;
use Nahid\Talk\Facades\Talk;

class ChatController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('talk');
        View::composer('chat.partials.peoplelist', function($view) {
            $threads = Talk::threads();
            $view->with(compact('threads'));
        });
    }

    public function index($id)
    {
        Talk::setAuthUserId(Auth::user()->id);
        $conversations = Talk::getMessagesByUserId($id, 0, 200);
        $messages = [];
        if(!$conversations) {
            $user = User::find($id);
        } else {
            $user = $conversations->withUser;
            $messages = $conversations->messages;
        }

        return view('chat.conversations', compact('messages', 'user'));
    }

    public function chatWithCompany($companyId, $toUserId)
    {
        $company = Company::findOrFail($companyId);
        Company::createConversationWithCompany(Auth::user()->id, $company);
        Talk::setAuthUserId(Auth::user()->id);
        $userId = $toUserId ?? $company->user_id;
        $conversations = Talk::getMessagesByUserId($userId, 0, 200);
        $messages = [];
        if(!$conversations) {
            $user = User::findOrFail($userId);
        } else {
            $user = $conversations->withUser;
            $messages = $conversations->messages;
        }

        return view('chat.conversations', compact('messages', 'user'));
    }

    public function chatWithUsers($company_id)
    {
        $company = Company::findOrFail($company_id);
        $userId = $company->user_id;
        Talk::setAuthUserId(Auth::user()->id);
        $conversations = Talk::getMessagesByUserId($userId, 0, 200);
        $messages = [];
        if(!$conversations) {
            $user = User::find($userId);
        } else {
            $user = $conversations->withUser;
            $messages = $conversations->messages;
        }

        return view('chat.conversations', compact('messages', 'user'));
    }

    public function support()
    {
        Talk::setAuthUserId(Auth::user()->id);
        $support = User::where('techsupport', 1)->firstOrFail();
        $conversations = Talk::getMessagesByUserId($support->id, 0, 5);
        $messages = [];
        if(!$conversations) {
            $user = $support;
        } else {
            $user = $conversations->withUser;
            $messages = $conversations->messages;
        }

        return view('chat.support', compact('messages', 'user'));
    }

    public function techSupport($id)
    {
        Talk::setAuthUserId(Auth::user()->id);
        if ($id > 0) {
            $user = User::findOrFail($id);
            $conversations = Talk::getMessagesByUserId($user->id, 0, 5);
        }
        else {
            $user = User::where('techsupport', 0)->firstOrFail();
            $conversations = Talk::getConversationsByUserId(Auth::id());
        }
        $messages = [];
        if(!$conversations) {
//            $user = $user;
        } else {
            $user = $conversations->withUser;
            $messages = $conversations->messages;
        }

        return view('chat.conversations', compact('messages', 'user'));
    }

    public function ajaxSendMessage(Request $request)
    {
        if ($request->ajax()) {
            Talk::setAuthUserId(Auth::user()->id);
            $rules = [
                'message-data'=>'required',
                '_id'=>'required'
            ];

            $this->validate($request, $rules);

            $body = $request->input('message-data');
            $userId = $request->input('_id');

            if ($message = Talk::sendMessageByUserId($userId, $body)) {
                $html = view('ajax.newMessageHtml', compact('message'))->render();
                return response()->json(['status'=>'success', 'html'=>$html], 200);
            }
        }
    }
}