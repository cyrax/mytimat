<?php

namespace App\Http\Controllers;

use App\Bookmark;
use App\Category;
use App\Company;
use App\Companyrate;
use App\Country;
use App\Member;
use App\MemberGroup;
use App\MemberRole;
use App\Nonworkingday;
use App\Order;
use App\Picture;
use App\Right;
use App\Service;
use App\Setting;
use App\UserMessage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CompaniesController extends Controller
{
    public function index()
    {
        $categories = Category::where('parent_id', 0)->get();
        foreach ($categories as $category) {
            $sub_categories[$category->id] = $category->categories()->get();
        }

        return view('company.index', compact('categories', 'sub_categories'));
    }

    public function add()
    {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }
//        if(Auth::user()->group != 'user') {
//            abort(403, 'You cannot add a company');
//        }
        $company = new Company();
        $categories = Category::all();

        return view('company.add', compact('categories', 'countries', 'company'));
    }

    public function edit($id, Request $request)
    {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }
        $company = Company::findOrFail($id);

        if ($id != Auth::user()->company_id && !Auth::user()->administrator) {
            abort(403, 'You cannot edit this company');
        }

        if ($request->isMethod('post')) {
            Company::updateOrSave($request, $company);
            Right::createRight($request, $id);
            Picture::createPicturesForCompany($request, $id);
            Nonworkingday::createNonWorkingDays($request, $id);
            Service::createCompanyService($request, $id);

            return redirect()->route('profile_index');
        }

        $categories = Category::all();

        return view('company.add', compact('categories', 'countries', 'company'));
    }



    public function store(Request $request)
    {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }
        if(Auth::user()->group != 'user') {
            abort(403, 'You cannot add a company');
        }
        Validator::make($request->all(), [
            'name' => 'required|max:255',
            'address' => 'required|max:255',
            'category_id' => 'required',
            'description' => 'required|max:1500',
            'country_id' => 'required',
            'city_id' => 'required',
            'features' => 'required',
            'rights_urls' => 'required|array',
            'pictures_urls' => 'required|array'
        ])->validate();

        $id = Company::updateOrSave($request);
        Right::createRight($request, $id);
        Picture::createPicturesForCompany($request, $id);
        Nonworkingday::createNonWorkingDays($request, $id);
        Service::createCompanyService($request, $id);
        $role = MemberRole::firstOrCreate(['name' => trans('app.administrator'), 'company_id' => $id]);

        Auth::user()->group = 'company_admin'; // TODO CHANGE OR REMOVE
        Auth::user()->company_id = $id;
        Auth::user()->save();

        $member = Member::create([
            'user_id' => Auth()->user()->id,
            'company_id' => $id,
            'name' => Auth::user()->name,
            'last_name' => Auth::user()->second_name,
            'accepts' => 1
        ]);
        $member->role = $role->id;
        $member->save();

        MemberGroup::create(['group' => 'company_admin', 'member_id' => $member->id]);

        UserMessage::companyCreated();

        return redirect()->route('profile-edit-get');
    }

    public function show($id)
    {
        $company = Company::with(['category'])->find($id);
        if (!$company) {
            abort(404, trans('app.company_not_found'));
        }

        if (!$company->approved) {
            return view('company.not_approved');
        }

        $data['bookmark'] = $company->bookmarks()->where('user_id', Auth::id())->first();
        $data['bookmark'] = empty($data['bookmark']) ? 0 : 1;

        $data['pictures'] = $company->pictures->toArray();
        
        $day_of_week = $this->day_of_the_week();

        $data['opened'] = $company[$day_of_week];

        if($company->nonworkingdays()->where('date', Carbon::now()->toDateString())->first()){
            $data['opened'] = 0;
        } elseif($company[$day_of_week.'_end'] < Carbon::now()->toTimeString()) {
            $data['opened'] = 0;
        }
        $data['now'] = Carbon::now();
        $data['day_of_week'] = $day_of_week;
        $data['non_working_days'] = $company->nonworkingdays()->get();
        $data['nwd_count'] = $data['non_working_days']->count();
        $data['non_working_days'] = $data['non_working_days']->toArray();

        $data['specialists'] = $company->members()
                                        ->with(['pictures', 'country', 'city', 'orders', 'schedule'])
                                        ->where(['accepts' => 1])
                                        ->get()
                                        ->toArray();

        $services['data'] = $company->services()->get()->sortBy('category_id');

        return view('company.show', compact('company', 'data', 'services', 'category'));
    }

    public function order($id, Request $request)
    {
        if(!Auth::check()){
            return redirect()->route('index');
        }

        $data = $request->all();
        $group = 0;

        $now = Carbon::now();

        foreach ($data['services'] as $service) {
            $validator = Validator::make($service, [
                'id' => 'required|numeric',
                'date' => 'required|date_format:"Y-m-d"',
                'time' => 'required',
                'specialist' => 'numeric',
            ]);

            if ($validator->fails()) {
                return redirect()
                    ->back()
                    ->withErrors($validator)
                    ->withInput();
            }

            if ($service['date'] < date('Y-m-d')) {
                return redirect()
                    ->back()
                    ->withErrors(['error', 'Order date cannot be less than now'])
                    ->withInput();
            }
            $service['time'] = date('H:i', strtotime($service['time'])).':00';
            $serviceModel = Service::with('company')->find($service['id']);
            $company = $serviceModel->company;
            $weekName = Service::getWeeks()[date('w', strtotime($service['date'])) - 1];
            $timeStart = $weekName.'_start';
            $timeEnd = $weekName.'_end';

            if (!$company->$weekName || ($company->$timeStart > $service['time'] || $company->$timeEnd < $service['time'])) {
                return redirect()->back()->withErrors(trans('error.company_not_working'));
            }

            $order = Order::where([
                'date' => $service['date'],
                'service_id' => $service['id'],
                'specialist_id' => $service['specialist'] ?? 0,
                'status' => 'accepted',
            ])->whereBetween('time', [$service['time'], date('H:i:s', strtotime('+'.$serviceModel->days.' days +'.$serviceModel->hours.' hours +'.$serviceModel->minutes.' minutes', strtotime($service['date'].' '.$service['time'])))]);

            if ($order->count() > 0) {
                return redirect()->back()->withErrors('Specialist in this time busy');
            }

            $massive = $service;

            $massive['service_id'] = $service['id'];
            $massive['company_id'] = $id;
            $massive['specialist_id'] = $service['specialist'];
            $massive['user_id'] = Auth::id();
            $massive['group'] = $group;
            $massive['created_at'] = $now;

            $orders = Order::create($massive);
            if($group == 0) {
                $group = $orders->id;
                $orders->group = $orders->id;
                $orders->save();
            }
        }

        if($orders) {
           $company = Company::find($request->user()->company_id);
           UserMessage::orderCreated($orders, $company);
            $request->session()->flash('services_added', 'added');
        } else {
            $request->session()->flash('services_not_added', 'not_added');
        }
        return redirect()->back()->with('services_added', 'added');
    }

    public function orderFromTable(Request $request)
    {
        if(!Auth::check()){
            return redirect()->route('index');
        }
        $validator = Validator::make($request->all(), [
            'service_id' => 'required|max:255',
            'specialist_id' => 'required|max:255',
            'order_date' => 'required|max:255',
            'company_id' => 'required|max:255|integer',
        ]);

        if ($validator->fails()) {
            abort(422, 'Request failed');
        }

        $data = $request->all();
        if ($data['order_date'] < date('Y-m-d'))
            return redirect()->back()->withErrors('Order date cannot be less than now');

        $order = new Order();
        $order->service_id = $data['service_id'];
        $order->specialist_id = $data['specialist_id'];
        $order->user_id = Auth()->id();
        $order->date = date('Y-m-d', strtotime($data['order_date']));
        $order->time = date('H:i:s', strtotime($data['order_date']));
        $order->status = Order::STATUS_NEW;
        $order->company_id = $data['company_id'];
        $order->group = time();
        $order->save();

        return redirect()->back();
    }

    public function companyOrder(Request $request)
    {
        if(!Auth::check()){
            return redirect()->route('index');
        }

        $validator = Validator::make($request->all(), [
            'service_id' => 'required|max:255',
            'order_date' => 'required|max:255',
            'order_time' => 'required|max:255',
            'company_id' => 'required|max:255|integer',
        ]);

        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator)
                ->withInput();
        }
        $specialist = $request['specialist_id'] ?? 0;

        if ($request['order_date'].' '.$request['order_time'] < date('Y-m-d H:i')) {
            return redirect()
                ->back()
                ->withErrors('Order date cannot be less than now');
        }
        $serviceModel = Service::with('company')->find($request['service_id']);
        $company = $serviceModel->company;
        $weekName = Service::getWeeks()[date('w', strtotime($request['order_date']))];
        if (!$company->$weekName) {
            return redirect()->back()->withErrors(trans('error.company_not_working'));
        }
        if ($specialist > 0) {
            $order = Order::where([
                'date' => $request['order_date'],
                'service_id' => $request['service_id'],
                'specialist_id' => $request['specialist_id'],
                'status' => 'accepted',
            ])->whereBetween('time', [$request['time'] . ':00', date('H:i:s', strtotime('+' . $serviceModel->days . ' days +' . $serviceModel->hours . ' hours +' . $serviceModel->minutes . ' minutes', strtotime($request['date'] . ' ' . $request['time'] . ':00')))]);

            if ($order->count() > 0) {
                return redirect()->back()->withErrors('Specialist in this time busy');
            }
        }

        $order = new Order();
        $order->service_id = $request['service_id'];
        $order->specialist_id = $specialist;
        $order->user_id = Auth()->id();
        $order->date = date('Y-m-d', strtotime($request['order_date']));
        $order->time = date('H:i:s', strtotime($request['order_time']));
        $order->status = Order::STATUS_NEW;
        $order->company_id = $request['company_id'];
        $order->group = time();
        $order->save();

        return redirect()->back();
    }


    public function orders(Request $request)
    {
        $member = Member::where('user_id', Auth::user()->id)->first();
        if (is_null($member))
            abort(403, 'You have no companies');
        return redirect()->route('company-specialist-orders', ['id' => $member->company_id]);
    }

    function add_bookmark ($id, Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        $company = Company::findOrFail($id);

        if(empty(Bookmark::where(['user_id'=>Auth::id(), 'company_id' => $id])->first())){
            $data['user_id'] = Auth::id();
            $data['company_id'] = $id;
            Bookmark::create($data);
        }

        return 'success';
    }

    function remove_bookmark ($id, Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        $company = Company::findOrFail($id);

        Bookmark::where(['user_id'=>Auth::id(), 'company_id' => $id])->delete();

        return 'success';
    }

    public function category_show($id) {
        $category = Category::where('url', $id)->firstOrFail();
        $children = $category->categories()->get();
        $child = $category;
        $parents = [];
        while($parent = $child->category()->first()){
            $parents[] = $parent;
            $child = $parent;
        }
        $parents = array_reverse($parents);
        $services = Service::where('category_id', $category->id);

        foreach ($children as $child) {
            $this->get_children_companies($child, $services);
        }

        $services = $services->select('company_id')->distinct()->paginate(12);

        foreach ($services as $service) {
            $data['companies'][] = $service->company()->first();
        }
        $data['companies'] = Company::where('category_id', $category->id)->get();

        $day_of_week = $this->day_of_the_week();

        return view('company.category', compact('category', 'data', 'day_of_week', 'services', 'parents', 'children'));
    }


    public function companies_by_rating()
    {
        $companies = Companyrate::select('company_id')->groupBy('company_id')->get()->toArray();
        foreach ($companies as $company) {
            $rates[$company['company_id']]['id'] = $company['company_id'];
            $rates[$company['company_id']]['avg'] = Companyrate::where('company_id', $company['company_id'])->avg('rate');
        }
        arsort($rates);
        unset($companies);
        foreach($rates as $rate) {
            $companies[] = Company::find($rate['id']);
        }

        $day_of_week = $this->day_of_the_week();

        return view('company.category', compact( 'companies', 'day_of_week'));
    }


    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search_text' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('category');
        }
        $search_text = $request['search_text'];
        $day_of_week = $this->day_of_the_week();

        $companies = Company::searchAll($request);

        $categories = Category::select('name')
            ->search($search_text)
            ->paginate(12);

        return view('company.search', compact('companies', 'search_text', 'day_of_week', 'categories'));
    }



    public function searchAjax(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search_text' => 'required'
        ]);

        if ($validator->fails()) {
            abort(422, 'Not enough parameters');
        }

        $result = [];
        $companies = Company::searchAll($request);
        $services = Service::searchAll($request);
        $categories = Category::select('name', 'url')
            ->search($request['search_text'])
            ->paginate(12);

        foreach ($companies as $company) {
            $result[] = ['url' => '/company/'.$company->name, 'value' => $company->name];
        }
        foreach ($categories as $category) {
            $result[] = ['url' => '/category/'.$category->url, 'value' => $category->name];
        }
        foreach ($services as $service) {
            $result[] = ['url' => '/company/'.optional($service->company)->name, 'value' => $service->name];
        }

        return response()->json($result);
    }

    public function rate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'company_id' => 'required|max:255',
            'value' => 'required|max:5'
        ]);

        if ($validator->fails()) {
            abort(422, 'not provided enough data');
        }

        $company = Company::findOrFail($request->input('company_id'));

        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->input('value');
        $rating->user_id = auth()->user()->id;
        $company->ratings()->save($rating);
    }


    public function get_children_companies($data, &$services) {
        $services = $services->orWhere('category_id', $data->id);
        foreach ($data->categories()->get() as $item) {
            $services = $services->orWhere('category_id', $item->id);
            $this->get_children_companies($item, $services);
        }
    }





    static function get_duration($days, $hours, $minutes) {
        $duration = '';
        if($days == 0 && $hours == 0 && $minutes == 0) {
            $duration = 'не указана';
        } else {
            if($days > 0) {
                $duration = $days.' '.trans('app.days');
            } else {
                if($hours > 0) {
                    $duration = $hours.' '.trans('app.hours');
                }
                if($minutes > 0) {
                    $duration .= $minutes.' '.trans('app.minutes');
                }
            }
        }
        return $duration;
    }

    public function day_of_the_week() {
        $day_of_week = date("N");
        switch ($day_of_week) {
            case 1:
                $day_of_week = 'monday';
                break;
            case 2:
                $day_of_week = 'tuesday';
                break;
            case 3:
                $day_of_week = 'wednesday';
                break;
            case 4:
                $day_of_week = 'thursday';
                break;
            case 5:
                $day_of_week = 'friday';
                break;
            case 6:
                $day_of_week = 'saturday';
                break;
            case 7:
                $day_of_week = 'sunday';
                break;
        }

        return $day_of_week;
    }

    static function dayOfWeek($day_of_week) {
        switch ($day_of_week) {
            case 1:
                $day_of_week = 'monday';
                break;
            case 2:
                $day_of_week = 'tuesday';
                break;
            case 3:
                $day_of_week = 'wednesday';
                break;
            case 4:
                $day_of_week = 'thursday';
                break;
            case 5:
                $day_of_week = 'friday';
                break;
            case 6:
                $day_of_week = 'saturday';
                break;
            case 7:
                $day_of_week = 'sunday';
                break;
        }

        return $day_of_week;
    }

    /**
     * @param Request $request
     * @return array|\Illuminate\Contracts\Translation\Translator|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|null|string
     */
    public function settings(Request $request)
    {
        if (!\Auth::check()) {
            abort(403, 'Forbidden');
        }
        /** @var Company $company */
        $company = Company::where('id', Auth::user()->company_id)->first();
        $data['setting'] = $company->setting;
        if ($request->isMethod('post')) {
            Setting::saveCompanySettings($request, $company);
            return trans('app.data_updated', [], \UriLocalizer::localeFromRequest());
        }
        return view('company.settings', compact('data'));
    }


    public function file_upload(){
        $upload = new UploadHydraFile;
    }
    public function picture_upload(){
        $upload = new UploadPictureFile;
    }


    function delete($file){
        if(@$_GET['id']){
            $path = 'public/uploads/post';
            $thumbs = $path.'/thumbs';
        }
        else{
            $path = 'public/uploads/guest_creat';
            $thumbs = $path.'/thumbs';
        }
        if(file_exists($path.'/'.$file)){
            $success = unlink($path.'/'.$file);
            $success = unlink($thumbs.'/'.$file);
        }
        else{
            $success = false;
        }
        $this->post->delete_image(@$_GET['image']);
        $info = new StdClass;
        $info->sucess = $success;
        $info->path = URL::to($path.'/'.$file);
        $info->file = is_file($path.'/'.$file);
        return Response::json(array($info));
    }

}
