<?php


namespace App\Http\Controllers;


class CityController extends Controller
{
    public function getByCountry($country_id)
    {
        $lang = \UriLocalizer::localeFromRequest();
        $attribute = 'title_'.$lang;
        $result = [];
        foreach (\App\City::select('city_id', $attribute)->where('country_id', $country_id)->where('important', 1)->get() as $city) {
            $result[] = ['id' => $city->city_id, 'name' => $city->$attribute];
        }
        return json_encode($result);
    }
}