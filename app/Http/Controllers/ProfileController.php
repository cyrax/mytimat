<?php

namespace App\Http\Controllers;

use App\ChangeNumber;
use App\City;
use App\Country;
use App\Language;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('exitifnotauth');
    }

    public function edit()
    {
        $languages = Language::all();
        $cities =  City::select('city_id as id', 'title_'.\UriLocalizer::localeFromRequest().' as name')
                        ->where('country_id', Auth::user()->country_id)->where('important', 1)->get();

        return view('profile.edit', compact('countries', 'languages', 'cities'));
    }

    public function index()
    {
        $country = Country::select("country_id as id", 'title_'.(\UriLocalizer::localeFromRequest()).' as name')->where('country_id', Auth::user()->country_id)->first();
        $language = Language::find(Auth()->user()->language_id);
        $city = City::select('city_id as id', 'title_'.\UriLocalizer::localeFromRequest().' as name')->where('city_id', Auth::user()->city_id)->first();

        return view('profile.index', compact('country', 'language', 'city'));
    }

    public function store_profile(Request $request) {
        if(empty($request['email'])){
            unset($request['email']);
        }
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'second_name' => 'required|max:255',
            'email' => 'email',
            'country_id' => 'required|numeric',
            'city_id' => 'required|numeric',
            'language_id' => 'required|numeric',
            'picture' => 'image',
            'vk' => 'max:255',
            'facebook' => 'max:255',
            'twitter' => 'max:255',
            'telegram' => 'max:255',
            'whatsapp' => 'max:255',
        ]);
        if(empty($request['email'])){
            $request['email'] = '';
        }

        if ($validator->fails()) {
            return redirect('profile/edit')
                ->withErrors($validator, 'profile')
                ->withInput();
        }

        if ($request->hasFile('picture')) {
            $name=time().mt_rand(1000,9999);

            $filename = $request->file('picture') . '';

            if (!file_exists('images/user/thumbnail/')) {
                mkdir('images/user/thumbnail/', 0777, true);
            }

            if (!$this->store_thumbnail($filename,$request->file('picture')->extension(),'images/user/thumbnail/'.$name.'.jpg', 200, 200)){
                $upload_error='Ошибка загрузки изображения';
                return redirect('profile/edit')
                    ->withInput($request->input())
                    ->withErrors($upload_error, 'profile');
            }

            if (!$this->store_resize($filename,$request->file('picture')->extension(),'images/user/'.$name.'.jpg')){
                $upload_error='Ошибка загрузки изображения';
                return redirect('profile/edit')
                    ->withInput($request->input())
                    ->withErrors($upload_error, 'profile');
            }

            if (file_exists('images/user/'.Auth::user()->avatar.'.jpg'))
            {
                unlink('images/user/'.Auth::user()->avatar.'.jpg');
            }
            if (file_exists('images/user/thumbnail/'.Auth::user()->avatar.'.jpg'))
            {
                unlink('images/user/thumbnail/'.Auth::user()->avatar.'.jpg');
            }

            Auth::user()->avatar = $name;
        }
        Auth::user()->update($request->all());
        
        $request->session()->flash('changes_saved', 'yes');

        return Redirect::back()->withInput()->with('changes_saved', 'yes');
    }

    public function store_password(Request $request) {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required|min:4',
            'password' => 'required|min:4|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect('profile/edit')
                ->withErrors($validator, 'password')
                ->withInput();
        }


        if (!Hash::check($request['old_password'], Auth::user()->password)) {
            $password_error='Введенный вами пароль не совпадает со старым';
            return redirect('profile/edit')
                ->withErrors($password_error, 'password')
                ->withInput();
        }

        $password = Hash::make($request['password']);

        Auth::user()->password = $password;
        Auth::user()->save();

        $request->session()->flash('password_saved', 'yes');
        /*TODO сделать сообщение об успешности сохранения в /profile; */

        return redirect('profile');
    }


    public function send_sms(Request $request){
        $validator = Validator::make($request->all(), [
            'phone' => 'required|string|max:255|min:2|unique:users'
        ]);

        if ($validator->fails()) {

            $data['error'] = 1;
            foreach ($validator->errors() as $key => $value) {
                $data['errors'][$key] = $value;
            }
            return response()->json([
                'error' => 1,
                'errors' => $validator->errors()
            ]);
        }
        $generated_password = rand(1000,99999);
        $response = Sms::sendSms($request['phone'], (string)$generated_password);

        if (strpos($response, 'ACCEPTED') === false) {
            return response()->json([
                'error' => 1,
                'sendingsms' => 'Ошибка отправки SMS. Свяжитесь с модераторами сайта',
                'errors' => ''
            ]);
        } else {
            $temp_data['phone'] = $request['phone'];
            $temp_data['user_id'] = Auth::id();
            $temp_data['code'] = $generated_password;
            ChangeNumber::create($temp_data);
        }
        return 'ok';
    }

    public function confirm_sms(Request $request){
        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
        ]);

        if ($validator->fails()) {
            return 'error';
        }

        $date = Carbon::now()->subMinutes(60)->toDateTimeString();

        ChangeNumber::where('created_at', '<=', $date)->delete();

        if(!$user = ChangeNumber::where('user_id', Auth::id())->latest()->first()){
            return 'error';
        }

        if($user->code != trim($request['code'])) {
            return 'error';
        }

        ChangeNumber::where('user_id', Auth::id())->delete();
        Auth::user()->phone = $user->phone;
        Auth::user()->save();

        $request->session()->flash('phone_changed', 'yes');
        /*TODO сделать сообщение об успешности сохранения в /profile; */

        return 'success';
    }


    public function get_cities($id) {
        $country = Country::findOrFail($id);
        $data = $country->cities()->select('city_id as id', 'title_'.(\UriLocalizer::localeFromRequest()).' as name')->get()->toArray();
        if(empty($data)) {
            $data = false;
        }

        return response()->json($data);
    }



    public function store_thumbnail($uploadedfile,$extension,$needed_name,$needed_width,$needed_height) {
        if($extension!='png' && $extension!='jpg' && $extension!='gif' && $extension!='jpeg')
        {
            return false;
        }
        if($extension=='gif') {
            $src = imagecreatefromgif($uploadedfile); //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='png') {
            $src = imagecreatefrompng($uploadedfile) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='jpg' || $extension=='jpeg') {
            $src = imagecreatefromjpeg($uploadedfile); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        list($width,$height)=getimagesize($uploadedfile);
        $tmp=imagecreatetruecolor($needed_width,$needed_height);
        $wasd=$width/$needed_width;
        $hasd=$height/$needed_height;
        imagecopyresampled($tmp,$src,0,0,$wasd>$hasd?intval(($width-$needed_width*$hasd)/2):0,$width<$height?intval(($height-$needed_height*$wasd)/2):0,$needed_width,$needed_height,$wasd>$hasd?intval($needed_width*$hasd):$width,$wasd<$hasd?intval($needed_height*$wasd):$height);
        $filename = $needed_name;
        if (!imagejpeg($tmp,$filename,100)){
            imagedestroy($src);
            imagedestroy($tmp);
            return false;
        }
        imagedestroy($src);
        imagedestroy($tmp);
        return true;
    }

    public function store_resize($uploadedfile,$extension,$needed_name) {
        if($extension!='png' && $extension!='jpg' && $extension!='gif' && $extension!='jpeg')
        {
            return false;
        }
        if($extension=='gif') {
            $src = imagecreatefromgif($uploadedfile); //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='png') {
            $src = imagecreatefrompng($uploadedfile) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='jpg' || $extension=='jpeg') {
            $src = imagecreatefromjpeg($uploadedfile); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        list($width,$height)=getimagesize($uploadedfile);
        if($width <=600 && $height <=600) {
            $new_width = $width;
            $new_height = $height;
        } elseif ($width > $height) {
            $new_width = 600;
            $new_height = $height*600/$width;
        } elseif ($width < $height) {
            $new_width = $width*600/$height;
            $new_height = 600;
        } else {
            $new_width = 600;
            $new_height = 600;
        }
        $tmp=imagecreatetruecolor($new_width,$new_height);
        imagecopyresampled($tmp,$src,0,0,0,0,$new_width,$new_height,$width,$height);

        $filename = $needed_name;
        if (!imagejpeg($tmp,$filename,100)){
            imagedestroy($src);
            imagedestroy($tmp);
            return false;
        }
        imagedestroy($src);
        imagedestroy($tmp);
        return true;
    }
}
