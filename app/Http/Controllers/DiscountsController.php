<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiscountsController extends Controller
{
    private $permitted = ['company_admin', 'company_manager'];

    public function manage() {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(!in_array(Auth::user()->group, $this->permitted)) {
            abort(403, 'Unauthorized action.');
        }


        return view('discounts.manage', compact('data'));
    }
}
