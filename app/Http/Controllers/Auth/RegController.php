<?php

namespace App\Http\Controllers\Auth;

use App\Sms;
use App\Tempuser;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function send_sms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|string|max:255|unique:users'
        ]);

        if ($validator->fails()) {
            return 'error';
        }

        $generated_password = $this->generatedPassword();
        $response = Sms::sendSms($request['phone'], (string)$generated_password);

        if (strpos($response, 'ACCEPTED') === false) {
            return 'error';
        } else {
            $temp_user = $request->all();
            $temp_user['password'] = $generated_password;
            Tempuser::create($temp_user);
        }
        return $request['phone'];
    }

    /**
     * Send restore sms
     *
     * @param Request $request
     * @return mixed|string
     */
    public function send_restore_sms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return 'error';
        }
        $user = User::where('phone', $request['phone'])->first();
        if (!$user){
            return 'error';
        }

        $generated_password = $this->generatedPassword();
        $response = Sms::sendSms($request['phone'], (string)$generated_password);

        if (strpos($response, 'ACCEPTED') === false) {
            return 'error';
        } else {
            $temp_user = $request->all();
            $temp_user['password'] = $generated_password;
            Tempuser::create($temp_user);
        }
        return $request['phone'];
    }

    public function confirm_sms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sms' => 'required|string',
        ]);

        if ($validator->fails()) {
            return 'error';
        }

        $date = Carbon::now()->subMinutes(60)->toDateTimeString();

        Tempuser::where('created_at', '<=', $date)->delete();

        if(!$user = Tempuser::where('phone', $request['phone'])->latest()->first()){
            return 'error';
        }

        if($user->password === trim($request['sms'])) {
            return 'success';
        } else {
            return 'error';
        }
    }

    public function confirm_restore_sms(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'sms' => 'required|string',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return 'error';
        }

//        $date = Carbon::now()->subMinutes(60)->toDateTimeString();

//        Tempuser::where('created_at', '<=', $date)->delete();

        if(!$user = Tempuser::where('phone', $request['phone'])->latest()->first()){
            return 'error';
        }

        if($user->password === trim($request['sms'])) {
            $user = User::where('phone', $request['phone'])->update(['password' => Hash::make($request['password'])]);
            // $user->password = Hash::make($request['password']);
            return 'success';
        } else {
            return 'error';
        }
    }


    public function send_sms1(Request $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'second_name' => 'required|string|max:255',
            'phone' => 'required|string|max:255|unique:users',
        ])->validate();
        $generated_password = $this->generatedPassword();

        $response = Sms::sendSms($request['phone'], $generated_password);

        if (strpos($response, 'ACCEPTED') === false) {
            return 'error';
        } else {
            $temp_user = $request->all();
            $temp_user['password'] = $generated_password;
            Tempuser::create($temp_user);
        }
        return redirect('register-user')->with('phone', $temp_user['phone']);
    }

    public function ready_to_complete(){
        if(session('phone')){
            $phone = session('phone');
        } elseif (!empty(trim(old('phone')))) {
            $phone = trim(old('phone'));
        } else {
            return redirect('/');
        }
        return view('auth.register-user', compact('phone'));
    }

    public function complete(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone'=>'required|string|max:255|unique:users',
            'password'=>'required|string|max:255',
            'name'=>'required|string|max:255',
            'second_name'=>'required|string|max:255',
            'email'=>'required|string|max:255'
        ]);
        if ($validator->fails()) {
            return redirect(url()->previous().'#login_register')
                ->withErrors($validator)
                ->withInput($request->all());
        }

        if(!$user = Tempuser::where('phone', $request['phone'])->latest()->first()){
            return redirect(url()->previous());
        }

        $date = Carbon::now()->subMinutes(60)->toDateTimeString();

        Tempuser::where('created_at', '<=', $date)->delete();

        if($user->password === trim($request['password'])) {
            Tempuser::where('phone', $request['phone'])->delete();
        } else {
            return redirect(url()->previous());
        }

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return redirect('profile');
    }


    public function complete1(Request $request)
    {
        Validator::make($request->all(), [
            'password' => 'required|string|max:255',
        ])->validate();

        $validator = Validator::make($request->all(), [
            'phone'=>'required|string|max:255|unique:users',
        ]);
        if ($validator->fails()) {
            return redirect('register');
        }

        $date = Carbon::now()->subMinutes(60)->toDateTimeString();

        Tempuser::where('created_at', '<=', $date)->delete();

        if(!$user = Tempuser::where('phone', $request['phone'])->latest()->first()){
            return redirect('register');
        }

        if($user->password === trim($request['password'])) {
            $data['name'] = $user->name;
            $data['second_name'] = $user->second_name;
            $data['phone'] = $user->phone;
            $data['password'] = $user->password;
            Tempuser::where('phone', $data['phone'])->delete();
        } else {
            return redirect('register-user')
                ->withInput($request->input());
        }

        event(new Registered($user = $this->create($data)));

        $this->guard()->login($user);

        return redirect('profile');
    }

    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'second_name' => $data['second_name'],
            'phone' => $data['phone'],
            'password' => bcrypt($data['password']),
        ]);
    }


    protected function guard()
    {
        return Auth::guard();
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'phone' => 'required|string|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }


    public function generatedPassword()
    {
        // mb unique here?
        $code = rand(1000,99999);
        return $code;
    }

    public function successLoginKit()
    {
        $app_id = env('ACCOUNTKIT_APP_ID');
        $secret = env('ACCOUNTKIT_APP_SECRET');
        $version = env('ACCOUNTKIT_VERSION');

        // Exchange authorization code for access token
        $token_exchange_url = 'https://graph.accountkit.com/'.$version.'/access_token?'.
            'grant_type=authorization_code'.
            '&code='.$_GET['code'].
            "&access_token=AA|$app_id|$secret";
        $data = $this->doCurl($token_exchange_url);
        var_dump($data);
        $user_id = $data['id'];
        $user_access_token = $data['access_token'];
        $refresh_interval = $data['token_refresh_interval_sec'];

        // Get Account Kit information
        $me_endpoint_url = 'https://graph.accountkit.com/'.$version.'/me?'.
            'access_token='.$user_access_token;
        $data = $this->doCurl($me_endpoint_url);
        dd($data);
        $phone = isset($data['phone']) ? $data['phone']['number'] : '';
        $email = isset($data['email']) ? $data['email']['address'] : '';
        dd($user_id, $refresh_interval, $phone, $email);
    }

    // Method to send Get request to url
    function doCurl($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $data = json_decode(curl_exec($ch), true);
        curl_close($ch);
        return $data;
    }
}
