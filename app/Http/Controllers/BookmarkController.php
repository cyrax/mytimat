<?php


namespace App\Http\Controllers;


use App\Bookmark;
use Illuminate\Support\Facades\Auth;

class BookmarkController extends Controller
{
    public function index()
    {
        $bookmarks = Bookmark::with('company')->where('user_id', Auth::id())->get();
        return view('bookmark.index', compact('bookmarks'));
    }
}