<?php

namespace App\Http\Controllers;

use App\Member;
use App\MemberGroup;
use App\MemberRole;
use App\Sms;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class MembersController extends Controller
{
    public function join() {
        return view('company.join');
    }

    public function members() {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(Auth::user()->group != 'company_admin') {
            abort(403, 'Unauthorized action.');
        }

        $data['company'] = Auth::user()->company()->first();

        $data['specialists'] = $data['company']->members()->get();
        $data['roles'] = $data['company']->roles()->get();

        return view('company.members', compact('data'));
    }

    public function delete(Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(Auth::user()->group != 'company_admin') {
            abort(403, 'Unauthorized action.');
        }

        $data = $request->all();

        $company = Auth::user()->company()->first();

        if($company->id != $data['company']) {
            abort(403, 'Unauthorized action.');
        }

        $user = User::findOrFail($data['user']);

        if($user->company_id != $data['company']){
            abort(403, 'Unauthorized action.');
        }

        $user->group = 'user';
        $user->company_id = 0;
        $user->specialist = 0;
        $user->save();

        return 'success';
    }

    public function delete_role(Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(Auth::user()->group != 'company_admin') {
            abort(403, 'Unauthorized action.');
        }

        $data = $request->all();
        MemberRole::where('id', $data['role'])->delete();

        return 'success';
    }

    public function update(Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(Auth::user()->group != 'company_admin') {
            abort(403, 'Unauthorized action.');
        }

        $data = $request->all();
        if(empty($data['specialists'])) {
            $request->session()->flash('no_changes', 'yes');
            return redirect('company/members');
        }

        // $company = Auth::user()->company()->first();

        foreach ($data['specialists'] as $service) {
            /** @var Member $member */
            $member = Member::find($service['id']);
            $user = User::find($member->user_id);

            if(empty($user)){
                continue;
            }

            if(!empty($service['phone'])) {
                $user->phone = $service['phone'];
            }
            MemberGroup::deleteOld($member->id);
            $user->specialist = 0;
            foreach ($service['group'] as $group) {
                $memberGroup = new MemberGroup();
                $memberGroup->member_id = $member->id;
                switch ($group) {
                    case 'company_admin':
                        $memberGroup->group = 'company_admin';
                        break;
                    case 'company_manager':
                        $memberGroup->group = 'company_manager';
                        break;
                    case 'company_specialist':
                        $user->specialist = 1;
                        $memberGroup->group = 'company_specialist';
                        break;
                }
                $memberGroup->save();

            }

            switch ($service['specialist']) {
                case 0:
                    $member->accepts = 0;
                    break;
                case 1:
                    $member->accepts = 1;
                    break;
            }
            $member->role = $service['role'] ?? 0;
            $member->save();
            $user->save();
        }

        $request->session()->flash('changes_saved', 'yes');
        return redirect('company/members');
    }

    public function store_role(Request $request)
    {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(Auth::user()->group != 'company_admin') {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'role' => 'required|max:255|min:2',
        ]);

        if ($validator->fails()) {
            return redirect('company/members#add_role')
                ->withErrors($validator, 'add_role')
                ->withInput();
        }

        $role = new MemberRole();
        $role->name = $request['role'];
        $role->company_id = Auth()->user()->company_id;
        $role->save();

        $request->session()->flash('changes_saved', 'yes');
        return redirect('company/members');
    }

    public function add_member(Request $request) {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(Auth::user()->group != 'company_admin') {
            abort(403, 'Unauthorized action.');
        }
        $validator = Validator::make($request->all(), [
            'phone' => 'required|max:255|min:2',
            'name' => 'required|max:255',
            'surname' => 'required|max:255',
            'group_type' => [
                'required',
                Rule::in(['master', 'manage_specialist', 'the_site_admin']),
            ],
            'specialist' => 'boolean',
        ]);

        if ($validator->fails()) {
            return redirect('company/members#add_member')
                ->withErrors($validator, 'add_member')
                ->withInput();
        }

        $new_user = $request->all();

        $company = Auth::user()->company()->first();

        $new_user['company_id'] = $company->id;

        switch ($new_user['group_type']){
            case 'master':
                $new_user['group'] = 'company_specialist';
                $new_user['group_name'] = ' specialist';
                break;
            case 'manage_specialist':
                $new_user['group'] = 'company_manager';
                $new_user['group_name'] = ' manager';
                break;
            case 'the_site_admin':
                $new_user['group'] = 'company_admin';
                $new_user['group_name'] = 'n administrator';
                break;
        }
        $generated_password = rand(100000,99999999);
        $text = 'You were registered at bukutime.com by company "'.$company->name.'" as a'.$new_user['group_name'].' . Your password is '.$generated_password.'. Use your phone number to sign in.';
        $response = Sms::sendSms($request['phone'], $text);

        if (strpos($response, 'ACCEPTED') === false) {
            $request->session()->flash('member_adding_error', 'yes');
        } else {
            $new_user['password'] = Hash::make($generated_password);
            $user = User::where('phone', $request['phone'])->first();

            try {
                if (is_null($user)) {
                    $user = new User();
                    $user->phone = $request['phone'];
                    $user->password = Hash::make($generated_password);
                    $user->name = $request['name'];
                    $user->second_name = $request['surname'];
                    $user->company_id = $company->id;
                }
                if ($new_user['group'] === 'company_specialist')
                    $user->specialist = true;
                $user->group = $new_user['group'];
                $user->save();

                $member = new Member();
                $member->user_id = $user->id;
                $member->company_id = $company->id;
                $member->name = $request['name'];
                $member->last_name = $request['surname'];
                $member->accepts = 1;
                $member->save();

                $memberGroup = new MemberGroup();
                $memberGroup->member_id = $member->id;
                $memberGroup->group = $new_user['group'];
                $memberGroup->save();
            }
            catch (\Exception $e) {
                dd($e->getMessage());
            }
            $request->session()->flash('member_added', 'yes');
        }
        return redirect('company/members');
    }
}
