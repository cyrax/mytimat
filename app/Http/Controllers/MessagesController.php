<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\UserMessage;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\Finder\Finder;

class MessagesController extends Controller
{
    public function index(Request $request){
        $query = [];
        if(!empty($request['start'])) {
            try {
                $start = \Carbon\Carbon::parse($request['start'])->toDateString();
                $query[] = ['user_messages.created_at', '>=', $start];
            }
            catch (\Exception $err) {
                $request['start'] = '';
            }
        }
        if(!empty($request['end'])) {
            try {
                $end = \Carbon\Carbon::parse($request['end'])->addDay()->toDateString();
                $query[] = ['user_messages.created_at', '<=', $end];
            }
            catch (\Exception $err) {
                $request['end'] = '';
            }
        }
        if(!empty($request['type'])) {
            switch ($request['type']) {
                case 'user':
                    $query['type'] = 'user';
                    break;
                case 'company':
                    $query['type'] = 'company';
                    break;
                case 'system':
                    $query['type'] = 'system';
                    break;
                default:
                    $request['type'] = '';
            }
        }
        if(!empty($request['status'])) {
            switch ($request['status']) {
                case 'read':
                    $query[] = ['read_at', '!=', null];
                    break;
                case 'non_read':
                    $query['read_at'] = null;
                    break;
                default:
                    $request['status'] = '';
            }
        }

        $messages = UserMessage::where('owner_id', Auth::id())
                        ->where($query)
//                        ->select('subject_id', 'user_messages.updated_at')
                        ->orderBy('id','desc');
//                        ->distinct();

        if(!empty(trim($request['text']))) {
            $messages = $messages->leftJoin('users', 'user_messages.companion_id', '=', 'users.id')->search(trim($request['text']));
        }
        $messages = $messages->get();
        $paginator = new LengthAwarePaginator($messages, $messages->count(), 10);
        if(!$request['page']){
            $request['page'] = 1;
        }
        $messages = $messages->forPage($request['page'],10);
        $paginator->withPath('messages');
        return view('messages.index', compact('messages', 'request', 'paginator'));
    }

    public function delete(Request $request){
        if($request['subjects']){
            foreach ($request['subjects'] as $key => $value) {
                UserMessage::where(['subject_id' => $key, 'owner_id' => Auth::id()])->delete();
            }
        }

        return back()->with('deleted', 'yes');
    }

    public function show($message_id, $subject_id = 1)
    {
      $message = UserMessage::find($message_id);
      $message->read_at = Carbon::now();
      $message->timestamps = false;
      $message->save();
      $message->timestamps = true;

      $messages = UserMessage::where(['owner_id' => Auth::id(), 'subject_id' => $subject_id])->latest()->get();
        // foreach ($messages as $message) {
        //     if($message->read_at == null){
        //         $message->read_at = Carbon::now();
        //         $message->timestamps = false;
        //         $message->save();
        //         $message->timestamps = true;
        //     }
        // }
        $conversation = $messages->toArray()[0];
        $subject = $conversation['subject'];
        if($conversation['type'] != 'system') {
            $companion = User::findOrFail($conversation['companion_id']);

            if($companion->avatar) {
                $avatars['companion'] = '/images/user/thumbnail/'.$companion->avatar.'.jpg';
            } else {
                $avatars['companion'] = '/image/no-avatar.jpg';
            }
        } else {
            $companion = 'system';
            $avatars['companion'] = '/image/system.jpg';
        }

        if(Auth::user()->avatar) {
            $avatars['owner'] = '/images/user/thumbnail/'.Auth::user()->avatar.'.jpg';
        } else {
            $avatars['owner'] = '/image/no-avatar.jpg';
        }
        $id = $subject_id;

        return view('messages.show',
            compact(
                'messages',
                'companion',
                'avatars',
                'id',
                'subject',
                'conversation'
            )
        );
    }

    public function store(Request $request, $id){
        $validator = Validator::make($request->all(), [
            'text' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect('user/messages/subject/'.$id);
        }

        $message = UserMessage::where('subject_id', $id)->first();

        if(empty($message)){
            $companion_id = $request['companion'];
            $subject = $request['subject'];
        } else {
            $companion_id = $message['companion_id'];
            $subject = $message['subject'];
        }

        if(Auth::user()->company_id) {
            $type = 'company';
        } else {
            $type = 'user';
        }


        if($message->companion->company_id) {
            $com_type = 'company';
        } else {
            $com_type = 'user';
        }

        $owner = UserMessage::create([
            'subject' => $subject,
            'text'=>$request['text'],
            'owner_id' => Auth::id(),
            'companion_id' => $companion_id,
            'from' => 'owner',
            'subject_id' => $id,
            'type' => $type,
        ]);

        $companion = UserMessage::create([
            'subject' => $subject,
            'text'=>$request['text'],
            'owner_id' => $companion_id,
            'companion_id' => Auth::id(),
            'from' => 'companion',
            'subject_id' => $id,
            'type' => $com_type,
        ]);

        $now = Carbon::now();

        UserMessage::where('subject_id', $id)->update(['updated_at' => $now]);


        $pictures = $request->allFiles();
        if(!empty($pictures)) {
            foreach ($pictures['pictures'] as $picture) {

                $name=time().mt_rand(1000,9999);

                $filename = $picture . '';

                if (!file_exists('images/attachment/thumbnail/')) {
                    mkdir('images/attachment/thumbnail/', 0777, true);
                }

                if (!$this->store_thumbnail($filename,$picture->extension(),'images/attachment/thumbnail/'.$name.'.jpg', 127, 80)){
                    continue;
                }

                if ($this->store_resize($filename,$picture->extension(),'images/attachment/'.$name.'.jpg')){
                    Attachment::create(['url' => $name,
                        'subject_id' => $id,
                        'owner' => $owner->id,
                        'companion' => $companion->id]);
                }
            }
        }

        return redirect('user/messages/subject/'.$id);
    }




    public function store_thumbnail($uploadedfile,$extension,$needed_name,$needed_width,$needed_height) {
        if($extension!='png' && $extension!='jpg' && $extension!='gif' && $extension!='jpeg')
        {
            return false;
        }
        if($extension=='gif') {
            $src = imagecreatefromgif($uploadedfile); //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='png') {
            $src = imagecreatefrompng($uploadedfile) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='jpg' || $extension=='jpeg') {
            $src = imagecreatefromjpeg($uploadedfile); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        list($width,$height)=getimagesize($uploadedfile);
        $tmp=imagecreatetruecolor($needed_width,$needed_height);
        $wasd=$width/$needed_width;
        $hasd=$height/$needed_height;
        imagecopyresampled($tmp,$src,0,0,$wasd>$hasd?intval(($width-$needed_width*$hasd)/2):0,$width<$height?intval(($height-$needed_height*$wasd)/2):0,$needed_width,$needed_height,$wasd>$hasd?intval($needed_width*$hasd):$width,$wasd<$hasd?intval($needed_height*$wasd):$height);
        $filename = $needed_name;
        if (!imagejpeg($tmp,$filename,100)){
            imagedestroy($src);
            imagedestroy($tmp);
            return false;
        }
        imagedestroy($src);
        imagedestroy($tmp);
        return true;
    }

    public function store_resize($uploadedfile,$extension,$needed_name) {
        if($extension!='png' && $extension!='jpg' && $extension!='gif' && $extension!='jpeg')
        {
            return false;
        }
        if($extension=='gif') {
            $src = imagecreatefromgif($uploadedfile); //если оригинал был в формате gif, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='png') {
            $src = imagecreatefrompng($uploadedfile) ;//если оригинал был в формате png, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        if($extension=='jpg' || $extension=='jpeg') {
            $src = imagecreatefromjpeg($uploadedfile); //если оригинал был в формате jpg, то создаем изображение в этом же формате. Необходимо для последующего сжатия
        }
        list($width,$height)=getimagesize($uploadedfile);
        if($width <=1000 && $height <=1000) {
            $new_width = $width;
            $new_height = $height;
        } elseif ($width > $height) {
            $new_width = 1000;
            $new_height = $height*1000/$width;
        } elseif ($width < $height) {
            $new_width = $width*1000/$height;
            $new_height = 1000;
        } else {
            $new_width = 1000;
            $new_height = 1000;
        }
        $tmp=imagecreatetruecolor($new_width,$new_height);
        imagecopyresampled($tmp,$src,0,0,0,0,$new_width,$new_height,$width,$height);

        $filename = $needed_name;
        if (!imagejpeg($tmp,$filename,100)){
            imagedestroy($src);
            imagedestroy($tmp);
            return false;
        }
        imagedestroy($src);
        imagedestroy($tmp);
        return true;
    }
}
