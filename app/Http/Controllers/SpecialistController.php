<?php


namespace App\Http\Controllers;


use App\Company;
use App\Country;
use App\Member;
use App\Order;
use App\Setting;
use App\SpecialistPictures;
use Dingo\Api\Exception\StoreResourceFailedException;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Waavi\Translation\Facades\UriLocalizer;

class SpecialistController extends Controller
{
    public function orders()
    {
        if(!Auth::check()) {
            abort(403, 'Unauthorized action.');
        }

        if(!Auth::user()->specialist) {
            abort(403, 'Unauthorized action.');
        }
        // we get all the memberships in which he or she in

        $data['orders'] = Order::with(['service', 'specialist', 'user'])->whereIn('specialist_id', Member::where('user_id', Auth::id())->select('id'))->get();

        $events = [];
        foreach ($data['orders'] as $order) {
            $service = $order->service;
            $color = '#4CAF50';
            switch ($order->status) {
                case Order::STATUS_NEW:
                    $color = '#3a87ad';
                    break;
                case ORDER::STATUS_REJECTED:
                    $color = '#F00';
                    break;
            }
            $events[] = \Calendar::event(
                $service->name . " \n". $order->company->name . "\n" . $order->specialist->last_name,
                false,
                $order->date . ' ' . $order->time,
                date('H:i:s', strtotime('+' . $service->days . ' days +' . $service->hours . ' hours +' . $service->minutes . ' minutes', strtotime($order->date . ' ' . $order->time))),
                0, // event ID
                [
                    'color' => $color,
                    'url' => '/order/' . $order->id,
                    'slotMinutes' => '5'
                ]
            );
        }

        $calendar = \Calendar::addEvents($events)
            ->setOptions([
                'firstDay' => 1,
                'defaultView' => 'agendaWeek',
                'aspectRatio' => '1.5',
                'allDaySlot' => false,
                'locale' => UriLocalizer::localeFromRequest()
            ])->setCallbacks([ //set fullcalendar callback options (will not be JSON encoded)
                'dayClick' => 'function(date) {
                    $("#chosenTime").text(date.format());
                    $("#order_date").val(date.format());
                    $("#new-order-modal").modal().show();
                    $(".modal-backdrop").remove();
                }'
            ]);

        return view('specialist.orders', compact('calendar'));
    }

    public function settings(Request $request)
    {
        if (!\Auth::check()) {
            abort(403, 'Forbidden');
        }
        $member = Member::where('user_id', auth()->user()->id)->first();
        $data['setting'] = $member->setting;
        if ($request->isMethod('post')) {
            Setting::saveSpecialistSettings($request, $member);
            return trans('app.data_updated', [], UriLocalizer::localeFromRequest());
        }
        return view('user.settings', compact('data'));
    }

    public function profile($id = null, Request $request)
    {
        if (!isset($id))
            $id = \auth()->user()->id;
        $member = Member::where(['user_id' => $id])->firstOrFail();

        $data['pictures'] = $member->pictures->toArray();
        $day_of_week = CompaniesController::dayOfWeek(date('N'));

        $data['opened'] = $member[$day_of_week];

        $data['now'] = Carbon::now();
        $data['day_of_week'] = $day_of_week;

        //$services = $member->services()->where('category_id', '!=', 0)->get()->sortBy('category_id');

        return view('specialist.profile', compact('member', 'data'));
    }

    public function rate(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'specialist_id' => 'required|max:255',
            'value' => 'required|max:5'
        ]);

        if ($validator->fails()) {
            abort(422, 'not provided enough data');
        }

        $member = Member::findOrFail($request->input('specialist_id'));

        $rating = new \willvincent\Rateable\Rating;
        $rating->rating = $request->input('value');
        $rating->user_id = auth()->user()->id;
        $member->ratings()->save($rating);
    }

    public function edit($id, Request $request)
    {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }
        $member = Member::where('id', $id)->where('user_id', auth()->user()->id)->firstOrFail();

        if ($request->isMethod('post')) {
            Member::updateOrSave($request, $member);
            SpecialistPictures::createFromRequest($request, $member->id);

            return redirect('/profile');
        }

        return view('specialist.edit', compact('categories', 'countries', 'member'));
    }

    public function availableSpecialists($company_id, Request $request)
    {
        $rules = [
            'date' => ['required'],
            'time' => ['required']
        ];

        $payload = app('request')->only('date', 'time');

        $validator = app('validator')->make($payload, $rules);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator);
        }

        $date = $request->get('date');
        $time = $request->get('time').':00';
        $dateTime = $date.' '.$time;

        $members = Member::whereHas('schedule', function ($query) use ($dateTime, $time) {
            $weekName = strtolower(date('l', strtotime($dateTime)));
            $query->where($weekName, 1);
            $query->where($weekName.'_start', '<=', $time);
            $query->where($weekName.'_end', '>', $time);
        })->with('pictures')
            ->select(['id', 'name', 'last_name'])
            ->where('company_id', $company_id)
            ->get()
            ->toArray();

        return response()->json($members);

    }

    public function picture_upload(){
        $upload = new UploadPictureFile;
    }
}