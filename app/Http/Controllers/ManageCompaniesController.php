<?php

namespace App\Http\Controllers;

use App\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ManageCompaniesController extends Controller
{
    private $manage = ['administrator', 'manager', 'company_admin'];
    private $administrate = ['administrator'];

    public function index() {
        if(!Auth::check()){
            abort(403, 'Unauthorized action.');
        }

        if(!in_array(Auth::user()->group, $this->manage)) {
            abort(403, 'Unauthorized action.');
        }

        $companies = Company::paginate(15);

        return view('company.manage', compact('companies'));
    }
}
