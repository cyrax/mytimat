<?php

namespace App\Http\Controllers;

use App\Category;
use App\Page;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PagesController extends Controller
{
    public function index(){
        $categories = Category::where('parent_id', 0)->limit(6)->get();
        return view('index', compact('categories'));
    }

    public function help() {
        return view('pages.help');
    }


    public function balance(){

        return view('balance.index');
    }

    public function view($url)
    {
        $page = Page::where('url', $url)->first();

        return view('pages.view', compact('page'));
    }

    public function about()
    {
        return view('pages.about');
    }

    public function contacts()
    {
        return view('pages.contacts');
    }
}
