<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class NormalizePhone
{
    /**
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request['phone']))
            $request['phone'] = str_replace(['+', '-', ')', '(', ' '], '', $request['phone']);

        return $next($request);
    }
}