<?php


namespace App\Http\Middleware;


use Closure;

class TranslationMiddleware extends \Waavi\Translation\Middleware\TranslationMiddleware
{
    public function handle($request, Closure $next, $segment = 0)
    {
        if (preg_match('#'.implode('|', ['admin', 'images', 'api']).'#', $request->path()))
            return $next($request);

        return parent::handle($request, $next, $segment);
    }
}