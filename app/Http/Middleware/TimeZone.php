<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

class TimeZone {

    /**
     * The current logged in user instance
     * @var [type]
     */
    protected $user;

    /**
     * creates an instance of the middleware
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->user = $auth->user();
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->setTimeZone($request);

        return $next($request);
    }

    /**
     * sets the time zone from cookie or from the user setting
     * @param Illuminate\Http\Request $request
     */
    public function setTimeZone($request)
    {
        if($this->user)
        {
          $time_zone = optional($this->user->city)->timezone;
            return date_default_timezone_set($time_zone ?? 'Asia/Almaty');
        }
    }
}
