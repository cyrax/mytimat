<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Companyrate
 *
 * @property int $id
 * @property int $rate
 * @property int $company_id
 * @property int $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Company $company
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Companyrate whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Companyrate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Companyrate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Companyrate whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Companyrate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Companyrate whereUserId($value)
 * @mixin \Eloquent
 */
class Companyrate extends Model
{
    protected $fillable = [
        'rate',
        'company_id',
        'user_id',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
