<?php


namespace App;

/**
 * Class Sms
 * @package App
 */
class Sms
{
    /**
     * @param string $to
     * @param string $text
     * @return mixed
     */
    public static function sendSms($to, $text)
    {
        $AUTH_ID = env('SMS_AUTH_ID');
        $AUTH_TOKEN = env('SMS_AUTH_TOKEN');
        $url = env('SMS_AUTH_URL').$to;
        $from = '77774874747';
        $to = str_replace('+','',$to);
        $data = array(
//            'src'       =>  $from,
//            'dst'       =>  $to,
            'text'       =>  $text,
//            'type'       =>  'sms',
//            'url'       =>  'http://friendly/api/sms',
//            'method'       =>  'POST',
//            'log'       =>  'true'
        );

        $data_string = json_encode($data);
        try {
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
            curl_setopt($ch, CURLOPT_TIMEOUT, 120);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERPWD, $AUTH_ID . ":" . $AUTH_TOKEN);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
            $response = curl_exec($ch);
            curl_close($ch);
            return $response;
        }
        catch (\Exception $e) {
            var_dump($e->getMessage());
            return 'error';
        }
    }
}
