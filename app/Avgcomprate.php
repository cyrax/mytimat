<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Avgcomprate
 *
 * @property int $id
 * @property int $company_id
 * @property float $rate
 * @property int $count
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avgcomprate whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avgcomprate whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avgcomprate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avgcomprate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avgcomprate whereRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Avgcomprate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Avgcomprate extends Model
{
    protected $table = 'avg_comp_rates';

    protected $fillable = [
        'company_id',
        'rate',
        'count',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }
}
