<?php

namespace App;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @package App
 * @property string $name
 * @property string $second_name
 * @property string $group
 * @property integer $company_id
 * @property boolean $specialist
 * @property string $phone
 * @property string $email
 * @property int $id
 * @property string|null $city
 * @property string|null $avatar
 * @property int $tims
 * @property int $language_id
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $country_id
 * @property int|null $city_id
 * @property string|null $vk
 * @property string|null $facebook
 * @property string|null $twitter
 * @property string|null $telegram
 * @property string|null $whatsapp
 * @property-read \App\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Companyrate[] $company_rates
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $i_approved_orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $i_ordered
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Message[] $messages
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders_to_me
 * @property-read \App\Company $own_company
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\User onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSecondName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereSpecialist($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTims($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereVk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereWhatsapp($value)
 * @method static \Illuminate\Database\Query\Builder|\App\User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\User withoutTrashed()
 * @mixin \Eloquent
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Member[] $members
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereDeletedAt($value)
 * @property-read \App\Setting $setting
 * @property int $company_admin
 * @property int $moderator
 * @property int $translator
 * @property int $techsupport
 * @property int $manager
 * @property int $user
 * @property int $administrator
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereAdministrator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCompanyAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereManager($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereModerator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTechsupport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereTranslator($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUser($value)
 */
class User extends Authenticatable
{
    use Notifiable;
    use \HighIdeas\UsersOnline\Traits\UsersOnlineTrait;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'second_name', 'email', 'phone', 'password', 'country_id', 'city_id',
        'group', 'company_id', 'specialist',
        'vk', 'facebook', 'twitter', 'telegram', 'whatsapp', 'language_id',
        'user', 'administrator', 'translator', 'specialist', 'moderator', 'techsupport'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getFullName(){
        return $this->attributes['name'].' '.$this->attributes['second_name'];
    }


    public function company_rates()
    {
        return $this->hasMany('App\Companyrate');
    }

    public function own_company()
    {
        return $this->hasOne('App\Company', 'user_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function orders_to_me()
    {
        return $this->hasMany('App\Order', 'specialist_id');
    }

    public function i_ordered()
    {
        return $this->hasMany('App\Order', 'user_id');
    }

    public function setting()
    {
        return $this->morphOne('App\Setting', 'settingable');
    }

    public function i_approved_orders()
    {
        return $this->hasMany('App\Order', 'approved_id');
    }

    public function members()
    {
        return $this->hasMany('App\Member');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'country_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'city_id');
    }

    /**
     * A user can have many messages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public static function groups()
    {
        return [
            'user' => 'Пользователь',
            'company_specialist' => 'Специалист компаний',
            'company_admin' => 'Администратор компаний',
            'company_manager' => 'Менеджер компаний',
            'administrator' => 'Администратор',
        ];
    }
}
