<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\City
 *
 * @property int $id
 * @property string $name
 * @property string $name_translation
 * @property int $country_id
 * @property-read \App\Country $country
 * @property-read mixed $raw
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereNameTranslation($value)
 * @mixin \Eloquent
 * @property int $city_id
 * @property int $important
 * @property int|null $region_id
 * @property string|null $title_ru
 * @property string|null $area_ru
 * @property string|null $region_ru
 * @property string|null $title_ua
 * @property string|null $area_ua
 * @property string|null $region_ua
 * @property string|null $title_be
 * @property string|null $area_be
 * @property string|null $region_be
 * @property string|null $title_en
 * @property string|null $area_en
 * @property string|null $region_en
 * @property string|null $title_es
 * @property string|null $area_es
 * @property string|null $region_es
 * @property string|null $title_pt
 * @property string|null $area_pt
 * @property string|null $region_pt
 * @property string|null $title_de
 * @property string|null $area_de
 * @property string|null $region_de
 * @property string|null $title_fr
 * @property string|null $area_fr
 * @property string|null $region_fr
 * @property string|null $title_it
 * @property string|null $area_it
 * @property string|null $region_it
 * @property string|null $title_pl
 * @property string|null $area_pl
 * @property string|null $region_pl
 * @property string|null $title_ja
 * @property string|null $area_ja
 * @property string|null $region_ja
 * @property string|null $title_lt
 * @property string|null $area_lt
 * @property string|null $region_lt
 * @property string|null $title_lv
 * @property string|null $area_lv
 * @property string|null $region_lv
 * @property string|null $title_cz
 * @property string|null $area_cz
 * @property string|null $region_cz
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaBe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaCz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaDe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaFr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaIt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaJa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaLt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaPl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaPt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereAreaUa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereImportant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionBe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionCz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionDe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionFr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionIt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionJa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionLt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionPl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionPt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereRegionUa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleBe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleCz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleDe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleFr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleIt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleJa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleLt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitlePl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitlePt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleUa($value)
 * @property string $title_hr
 * @method static \Illuminate\Database\Eloquent\Builder|\App\City whereTitleHr($value)
 */
class City extends Model
{
    protected $primaryKey = 'city_id';
    public $timestamps = false;

    protected $translatableAttributes = ['name'];

    public $fillable = [
        'title_ru',
        'title_en',
        'title_hr',
        'title_it',
        'country_id',
        'timezone',
    ];

    public function country()
    {
        return $this->belongsTo('App\Country');
    }
}
