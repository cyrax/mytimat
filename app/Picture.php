<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\Picture
 *
 * @property int $id
 * @property int $company_id
 * @property string $url
 * @property int $main
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Picture whereUrl($value)
 * @mixin \Eloquent
 */
class Picture extends Model
{
    protected $fillable = [
        'company_id',
        'url',
        'main',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public static function createPicturesForCompany(Request $request, $id)
    {
        $avatar = 0;
        $ids = [];
        if (isset($request['pictures_urls'])) {
          foreach ($request['pictures_urls'] as $pictures_url) {
              if (!preg_match('/\.(gif|jpe?g|png)$/i', $pictures_url)) {
                  continue;
              }
              $data = ['company_id' => $id, 'url' => $pictures_url, 'main' => $avatar == 0 ? 1 : 0];
              if (isset($pictures_url['id']) && !empty($pictures_url['id'])) {
                  Picture::where('id', $pictures_url['id'])->update($data);
                  $ids[] = $pictures_url['id'];
              }
              else {
                  $ids[] = Picture::create($data)->id;
              }
              $avatar++;
          }
        }
        Picture::whereNotIn('id', $ids)->where('company_id', $id)->delete();
    }
}
