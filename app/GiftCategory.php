<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\GiftCategory
 *
 * @property int $id
 * @property string $name
 * @property int $parent_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Gift[] $gifts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GiftCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GiftCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GiftCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GiftCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\GiftCategory whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class GiftCategory extends Model
{
    protected $table = 'gift_categories';

    protected $fillable = [
        'name',
        'parent_id'
    ];

    public function gifts() {
        return $this->hasMany('App\Gift', 'category_id');
    }
}
