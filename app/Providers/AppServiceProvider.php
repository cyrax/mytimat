<?php

namespace App\Providers;

use App\Country;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

//        Schema::defaultStringLength(191);
        view()->composer('partials.geo.country', function($view) {
            $countries = Country::select("country_id as id", 'title_'.(\UriLocalizer::localeFromRequest()).' as name')->get();
            $view->with('countries', $countries);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      if ($this->app->environment() == 'local') {
          $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
      }
    }
}
