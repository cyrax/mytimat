<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Language
 *
 * @property int $id
 * @property string $name
 * @property string $english_name
 * @property string|null $image
 * @property int $active
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Language whereActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Language whereEnglishName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Language whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Language whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Language whereName($value)
 * @mixin \Eloquent
 */
class Language extends Model
{
    public $timestamps = false;

    public $fillable = [
        'name',
        'english_name',
        'image',
    ];
}
