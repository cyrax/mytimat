<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Tempuser
 *
 * @property int $id
 * @property string $phone
 * @property string $password
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tempuser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tempuser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tempuser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tempuser wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Tempuser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Tempuser extends Model
{
    protected $fillable = [
        'phone',
        'password',
    ];
}
