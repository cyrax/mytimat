<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\Right
 *
 * @property int $id
 * @property int $company_id
 * @property string $url
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereUrl($value)
 * @mixin \Eloquent
 */
class Right extends Model
{
    protected $fillable = [
        'company_id',
        'url'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    /**
     * @param Request $request
     * @param integer $id
     */
    public static function createRight(Request $request, $id)
    {
        foreach ($request['rights_urls'] as $rights_url) {
            if (!preg_match('/\.(gif|jpe?g|png)$/i', $rights_url)) {
                continue;
            }
            Right::firstOrCreate(['url' => $rights_url, 'company_id' => $id]);
        }
    }
}
