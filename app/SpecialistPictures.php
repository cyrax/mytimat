<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\SpecialistPictures
 *
 * @property int $id
 * @property int $specialist_id
 * @property string $url
 * @property int $main
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Member $specialist
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpecialistPictures whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpecialistPictures whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpecialistPictures whereMain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpecialistPictures whereSpecialistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpecialistPictures whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\SpecialistPictures whereUrl($value)
 * @mixin \Eloquent
 */
class SpecialistPictures extends Model
{
    protected $fillable = [
        'company_id',
        'url',
        'main',
        'specialist_id'
    ];

    public function specialist()
    {
        return $this->belongsTo('App\Member');
    }

    public static function createFromRequest(Request $request, $id)
    {
        $avatar = 0;
        if ($request['pictures_urls']) {
            foreach ($request['pictures_urls'] as $pictures_url) {
                if (!preg_match('/\.(gif|jpe?g|png)$/i', $pictures_url)) {
                    continue;
                }
                self::updateOrCreate(['specialist_id' => $id, 'url' => $pictures_url], ['main' => $avatar == 0 ? 1 : 0]);
                $avatar++;
            }
        }
    }
}
