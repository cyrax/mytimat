<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'content_ru',
        'content_en',
        'content_it',
        'content_hr',
        'title_ru',
        'title_en',
        'title_it',
        'title_hr',
    ];
}
