<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 *
 * @package App
 * @property integer $service_id
 * @property integer $company_id
 * @property integer $specialist_id
 * @property integer $user_id
 * @property string $date
 * @property string $time
 * @property integer $group
 * @property string $status
 * @property integer $approved_id
 * @property \DateTime $created_at
 * @property \DateTime $updated_at
 * @property string $reject_status
 * @property string $reject_text
 * @property Company $company
 * @property Service $service
 * @property Member $specialist
 * @property int $id
 * @property-read \App\User $user
 * @property-read \App\User|null $user_approved
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereApprovedId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereRejectStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereRejectText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereSpecialistId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUserId($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    const STATUS_ACCEPTED = 'accepted';
    const STATUS_REJECTED = 'rejected';
    const STATUS_NEW = 'new';
    const STATUS_WAITING = 'waiting';
    const STATUS_NEED_ACCEPT_CHANGES = 'need_accept_changes';
    const STATUS_REACCEPT_CHANGES = 'reaccept_changes';

    protected $fillable = [
        'service_id',
        'company_id',
        'specialist_id',
        'user_id',
        'date',
        'time',
        'group',
        'created_at',
        'status'
    ];

    public static function rejectedStatuses()
    {
        return [
            'time' => 'Недоступно время',
            'date' => 'Недоступна дата',
            'specialist' => 'Недоступен специалист',
        ];
    }

    public function service()
    {
        return $this->belongsTo('App\Service', 'service_id');
    }

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function specialist()
    {
        return $this->belongsTo('App\Member', 'specialist_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function user_approved()
    {
        return $this->belongsTo('App\User', 'approved_id');
    }

    public static function getStatuses()
    {
        return [
            'new',
            'accepted',
            'rejected',
            'waiting',
            'need_accept_changes',
            'reaccept_changes'
        ];
    }

    public function getStatus()
    {
        return trans('app.'.$this->status);
    }
}
