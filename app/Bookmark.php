<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Bookmark
 *
 * @property int $id
 * @property int $user_id
 * @property int $company_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Company $company
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bookmark whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bookmark whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bookmark whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bookmark whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Bookmark whereUserId($value)
 * @mixin \Eloquent
 */
class Bookmark extends Model
{
    public $fillable = [
        'user_id',
        'company_id',
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
