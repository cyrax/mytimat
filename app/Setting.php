<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\Setting
 *
 * @property int $id
 * @property int $settable_id
 * @property string $settable_type
 * @property int $display_phone
 * @property int $display_email
 * @property int $display_social
 * @property int $is_active
 * @property int $chat_with_admin
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereChatWithAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereDisplayEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereDisplayPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereDisplaySocial($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereIsActive($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereSettableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereSettableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int $settingable_id
 * @property string $settingable_type
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $settingable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereSettingableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Setting whereSettingableType($value)
 */
class Setting extends Model
{
    protected $fillable = [
        'settable_id',
        'settable_type',
        'display_phone',
        'display_email',
        'display_social',
        'is_active',
        'chat_with_admin',
    ];
    public function settingable()
    {
        return $this->morphTo();
    }

    public static function saveUserSettings(Request $request)
    {
        $setting = Setting::firstOrNew(['settingable_id' => auth()->user()->id, 'settingable_type' => User::class]);
        $setting->display_phone = 0;
        $setting->display_social = 0;
        $setting->display_email = 0;

        if (isset($request['email']))
            $setting->display_email = 1;
        if (isset($request['phone']))
            $setting->display_phone = 1;
        if (isset($request['social']))
            $setting->display_social = 1;

        auth()->user()->setting()->save($setting);
    }

    public static function saveSpecialistSettings(Request $request, Member $member)
    {
        $setting = Setting::firstOrNew(['settingable_id' => $member->id, 'settingable_type' => Member::class]);
        $setting->display_phone = 0;
        $setting->display_social = 0;
        $setting->display_email = 0;

        if (isset($request['email']))
            $setting->display_email = 1;
        if (isset($request['phone']))
            $setting->display_phone = 1;
        if (isset($request['social']))
            $setting->display_social = 1;

        $member->setting()->save($setting);
    }

    /**
     * @param Request $request
     * @param Company $company
     */
    public static function saveCompanySettings(Request $request, Company $company)
    {
        $setting = self::firstOrNew(['settingable_id' => $company->id, 'settingable_type' => Company::class]);
        $setting->is_active = 0;
        $setting->chat_with_admin = 0;

        if (isset($request['is_active']))
            $setting->is_active = 1;
        if (isset($request['chat_with_admin']))
            $setting->chat_with_admin = 1;

        $company->setting()->save($setting);
    }
}
