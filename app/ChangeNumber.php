<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\ChangeNumber
 *
 * @property int $id
 * @property string $phone
 * @property int $user_id
 * @property string $code
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChangeNumber whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChangeNumber whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChangeNumber whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChangeNumber wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChangeNumber whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\ChangeNumber whereUserId($value)
 * @mixin \Eloquent
 */
class ChangeNumber extends Model
{
    protected $table = 'change_number';
    protected $fillable = [
        'phone',
        'user_id',
        'code',
    ];
}
