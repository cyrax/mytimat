<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

/**
 * App\Nonworkingday
 *
 * @property int $id
 * @property int $company_id
 * @property string $date
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Company $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nonworkingday whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nonworkingday whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nonworkingday whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nonworkingday whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Nonworkingday whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Nonworkingday extends Model
{
    protected $fillable = [
        'company_id',
        'date'
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    /**
     * @param Request $request
     * @param $id
     */
    public static function createNonWorkingDays(Request $request, $id)
    {
        if ($request['non_working_days']) {
            foreach ($request['non_working_days'] as $non_working_day) {
                Nonworkingday::updateOrCreate(['date' => $non_working_day, 'company_id' => $id]);
            }
        }
    }
}
