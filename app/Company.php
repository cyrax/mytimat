<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Nahid\Talk\Conversations\Conversation;
use Nicolaslopezj\Searchable\SearchableTrait;
use willvincent\Rateable\Rateable;

/**
 * Class Company
 *
 * @package App
 * @property integer $id
 * @property string $name
 * @property integer $user_id
 * @property string $address
 * @property string $description
 * @property string $features
 * @property string $phone
 * @property string $email
 * @property string $site
 * @property string $facebook
 * @property string $vk
 * @property string $twitter
 * @property string $whatsapp
 * @property Category $category
 * @property int $category_id
 * @property int $language_id
 * @property int $country_id
 * @property int $city_id
 * @property int $animals
 * @property int $departure
 * @property int $adult
 * @property string|null $telegram
 * @property int $lunch
 * @property int $monday
 * @property int $tuesday
 * @property int $wednesday
 * @property int $thursday
 * @property int $friday
 * @property int $saturday
 * @property int $sunday
 * @property string|null $monday_start
 * @property string|null $monday_end
 * @property string|null $tuesday_start
 * @property string|null $tuesday_end
 * @property string|null $wednesday_start
 * @property string|null $wednesday_end
 * @property string|null $thursday_start
 * @property string|null $thursday_end
 * @property string|null $friday_start
 * @property string|null $friday_end
 * @property string|null $saturday_start
 * @property string|null $saturday_end
 * @property string|null $sunday_start
 * @property string|null $sunday_end
 * @property string|null $lunch_start
 * @property string|null $lunch_end
 * @property int $self_decision
 * @property int $approved
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string|null $lang
 * @property-read \App\Avgcomprate $avgrate
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Bookmark[] $bookmarks
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Member[] $members
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Nonworkingday[] $nonworkingdays
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @property-read \App\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Picture[] $pictures
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Companyrate[] $rates
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Right[] $rights
 * @property-read \App\MemberRole $roles
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Service[] $services
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company search($search, $threshold = null, $entireText = false, $entireTextOnly = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company searchRestricted($search, $restriction, $threshold = null, $entireText = false, $entireTextOnly = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereAdult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereAnimals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereApproved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereDeparture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereFeatures($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereFriday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereFridayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereFridayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereLanguageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereLunch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereLunchEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereLunchStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereMonday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereMondayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereMondayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSaturday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSaturdayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSaturdayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSelfDecision($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSunday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSundayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereSundayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereThursday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereThursdayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereThursdayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTuesday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTuesdayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTuesdayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereVk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereWednesday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereWednesdayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereWednesdayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Company whereWhatsapp($value)
 * @mixin \Eloquent
 * @property-read mixed $average_rating
 * @property-read mixed $sum_rating
 * @property-read mixed $user_average_rating
 * @property-read mixed $user_sum_rating
 * @property-read \Illuminate\Database\Eloquent\Collection|\willvincent\Rateable\Rating[] $ratings
 * @property-read \App\Setting $setting
 */
class Company extends Model
{
    use SearchableTrait;
    use Rateable;

    protected $table = 'companies';

    protected $primaryKey = 'id';

    public function pictures()
    {
        return $this->hasMany('App\Picture');
    }

    public function nonworkingdays()
    {
        return $this->hasMany('App\Nonworkingday');
    }

    public function rights()
    {
        return $this->hasMany('App\Right');
    }

    public function services()
    {
        return $this->hasMany('App\Service');
    }

    public function rates()
    {
        return $this->hasMany('App\Companyrate');
    }

    public function owner()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function members()
    {
        return $this->hasMany('App\Member', 'company_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public function avgrate()
    {
        return $this->hasOne('App\Avgcomprate','company_id');
    }

    public function roles()
    {
        return $this->hasOne('App\MemberRole','company_id');
    }

    public function bookmarks()
    {
        return $this->hasMany('App\Bookmark');
    }

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function setting()
    {
        return $this->morphOne('App\Setting', 'settingable');
    }

    public static function statuses()
    {
        return [
            'Not approved',
            'Approved',
            'Declined'
        ];
    }

    public function getStatus()
    {
        return self::statuses()[$this->approved];
    }

    public function getMinStartTime()
    {
        return min(array_diff([
            $this->monday_start,
            $this->tuesday_start,
            $this->wednesday_start,
            $this->thursday_start,
            $this->friday_start,
            $this->saturday_start,
            $this->sunday_start
        ], [null]));
    }

    public function getMaxEndTime()
    {
        return max(array_diff([
            $this->monday_end,
            $this->tuesday_end,
            $this->wednesday_end,
            $this->thursday_end,
            $this->friday_end,
            $this->saturday_end,
            $this->sunday_end
        ], [null]));
    }




    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'services.name' => 10,
            'services.description' => 10,
            'companies.name' => 10,
            'companies.description' => 2,
            'companies.features' => 1,
            'companies.user_id' => 3,
        ],
        'joins' => [
            'services' => ['companies.id','services.company_id'],
        ],
    ];

    /**
     * @param Request $request
     * @return int
     */
    public static function updateOrSave(Request $request, $company = null)
    {
        if (is_null($company)) {
            $company = new Company;
            $company->user_id = Auth::id();
        }
        $company->lang = $request['lang'];
        $company->name = $request['name'];
        $company->address = $request['address'];
        $company->category_id = $request['category_id'];
        $company->language_id = 0;
        $company->description = $request['description'];
        $company->country_id = $request['country_id'];
        $company->city_id = $request['city_id'];
        $company->features = $request['features'];
        $animals = !empty($request['permissions']['animals']) ? $request['permissions']['animals'] : 0;
        $company->animals = $animals == 1 ? 1 : 0;
        $departure = !empty($request['permissions']['departure']) ? $request['permissions']['departure'] : 0;
        $company->departure = $departure == 1 ? 1 : 0;
        $adult = !empty($request['permissions']['adult']) ? $request['permissions']['adult'] : 0;
        $company->adult = $adult == 1 ? 1 : 0;
        $company->phone = $request['phone'];
        $company->site = $request['site'];
        $company->email = $request['email'];
        $company->vk = $request['vk'];
        $company->facebook = $request['facebook'];
        $company->twitter = $request['twitter'];
        $company->telegram = $request['telegram'];
        $company->whatsapp = $request['whatsapp'];
        $self = !empty($request['self-decision']) ? $request['self-decision'] : 0;
        $company->self_decision = $self == 1 ? 1 : 0;

        $company->lunch = 0; $company->monday = 0; $company->tuesday = 0; $company->wednesday = 0;
        $company->thursday = 0; $company->friday = 0; $company->saturday = 0; $company->sunday = 0;

        $monday = !empty($request['monday']) ? $request['monday'] : 0;
        if ($monday == 1){
            $company->monday = 1;
            $company->monday_start = $request['monday-start'];
            $company->monday_end = $request['monday-end'];
        }
        $tuesday = !empty($request['tuesday']) ? $request['tuesday'] : 0;
        if ($tuesday == 1){
            $company->tuesday = 1;
            $company->tuesday_start = $request['tuesday-start'];
            $company->tuesday_end = $request['tuesday-end'];
        }
        $wednesday = !empty($request['wednesday']) ? $request['wednesday'] : 0;
        if ($wednesday == 1){
            $company->wednesday = 1;
            $company->wednesday_start = $request['wednesday-start'];
            $company->wednesday_end = $request['wednesday-end'];
        }
        $thursday = !empty($request['thursday']) ? $request['thursday'] : 0;
        if ($thursday == 1){
            $company->thursday = 1;
            $company->thursday_start = $request['thursday-start'];
            $company->thursday_end = $request['thursday-end'];
        }
        $friday = !empty($request['friday']) ? $request['friday'] : 0;
        if ($friday == 1){
            $company->friday = 1;
            $company->friday_start = $request['friday-start'];
            $company->friday_end = $request['friday-end'];
        }
        $saturday = !empty($request['saturday']) ? $request['saturday'] : 0;
        if ($saturday == 1){
            $company->saturday = 1;
            $company->saturday_start = $request['saturday-start'];
            $company->saturday_end = $request['saturday-end'];
        }
        $sunday = !empty($request['sunday']) ? $request['sunday'] : 0;
        if ($sunday == 1){
            $company->sunday = 1;
            $company->sunday_start = $request['sunday-start'];
            $company->sunday_end = $request['sunday-end'];
        }
        $lunch = !empty($request['lunch']) ? $request['lunch'] : 0;
        if ($lunch != 1){
            $company->lunch = 1;
            $company->lunch_start = $request['lunch-start'];
            $company->lunch_end = $request['lunch-end'];
        }
        if (Auth::user()->administrator) {
            $company->approved = $request['approved'];
        }

        $company->save();

        return $company->id;
    }


    /**
     * @param integer $user
     * @param Company $company
     */
    public static function createConversationWithCompany($userId, $company)
    {
        $members = Member::where('company_id', $company->id)->get();
        foreach ($members as $member) {
            $conversation = Conversation::firstOrNew([
                'user_one' => min($userId, $member->user_id),
                'user_two' => max($userId, $member->user_id)
            ]);
            $conversation->status = 1;
            $conversation->save();
        }
    }

    /**
     * @param Request $request
     * @return Company|\Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Builder
     */
    public static function searchAll(Request $request)
    {
        $search_text = $request['search_text'];
        $price_from = $request['price_from'];
        $price_to = $request['price_to'];

        return static::select('companies.id','companies.name','companies.features','companies.address','companies.phone','companies.adult','companies.departure','companies.animals')
            ->distinct()
            ->search($search_text)
      			->join('services', 'services.company_id', '=', 'companies.id')
      			->when(!empty($price_from), function($query) use ($price_from) {
      				return $query->where('price', '>=', $price_from);
            })
      			->when(!empty($price_to), function($query) use ($price_to) {
      				return $query->where('price', '<=', $price_to);
            })
      			->when(!empty($request['city']), function($query) use ($request) {
      				return $query->where('city_id', $request['city']);
            })
      			->when(!empty($request['category_id']), function($query) use ($request) {
      				return $query->where('companies.category_id', $request['category_id']);
            })
      			->paginate(12);

    }
}
