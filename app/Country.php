<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Country
 * @property int $country_id
 * @property string|null $title_ru
 * @property string|null $title_ua
 * @property string|null $title_be
 * @property string|null $title_en
 * @property string|null $title_es
 * @property string|null $title_pt
 * @property string|null $title_de
 * @property string|null $title_fr
 * @property string|null $title_it
 * @property string|null $title_pl
 * @property string|null $title_ja
 * @property string|null $title_lt
 * @property string|null $title_lv
 * @property string|null $title_cz
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleBe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleCz($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleDe($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleEn($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleEs($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleFr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleIt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleJa($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleLt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleLv($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitlePl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitlePt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleUa($value)
 * @property string $title_hr
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Country whereTitleHr($value)
 */
class Country extends Model
{
    use \Waavi\Translation\Traits\Translatable;
    public $timestamps = false;

    protected $translatableAttributes = ['name'];
    protected $primaryKey = 'country_id';

    public $fillable = [
        'name',
        'name_translation',
    ];

    public function cities()
    {
        return $this->hasMany('App\City', 'country_id');
    }
}
