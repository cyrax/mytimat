<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * Class Service
 *
 * @package App
 * @property string $name
 * @property integer $company_id
 * @property integer $category_id
 * @property string $description
 * @property integer $price
 * @property string $currency
 * @property integer $days
 * @property int $id
 * @property string|null $supposed_category
 * @property int $hours
 * @property int $minutes
 * @property int $animals
 * @property int $departure
 * @property int $adult
 * @property int $interval
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Category|null $category
 * @property-read \App\Company $company
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereAdult($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereAnimals($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDays($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDeparture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereMinutes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereSupposedCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property \Carbon\Carbon|null $deleted_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\Service onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service search($search, $threshold = null, $entireText = false, $entireTextOnly = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service searchRestricted($search, $restriction, $threshold = null, $entireText = false, $entireTextOnly = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Service withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Service withoutTrashed()
 */
class Service extends Model
{
    use SearchableTrait;
    use SoftDeletes;

    protected $primaryKey = 'id';
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'company_id',
        'name',
        'supposed_category',
        'category_id',
        'description',
        'price',
        'currency',
        'days',
        'hours',
        'minutes',
        'animals',
        'departure',
        'adult',
    ];
    public static function getWeeks()
    {
        return [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday'
        ];
    }

    /**
     * @param string $week
     * @return bool
     *
     */
    public static function isWeekend($week)
    {
        return in_array($week, ['saturday', 'sunday']);
    }

    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'name' => 10,
            'description' => 9,
        ]
    ];

    public function company()
    {
        return $this->belongsTo('App\Company');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function orders()
    {
        return $this->hasMany('App\Order');
    }

    public static function searchAll(Request $request)
    {
        $services = static::select('name')
            ->search($request['search_text'])
            ->paginate(12);

        return $services;
    }

    /**
     * @param Request $request
     * @param integer $id
     */
    public static function createCompanyService(Request $request, $id)
    {
        if ($request['services']) {
            $ids = [];
            foreach ($request['services'] as $service) {
                if (empty($service['name'])) {
                    continue;
                }
                $data['company_id'] = $id;
                $data['name'] = $service['name'];
//                $data['supposed_category'] = $service['category'];
//                $data['category_id'] = $service['category'];
                $data['description'] = $service['description'];
                $data['price'] = $service['price'] ?? 0;
                $data['currency'] = 0;
                $animals = !empty($service['permissions']['animals']) ? $service['permissions']['animals'] : 0;
                $departure = !empty($service['permissions']['departure']) ? $service['permissions']['departure'] : 0;
                $adult = !empty($service['permissions']['adult']) ? $service['permissions']['adult'] : 0;
                $data['days'] = $service['duration_days'];
                $data['hours'] = $service['duration_hours'];
                $data['minutes'] = $service['duration_minutes'];
                $data['animals'] = $animals == 1 ? 1 : 0;
                $data['departure'] = $departure == 1 ? 1 : 0;
                $data['adult'] = $adult == 1 ? 1 : 0;
                $data['interval'] = $service['interval'];
                if (isset($service['id']) && !empty($service['id'])) {
                    Service::where('id', $service['id'])->update($data);
                    $ids[] = $service['id'];
                }
                else {
                    $ids[] = Service::create($data)->id;
                }
            }
            Service::whereNotIn('id', $ids)->where('company_id', $id)->delete();
        }
    }
}
