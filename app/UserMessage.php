<?php

namespace App;

use App\Mail\ServiceReserved;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Nicolaslopezj\Searchable\SearchableTrait;

/**
 * App\UserMessage
 *
 * @property int $id
 * @property string|null $subject
 * @property string|null $text
 * @property int $owner_id
 * @property int $companion_id
 * @property string $from
 * @property int $subject_id
 * @property string $type
 * @property string|null $read_at
 * @property \Carbon\Carbon|null $deleted_at
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\User $companion
 * @property-read \App\User $owner
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage onlyTrashed()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage search($search, $threshold = null, $entireText = false, $entireTextOnly = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage searchRestricted($search, $restriction, $threshold = null, $entireText = false, $entireTextOnly = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereCompanionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereFrom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereReadAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereSubject($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\UserMessage whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\UserMessage withoutTrashed()
 * @mixin \Eloquent
 */
class UserMessage extends Model
{
    use SearchableTrait; // Реализация Корзины TODO добавить автоудаление через 30 дней
    use SoftDeletes;

    protected $table = 'user_messages';

    protected $fillable = [
        'subject',
        'text',
        'from',
        'subject_id',
        'owner_id',
        'companion_id',
        'type',
    ];

    protected $dates = ['deleted_at'];

    public function owner()
    {
        return $this->belongsTo('App\User', 'owner_id');
    }

    public function companion()
    {
        return $this->belongsTo('App\User', 'companion_id');
    }

    /**
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments($type)
    {
        return $this->hasMany('App\Attachment', $type);
    }



    protected $searchable = [
        /**
         * Columns and their priority in search results.
         * Columns with higher values are more important.
         * Columns with equal values have equal importance.
         *
         * @var array
         */
        'columns' => [
            'messages.subject' => 10,
            'messages.text' => 1,
            'users.name' => 1,
            'users.second_name' => 1,
        ],
    ];

    public static function companyCreated()
    {
        $message = new UserMessage();
        $message->subject = trans('app.register_request_for_company');
        $message->text = 'Ваш запрос успешно подан';
        $message->from = 'owner';
        $message->owner_id = Auth::user()->id;
        $message->subject_id = 1;
        $message->companion_id = 1;
        $message->type = 'system';
        $message->save();
    }

    public static function companyStatusChanged($status, $user)
    {
        $message = new UserMessage();
        $message->subject = trans('app.company_status_changed');
        $message->text = trans('app.company_status_changed_for') . $status;
        $message->from = 'owner';
        $message->owner_id = $user;
        $message->subject_id = 1;
        $message->companion_id = 1;
        $message->type = 'system';
        $message->save();
    }

    public static function orderCreated($order, $company)
    {
        $specialist = Member::with('user')->find($order->specialist_id);

        if (isset($specialist)) {

            $message = new UserMessage();
            $message->subject = trans('app.you_have_new_order');
            $message->text = 'Вам создали новый заказ #' . $order->id . '. Компания ' . $company->name . '. Название услуги: ' . optional($order->service)->name . '. Дата ' . $order->date . ' ' . $order->time;
            $message->from = 'owner';
            $message->owner_id = $specialist->user_id;
            $message->subject_id = 1;
            $message->companion_id = 1;
            $message->type = 'system';
            $message->save();

            if (!empty($specialist->user->email))
                Mail::to($specialist->user->email)
                    ->send(new ServiceReserved($order));
        }

//        if (optional($specialist)->user_id != $company->user_id) {

            $message = new UserMessage();
            $message->subject = trans('app.you_successfully_created_order');
            $message->text = 'Вы успешно создали заказ. Ваш id заказа ' . $order->id . trans('app.specialist') . (isset($specialist) ? $specialist->getFullName() : trans('app.any'));
            $message->from = 'owner';
            $message->owner_id = $company->user_id;
            $message->subject_id = 1;
            $message->companion_id = 1;///
            $message->type = 'system';
            $message->save();

            if (!empty($company->email))
                Mail::to($company->email)
                    ->send(new ServiceReserved($order));

//        }
    }

    public static function needToAcceptChangesByCompany($companyName, $user_id, $reason = '...')
    {
        $userMessage = new UserMessage();
        $userMessage->subject = 'К сожалению, время или специалиста вашей заявки было изменено услугодателем.';
        $userMessage->text = 'Ваша заявка с указанными вами параметрами не может быть принята в обработку по следующим причинам: '.$reason.'
        "'.$companyName.'" предлагает вам следующие изменения к заявке чтобы принять его в работу.
        Просим вас подтвердить изменения в течении 2 часов, в течении которого заявка будет забронирована за вами, или предложите свои изменения,
        после этого времени предложенные изменения к заявке могут стать не актуальны.
        ';
        $userMessage->from = 'owner';
        $userMessage->owner_id = $user_id;
        $userMessage->subject_id = 1;
        $userMessage->companion_id = 1;
        $userMessage->type = 'system';
        $userMessage->save();
    }

    public static function needToAcceptChangesByUser($userName, $user_id, $reason = '...')
    {
        $userMessage = new UserMessage();
        $userMessage->subject = 'К сожалению, время или специалиста вашей заявки было изменено пользователем.';
        $userMessage->text = str_replace('{username}', $userName,
            'Ваша заявка с указанными вами параметрами не может быть принята в обработку по следующим причинам: '.$reason.'
        {username} предлагает вам следующие изменения к заявке чтобы принять его в работу.
        Просим вас подтвердить изменения в течении 2 часов, в течении которого заявка будет забронирована за вами, или предложите свои изменения,
        после этого времени предложенные изменения к заявке могут стать не актуальны.
        ');
        $userMessage->from = 'owner';
        $userMessage->owner_id = $user_id;
        $userMessage->subject_id = 1;
        $userMessage->companion_id = 1;
        $userMessage->type = 'system';
        $userMessage->save();
    }

    public static function orderAccepted($order)
    {
        $userMessage = new UserMessage();
        $userMessage->subject = 'Поздравляем. Ваша заявка подтверждена.';
        $userMessage->text = 'Поздравляем. Ваша заявка #'.$order->id.' одобрена. Компания: ' . $order->company->name . '. Время: ' . $order->date .' '. $order->time. '. Специалист: ' . optional($order->specialist)->name;
        $userMessage->from = 'owner';
        $userMessage->owner_id = $order->user_id;
        $userMessage->subject_id = 1;
        $userMessage->companion_id = 1;
        $userMessage->type = 'system';
        $userMessage->save();

        if (!empty(optional($order->user)->email))
            Mail::to(optional($order->user->email))
                ->send(new ServiceReserved($order));
    }

    public static function orderRejected(Order $order)
    {
        $userMessage = new UserMessage();
        $userMessage->subject = 'К сожалению, ваша заявка отклонена';
        $userMessage->text = str_replace('{orderId}', $order->id,
            'К сожалению ваша заявка {orderId} откланена по причине '. $order->reject_text.' статус: '. $order->reject_status);
        $userMessage->from = 'owner';
        $userMessage->owner_id = $order->user_id;
        $userMessage->subject_id = 1;
        $userMessage->companion_id = 1;
        $userMessage->type = 'system';
        $userMessage->save();


    }

    public static function orderRejectedByUser ($orderId, $userId)
    {
        $userMessage = new UserMessage();
        $userMessage->subject = 'Заявка отменена пользователем';
        $userMessage->text = str_replace('{orderId}', $orderId,
            'К сожалению заявка {orderId} откланена :(');
        $userMessage->from = 'owner';
        $userMessage->owner_id = $userId;
        $userMessage->subject_id = 1;
        $userMessage->companion_id = 1;
        $userMessage->type = 'system';
        $userMessage->save();
    }

    public static function orderAcceptedByUser ($orderId, $userId)
    {
        $userMessage = new UserMessage();
        $userMessage->subject = 'Заявка принята пользователем';
        $userMessage->text = str_replace('{orderId}', $orderId,
            'Заявка {orderId} принята пользователем :)');
        $userMessage->from = 'owner';
        $userMessage->owner_id = $userId;
        $userMessage->subject_id = 1;
        $userMessage->companion_id = 1;
        $userMessage->type = 'system';
        $userMessage->save();
    }
}
