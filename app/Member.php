<?php


namespace App;


use Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use willvincent\Rateable\Rateable;

/**
 * Class Member
 *
 * @package App
 * @property integer $user_id
 * @property integer $member_id
 * @property string $name
 * @property string $surname
 * @property string $group
 * @property int $id
 * @property string $last_name
 * @property int $accepts
 * @property int $country_id
 * @property int $city_id
 * @property string $vk
 * @property string $facebook
 * @property string $twitter
 * @property string $telegram
 * @property string $whatsapp
 * @property string $phone
 * @property string $site
 * @property string $email
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property int|null $role
 * @property-read \App\Company $member
 * @property-read \App\MemberRole|null $roles
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereAccepts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereUserId($value)
 * @mixin \Eloquent
 * @property int $company_id
 * @property-read \App\Company $company
 * @property-read mixed $average_rating
 * @property-read mixed $sum_rating
 * @property-read mixed $user_average_rating
 * @property-read mixed $user_sum_rating
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\SpecialistPictures[] $pictures
 * @property-read \Illuminate\Database\Eloquent\Collection|\willvincent\Rateable\Rating[] $ratings
 * @property-read \App\Schedule $schedule
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereCityId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereCountryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereFacebook($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereSite($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereTelegram($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereTwitter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereVk($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Member whereWhatsapp($value)
 * @property-read \App\MemberInfo $info
 * @property-read \App\Setting $setting
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\MemberGroup[] $groups
 * @property-read \App\City|null $city
 * @property-read \App\Country|null $country
 */
class Member extends Model
{
    public $table = 'members';
    use Rateable;

    public $fillable = [
        'user_id',
        'company_id',
        'name',
        'surname',
        'group',
        'accepts',
        'country_id',
        'city_id',
        'vk',
        'facebook',
        'twitter',
        'telegram',
        'whatsapp',
        'phone',
        'site',
        'email'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function orders()
    {
        return $this->hasMany('App\Order', 'specialist_id');
    }

    public function schedule()
    {
        return $this->morphOne('App\Schedule', 'scheduled');
    }

    public function setting()
    {
        return $this->morphOne('App\Setting', 'settingable');
    }

    public function company()
    {
        return $this->belongsTo('App\Company', 'company_id');
    }

    public function roles()
    {
        return $this->belongsTo('App\MemberRole', 'role');
    }

    public function pictures()
    {
        return $this->hasMany('App\SpecialistPictures', 'specialist_id');
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'country_id');
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'city_id');
    }

    public function info()
    {
        return $this->hasOne('App\MemberInfo');
    }

    public function groups()
    {
        return $this->hasMany('App\MemberGroup');
    }

    public static function getMemberGroups($memberId)
    {
        $result = [];
        $memberGroups = MemberGroup::where('member_id', $memberId)->get();
        foreach ($memberGroups as $memberGroup) {
            $result[] = $memberGroup->group;
        }
        return $result;
    }

    public static function getMemberGroup($group)
    {
        return self::memberGroups()[$group];
    }

    public static function memberGroups()
    {
        return [
            'company_admin' => 'Администратор',
            'company_manager' => 'Менеджер',
            'company_specialist' => 'Специалист',
        ];
    }

    public static function getUserGroups()
    {
        return [
            'user',
            'administrator',
            'manager',
            'translator',
            'company_admin',
            'company_manager',
            'company_specialist'
        ];
    }

    public static function isAccepts($type)
    {
        return static::acceptTypes()[$type];
    }

    public static function acceptTypes()
    {
        return [
            'Не принимает',
            'Принимает'
        ];
    }

    public function getFullName()
    {
        return $this->name. ' ' .$this->last_name;
    }

    /**
     * @param Request $request
     * @param Member $member
     *
     * @return integer
     */
    public static function updateOrSave(Request $request, $member = null)
    {
        if (is_null($member)) {
            $member = new self;
            $schedule = new Schedule();
        }
        else {
            $schedule = $member->schedule;
        }

        $member->user_id = Auth::id();
        $member->name = $request['name'];
        $member->phone = $request['phone'];
        $member->site = $request['site'];
        $member->email = $request['email'];
        $member->vk = $request['vk'];
        $member->facebook = $request['facebook'];
        $member->twitter = $request['twitter'];
        $member->telegram = $request['telegram'];
        $member->whatsapp = $request['whatsapp'];
        $member->country_id = $request['country_id'];
        $member->city_id = $request['city_id'];

        $schedule->lunch = 0;
        $schedule->monday = 0;
        $schedule->tuesday = 0;
        $schedule->wednesday = 0;
        $schedule->thursday = 0;
        $schedule->friday = 0;
        $schedule->saturday = 0;
        $schedule->sunday = 0;
        $schedule->monday_start = null;
        $schedule->tuesday_start = null;
        $schedule->wednesday_start = null;
        $schedule->thursday_start = null;
        $schedule->friday_start = null;
        $schedule->saturday_start = null;
        $schedule->sunday_start = null;
        $schedule->monday_end = null;
        $schedule->tuesday_end = null;
        $schedule->wednesday_end = null;
        $schedule->thursday_end = null;
        $schedule->friday_end = null;
        $schedule->saturday_end = null;
        $schedule->sunday_end = null;

        $monday = !empty($request['monday']) ? $request['monday'] : 0;
        if ($monday == 1){
            $schedule->monday = 1;
            $schedule->monday_start = $request['monday-start'];
            $schedule->monday_end = $request['monday-end'];
        }
        $tuesday = !empty($request['tuesday']) ? $request['tuesday'] : 0;
        if ($tuesday == 1){
            $schedule->tuesday = 1;
            $schedule->tuesday_start = $request['tuesday-start'];
            $schedule->tuesday_end = $request['tuesday-end'];
        }
        $wednesday = !empty($request['wednesday']) ? $request['wednesday'] : 0;
        if ($wednesday == 1){
            $schedule->wednesday = 1;
            $schedule->wednesday_start = $request['wednesday-start'];
            $schedule->wednesday_end = $request['wednesday-end'];
        }
        $thursday = !empty($request['thursday']) ? $request['thursday'] : 0;
        if ($thursday == 1){
            $schedule->thursday = 1;
            $schedule->thursday_start = $request['thursday-start'];
            $schedule->thursday_end = $request['thursday-end'];
        }
        $friday = !empty($request['friday']) ? $request['friday'] : 0;
        if ($friday == 1){
            $schedule->friday = 1;
            $schedule->friday_start = $request['friday-start'];
            $schedule->friday_end = $request['friday-end'];
        }
        $saturday = !empty($request['saturday']) ? $request['saturday'] : 0;
        if ($saturday == 1){
            $schedule->saturday = 1;
            $schedule->saturday_start = $request['saturday-start'];
            $schedule->saturday_end = $request['saturday-end'];
        }
        $sunday = !empty($request['sunday']) ? $request['sunday'] : 0;
        if ($sunday == 1){
            $schedule->sunday = 1;
            $schedule->sunday_start = $request['sunday-start'];
            $schedule->sunday_end = $request['sunday-end'];
        }
        $lunch = !empty($request['lunch']) ? $request['lunch'] : 0;
        if ($lunch != 1){
            $schedule->lunch = 1;
            $schedule->lunch_start = $request['lunch-start'];
            $schedule->lunch_end = $request['lunch-end'];
        }

        $member->schedule()->save($schedule);

        $member->save();

        MemberInfo::updateOrCreate(['member_id' => $member->id], ['about' => $request['description']]);

        return $member->id;
    }

}