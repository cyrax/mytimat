<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Schedule
 *
 * @property int $id
 * @property int $scheduled_id
 * @property string $scheduled_type
 * @property int $lunch
 * @property int $monday
 * @property int $tuesday
 * @property int $wednesday
 * @property int $thursday
 * @property int $friday
 * @property int $saturday
 * @property int $sunday
 * @property string|null $monday_start
 * @property string|null $monday_end
 * @property string|null $tuesday_start
 * @property string|null $tuesday_end
 * @property string|null $wednesday_start
 * @property string|null $wednesday_end
 * @property string|null $thursday_start
 * @property string|null $thursday_end
 * @property string|null $friday_start
 * @property string|null $friday_end
 * @property string|null $saturday_start
 * @property string|null $saturday_end
 * @property string|null $sunday_start
 * @property string|null $sunday_end
 * @property string|null $lunch_start
 * @property string|null $lunch_end
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $schedulable
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereFriday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereFridayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereFridayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereLunch($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereLunchEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereLunchStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereMonday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereMondayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereMondayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereSaturday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereSaturdayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereSaturdayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereScheduledId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereScheduledType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereSunday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereSundayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereSundayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereThursday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereThursdayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereThursdayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereTuesday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereTuesdayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereTuesdayStart($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereWednesday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereWednesdayEnd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Schedule whereWednesdayStart($value)
 * @mixin \Eloquent
 */
class Schedule extends Model
{
    protected $fillable = [
        'scheduled_id',
        'scheduled_type',
        'lunch',
        'monday',
        'tuesday',
        'wednesday',
        'thursday',
        'friday',
        'saturday',
        'sunday',
        'monday_start',
        'monday_end',
        'tuesday_start',
        'tuesday_end',
        'wednesday_start',
        'wednesday_end',
        'thursday_start',
        'thursday_end',
        'friday_start',
        'friday_end',
        'saturday_start',
        'saturday_end',
        'sunday_start',
        'sunday_end',
        'lunch_start',
        'lunch_end',
    ];

    public function schedulable()
    {
        return $this->morphTo();
    }
}
