<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Gift
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $picture
 * @property int $price
 * @property int $quantity
 * @property int $category_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\GiftCategory $category
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift wherePicture($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Gift whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Gift extends Model
{
    protected $fillable = [
        'name',
        'text',
        'picture',
        'price',
        'quantity',
        'category_id',
    ];

    public function category() {
        return $this->belongsTo('App\GiftCategory', 'category_id');
    }
}
