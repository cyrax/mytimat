<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Attachment
 *
 * @property int $id
 * @property string $url
 * @property string $subject_id
 * @property string $owner
 * @property string $companion
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\UserMessage $companions
 * @property-read \App\UserMessage $owners
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereCompanion($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereOwner($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereSubjectId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Attachment whereUrl($value)
 * @mixin \Eloquent
 */
class Attachment extends Model
{
    protected $fillable = [
        'url',
        'subject_id',
        'owner',
        'companion',
    ];

    public function owners()
    {
        return $this->belongsTo('App\UserMessage', 'owner');
    }

    public function companions()
    {
        return $this->belongsTo('App\UserMessage', 'companion');
    }

}
