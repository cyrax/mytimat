<?php


namespace App;


use Illuminate\Database\Eloquent\Model;

/**
 * Class MemberGroup
 *
 * @package App
 * @property integer $id
 * @property integer $member_id
 * @property string $group
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberGroup whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberGroup whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberGroup whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberGroup whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberGroup whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MemberGroup extends Model
{
    public $table = 'member_groups';

    protected $fillable = [
        'member_id',
        'group'
    ];

    public static function deleteOld($memberId)
    {
        static::where('member_id', $memberId)->delete();
    }
}