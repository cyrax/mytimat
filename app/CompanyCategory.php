<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\CompanyCategory
 *
 * @property int $id
 * @property string $name
 * @property string $url
 * @property int $parent_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\CompanyCategory[] $categories
 * @property-read \App\CompanyCategory $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Company[] $company
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyCategory whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyCategory whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyCategory whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyCategory whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyCategory whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\CompanyCategory whereUrl($value)
 * @mixin \Eloquent
 */
class CompanyCategory extends Model
{
    protected $table = 'company_categories';

    protected $fillable = [
        'name',
        'url',
        'parent_id',
    ];

    public function company() {
        return $this->hasMany('App\Company', 'category_id');
    }

    public function category()
    {
        return $this->belongsTo('App\CompanyCategory', 'parent_id');
    }

    public function categories()
    {
        return $this->hasMany('App\CompanyCategory', 'parent_id');
    }
}
