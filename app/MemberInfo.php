<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class MemberInfo
 *
 * @property int $id
 * @property string $lang
 * @property string $name
 * @property string $last_name
 * @property string $father_name
 * @property string $about
 * @property integer $member_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @package App
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereFatherName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $patronymic
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo whereMemberId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberInfo wherePatronymic($value)
 */
class MemberInfo extends Eloquent
{
	protected $table = 'member_info';

	protected $fillable = [
		'lang',
		'name',
		'last_name',
		'father_name',
		'about',
        'member_id'
	];
}
