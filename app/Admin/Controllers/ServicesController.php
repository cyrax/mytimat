<?php


namespace App\Admin\Controllers;


use App\Category;
use App\Service;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class ServicesController
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Компании');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Service::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->column('company_id');
            $grid->column('name');
            $grid->column('category_id');

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Изменить сервис');
            $content->description('');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Service::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->display('name');

            $form->select('category_id')->options(function () {
                $categories = Category::all();
                $result = [];
                foreach ($categories as $category) {
                    $result += [$category->id => $category->name];
                }
                return $result;
            });
        });
    }
}