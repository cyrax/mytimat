<?php


namespace App\Admin\Controllers;


use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use App\Language;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use PHPUnit\Framework\Constraint\Count;

class CountryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Страны');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Изменить страну');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить страну');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Country::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('title_ru');
            $grid->column('title_en');
            $grid->column('title_hr');
            $grid->column('title_it');

//            $grid->category()->display(function ($parentId) {
//                return isset($parentId) ? $parentId['name'] : '';
//            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Country::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('title_ru');
            $form->text('title_en');
            $form->text('title_hr');
            $form->text('title_it');
        });
    }

}