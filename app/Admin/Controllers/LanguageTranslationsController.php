<?php


namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Waavi\Translation\Models\Language;
use Waavi\Translation\Models\Translation;

class LanguageTranslationsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Языки');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Изменить язык');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить язык');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Translation::class, function (Grid $grid) {

            $grid->column('locale')->sortable();
            $grid->column('group')->sortable();
            $grid->column('item')->sortable();
            $grid->column('text');

            $grid->filter(function ($filter) {
                $filter->like('locale');
                $filter->like('group');
                $filter->like('item');
                $filter->like('text');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Translation::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->select('locale')->options(function() {
                $languages = Language::all();
                $result = [];
                foreach ($languages as $language) {
                    $result = array_merge($result, [$language->locale => $language->name]);
                }
                return $result;
            })->default('ru');
            $form->display('group', 'Group')->value('app');
            $form->display('item', 'Item');
            $form->text('text', 'Value');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }
}