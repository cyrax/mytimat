<?php


namespace App\Admin\Controllers;


use App\City;
use App\Country;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class CityController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Города');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Изменить город');
            $content->description('');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить город');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(City::class, function (Grid $grid) {

            $grid->id('city_id')->sortable();

            $grid->column('title_ru', 'Название на русском');
            $grid->column('title_en', 'Название на английском');
            $grid->column('title_hr', 'Название на хорватском');
            $grid->column('title_it', 'Название на итальянском');
            $grid->column('country_id')->display(function ($countryId) {
                return optional(Country::find($countryId))->title_ru;
            });

//            $grid->category()->display(function ($parentId) {
//                return isset($parentId) ? $parentId['name'] : '';
//            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(City::class, function (Form $form) {
            $form->display('city_id', 'ID');
            $form->hidden('important', 1)->value(1);
            $form->hidden('timezone', 'Timezone');
            $form->text('title_ru', 'Название на русском');
            $form->text('title_en', 'Название на английском');
            $form->text('title_hr', 'Название на хорватском');
            $form->text('title_it', 'Название на итальянском');
            $form->select('country_id')->options(function () {
                $countries = Country::all();
                $result = [0 => 'Choose country'];
                foreach ($countries as $country) {
                    $result = $result + [$country->country_id => $country->title_ru];
                }

                return $result;
            });
            $form->saving(function (Form $form) {
                $request = request();
//                dump($form);
                $country = Country::find($request['country_id']);
                $address = $country->title_en . '+' . $request['title_en'];
                $address = json_decode(file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$address.'&key=AIzaSyDV61DqeVe7pKi5H5cQNmRZbHvX-DPQ59I'));
                $location = $address->results[0]->geometry->location;
                $address = json_decode(file_get_contents('http://api.geonames.org/timezoneJSON?lat='.$location->lat.'&lng='.$location->lng.'&username=bukutime'));
                $form->timezone = $address->timezoneId;
            });
        });
    }

}
