<?php

namespace App\Admin\Controllers;

use App\Company;
use App\Http\Controllers\Controller;
use App\User;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class UsersController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Компании');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Изменить кмпанию');
            $content->description('');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить компанию');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(User::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('name');
            $grid->column('second_name');
            $grid->column('email');
            $grid->column('phone');
            $grid->column('company_id');

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(User::class, function (Form $form) {
            $form->display('id', 'ID');
            $form->text('name');
            $form->text('second_name');
            $form->text('phone');
            $form->select('group')->options(function ($group) {
                $result = User::groups();
                // $result['val'] = $group;
                return $result;
            });
            $form->select('company_id')->options(function($companyId) {
                $result = [0 => 'Выберите компанию'];
                foreach (Company::all() as $company) {
                    $result += [$company->id => $company->name];
                }
                $result['val'] = $companyId;
                return $result;
            });
        });
    }
}