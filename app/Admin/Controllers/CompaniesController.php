<?php


namespace App\Admin\Controllers;

use App\Company;
use App\Http\Controllers\Controller;
use App\Mail\CompanyStatusChanged;
use App\User;
use App\UserMessage;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CompaniesController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Компании');
            $content->description('');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Изменить кмпанию');
            $content->description('');
            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header('Добавить компанию');
            $content->description('');

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Company::class, function (Grid $grid) {

            $grid->id('ID')->sortable();

            $grid->column('name');
            $grid->column('address');
            $grid->column('phone');
            $grid->column('email');
            $grid->column('approved');

            $grid->owner()->display(function ($owner) {
                return isset($owner) ? $owner['name'] : '';
            });

            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Company::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->display('name');
            $form->radio('approved')
                    ->options(Company::statuses());
            $form->submitted(function (Form $form) {
                $request = request();
                $company = Company::find($form->model()->id);
                $company->approved = $request['approved'];
                if (isset($company->email))
//                    Mail::to($company->email)
//                        ->send(new CompanyStatusChanged($company));
                UserMessage::companyStatusChanged(trans('app.'.$company->getStatus() ), $company->user_id);
            });
            $form->select('user_id')->options(function($ownerId) {
                    $result = [];
                    foreach (User::all() as $user) {
                        $result += [$user->id => $user->name];
                    }
                    $result['val'] = $ownerId;
                    return $result;
            });
        });
    }

}
