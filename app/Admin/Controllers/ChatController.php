<?php


namespace App\Admin\Controllers;


use App\Http\Controllers\Controller;
use App\User;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;

class ChatController extends Controller
{
    public function index()
    {
        return Admin::content(function (Content $content) {
            $content->header('Help chat');
            $users = '';
            foreach (User::all() as $user) {
                $image = isset($user->avatar) ? "<img src='/images/user/$user->avatar.jpg' width='100px'>" : '';
                $users .= "<table class='table'>
                        <tr>
                            <td>$image
                                $user->name
                            </td>
                            <td>
                                <a href='#' class='btn btn-success pull-right'>Send Message</a>
                            </td>
                        </tr>
                    </table>";
                // ". route('message.read', ['id'=>$user->id]) . "
            }

            $content->body("<div class='container'>
                <div class='row'>
                    <div class='col-md-8 col-md-offset-2'>
                        <div class='panel panel-default'>
                            <div class='panel-heading'>All Users</div>
                            <div class='panel-body'>
                            $users
                            </div>
                        </div>
                    </div>
                </div>
            </div>");
        });
    }
}