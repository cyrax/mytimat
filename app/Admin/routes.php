<?php

use Illuminate\Routing\Router;


// Route::group(['prefix' => UriLocalizer::localeFromRequest(), 'middleware' => 'localize'], function() {
    Admin::registerAuthRoutes();
    Route::group([
        'prefix' => config('admin.route.prefix'),
        'namespace' => config('admin.route.namespace'),
        'middleware' => config('admin.route.middleware'),
    ], function (Router $router) {
        $router->get('/', 'HomeController@index');
        $router->resource('categories', CategoryController::class);
        $router->resource('languages', LanguageController::class);
        $router->resource('language-translations', LanguageTranslationsController::class);
        $router->resource('cities', CityController::class);
        $router->resource('countries', CountryController::class);
        $router->resource('companies', CompaniesController::class);
        $router->resource('users', UsersController::class);
        $router->resource('order', OrderController::class);
        $router->resource('services', ServicesController::class);
        $router->resource('pages', PagesController::class);

    });

// });

