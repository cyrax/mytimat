<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MemberRole
 *
 * @package App
 * @property string $name
 * @property integer $company_id
 * @property int $id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberRole whereCompanyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberRole whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberRole whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberRole whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\MemberRole whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class MemberRole extends Model
{
    public $table = 'roles';

    protected $fillable = [
        'name',
        'company_id'
    ];


}